# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Android\sdk\tools\proguard\proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

 -ignorewarnings

-keep class toml.movitwallet.models.** { *; }
-keep class toml.movitwallet.jsonmodels.** { *; }


-keepattributes Signature
-keepattributes InnerClasses
-dontnote
-dontwarn okio.**
-dontwarn retrofit2.Platform$Java8

#-dontwarn org.w3c.dom.bootstrap.DOMImplementationRegistry

-libraryjars <java.home>/lib/rt.jar(java/**,javax/**)

-keep class retrofit2.** { *; }
-keep public class org.simpleframework.**{ *; }
-keep class org.simpleframework.xml.**{ *; }
-keep class org.simpleframework.xml.core.**{ *; }
-keep class org.simpleframework.xml.util.**{ *; }






#-libraryjars libs/jackson-annotations-2.0.0.jar-libraryjars libs/jackson-core-2.0.0.jar

-keepattributes *Annotation*,EnclosingMethod
-keepnames class org.codehaus.jackson.** { *; }
-dontwarn javax.xml.**
-dontwarn javax.xml.stream.events.**
-dontwarn com.fasterxml.jackson.databind.**