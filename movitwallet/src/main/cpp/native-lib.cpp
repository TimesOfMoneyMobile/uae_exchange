//
// Created by ketanb on 6/21/2017.
//
#include <jni.h>


extern "C" {
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callStaging(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("https://movit.timesofmoney.in/mps/");
}
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callQA(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("https://qamovit.timesofmoney.in/mps/");
    //return env->NewStringUTF("http://10.158.202.61:8080/mps/");
}
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callProduction(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("https://wayveonline.com/mps/");
}
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callPrepod(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("https://dw.uaeexchange.com/mps/");
}
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callDemo(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("http://demo.timesofmoney.com/mps/");
}
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callLocal(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("http://10.158.208.130:8080/mps/");
}


JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callStagingBiller(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("https://movit.timesofmoney.in/mps/billers/rest/");
}
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callQABiller(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("https://qamovit.timesofmoney.in/billers/rest/");
    //return env->NewStringUTF("http://10.158.202.61:8080/mps/");
}
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callProductionBiller(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("https://wayveonline.com/billers/rest/");

}
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callPrepodBiller(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("https://dw.uaeexchange.com/billers/rest/");
}
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callDemoBiller(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("http://demo.timesofmoney.com/billers/rest/");
}
JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_callLocalBiller(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("http://10.158.208.58:8080/billers/rest/");

}

JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_getDefaultKeyCPP(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("c292a6e6c19b7403cd87949d0ad45021");

}

JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_getDefaultIVCPP(JNIEnv *env, jobject instance) {
    return env->NewStringUTF("288dca8258b1dd7c");

}

JNIEXPORT jstring JNICALL
Java_toml_movitwallet_utils_MovitConsumerApp_getDefaultMobileNumberCPP(JNIEnv *env,
                                                                       jobject instance) {
    return env->NewStringUTF("01234");

}

}
