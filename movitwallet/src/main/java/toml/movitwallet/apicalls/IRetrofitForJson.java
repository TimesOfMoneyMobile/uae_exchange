package toml.movitwallet.apicalls;


import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by mayurn on 9/22/2017.
 */

public interface IRetrofitForJson {


    @POST("merchantservice/getdetails/")
    Call<ResponseBody> getProviders(@Body RequestBody requestParam);

}
