package toml.movitwallet.apicalls;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import toml.movitwallet.BuildConfig;
import toml.movitwallet.R;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;

/**
 * Created by mayurn on 9/23/2017.
 */

public class RetrofitTaskForJSON {


    private static Retrofit retroFitClient = null;
    private static RetrofitTaskForJSON retrofitTaskForJSON = null;
    private IRetrofitTask iRetrofitTask;


    public static RetrofitTaskForJSON getInstance() {
        if (retrofitTaskForJSON == null) {
            retrofitTaskForJSON = new RetrofitTaskForJSON();
        }

        return retrofitTaskForJSON;
    }


    public static Retrofit getClientForJSON() {


        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        httpClient.connectTimeout(30, TimeUnit.SECONDS);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Authorization", Constants.BASIC_AUTH_VALUE)
                        .header("Accept", "application/json")
                        .header("Content-Type", "application/json")
                        .build();

                return chain.proceed(request);
            }
        });

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(interceptor);
        }

        OkHttpClient client = httpClient.build();

        if (retroFitClient == null) {
            retroFitClient = new Retrofit.Builder()
                    .baseUrl(MovitConsumerApp.getInstance().getBillersBaseURL())
                    .client(client)
                    .build();

        }
        return retroFitClient;
    }

    public void executeTaskForJSON(RequestBody jsonRequestBody, final IRetrofitTask callback) {
        IRetrofit iRetrofitForJson = RetrofitTaskForJSON.getClientForJSON().create(IRetrofit.class);
        Call<ResponseBody> getProviderResponseCall = iRetrofitForJson.getProviders(jsonRequestBody);
        getProviderResponseCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                if (response.code() == 200) {
                    try {

                        String apiResponse = response.body().string();
                        callback.onSuccess(apiResponse);
                    } catch (Exception e) {
                        LogUtils.Exception(e);
                        callback.onFailed(Constants.TIME_OUT_CODE, "" + e.getMessage());
                    }
                } else {
                    //  callback.onFailed(Constants.TIME_OUT_CODE, "" + response.code());
                    callback.onFailed("061", MovitConsumerApp.getInstance().getContext().getString(R.string.oops_something_went_wrong));
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onFailed("061", MovitConsumerApp.getInstance().getContext().getString(R.string.oops_something_went_wrong));
            }
        });
    }


    public void executeBookRequest(RequestBody jsonRequestBody, final IRetrofitTask callback) {
        IRetrofit iRetrofitForJson = RetrofitTaskForJSON.getClientForJSON().create(IRetrofit.class);
        Call<ResponseBody> getProviderResponseCall = iRetrofitForJson.processRequest(jsonRequestBody);
        getProviderResponseCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                try {
                    if (response.code() == 200) {
                        String apiResponse = response.body().string();
                        callback.onSuccess(apiResponse);
                    }
                    else
                    {
                        callback.onFailed("061", MovitConsumerApp.getInstance().getContext().getString(R.string.oops_something_went_wrong));
                    }

                } catch (Exception e) {
                    LogUtils.Exception(e);
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
                callback.onFailed("061", MovitConsumerApp.getInstance().getContext().getString(R.string.oops_something_went_wrong));
            }
        });
    }

    public interface IRetrofitTask {
        void onSuccess(String response);

        void onFailed(String errorCode, String errorMessage);
    }
}
