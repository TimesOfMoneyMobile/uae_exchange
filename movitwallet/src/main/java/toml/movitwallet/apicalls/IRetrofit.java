package toml.movitwallet.apicalls;


import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

/**
 * Created by kunalk on 1/20/2016.
 */
public interface IRetrofit {
    @FormUrlEncoded
    @POST("mobileRequestHandler")
    Call<ResponseBody> executeAPI(@Field("mobileRequestXML") String mobileRequestXML);


    @Multipart
    @POST("CustomerRegistration")
    Call<ResponseBody> registerCustomer(@PartMap Map<String, RequestBody> map);


    //For Biller getDetails About Provider and product
    @POST("merchantservice/getdetails/")
    Call<ResponseBody> getProviders(@Body RequestBody requestParam);

    //For Biller getDetails About Processing Request
    @POST("merchantservice/processrequest/")
    Call<ResponseBody> processRequest(@Body RequestBody requestParam);


   /* @GET("mobileRequestHandler")
    Call<ResponseBody> getSecurityKey(@Query("mobileRequestXML") String mobileRequestXML);*/


    @FormUrlEncoded
    @POST("mobileReqHandler")
    Call<ResponseBody> getSecurityKey(@Field("mobileRequestXML") String mobileRequestXML, @Field("SaltRequired") String saltrequired);
}
