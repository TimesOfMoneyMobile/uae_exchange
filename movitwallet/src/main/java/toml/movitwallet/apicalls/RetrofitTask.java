package toml.movitwallet.apicalls;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import toml.movitwallet.BuildConfig;
import toml.movitwallet.R;

import toml.movitwallet.utils.AESecurity;
import toml.movitwallet.utils.AppLogger;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;


/**
 * Created by kunalk on 1/29/2016.
 */

public class RetrofitTask {

    private String currentRequestType;


    private static volatile RetrofitTask retrofitTask;
    private IRetrofit iRetrofit;

    public static RetrofitTask getInstance() {
        if (retrofitTask == null) {
            retrofitTask = new RetrofitTask();
        }

        return retrofitTask;
    }


    public static void makeNull() {
        retrofitTask = null;
    }

    public RetrofitTask() {


        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(30, TimeUnit.SECONDS);
        builder.connectTimeout(30, TimeUnit.SECONDS);


       /* OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        httpClient.connectTimeout(30, TimeUnit.SECONDS);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Content-Type", "multipart/form-data")
                        .build();

                return chain.proceed(request);
            }
        });
*/

        LogUtils.Verbose("Server URL _ Retro Fit", MovitConsumerApp.getInstance().getBaseURL());

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }


        OkHttpClient client = builder.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MovitConsumerApp.getInstance().getBaseURL())
                .client(client)
                .build();

        iRetrofit = retrofit.create(IRetrofit.class);

    }


//    public void executeGetKey(String param, final IRetrofitTask callback) {
//
//
//        this.callback=callback;
//        if (!Constants.isNetworkAvailable(MovitConsumerApp.getInstance().getContext())) {
//            callback.onFailed("9999", null);
//            return;
//        }
//
//        param = Constants.SERVICE_PROVIDER + Constants.PIPE_SEPARATOR + Constants.USER_TYPE + Constants.PIPE_SEPARATOR + param;
//        String saltrequired = "Y";
//
//        Call<ResponseBody> responseBodyCall = iRetrofit.getSecurityKey(param, saltrequired);
//
//        enqueuCall(param, responseBodyCall, false);
//
//
//    }


    public void executeTask(String XMLParam, String reqType, final IRetrofitTask callback) {

        //  this.callback = callback;
        this.currentRequestType = reqType;

        LogUtils.Verbose("TAG", " In Retrofit task " + callback.getClass().getCanonicalName());

        if (!Constants.isNetworkAvailable(MovitConsumerApp.getInstance().getContext())) {
            callback.onFailed("9999", Constants.NO_NETWORK);
            return;
        }


        XMLParam = Constants.getReqXML(reqType, XMLParam);


        String encryptXML = null;

        // Print request in log file

        // AppLogger.appendLog(MovitConsumerApp.getInstance().getContext(), AppLogger.REQUEST + reqType, XMLParam);


        String selectedLanguage = AppSettings.getData(MovitConsumerApp.getInstance().getContext(), AppSettings.APPLANGUAGE).equals("ar") ? Constants.AR_LANGUAGE_CODE : Constants.EN_LANGUAGE_CODE;

        StringBuilder postParam = new StringBuilder();


       /* postParam.append(Constants.SERVICE_PROVIDER + Constants.PIPE_SEPARATOR + Constants.USER_TYPE + Constants.PIPE_SEPARATOR + MovitConsumerApp.getInstance().getMobileNumber() + Constants.PIPE_SEPARATOR + selectedLanguage
                + Constants.PIPE_SEPARATOR).append(encryptXML);*/



        if (!reqType.equals("GetKey")) {

            try {
                encryptXML = AESecurity.getInstance().encryptString(XMLParam);
                postParam.append(Constants.SERVICE_PROVIDER + Constants.PIPE_SEPARATOR + Constants.USER_TYPE + Constants.PIPE_SEPARATOR + MovitConsumerApp.getInstance().getMobileNumber() + Constants.PIPE_SEPARATOR + selectedLanguage
                        + Constants.PIPE_SEPARATOR).append(encryptXML);
            } catch (Exception e) {
                callback.onFailed("9998", e.getMessage());
                return;
            }

        } else {

            try {
                encryptXML = AESecurity.getInstance().encryptStringWithDefaultKeyIV(XMLParam);
                postParam.append(Constants.SERVICE_PROVIDER + Constants.PIPE_SEPARATOR + Constants.USER_TYPE + Constants.PIPE_SEPARATOR + MovitConsumerApp.getInstance().getDefaultMobileNumber() + Constants.PIPE_SEPARATOR + selectedLanguage
                        + Constants.PIPE_SEPARATOR).append(encryptXML);
            } catch (Exception e) {
                callback.onFailed("9998", e.getMessage());
                return;
            }


        }


        Call<ResponseBody> responseBodyCall = iRetrofit.executeAPI(postParam.toString());

        // Previous Code
        if (reqType.equals("GetKey"))
            enqueuCall(XMLParam, responseBodyCall, true, callback);
        else
            enqueuCall(XMLParam, responseBodyCall, false, callback);

        //enqueuCall(XMLParam, responseBodyCall, true, callback);

    }


    public void executeRegisterCustomer(Map<String, RequestBody> param, final IRetrofitTask callback) {
        Call<ResponseBody> responseBodyCall = iRetrofit.registerCustomer(param);

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    String apiResponse = response.body().string();


                    apiResponse = AESecurity.getInstance().decryptString(apiResponse);

                    LogUtils.Verbose("API Response ", apiResponse);


                    callback.onSuccess(apiResponse);

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed("9998", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("TAG", " Fail " + t.toString());
                callback.onFailed("061", MovitConsumerApp.getInstance().getContext().getString(R.string.oops_something_went_wrong));
            }
        });

    }


    private void enqueuCall(String xmlParam, Call<ResponseBody> responseBodyCall, final boolean isGetKeyCall, final IRetrofitTask callback) {

        LogUtils.Verbose("TAG", "XML Request " + xmlParam);

        responseBodyCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String apiResponse = response.body().string();

                        if (isGetKeyCall) {
                            apiResponse = AESecurity.getInstance().decryptStringWithDefaultKeyIV(apiResponse);
                        } else {
                            apiResponse = AESecurity.getInstance().decryptString(apiResponse);
                        }


                        LogUtils.Verbose("API Response ", apiResponse);
                        if (apiResponse.contains("Invalid Session")) {

                            Toast.makeText(MovitConsumerApp.getInstance().getContext(), "Your session has expired. Please log-in again.", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent("message");
                            intent.putExtra("type", 1);
                            MovitConsumerApp.getInstance().getContext().sendBroadcast(intent);

//                            AlertDialog.Builder builder = new AlertDialog.Builder(MovitConsumerApp.getInstance().getContext());
//                            builder.setMessage("Session Expired")
//
//                                    .setCancelable(false)
//                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            //do things
//
//
//                                        }
//                                    });
//                            AlertDialog alert = builder.create();
//                            alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//                            alert.show();


                        }

                        callback.onSuccess(apiResponse);

                        // Print response in log file
                        //  AppLogger.appendLog(MovitConsumerApp.getInstance().getContext(), AppLogger.RESPONSE + currentRequestType, apiResponse);


                    } catch (Exception e) {
                        LogUtils.Exception(e);
                        callback.onFailed("9998", e.getMessage());
                        makeNull();
                    }
                } else {

                    callback.onFailed("9997",  MovitConsumerApp.getInstance().getContext().getString(R.string.oops_something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onFailed("061", MovitConsumerApp.getInstance().getContext().getString(R.string.oops_something_went_wrong));
            }


        });

    }

    public interface IRetrofitTask {
        void onSuccess(String response);

        void onFailed(String errorCode, String errorMessage);
    }

}
