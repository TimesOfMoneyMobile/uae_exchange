package toml.movitwallet.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by parinati on 22-08-2015.
 */
public class Security {



    private static final byte[] SALT = { (byte) 0xC7, (byte) 0xB5, (byte) 0x99,
            (byte) 0xF4, (byte) 0x4B, (byte) 0x7C, (byte) 0x81, (byte) 0x77 };
    private static final int ITERATION_COUNT = 65536;
    private static final int KEY_LENGTH = 256;
    private static final int IV_LENGTH = 16;
    private Cipher eCipher;
    private Cipher dCipher;
    private byte[] encrypt;
    private byte[] iv;

    public Security(String passPhrase) {
        try {
            SecretKeyFactory secretKeyFactory = SecretKeyFactory
                    .getInstance("PBKDF2WithHmacSHA1");
            KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), SALT,
                    ITERATION_COUNT, KEY_LENGTH);
            SecretKey secretKeyTemp = secretKeyFactory.generateSecret(keySpec);
            SecretKey secretKey = new SecretKeySpec(secretKeyTemp.getEncoded(),
                    "AES");

            eCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            eCipher.init(Cipher.ENCRYPT_MODE, secretKey);

            dCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            iv = eCipher.getParameters()
                    .getParameterSpec(IvParameterSpec.class).getIV();
            dCipher.init(Cipher.DECRYPT_MODE, secretKey,
                    new IvParameterSpec(iv));
        } catch (Exception ex) {
            ex.printStackTrace();
        }




    }



    public byte[] encrypt(byte[] plain) throws Exception {
        return eCipher.doFinal(plain);
    }

    private byte[] extractIV() {
        byte[] iv = new byte[IV_LENGTH];
        System.arraycopy(encrypt, 0, iv, 0, iv.length);
        return iv;
    }
     public String decrypt() {
        String decStr = null;

        try {
            byte[] bytes = extractCipherText();

            byte[] decrypted = decrypt(bytes);
            decStr = new String(decrypted, "UTF8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return decStr;
    }

    private byte[] extractCipherText() {
        byte[] ciphertext = new byte[encrypt.length - IV_LENGTH];
        System.arraycopy(encrypt, 16, ciphertext, 0, ciphertext.length);
        return ciphertext;
    }

    public byte[] decrypt(byte[] encrypt) throws Exception {
        return dCipher.doFinal(encrypt);
    }

    public static String generateNewKey() {
        String newKey = null;

        try {
            // Get the KeyGenerator
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(256); // 128, 192 and 256 bits available

            // Generate the secret key specs.
            SecretKey skey = kgen.generateKey();
            byte[] raw = skey.getEncoded();

            newKey = new String(Base64.encodeBase64(raw));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return newKey;
    }

    public static String generateNewSalt() {
        char[] newSalt = null;

        try {
            byte[] salt = new byte[8];

            // Generate random salt
            SecureRandom rnd = new SecureRandom();
            rnd.nextBytes(salt);

            newSalt = Hex.encodeHex(salt);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return newSalt.toString();
    }
    }


