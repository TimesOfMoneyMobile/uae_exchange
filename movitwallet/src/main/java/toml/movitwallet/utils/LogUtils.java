package toml.movitwallet.utils;

import android.util.Log;

import toml.movitwallet.BuildConfig;


/**
 * Created by kunalk on 10/27/2015.
 */
public class LogUtils {


    public static void Verbose(String tag, String message) {

        if (BuildConfig.DEBUG) {
            Log.v(tag, message);

        }

    }




    public static void Exception(Exception e) {

        if (BuildConfig.DEBUG)
            e.printStackTrace();

       // Crashlytics.logException(e);

    }

    public static void Exception(Exception e, String request, String response) {

        if (BuildConfig.DEBUG)
            e.printStackTrace();

        if (response == null)
            response = "";

    }

    public static void Exception(Exception e, String request) {

        if (BuildConfig.DEBUG)
            e.printStackTrace();



    }


}
