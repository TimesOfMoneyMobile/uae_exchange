package toml.movitwallet.utils;

/**
 * Created by kunalk on 2/1/2016.
 */

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class AESecurity {


    private static volatile AESecurity aeSecurity;


    public static AESecurity getInstance() {
        if (aeSecurity == null)
            aeSecurity = new AESecurity();

        return aeSecurity;
    }

    /**
     * Encryption mode enumeration
     */
    private enum EncryptMode {
        ENCRYPT, DECRYPT;
    }

    // cipher to be used for encryption and decryption
    Cipher _cx;

    // encryption key and initialization vector
    byte[] _key, _iv;

    public AESecurity() {
        // initialize the cipher with transformation AES/CBC/PKCS5Padding
        try {
            _cx = Cipher.getInstance(Constants.CIPHER_TRANSFORMATION);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();


        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        _key = new byte[32]; //256 bit key space
        _iv = new byte[16]; //128 bit IV
    }

    /**
     * @param _inputText     Text to be encrypted or decrypted
     * @param _encryptionKey Encryption key to used for encryption / decryption
     * @param _mode          specify the mode encryption / decryption
     * @param _initVector    Initialization vector
     * @return encrypted or decrypted string based on the mode
     * @throws UnsupportedEncodingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private String encryptDecrypt(String _inputText, String _encryptionKey,
                                  EncryptMode _mode, String _initVector) throws UnsupportedEncodingException,
            InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        String _out = "";// output string
        //_encryptionKey = md5(_encryptionKey);
        //System.out.println("key="+_encryptionKey);

        // _initVector = "288dca8258b1dd7c";
        LogUtils.Verbose("SecurityIV_AES", _initVector);
        int len = _encryptionKey.getBytes("UTF-8").length; // length of the key	provided

        if (_encryptionKey.getBytes("UTF-8").length > _key.length)
            len = _key.length;

        int ivlen = _initVector.getBytes("UTF-8").length;

        if (_initVector.getBytes("UTF-8").length > _iv.length)
            ivlen = _iv.length;

        System.arraycopy(_encryptionKey.getBytes("UTF-8"), 0, _key, 0, len);
        System.arraycopy(_initVector.getBytes("UTF-8"), 0, _iv, 0, ivlen);
        //KeyGenerator _keyGen = KeyGenerator.getInstance("AES");
        //_keyGen.init(128);

        SecretKeySpec keySpec = new SecretKeySpec(_key, Constants.ENCRYPTIONALGO); // Create a new SecretKeySpec
        // for the
        // specified key
        // data and
        // algorithm
        // name.

        IvParameterSpec ivSpec = new IvParameterSpec(_iv); // Create a new
        // IvParameterSpec
        // instance with the
        // bytes from the
        // specified buffer
        // iv used as
        // initialization
        // vector.

        // encryption
        if (_mode.equals(EncryptMode.ENCRYPT)) {
            // Potentially insecure random numbers on Android 4.3 and older.
            // Read
            // https://android-developers.blogspot.com/2013/08/some-securerandom-thoughts.html
            // for more info.
            _cx.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);// Initialize this cipher instance
            byte[] results = _cx.doFinal(_inputText.getBytes("UTF-8")); // Finish
            // multi-part
            // transformation
            // (encryption)
            _out = Base64.encodeToString(results, Base64.DEFAULT); // ciphertext
            // output
        }

        // decryption
        if (_mode.equals(EncryptMode.DECRYPT)) {
            _cx.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);// Initialize this ipher instance

            LogUtils.Verbose("SecurityIV_AES_DECRYPT_Start", _initVector);
            byte[] decodedValue = Base64.decode(_inputText.getBytes(),
                    Base64.DEFAULT);
            byte[] decryptedVal = _cx.doFinal(decodedValue); // Finish
            // multi-part
            // transformation
            // (decryption)
            _out = new String(decryptedVal);
            LogUtils.Verbose("SecurityIV_AES_DECRYPT_END", _initVector);
        }
        //  System.out.println(_out);
        return _out; // return encrypted/decrypted string
    }


    public String encryptString(String _plainText) throws Exception {

        return encryptDecrypt(_plainText, MovitConsumerApp.getInstance().getKey(), EncryptMode.ENCRYPT, MovitConsumerApp.getInstance().getSecurityIv());
    }


    public String encryptStringWithDefaultKeyIV(String _plainText) throws Exception {

        return encryptDecrypt(_plainText, MovitConsumerApp.getInstance().getDefaultKey(), EncryptMode.ENCRYPT, MovitConsumerApp.getInstance().getDefaultSecurityIV());
    }

    /***
     * This funtion decrypts the encrypted text to plain text using the key
     * provided. You'll have to use the same key which you used during
     * encryprtion
     *
     * @param _encryptedText Encrypted/Cipher text to be decrypted

     * @return encrypted value
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */


    public String decryptString(String _encryptedText) throws Exception {


        return encryptDecrypt(_encryptedText, MovitConsumerApp.getInstance().getKey(), EncryptMode.DECRYPT, MovitConsumerApp.getInstance().getSecurityIv());
    }


    public String decryptStringWithDefaultKeyIV(String _encryptedText) throws Exception {


        return encryptDecrypt(_encryptedText, MovitConsumerApp.getInstance().getDefaultKey(), EncryptMode.DECRYPT, MovitConsumerApp.getInstance().getDefaultSecurityIV());
    }

}
