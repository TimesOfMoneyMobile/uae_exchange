package toml.movitwallet.utils;


import android.app.Application;
import android.content.Context;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.List;

import toml.movitwallet.BuildConfig;
import toml.movitwallet.R;
import toml.movitwallet.models.ContactsModel;
import toml.movitwallet.models.Wallet.WalletId;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by kunalk on 4/12/2016.
 */
public class MovitConsumerApp extends Application {

    private static MovitConsumerApp movitConsumerApp;
    private List<WalletId> walletIds;

    List<ContactsModel> listSelectedContacts = new ArrayList<>();
    List<ContactsModel> listRecentContacts = new ArrayList<>();

    public List<ContactsModel> getListSelectedContacts() {
        return listSelectedContacts;
    }

    public void setListSelectedContacts(List<ContactsModel> listSelectedContacts) {
        this.listSelectedContacts = listSelectedContacts;
    }

    public List<ContactsModel> getListRecentContacts() {
        return listRecentContacts;
    }

    public void setListRecentContacts(List<ContactsModel> listRecentContacts) {
        this.listRecentContacts = listRecentContacts;
    }


    //public static String BASE_URL = "qamovit.timesofmoney.in";
    public static String BASE_URL = "http://10.158.202.61:8080";

    private String key;

    public String getDefaultMobileNumber() {
        return strDefaultMobileNumber;
    }

    public void setDefaultMobileNumber(String strDefaultMobileNumber) {
        this.strDefaultMobileNumber = strDefaultMobileNumber;
    }

    public String getDefaultKey() {
        return strDefaultKey;
    }

    public void setDefaultKey(String strDefaultKey) {
        this.strDefaultKey = strDefaultKey;
    }

    public String getDefaultSecurityIV() {
        return strDefaultSecurityIV;
    }

    public void setDefaultSecurityIV(String strDefaultSecurityIV) {
        this.strDefaultSecurityIV = strDefaultSecurityIV;
    }

    private String securityIv;
    private String strSalt;
    private String kycStatus;
    private String changeMPIN;
    private String strMobileNumber;
    private String strDefaultMobileNumber;
    private String strDefaultKey;
    private String strDefaultSecurityIV;
    private String otpAmount;
    private String walletBalance;
    private String minAmount;
    private String maxAmount;
    private String strSessionID = "";


    public static MovitConsumerApp getInstance() {
        return movitConsumerApp;
    }


    public Context getContext() {
        return this;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        movitConsumerApp = this;

        setDefaultKey(getDefaultKeyCPP());
        setDefaultSecurityIV(getDefaultIVCPP());
        setDefaultMobileNumber(getDefaultMobileNumberCPP());


      /*  setKey(getDefaultKeyCPP());
        setSecurityIv(getDefaultIVCPP());
        setMobileNumber(getDefaultMobileNumberCPP());*/

        AppLogger.deleteLog(getInstance().getContext());

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Quicksand-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

/*    Load url for Native c++ library*/

    static {
        System.loadLibrary("native-lib");
    }


    private native String callStaging();

    private native String callQA();

    private native String callProduction();

    private native String callPrepod();

    private native String callDemo();

    private native String callLocal();

    private native String callStagingBiller();

    private native String callQABiller();

    private native String callProductionBiller();

    private native String callPrepodBiller();

    private native String callDemoBiller();

    private native String callLocalBiller();

    private native String getDefaultKeyCPP();

    private native String getDefaultIVCPP();

    private native String getDefaultMobileNumberCPP();


    public String getSecurityIv() {
        return securityIv;
    }

    public void setSecurityIv(String securityIv) {
        this.securityIv = securityIv;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getChangeMPIN() {
        return changeMPIN;
    }

    public void setChangeMPIN(String changeMPIN) {
        this.changeMPIN = changeMPIN;
    }


    public String getMobileNumber() {
        return strMobileNumber;
    }


    public void setMobileNumber(String strMobileNumber) {
        this.strMobileNumber = strMobileNumber;
    }

    public String getOtpAmount() {
        return otpAmount;
    }

    public void setOtpAmount(String otpAmount) {
        this.otpAmount = otpAmount;
    }

    public String getSessionID() {
        return strSessionID;
    }

    public void setSessionID(String strSessionID) {
        if (strSessionID == null)
            strSessionID = "";
        this.strSessionID = strSessionID;
    }

    public String getSalt() {
        return strSalt;
    }

    public void setSalt(String strSalt) {
        this.strSalt = strSalt;
    }


    public String getKycStatus() {
        return kycStatus;
    }

    public void setKycStatus(String kycStatus) {
        this.kycStatus = kycStatus;
    }


    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }

    public String getWalletBalance() {
        return walletBalance;
    }

    public String getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(String minAmount) {
        this.minAmount = minAmount;
    }

    public String getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(String maxAmount) {
        this.maxAmount = maxAmount;
    }


    public WalletId getPrimaryWallet() {

        if (walletIds != null && walletIds.size() > 0) {

            for (WalletId walletId : walletIds) {
                if (walletId.isPrimary.equalsIgnoreCase("Y"))
                    return walletId;
            }
        }

        return null;
    }

    public List<WalletId> getWalletIds() {
        return walletIds;
    }

    public void setWalletIds(List<WalletId> walletIds) {
        this.walletIds = walletIds;

    }

    public void setWalletBalanceByID(String id, String balance) {
        for (WalletId walletId : walletIds) {

            if (walletId.getWalletId().equals(id)) {
                walletId.setBalance(balance);
                setWalletBalance(balance);
                break;
            }
        }
    }


    public String getIMEINo() {
        if (Constants.DEVICE_ID) {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            return telephonyManager.getDeviceId();
            //return "1234";
        } else {
            return "1234";
        }
    }


    public String getBaseURL() {
        String url = "";

        if (!BuildConfig.DEBUG) {
            url = callQA();
        } else {
            url = callQA();
        }
        return url;
    }


    public String getBillersBaseURL() {
        String url = "";

        if (!BuildConfig.DEBUG) {
            url = callQABiller();
        } else {
            url = callQABiller();
        }

        return url;

    }


}
