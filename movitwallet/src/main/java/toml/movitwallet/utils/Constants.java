package toml.movitwallet.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import toml.movitwallet.jsonmodels.GeneralJSONRequest;
import toml.movitwallet.jsonmodels.JsonRequest;
import toml.movitwallet.jsonmodels.RequestDetail;
import toml.movitwallet.models.Header;
import toml.movitwallet.models.Request;


/**
 * Created by kunalk on 4/7/2016.
 */
public class Constants {

    public static final int SUCCESS_P2P = 0;
    public static final int SUCCESS_P2M = 1;
    public static final int SUCCESS_OTP = 2;

    public static final String DISPLAY_TYPE_TXN_STATEMENT = "TXN_STATEMENT";
    public static final String DISPLAY_TYPE_P2P_RECENT = "P2P_RECENT";


    public static final String SERVICE_PROVIDER = "uaexchange";
    public static final String SERVICE_PROVIDER_imbank = "mercury";
    //   public static final String SERVICE_PROVIDER = "mercury";
    public static final String CHANNEL_ID = "APP";


    public static final String CIPHER_TRANSFORMATION = "AES/CBC/PKCS5Padding";
    public static final String HASH_ALGORITHM1 = "SHA-256";
    public static final String HASH_ALGORITHM2 = "HmacSHA256";
    public static final String ENCRYPTIONALGO = "AES";
    public static final String MANGE_PROFILE_REQUESTTYPE = "ManagedProfile";
    public static final String BILLER = "BILLER";

    //Biller API Constants JSON Keys Constants
    public static String BILLER_MERCHANT = "";
    public static final String GET_PROVIDERS = "getProviders";
    public static final String MOBILE_NO_KEY = "mobileNo";
    public static final String USER_TYPE_KEY = "userType";
    public static final String SP_ID_KEY = "spId";
    public static final String JSONREQUEST_KEY = "jsonRequest";
    public static final String JSONRESPONSE_KEY = "jsonResponse";
    public static final String JSONRESPONSE_STATUS = "status";
    public static final String BOOK_REQUEST_TYPE = "bookRequest";
    public static final String COUNTRY_CODE = "971";
    public static final String JSON_MESSAGE_RESPONSE = "message";
    public static final String JSON_RESPONSE_CODE = "responseCode";
    public static final String ETISALAT_SKU_CODE = "ETAEAE57434";
    public static final String DU_SKU_CODE = "ETAEAE57434";
    public static final String TIME_OUT_CODE = "504";
    public static final String BASIC_AUTH_VALUE = "Basic dXNlcjp1c2VyMTIz";
    public static String PAYMODE_TYPE = "W2W";
    public static String GET_PRODUCT_LIST = "getProductList";
    public static String COUNTRY_ISO_CODE = "AE";

    public static boolean DEVICE_ID = false;

    public static final String CLASSIC = "CLASSIC";
    public static final String GOLD = "GOLD";
    public static final String PLATINUM = "PLATINUM";
    public static final String INFINITE = "INFINITE";
    public static final String ELECTRON = "ELECTRON";
    public static final String DEFAULT = "QNB ALAHLI Visa Debit Card";
    public static final String NOTIFICATION_TITLE = "QNB ALAHLI mVISA";
    public static final String USER_TYPE = "CU";
    public static final String PIPE_SEPARATOR = "|";
    public static final String EN_LANGUAGE_CODE = "en";
    public static final String AR_LANGUAGE_CODE = "ar";
    public static final String NO_NETWORK = "Please check your internet connection.";


    public static final String ERROCODE_INVALID_MERCHANT_ID = "096";

    // NEO Constants
    public static final String NEOMerchantId = "201708011000001";
    public static final String NEOCollaboratorId = "NI";
    public static final String NEOEncDecKey = "GwRoHWfXjTO + vKZHcbBc7u0OT8rkT2OYLS9Y5Qx9Sqs=";


    //private RequestDetail requestDetail;


    public static boolean isNetworkAvailable(Context context) {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }


// Ref : http://stackoverflow.com/questions/18714616/convert-hex-string-to-byte

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }


        return data;
    }

    final protected static char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static String byteArrayToHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;

        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        LogUtils.Verbose("Byte To String", new String(hexChars));

        return new String(hexChars);
    }

    //Returning the hashMpin with check of salt non salt
    public static String getEncryptedMpin(String mpin) {
        if (TextUtils.isEmpty(MovitConsumerApp.getInstance().getSalt())) {
            return getHash(mpin.getBytes());
        } else {
            return getHash(mpin, MovitConsumerApp.getInstance().getSalt());
        }
    }


    //This Hash Method if salt is not coming
    public static String getHash(byte[] bytes) {
        try {
            MessageDigest digest = MessageDigest.getInstance(HASH_ALGORITHM1);
            digest.update(bytes);
            if (MovitConsumerApp.getInstance().getSalt() != null && !TextUtils.isEmpty(MovitConsumerApp.getInstance().getSalt()))
                digest.update(hexStringToByteArray(MovitConsumerApp.getInstance().getSalt()));

            return byteArrayToHexString(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    //This Hash Method if salt is coming


    public static String getHash(String data, String secret) {
        try {
            SecretKeySpec signingKey = null;
            try {
                signingKey = new SecretKeySpec(secret.getBytes("UTF-8"), HASH_ALGORITHM2);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Mac mac = Mac.getInstance(HASH_ALGORITHM2);

            try {
                mac.init(signingKey);
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }
            byte[] rawHmac = new byte[0];
            try {
                rawHmac = mac.doFinal(data.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // Byte [] to String conversion

            return byteArrayToHexString(rawHmac);
            //  LogUtils.Verbose("Byte To String", bytesToHex(rawHmac));


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isValidEmail(String emailStr) {
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static boolean isValidPAN(String inputString) {

        String[] validCards = {"473864", "434175", "473865", "473866", "484890"};

        for (int i = 0; i < validCards.length; i++) {
            if (inputString.contains(validCards[i])) {
                return true;
            }
        }
        return false;
    }


    public static void showToast(Context context, String msg) {
        if (context != null)
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static String getReqXML(String reqType, String reqDetails) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>\n" +
                "<MpsXml>\n" +
                "\t<Header>\n" +
                "\t\t<ChannelId>APP</ChannelId>\n" +
                "\t\t<Timestamp>" + sdf.format(calendar.getTime()) + "</Timestamp>\n" +
                "\t\t<SessionId>" + MovitConsumerApp.getInstance().getSessionID() + "</SessionId>\n" +
                "\t\t<ServiceProvider>" + SERVICE_PROVIDER + "</ServiceProvider>\n" +
                "\t</Header>\n" +
                "\t<Request>\n" +
                "\t\t<RequestType>" + reqType + "</RequestType>\n" +
                "\t\t<UserType>CU</UserType>\n" +
                "\t\t</Request>\n" +
                "\t<RequestDetails>\n" +
                reqDetails +
                "\n\t\t<ResponseURL>{ResponseURL}</ResponseURL>\n" +
                "\t\t<ResponseVar>{ResponseVar}</ResponseVar>\n" +
                "\t</RequestDetails>\n" +
                "</MpsXml>\n";

        // LogUtils.Verbose("XML Req ", xml);
        return xml;
    }

    public static String getReqXMLC2C(String reqType, String reqDetails) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>\n" +
                "<MpsXml>\n" +
                "\t<Header>\n" +
                "\t\t<ChannelId>APP</ChannelId>\n" +
                "\t\t<Timestamp>" + sdf.format(calendar.getTime()) + "</Timestamp>\n" +
                "\t\t<SessionId>" + MovitConsumerApp.getInstance().getSessionID() + "</SessionId>\n" +
                "\t\t<ServiceProvider>" + SERVICE_PROVIDER_imbank + "</ServiceProvider>\n" +
                "\t</Header>\n" +
                "\t<Request>\n" +
                "\t\t<RequestType>" + reqType + "</RequestType>\n" +
                "\t\t<UserType>CU</UserType>\n" +
//                "\t\t<Paymode>C2C</Paymode>" +
                "\t</Request>\n" +
                "\t<RequestDetails>\n" +
                reqDetails +
                "\n\t\t<ResponseURL>{ResponseURL}</ResponseURL>\n" +
                "\t\t<ResponseVar>{ResponseVar}</ResponseVar>\n" +
                "\t</RequestDetails>\n" +
                "</MpsXml>\n";

        LogUtils.Verbose("XML Req ", xml);
        return xml;
    }

    public static void setCardType(Context context, TextView txtQnbCard, TextView txtQnbCard1, String cardno) {

        cardno = cardno.replaceFirst(" ", "");

        String type = Constants.getCardType(cardno);

//
//        if (type.equals(Constants.CLASSIC)) {
//            txtQnbCard.setText(context.getString(R.string.qnbclassic));
//            if (txtQnbCard1 != null)
//                txtQnbCard1.setText(context.getString(R.string.qnbclassic1));
//
//        } else if (type.equals(Constants.GOLD)) {
//            txtQnbCard.setText(context.getString(R.string.qnbgold));
//            if (txtQnbCard1 != null && AppSettings.getData(context, AppSettings.APPLANGUAGE).equals("ar"))
//                txtQnbCard1.setText(" " + context.getString(R.string.qnbgold1));
//
//        } else if (type.equals(Constants.PLATINUM)) {
//            txtQnbCard.setText(context.getString(R.string.qnbplatinum));
//            if (txtQnbCard1 != null && AppSettings.getData(context, AppSettings.APPLANGUAGE).equals("ar"))
//                txtQnbCard1.setText(" " + context.getString(R.string.qnbplatinum1));
//        } else if (type.equals(Constants.ELECTRON)) {
//            txtQnbCard.setText(context.getString(R.string.qnbelectron));
//            if (txtQnbCard1 != null && AppSettings.getData(context, AppSettings.APPLANGUAGE).equals("ar"))
//                txtQnbCard1.setText(" " + context.getString(R.string.qnbelectron1));
//        } else if (type.equals(Constants.INFINITE)) {
//            txtQnbCard.setText(context.getString(R.string.qnbinfinite));
//            if (txtQnbCard1 != null && AppSettings.getData(context, AppSettings.APPLANGUAGE).equals("ar"))
//                txtQnbCard1.setText(" " + context.getString(R.string.qnbinfinite1));
//        } else {
//            txtQnbCard.setText(context.getString(R.string.qnbclassic));
//            if (txtQnbCard1 != null && AppSettings.getData(context, AppSettings.APPLANGUAGE).equals("ar"))
//                txtQnbCard1.setText(" " + context.getString(R.string.qnbclassic1));
//        }


    }

    public static String addSpaceToString(String input, int code) {
        String output = input;

        switch (code) {
            case 1:
                output = String.format("%1$" + (-25) + "s", output);
                break;
            case 2:
                output = String.format("%1$" + (-13) + "s", output);
                break;

            default:
                break;
        }


        return output;
    }

    public static String getCardType(String cardno) {

        cardno = cardno.substring(0, 6);

        if (cardno.equals("473864"))
            return CLASSIC;
        else if (cardno.equals("434175"))
            return ELECTRON;
        else if (cardno.equals("473865"))
            return GOLD;
        else if (cardno.equals("484890"))
            return INFINITE;
        else if (cardno.equals("473866"))
            return PLATINUM;
        else
            return DEFAULT;

    }

    public static void changeLanguage(String language, Context context) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }

    public static String getOnlyDigits(String s) {
        s.replaceAll(" ", "");
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        String number = matcher.replaceAll("");
        return number;
    }

    public static String getOnlyStrings(String s) {
        Pattern pattern = Pattern.compile("[^a-z A-Z]");
        Matcher matcher = pattern.matcher(s);
        String number = matcher.replaceAll("");
        return number;
    }

    public static String formatAmount(String amount) {
        try {
            NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(2);

            Double amt = Double.parseDouble(amount);
            amount = nf.format(amt);
        } catch (Exception e) {
            LogUtils.Exception(e);
        }
        return amount;
    }

    public static void hideKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = ((Activity) context).getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    public static String decodeString(String input) {

        if (input == null || input.equalsIgnoreCase("null"))
            input = "N.A.";

        if (input.length() < 4) {
            return input;
        }

        try {


            byte[] data = Base64.decode(input, Base64.DEFAULT);
            String temp = new String(data);
            LogUtils.Verbose("TAG", "Decode " + temp);
            char[] ch = temp.toCharArray();
            for (Character c : ch) {
                if (String.valueOf(c).matches("[a-zA-Z0-9,.\\-\\s]+") || isProbablyArabic(String.valueOf(c)))
                    continue;
                else
                    return input;
            }
            return temp;

        } catch (Exception e) {
            return input;
        }
    }

    public static String decryptBase64EncodedText(String value) {
        if (value != null) {
            String pattern = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(value);
            if (m.find()) {
                String decodedString = new String(org.apache.commons.codec.binary.Base64.decodeBase64(value
                        .getBytes()), Charset.forName("UTF-8"));
                if (isProbablyArabic(decodedString)) {
                    return decodedString;
                } else {
                    if (decodedString.matches("[a-zA-Z0-9\\s.,-]+")) {
                        return decodedString;
                    }
                }

            }
        }
        return value;
    }

    public static boolean isProbablyArabic(String s) {
        for (int i = 0; i < s.length(); ) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }


    /*public static boolean checkForEncode(String string) {
        String pattern =
                "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(string);
        boolean isBase64 = false;

        if (m.find()) {
            byte[] decoded = Base64.decodeBase64(string);
            String temp = new String(decoded);
            if (temp.matches("[a-zA-Z0-9\\s]+")) {
                isBase64 = true;
            } else {
                isBase64 = false;
            }
        } else {
            isBase64 = false;
        }

        return isBase64;
    }*/

    public static boolean isValidMPIN(String str) {


        ArrayList<String> SameStr = new ArrayList<>();
        SameStr.add("000000");
        SameStr.add("111111");
        SameStr.add("222222");
        SameStr.add("333333");
        SameStr.add("444444");
        SameStr.add("555555");
        SameStr.add("666666");
        SameStr.add("777777");
        SameStr.add("888888");
        SameStr.add("999999");

        String SequenceAsc = "0123456789";
        String SequenceDes = "9876543210";

        if (SameStr.contains(str) || SequenceAsc.contains(str) || SequenceDes.contains(str))
            return false;

        return true;


    }


    public static String formattedDateFromString(String inputFormat, String outputFormat, String inputDate) {
        if (inputFormat.equals("")) { // if inputFormat = "", set a default input format.
            inputFormat = "yyyy-MM-dd HH:mm:ss";
        } else if (inputFormat.equals("yyyy-MM-dd hh:mm:ss")) {
            inputFormat = "yyyy-MM-dd HH:mm:ss";
        } else if (inputFormat.equals("dd-MM-yyyy hh:mm:ss")) {
            inputFormat = "dd-MM-yyyy HH:mm:ss";
        }
        // Ignoring the inputFormat passing through method calls
        //   inputFormat = "yyyy-MM-dd HH:mm:ss";;

        if (outputFormat.equals("")) {
            outputFormat = "EEEE d 'de' MMMM 'del' yyyy"; // if inputFormat = "", set a default output format.
        }
        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.ENGLISH);
        SimpleDateFormat df_output = new SimpleDateFormat("MMM dd'' yyyy | HH:mm", Locale.ENGLISH);

        // You can set a different Locale, This example set a locale of Coun
        // try Mexico.
        //SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, new Locale("es", "MX"));
        //SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, new Locale("es", "MX"));

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (Exception e) {
            LogUtils.Verbose("formattedDateFromString", "Exception in formateDateFromstring(): " + e.getMessage());
            return inputDate;
        }
        return outputDate + " HRS";

    }

    public static String maskCardNumber(String cardNo) {

        try {
            StringBuilder cardno = new StringBuilder(cardNo);
            cardno.insert(4, " ");
            cardno.insert(9, " ");
            cardno.insert(14, " ");

            cardno.setCharAt(0, 'X');
            cardno.setCharAt(1, 'X');
            cardno.setCharAt(2, 'X');
            cardno.setCharAt(3, 'X');
            cardno.setCharAt(5, 'X');
            cardno.setCharAt(6, 'X');


            return cardno.toString();
        } catch (Exception e) {
            LogUtils.Exception(e);
        }

        return cardNo;
    }


    public static boolean isDeviceRooted() {

        return checkRootMethod1() || checkRootMethod2() || checkRootMethod3() || checkRootMethod4("su");
    }

    private static boolean checkRootMethod1() {
        String buildTags = android.os.Build.TAGS;
        return buildTags != null && buildTags.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        String[] paths = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    private static boolean checkRootMethod3() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            if (in.readLine() != null) return true;
            return false;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

    public static boolean checkRootMethod4(String binaryName) {
        boolean found = false;
        if (!found) {
            String[] places = {"/sbin/", "/system/bin/", "/system/xbin/",
                    "/data/local/xbin/", "/data/local/bin/",
                    "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
            for (String where : places) {
                if (new File(where + binaryName).exists()) {
                    found = true;

                    break;
                }
            }
        }
        return found;
    }

    public static RequestBody getJSONObjectBiller(String requestType, RequestDetail requestDetail) {

        GeneralJSONRequest generalJSONRequest;
        JsonRequest jsonRequest;
        JSONObject jsonObjectRequest;

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);

        //Set General Whole JSON
        generalJSONRequest = new GeneralJSONRequest();
        generalJSONRequest.setMobileNo(MovitConsumerApp.getInstance().getMobileNumber());
        generalJSONRequest.setSpId(Constants.SERVICE_PROVIDER);
        generalJSONRequest.setUserType(Constants.USER_TYPE);

        //Set Intermediate JSON
        jsonRequest = new JsonRequest();

        //Set Header
        Header header = new Header();
        header.setChannelId(Constants.CHANNEL_ID);
        header.setTimestamp(sdf.format(calendar.getTime()));
        header.setSessionId(MovitConsumerApp.getInstance().getSessionID());
        header.setServiceProvider(Constants.SERVICE_PROVIDER);
        jsonRequest.setHeader(header);

        Request request = new Request();
        request.setUserType(Constants.USER_TYPE);
        request.setMerchantId(Constants.BILLER_MERCHANT);
        request.setMobileNumber(MovitConsumerApp.getInstance().getMobileNumber());
        request.setRequestType(requestType);
        request.setPaymodeType(Constants.PAYMODE_TYPE);
        jsonRequest.setRequest(request);

        jsonRequest.setRequestDetail(requestDetail);

        generalJSONRequest.setJsonRequest(jsonRequest);


        Gson gson = new Gson();
        String jsonRequestString = gson.toJson(jsonRequest);
        LogUtils.Verbose("Plain JSONRequest", jsonRequestString);

        try {
            String encryptedJSON = null;

            encryptedJSON = AESecurity.getInstance().encryptString(jsonRequestString.trim());
            jsonObjectRequest = new JSONObject();
            jsonObjectRequest.put(Constants.MOBILE_NO_KEY, MovitConsumerApp.getInstance().getMobileNumber());
            jsonObjectRequest.put(Constants.USER_TYPE_KEY, Constants.USER_TYPE);
            jsonObjectRequest.put(Constants.SP_ID_KEY, Constants.SERVICE_PROVIDER);
            jsonObjectRequest.put(Constants.JSONREQUEST_KEY, encryptedJSON);

            String finalJSONString = jsonObjectRequest.toString();
            MediaType type = MediaType.parse("application/json; charset=utf-8");
            RequestBody jsonRequestBody = RequestBody.create(type, finalJSONString);
            return jsonRequestBody;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getLastNineDigitPhoneNumber(String str) {

        str = Constants.getOnlyDigits(str);

        //  str.replaceAll(" ", "");

        if (!TextUtils.isEmpty(str) && str.length() > 9) {
            return str.substring(str.length() - 9);
        } else {
            return str;
        }
    }
}

