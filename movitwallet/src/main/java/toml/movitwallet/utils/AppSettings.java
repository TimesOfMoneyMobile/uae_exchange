package toml.movitwallet.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kunalk on 4/13/2016.
 */
public class AppSettings {

    public static final String RESEND_MPIN_COUNT = "resend_mpin_count";
    // public static String MOBILE_NUMBER="mobile";

    //  public static final String SESSION_ID="sessionid";
    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String ADDRESS = "address";
    public static final String EMAIL_ID = "EMAIL_ID";
    public static final String LASTTRANSACTION = "lastlogin";
    public static final String WALLETS = "wallets";
    public static final String isTOKENSENDTOSERVER = "isTOKENSENDTOSERVER";
    public static final String NOTIFICATIONS = "NOTIFICATIONS";
    public static final String APPLANGUAGE = "APPLANGUAGE";
    public static final String NOTIFICATION_COUNT = "NOTIFICATION_COUNT";
    public static final String LASTLOGIN = "LASTLOGIN";
    public static final String ISCHECKEDTERMS = "ISCHECKEDTERMS";
    public static final String ISSESSIONEXPIRED = "ISSESSIONEXPIRED";
    public static final String ALIAS = "alias";
    public static final String NOTIFICATION_TOGGLE = "NOTIFICATION_TOGGLE";
    public static final String NOTIFICATION_ACTIVE = "NOTIFICATION_ACTIVE";
    public static final String IS_FIRSTTIMELOGIN = "IS_FIRSTTIMELOGIN";

    public static final String OTP_AMOUNT = "OTP_AMOUNT";

    public static String IS_LOGGED_IN = "IsLoggedIn";


    public static String BASE_URL = "BASE_URL";


    public static void putData(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("consumer", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public static String getData(Context context, String key) {
        if (context == null)
            return "";
        SharedPreferences preferences = context.getSharedPreferences("consumer", context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public static void putBooleanData(Context context, String key, boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("consumer", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBooleanData(Context context, String key) {
        if (context == null)
            return false;
        SharedPreferences preferences = context.getSharedPreferences("consumer", context.MODE_PRIVATE);
        if (key.equalsIgnoreCase(AppSettings.NOTIFICATION_ACTIVE))
            return preferences.getBoolean(key, true);
        else if (key.equalsIgnoreCase(AppSettings.NOTIFICATION_TOGGLE)) {
            return preferences.getBoolean(key, false);
        } else {
            return preferences.getBoolean(key, false);
        }
    }
}
