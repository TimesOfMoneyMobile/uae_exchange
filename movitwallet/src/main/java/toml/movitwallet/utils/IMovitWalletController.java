package toml.movitwallet.utils;

/**
 * Created by kunalk on 7/3/17.
 */

public interface IMovitWalletController<T> {

    void onSuccess(T response);

    void onFailed(String errorCode, String reason);
}
