package toml.movitwallet.utils;

import android.os.Bundle;
import android.widget.Toast;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kunalk on 4/27/2016.
 */
public class DecodeQRCode {

    public static Map<String, String> decodeQR(String str) {

        try {

            Map<String, String> map = new HashMap<String, String>();
            List<String> commaSepratedList = Arrays.asList(str.split(","));
            for (int i = 0; i < commaSepratedList.size(); i++) {
                List<String> eqaualSplitedList = Arrays.asList(commaSepratedList.get(i).split("="));
                map.put(eqaualSplitedList.get(0), eqaualSplitedList.get(1));
            }
            return map;
        }catch (Exception ex)
        {
            return null;
        }
    }

    private static String getAttrByTagNo(int tagno) {

        switch (tagno) {
            case 0:
                return "merchantID";
            case 1:
                return "merchantName";

            case 2:
                return "merchantCategory";

            case 3:
                return "city";

            case 4:
                return "country";

            case 5:
                return "currency";

            case 6:
                return "amount";

            case 7:
                return "primaryId";

            case 8:
                return "secondaryId";

            case 9:
                return "tip";


        }

        return null;
    }


    public static int lengthParameter(String firstChar) {

        int length = 1010;

        switch (firstChar) {
            case "0":
                length = 0;
                break;

            case "1":
                length = 1;
                break;

            case "2":
                length = 2;
                break;

            case "3":
                length = 3;
                break;

            case "4":
                length = 4;
                break;

            case "5":
                length = 5;
                break;

            case "6":
                length = 6;
                break;

            case "7":
                length = 7;
                break;

            case "8":
                length = 8;
                break;

            case "9":
                length = 9;
                break;

            case "A":
                length = 10;
                break;

            case "B":
                length = 11;
                break;

            case "C":
                length = 12;
                break;

            case "D":
                length = 13;
                break;

            case "E":
                length = 14;
                break;

            case "F":
                length = 15;
                break;

            case "G":
                length = 16;
                break;

            case "H":
                length = 17;
                break;

            case "I":
                length = 18;
                break;

            case "J":
                length = 19;
                break;

            case "K":
                length = 20;
                break;

            case "L":
                length = 21;
                break;

            case "M":
                length = 22;
                break;

            case "N":
                length = 23;
                break;

            case "O":
                length = 24;
                break;

            case "P":
                length = 25;
                break;

            case "Q":
                length = 26;
                break;

            case "R":
                length = 27;
                break;

            default:
                length = 1010;
                break;
        }


        return length;
    }
}
