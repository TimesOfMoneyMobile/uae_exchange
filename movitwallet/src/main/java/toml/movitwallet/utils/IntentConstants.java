package toml.movitwallet.utils;

/**
 * Created by kunalk on 10/3/17.
 */

public class IntentConstants {

    public static final String CARD_NUMBER = "cardnumber";
    public static final String WALLET_ID = "WALLET_ID";
    public static final String TOKEN = "TOKEN";
    public static final String MVISA_ID = "mvisaid";
    public static final String NAME = "name";
    public static final String P2PTXNRESPONSE = "P2PTXNRESPONSE";
    public static final String TYPE = "TYPE";
    public static final String AMOUNT = "Amount";
    public static final String TXNID = "TXNID";
    public static final String FEE = "FEE";
    public static final String TIMESTAMP = "TIMESTAMP";
    public static final String BALANCE = "TXNID";
    public static final String OTP = "OTP";
    public static final String PROMOTIONALBALANCE = "PromotionalBalance";
    public static final String ResponseType = "ResponseType";
    public static final String Message = "Message";
    public static final String Reason = "Reason";
    public static final String CompanyName = "CompanyName";
    public static final String TipFlag = "TipFlag";
    public static final String City = "City";
    public static final String CountryCode = "CountryCode";
    public static final String Country = "Country";

    public static final String MerchantCategory = "MerchantCategory";
    public static final String MerchantName = "MerchantName";
    public static final String DisplayName = "DisplayName";
    public static final String MerchantId = "MerchantId";
    public static final String ConvenienceFee = "ConvenienceFee";
    public static final String ErrorCode = "ErrorCode";
    public static final String Tip = "Tip";
    public static final String Tip_A = "Tip_A";
    public static final String Tip_B = "Tip_B";
    public static final String isTip_Percentage = "isTip_Percentage";
    public static final String isTip_ConvenienceFee = "isTip_ConvenienceFee";


    public static final String currency = "currency";

    public static final String primaryId = "primaryId";
    public static final String secondaryId = "secondaryId";
    public static final String CardAcceptorTerminalAddr = "CardAcceptorTerminalAddr";

    public static final String VirtualPAN = "VirtualPAN";
    public static final String ExpiryDate = "ExpiryDate";
    public static final String CVV = "CVV";
    public static final String PIN = "PIN";
    public static final String Token = "Token";
    public static final String VISACARD = "VISACARD";

    public static final String Bundle = "bundle";
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String DOB = "DOB";
    public static final String EMAIL_ID = "EMAIL_ID";
    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
    public static final String REFERRAL_CODE = "REFERRAL_CODE";
    public static final String PHOTO_URL = "PHOTO_URL";
    public static final String SOCIAL_ID = "SOCIAL_ID";
    public static final String SOCIAL_PROFILE = "SOCIAL_PROFILE";

    public static final String MONEYREQUEST = "MONEYREQUEST";
    public static final String STATUS = "STATUS";
    public static final String TRANSACTION_TYPE = "TRANSACTIONTYPE";
    public static final String P2M = "P2M";
    public static final String ACTIVITY_TO_OPEN = "ACTIVITY_TO_OPEN";
    public static final String FROM_ACTIVITY = "FROM_ACTIVITY";
    public static final String MERCHANTID = "MERCHANT_ID";
    public static final String MERCHANTNAME = "MERCHANT_NAME";
    public static final String EXTERNAL_MERCHANTID = "EXTERNAL_ID";
    public static final String QR_CODE_AMOUNT = "AMOUNT";


    public static final String SEND_VALUE = "SEND_VALUE";
    public static final String SEND_CURRENCY_ISO = "SEND_CURRENCY_ISO";
    public static final String SKU_CODE = "SKU_CODE";
    public static final String ACCOUNT_NO = "ACCOUNT_NO";
    public static final String DISTRIBUTER_REF = "DISTRIBUTER_REF";
    public static final String VALIDATE_ONLY = "VALIDATE_ONLY";
    public static final String RECHARGE_TYPE = "RECHARGE";
    public static final String TRANSACTIONFEES = "TransactionFees";
    public static final String FROM_ACTIVITY_REGISTRATION = "FROM_ACTIVITY_REGISTRATION";
    public static final String TRANSFER_AMOUNT = "TRANSFER_AMOUNT";
    public static final String PROMOCODE = "PROMOCODE";
    public static final String DISCOUNT = "Discount";
    public static final String DISCOUNT_ON = "DiscountOn";
    public static final String OFFER = "OFFER";

    public static Object Success = "Success";

    public static final String SECURITY_QUESTION_ANSWER = "SECURITY_QUESTION_ANSWER";
    public static final String REG_REQUEST = "REG REQUEST";
    public static final String RESET_QUESTIONS_REQUEST = "RESET QUESTIONS REQUEST";

    public static final String PG_MERCHANT_ID = "PGMerchantId";
    public static final String COLLABORATOR_ID = "CollaboratorID";
    public static final String ENCRYPTED_REQUEST = "EncryptedRequest";
    public static final String PG_MERCHANT_ORDER_NO = "PGMerchantOrderNo";


    public static final String PendingRequest = "PendingRequest";
    public static final String OffersCount = "OffersCount";
}
