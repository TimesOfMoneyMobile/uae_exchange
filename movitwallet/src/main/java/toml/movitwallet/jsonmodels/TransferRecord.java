package toml.movitwallet.jsonmodels;

/**
 * Created by mayurn on 9/26/2017.
 */

public class TransferRecord {

    private String ReceiptParams;

    private String ReceiptText;

    private String CommissionApplied;

    private TransferId TransferId;

    private String ProcessingState;

    private String AccountNumber;

    private String StartedUtc;

    private Price Price;

    private String CompletedUtc;

    private String SkuCode;

    public String getReceiptParams ()
    {
        return ReceiptParams;
    }

    public void setReceiptParams (String ReceiptParams)
    {
        this.ReceiptParams = ReceiptParams;
    }

    public String getReceiptText ()
    {
        return ReceiptText;
    }

    public void setReceiptText (String ReceiptText)
    {
        this.ReceiptText = ReceiptText;
    }

    public String getCommissionApplied ()
    {
        return CommissionApplied;
    }

    public void setCommissionApplied (String CommissionApplied)
    {
        this.CommissionApplied = CommissionApplied;
    }

    public TransferId getTransferId ()
    {
        return TransferId;
    }

    public void setTransferId (TransferId TransferId)
    {
        this.TransferId = TransferId;
    }

    public String getProcessingState ()
    {
        return ProcessingState;
    }

    public void setProcessingState (String ProcessingState)
    {
        this.ProcessingState = ProcessingState;
    }

    public String getAccountNumber ()
    {
        return AccountNumber;
    }

    public void setAccountNumber (String AccountNumber)
    {
        this.AccountNumber = AccountNumber;
    }

    public String getStartedUtc ()
    {
        return StartedUtc;
    }

    public void setStartedUtc (String StartedUtc)
    {
        this.StartedUtc = StartedUtc;
    }

    public Price getPrice ()
    {
        return Price;
    }

    public void setPrice (Price Price)
    {
        this.Price = Price;
    }

    public String getCompletedUtc ()
    {
        return CompletedUtc;
    }

    public void setCompletedUtc (String CompletedUtc)
    {
        this.CompletedUtc = CompletedUtc;
    }

    public String getSkuCode ()
    {
        return SkuCode;
    }

    public void setSkuCode (String SkuCode)
    {
        this.SkuCode = SkuCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ReceiptParams = "+ReceiptParams+", ReceiptText = "+ReceiptText+", CommissionApplied = "+CommissionApplied+", TransferId = "+TransferId+", ProcessingState = "+ProcessingState+", AccountNumber = "+AccountNumber+", StartedUtc = "+StartedUtc+", Price = "+Price+", CompletedUtc = "+CompletedUtc+", SkuCode = "+SkuCode+"]";
    }
}
