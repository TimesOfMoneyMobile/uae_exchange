package toml.movitwallet.jsonmodels;

/**
 * Created by mayurn on 11/3/2017.
 */

public class RechargeSuccessResponsePG
{
    private String CollaboratorID;

    private String PGMerchantId;

    private String EncryptedRequest;

    private String PGMerchantOrderNo;

    public String getCollaboratorID ()
    {
        return CollaboratorID;
    }

    public void setCollaboratorID (String CollaboratorID)
    {
        this.CollaboratorID = CollaboratorID;
    }

    public String getPGMerchantId ()
    {
        return PGMerchantId;
    }

    public void setPGMerchantId (String PGMerchantId)
    {
        this.PGMerchantId = PGMerchantId;
    }

    public String getEncryptedRequest ()
    {
        return EncryptedRequest;
    }

    public void setEncryptedRequest (String EncryptedRequest)
    {
        this.EncryptedRequest = EncryptedRequest;
    }

    public String getPGMerchantOrderNo ()
    {
        return PGMerchantOrderNo;
    }

    public void setPGMerchantOrderNo (String PGMerchantOrderNo)
    {
        this.PGMerchantOrderNo = PGMerchantOrderNo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CollaboratorID = "+CollaboratorID+", PGMerchantId = "+PGMerchantId+", EncryptedRequest = "+EncryptedRequest+", PGMerchantOrderNo = "+PGMerchantOrderNo+"]";
    }
}