package toml.movitwallet.jsonmodels;

/**
 * Created by mayurn on 9/25/2017.
 */

public class ProviderItems {
    private String Name;

    private String ProviderCode;

    private String ValidationRegex;

    private String CountryIso;

    private String ShortName;

    private String[] RegionCodes;

    private String CustomerCareNumber;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getProviderCode ()
    {
        return ProviderCode;
    }

    public void setProviderCode (String ProviderCode)
    {
        this.ProviderCode = ProviderCode;
    }

    public String getValidationRegex ()
    {
        return ValidationRegex;
    }

    public void setValidationRegex (String ValidationRegex)
    {
        this.ValidationRegex = ValidationRegex;
    }

    public String getCountryIso ()
    {
        return CountryIso;
    }

    public void setCountryIso (String CountryIso)
    {
        this.CountryIso = CountryIso;
    }

    public String getShortName ()
    {
        return ShortName;
    }

    public void setShortName (String ShortName)
    {
        this.ShortName = ShortName;
    }

    public String[] getRegionCodes ()
    {
        return RegionCodes;
    }

    public void setRegionCodes (String[] RegionCodes)
    {
        this.RegionCodes = RegionCodes;
    }

    public String getCustomerCareNumber ()
    {
        return CustomerCareNumber;
    }

    public void setCustomerCareNumber (String CustomerCareNumber)
    {
        this.CustomerCareNumber = CustomerCareNumber;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Name = "+Name+", ProviderCode = "+ProviderCode+", ValidationRegex = "+ValidationRegex+", CountryIso = "+CountryIso+", ShortName = "+ShortName+", RegionCodes = "+RegionCodes+", CustomerCareNumber = "+CustomerCareNumber+"]";
    }
}
