package toml.movitwallet.jsonmodels;

import java.util.List;

/**
 * Created by mayurn on 9/25/2017.
 */

public class ProviderResponse {
    private List<ProviderItems> Items;

    private String[] ErrorCodes;

    private String ResultCode;

    public List<ProviderItems> getItems ()
    {
        return Items;
    }

    public void setItems (List<ProviderItems> Items)
    {
        this.Items = Items;
    }

    public String[] getErrorCodes ()
    {
        return ErrorCodes;
    }

    public void setErrorCodes (String[] ErrorCodes)
    {
        this.ErrorCodes = ErrorCodes;
    }

    public String getResultCode ()
    {
        return ResultCode;
    }

    public void setResultCode (String ResultCode)
    {
        this.ResultCode = ResultCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ProviderItems = "+Items+", ErrorCodes = "+ErrorCodes+", ResultCode = "+ResultCode+"]";
    }
}
