package toml.movitwallet.jsonmodels;

/**
 * Created by mayurn on 9/22/2017.
 */

public class GeneralJSONRequest
{
    private JsonRequest jsonRequest;

    private String spId;

    private String mobileNo;

    private String userType;

    public JsonRequest getJsonRequest ()
    {
        return jsonRequest;
    }

    public void setJsonRequest (JsonRequest jsonRequest)
    {
        this.jsonRequest = jsonRequest;
    }

    public String getSpId ()
    {
        return spId;
    }

    public void setSpId (String spId)
    {
        this.spId = spId;
    }

    public String getMobileNo ()
    {
        return mobileNo;
    }

    public void setMobileNo (String mobileNo)
    {
        this.mobileNo = mobileNo;
    }

    public String getUserType ()
    {
        return userType;
    }

    public void setUserType (String userType)
    {
        this.userType = userType;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [jsonRequest = "+jsonRequest+", spId = "+spId+", mobileNo = "+mobileNo+", userType = "+userType+"]";
    }
}