package toml.movitwallet.jsonmodels;

import java.util.List;

/**
 * Created by mayurn on 9/26/2017.
 */

public class RechargeSuccessResponseFromWallet {

    private List<String> ErrorCodes;

    private TransferRecord TransferRecord;

    private String ResultCode;

    public List<String> getErrorCodes ()
    {
        return ErrorCodes;
    }

    public void setErrorCodes (List<String> ErrorCodes)
    {
        this.ErrorCodes = ErrorCodes;
    }

    public TransferRecord getTransferRecord ()
    {
        return TransferRecord;
    }

    public void setTransferRecord (TransferRecord TransferRecord)
    {
        this.TransferRecord = TransferRecord;
    }

    public String getResultCode ()
    {
        return ResultCode;
    }

    public void setResultCode (String ResultCode)
    {
        this.ResultCode = ResultCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ErrorCodes = "+ErrorCodes+", TransferRecord = "+TransferRecord+", ResultCode = "+ResultCode+"]";
    }
}
