package toml.movitwallet.jsonmodels;

/**
 * Created by mayurn on 9/23/2017.
 */

public class JSONHeader {

    private String SessionId;

    private String ChannelId;

    private String ServiceProvider;

    private String Timestamp;

    public String getSessionId() {
        return SessionId;
    }

    public void setSessionId(String SessionId) {
        this.SessionId = SessionId;
    }

    public String getChannelId() {
        return ChannelId;
    }

    public void setChannelId(String ChannelId) {
        this.ChannelId = ChannelId;
    }

    public String getServiceProvider() {
        return ServiceProvider;
    }

    public void setServiceProvider(String ServiceProvider) {
        this.ServiceProvider = ServiceProvider;
    }

    public String getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(String Timestamp) {
        this.Timestamp = Timestamp;
    }

    @Override
    public String toString() {
        return "ClassPojo [SessionId = " + SessionId + ", ChannelId = " + ChannelId + ", ServiceProvider = " + ServiceProvider + ", Timestamp = " + Timestamp + "]";
    }
}


