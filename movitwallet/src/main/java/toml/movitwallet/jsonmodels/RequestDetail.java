package toml.movitwallet.jsonmodels;

/**
 * Created by mayurn on 9/23/2017.
 */

public class RequestDetail {



    String AccountNumber;
    String CountryISOCode;
    String ProviderCode;
    String SkuCode;
    String SendValue;
    String SendCurrencyIso;
    String DistributorRef;
   // boolean ValidateOnly;

    String ValidateOnly;


    String OTP;


    String PromoCode;



    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }


    public void setPromoCode(String promoCode) {
        PromoCode = promoCode;
    }

    public String getSkuCode() {
        return SkuCode;
    }

    public void setSkuCode(String skuCode) {
        SkuCode = skuCode;
    }

    public String getSendValue() {
        return SendValue;
    }

    public void setSendValue(String sendValue) {
        SendValue = sendValue;
    }

    public String getSendCurrencyIso() {
        return SendCurrencyIso;
    }

    public void setSendCurrencyIso(String sendCurrencyIso) {
        SendCurrencyIso = sendCurrencyIso;
    }

    public String getDistributorRef() {
        return DistributorRef;
    }

    public void setDistributorRef(String distributorRef) {
        DistributorRef = distributorRef;
    }

    /*public boolean getValidateOnly() {
        return ValidateOnly;
    }

    public void setValidateOnly(boolean validateOnly) {
        ValidateOnly = validateOnly;
    }*/

    public void setValidateOnly(String validateOnly) {
        ValidateOnly = validateOnly;
    }
    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getCountryISOCode() {
        return CountryISOCode;
    }

    public void setCountryISOCode(String countryISOCode) {
        CountryISOCode = countryISOCode;
    }

    public String getProviderCode() {
        return ProviderCode;
    }

    public void setProviderCode(String providerCode) {
        ProviderCode = providerCode;
    }

}
