package toml.movitwallet.jsonmodels;

/**
 * Created by mayurn on 9/26/2017.
 */

public class TransferId {

    private String DistributorRef;

    private String TransferRef;

    public String getDistributorRef ()
    {
        return DistributorRef;
    }

    public void setDistributorRef (String DistributorRef)
    {
        this.DistributorRef = DistributorRef;
    }

    public String getTransferRef ()
    {
        return TransferRef;
    }

    public void setTransferRef (String TransferRef)
    {
        this.TransferRef = TransferRef;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DistributorRef = "+DistributorRef+", TransferRef = "+TransferRef+"]";
    }
}
