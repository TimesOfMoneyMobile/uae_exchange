package toml.movitwallet.jsonmodels;

import java.util.List;

/**
 * Created by mayurn on 9/25/2017.
 */

public class ProductListResponse {

    //The refrence name is having significance Items should be the name
    private List<ProductListItem> Items;

    private List<String> ErrorCodes;

    private String ResultCode;

    public List<ProductListItem> getProductListItem ()
    {
        return Items;
    }

    public void setProductListItem (List<ProductListItem> Items)
    {
        this.Items = Items;
    }

    public List<String> getErrorCodes ()
    {
        return ErrorCodes;
    }

    public void setErrorCodes (List<String> ErrorCodes)
    {
        this.ErrorCodes = ErrorCodes;
    }

    public String getResultCode ()
    {
        return ResultCode;
    }

    public void setResultCode (String ResultCode)
    {
        this.ResultCode = ResultCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ProductListItem = "+Items+", ErrorCodes = "+ErrorCodes+", ResultCode = "+ResultCode+"]";
    }
}
