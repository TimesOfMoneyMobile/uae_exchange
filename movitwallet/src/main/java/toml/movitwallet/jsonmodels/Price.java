package toml.movitwallet.jsonmodels;

/**
 * Created by mayurn on 9/26/2017.
 */

public class Price {

        private String DistributorFee;

        private String ReceiveValueExcludingTax;

        private String SendCurrencyIso;

        private String SendValue;

        private String TaxCalculation;

        private String ReceiveCurrencyIso;

        private String CustomerFee;

        private String TaxName;

        private String TaxRate;

        private String ReceiveValue;

        public String getDistributorFee ()
        {
            return DistributorFee;
        }

        public void setDistributorFee (String DistributorFee)
        {
            this.DistributorFee = DistributorFee;
        }

        public String getReceiveValueExcludingTax ()
        {
            return ReceiveValueExcludingTax;
        }

        public void setReceiveValueExcludingTax (String ReceiveValueExcludingTax)
        {
            this.ReceiveValueExcludingTax = ReceiveValueExcludingTax;
        }

        public String getSendCurrencyIso ()
        {
            return SendCurrencyIso;
        }

        public void setSendCurrencyIso (String SendCurrencyIso)
        {
            this.SendCurrencyIso = SendCurrencyIso;
        }

        public String getSendValue ()
        {
            return SendValue;
        }

        public void setSendValue (String SendValue)
        {
            this.SendValue = SendValue;
        }

        public String getTaxCalculation ()
    {
        return TaxCalculation;
    }

        public void setTaxCalculation (String TaxCalculation)
        {
            this.TaxCalculation = TaxCalculation;
        }

        public String getReceiveCurrencyIso ()
        {
            return ReceiveCurrencyIso;
        }

        public void setReceiveCurrencyIso (String ReceiveCurrencyIso)
        {
            this.ReceiveCurrencyIso = ReceiveCurrencyIso;
        }

        public String getCustomerFee ()
        {
            return CustomerFee;
        }

        public void setCustomerFee (String CustomerFee)
        {
            this.CustomerFee = CustomerFee;
        }

        public String getTaxName ()
    {
        return TaxName;
    }

        public void setTaxName (String TaxName)
        {
            this.TaxName = TaxName;
        }

        public String getTaxRate ()
        {
            return TaxRate;
        }

        public void setTaxRate (String TaxRate)
        {
            this.TaxRate = TaxRate;
        }

        public String getReceiveValue ()
        {
            return ReceiveValue;
        }

        public void setReceiveValue (String ReceiveValue)
        {
            this.ReceiveValue = ReceiveValue;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [DistributorFee = "+DistributorFee+", ReceiveValueExcludingTax = "+ReceiveValueExcludingTax+", SendCurrencyIso = "+SendCurrencyIso+", SendValue = "+SendValue+", TaxCalculation = "+TaxCalculation+", ReceiveCurrencyIso = "+ReceiveCurrencyIso+", CustomerFee = "+CustomerFee+", TaxName = "+TaxName+", TaxRate = "+TaxRate+", ReceiveValue = "+ReceiveValue+"]";
        }

}
