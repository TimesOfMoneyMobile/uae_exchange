package toml.movitwallet.jsonmodels;

import java.util.List;

/**
 * Created by mayurn on 9/25/2017.
 */

public class ProductListItem {

    private String min;

    private String max;

    private String UatNumber;

    private Maximum Maximum;

    private String DefaultDisplayText;

    private String ProviderCode;

    private String LocalizationKey;

    private String CommissionRate;

    private String[] SettingDefinitions;

    private String ProcessingMode;

    private Minimum Minimum;

    private String RedemptionMechanism;

    private String[] Benefits;

    private List<String> list_item;

    private String RegionCode;

    private String SkuCode;

    public String getMin ()
    {
        return min;
    }

    public void setMin (String min)
    {
        this.min = min;
    }

    public String getMax ()
    {
        return max;
    }

    public void setMax (String max)
    {
        this.max = max;
    }

    public String getUatNumber ()
    {
        return UatNumber;
    }

    public void setUatNumber (String UatNumber)
    {
        this.UatNumber = UatNumber;
    }

    public Maximum getMaximum ()
    {
        return Maximum;
    }

    public void setMaximum (Maximum Maximum)
    {
        this.Maximum = Maximum;
    }

    public String getDefaultDisplayText ()
    {
        return DefaultDisplayText;
    }

    public void setDefaultDisplayText (String DefaultDisplayText)
    {
        this.DefaultDisplayText = DefaultDisplayText;
    }

    public String getProviderCode ()
    {
        return ProviderCode;
    }

    public void setProviderCode (String ProviderCode)
    {
        this.ProviderCode = ProviderCode;
    }

    public String getLocalizationKey ()
    {
        return LocalizationKey;
    }

    public void setLocalizationKey (String LocalizationKey)
    {
        this.LocalizationKey = LocalizationKey;
    }

    public String getCommissionRate ()
    {
        return CommissionRate;
    }

    public void setCommissionRate (String CommissionRate)
    {
        this.CommissionRate = CommissionRate;
    }

    public String[] getSettingDefinitions ()
    {
        return SettingDefinitions;
    }

    public void setSettingDefinitions (String[] SettingDefinitions)
    {
        this.SettingDefinitions = SettingDefinitions;
    }

    public String getProcessingMode ()
    {
        return ProcessingMode;
    }

    public void setProcessingMode (String ProcessingMode)
    {
        this.ProcessingMode = ProcessingMode;
    }

    public Minimum getMinimum ()
    {
        return Minimum;
    }

    public void setMinimum (Minimum Minimum)
    {
        this.Minimum = Minimum;
    }

    public String getRedemptionMechanism ()
    {
        return RedemptionMechanism;
    }

    public void setRedemptionMechanism (String RedemptionMechanism)
    {
        this.RedemptionMechanism = RedemptionMechanism;
    }

    public String[] getBenefits ()
    {
        return Benefits;
    }

    public void setBenefits (String[] Benefits)
    {
        this.Benefits = Benefits;
    }

    public List<String> getList_item ()
    {
        return list_item;
    }

    public void setList_item (List<String> list_item)
    {
        this.list_item = list_item;
    }

    public String getRegionCode ()
    {
        return RegionCode;
    }

    public void setRegionCode (String RegionCode)
    {
        this.RegionCode = RegionCode;
    }

    public String getSkuCode ()
    {
        return SkuCode;
    }

    public void setSkuCode (String SkuCode)
    {
        this.SkuCode = SkuCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [min = "+min+", max = "+max+", UatNumber = "+UatNumber+", Maximum = "+Maximum+", DefaultDisplayText = "+DefaultDisplayText+", ProviderCode = "+ProviderCode+", LocalizationKey = "+LocalizationKey+", CommissionRate = "+CommissionRate+", SettingDefinitions = "+SettingDefinitions+", ProcessingMode = "+ProcessingMode+", Minimum = "+Minimum+", RedemptionMechanism = "+RedemptionMechanism+", Benefits = "+Benefits+", list_item = "+list_item+", RegionCode = "+RegionCode+", SkuCode = "+SkuCode+"]";
    }
}
