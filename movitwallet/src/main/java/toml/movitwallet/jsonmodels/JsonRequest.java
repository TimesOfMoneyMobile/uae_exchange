package toml.movitwallet.jsonmodels;

import toml.movitwallet.models.Header;
import toml.movitwallet.models.Request;

/**
 * Created by mayurn on 9/22/2017.
 */

public class JsonRequest
{
    private toml.movitwallet.models.Header Header;

    private RequestDetail RequestDetail;

    private toml.movitwallet.models.Request Request;

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

    public RequestDetail getRequestDetail()
    {
        return RequestDetail;
    }

    public void setRequestDetail(RequestDetail GetProviderRequest)
    {
        this.RequestDetail = GetProviderRequest;
    }

    public Request getRequest ()
    {
        return Request;
    }

    public void setRequest (Request Request)
    {
        this.Request = Request;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Header = "+Header+", RequestDetail = "+ RequestDetail +", Request = "+Request+"]";
    }

}


