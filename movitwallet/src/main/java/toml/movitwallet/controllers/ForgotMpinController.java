package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.AESecurity;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 8/8/2017.
 */

public class ForgotMpinController implements MovitWalletController {


    private String strMobile,strEmail,strDate;
    private IMovitWalletController iMovitWalletController;


    public ForgotMpinController(String strDate, String strMobile, String strEmail) {
        this.strDate = strDate;
        this.strMobile=strMobile;
        this.strEmail = strEmail;


    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        getKey();

    }


    private void getKey() {
        new GetKeyController().fetchSessionKeyByMobile(strMobile, new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                callAPI();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

                iMovitWalletController.onFailed(errorCode, reason);
            }
        });
    }


    private void callAPI() {

        String xmlParam =   "\t\t<MobileNumber>" + strMobile + "</MobileNumber>\n" +
                "\t\t<DateOfBirth>" + strDate + "</DateOfBirth>\n" +
                "\t\t<PinCode>"+""+"</PinCode>";
//                "\t\t<EmailId>" + strEmail + "</EmailId>";


        RetrofitTask.getInstance().executeTask(xmlParam, "ForgotMpin", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);

                    LogUtils.Verbose("ForgotMpinController",decrypted);

                    ArrayList<String> list = new ArrayList<>();
                    list.add(IntentConstants.ResponseType);
                    list.add(IntentConstants.ErrorCode);
                    list.add(IntentConstants.Reason);


                    list.add("Message");



                    Bundle responseMap = GenericResponseHandler.parseElements(decrypted, list);


                    if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {
                        iMovitWalletController.onSuccess(responseMap);
                    }else{
                        iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(),responseMap.get(IntentConstants.Reason).toString());
                    }

                }catch (Exception e){
                    e.printStackTrace();

                    iMovitWalletController.onFailed("", e.getMessage());
                }

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }


}
