package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.PendingRequest;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 8/10/2017.
 */

public class PendingRequestListController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;

    public PendingRequestListController() {

    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();
    }

    private void callApi() {

        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<WalletId>" + MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId() + "</WalletId>\n";

        RetrofitTask.getInstance().executeTask(xmlParam, "ListReqMoney", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                handleSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {

            }
        });

    }

    private void handleSuccess(String response) {

        final Serializer serializer = new Persister();

        try {
            PendingRequest pendingReqResponse = serializer.read(PendingRequest.class, response, false);
            if(pendingReqResponse != null)
            {
                if (pendingReqResponse.getResponse().getResponseType().equals("Success"))
                {
                    iMovitWalletController.onSuccess(pendingReqResponse.getResponseDetails().getMoneyRequests().getMoneyRequestList());
                }
                else
                {
                    iMovitWalletController.onFailed(pendingReqResponse.getResponseDetails().getErrorCode(),pendingReqResponse.getResponseDetails().getReason());
                }
                            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
