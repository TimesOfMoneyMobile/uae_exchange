package toml.movitwallet.controllers;

import android.os.Bundle;
import android.text.TextUtils;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.ResetQuestionsRequest;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 11/3/2017.
 */

public class ResetSecurityQuestionController implements MovitWalletController {
    String MobileNumber = "";
    String DateOfBirth = "";
    String EmiratesID = "";
    String EmailId = "";
    String KYCDocumentsResult = "";
    String SecurityQuestionResult = "";
    private IMovitWalletController iMovitWalletController;

    public ResetSecurityQuestionController(ResetQuestionsRequest resetQuestionsRequest) {
        MobileNumber = resetQuestionsRequest.getMobileNumber();
        DateOfBirth = resetQuestionsRequest.getDateOfBirth();
        EmiratesID = resetQuestionsRequest.getEmiratesID();
        EmailId = resetQuestionsRequest.getEmailId();
        KYCDocumentsResult = resetQuestionsRequest.getKYCDocumentsResult();
        SecurityQuestionResult = resetQuestionsRequest.getSecurityQuestionResult();
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        if (TextUtils.isEmpty(MovitConsumerApp.getInstance().getKey())) {

            getKey();
        } else {
            callResetSecurityQuestion();
        }
    }

    private void getKey() {

        new GetKeyController().fetchSessionKeyByMobile(MobileNumber, new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {


                //callAPI();

                callResetSecurityQuestion();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

            }
        });

    }

    private void callResetSecurityQuestion() {
        String xmlParam = "\t\t<MobileNumber>" + MobileNumber + "</MobileNumber>\n" +
                "\t\t<DateOfBirth>" + DateOfBirth + "</DateOfBirth>\n" +
                "\t\t<EmailId>" + EmailId + "</EmailId>\n" +
                "\t\t<KYCDocuments>\n" + KYCDocumentsResult + "</KYCDocuments>\n" +
                "\t\t<SecurityQuestions>\n" + SecurityQuestionResult + "</SecurityQuestions>";

        RetrofitTask.getInstance().executeTask(xmlParam, "ResetQuestions", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                handleSuccess(response);

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });
    }

    private void handleSuccess(String response) {

        ArrayList<String> list = new ArrayList<>();
        list.add(IntentConstants.ResponseType);
        list.add(IntentConstants.Message);
        list.add(IntentConstants.ErrorCode);
        list.add(IntentConstants.Reason);

        Bundle responseMap = GenericResponseHandler.parseElements(response, list);
        if (responseMap.get(IntentConstants.ResponseType).equals(IntentConstants.Success)) {

            iMovitWalletController.onSuccess(responseMap);

        } else {
            iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
        }

    }
}
