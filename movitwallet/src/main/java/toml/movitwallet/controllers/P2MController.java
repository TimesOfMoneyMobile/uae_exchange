package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.AESecurity;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 8/8/2017.
 */

public class P2MController  implements MovitWalletController {

    String strMerchatID,strAmount,strTpin,strPromoCode;
    private IMovitWalletController iMovitWalletController;

    public P2MController(String strMerchatID, String strAmount, String strTpin,String strPromoCode) {
        this.strMerchatID = strMerchatID;
        this.strAmount=strAmount;
        this.strTpin = strTpin;
        this.strPromoCode = strPromoCode;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();

    }

    private void callApi() {

        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<MerchantId>"+ strMerchatID + "</MerchantId>\n" +
                "\t\t<Amount>"+strAmount + "</Amount>\n" +
                //"\t\t<Tpin>" + Constants.getEncryptedMpin(strTpin) + "</Tpin>\n" +
                "\t\t<Tpin>" + strTpin + "</Tpin>\n" +
                "\t\t<WalletId>" + MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId() + "</WalletId>\n" +
                "\t\t<PromoCode>"+strPromoCode +"</PromoCode>\n"  +
                "\t\t<ResponseURL>"+MovitConsumerApp.getInstance().getBaseURL()+"</ResponseURL>\n" +
                "\t\t<ResponseVar>"+ MovitConsumerApp.getInstance().getBaseURL()+ "</ResponseVar>";

        RetrofitTask.getInstance().executeTask(xmlParam, "P2M", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {


                try {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(IntentConstants.ResponseType);
                    list.add(IntentConstants.ErrorCode);
                    list.add(IntentConstants.Reason);
                    list.add(IntentConstants.Message);
                    list.add(IntentConstants.TXNID);
                    list.add(IntentConstants.AMOUNT);
                    list.add(IntentConstants.FEE);
                    list.add(IntentConstants.BALANCE);
                    list.add(IntentConstants.OTP);
                    list.add(IntentConstants.PROMOTIONALBALANCE);

                    Bundle responseMap = GenericResponseHandler.parseElements(response, list);
                    if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                        iMovitWalletController.onSuccess(responseMap);

                    }else{
                        iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(),responseMap.get(IntentConstants.Reason).toString());
                    }




                }catch (Exception e){
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());

                }



            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }
}
