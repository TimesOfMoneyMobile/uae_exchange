package toml.movitwallet.controllers;

import android.os.Bundle;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;
import java.util.Map;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.LoginResponse;
import toml.movitwallet.models.RegistrationResponse;
import toml.movitwallet.utils.AESecurity;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 8/1/2017.
 */

public class ForgotTpinController implements MovitWalletController {

    private String strMobile,strEmail,strMpin;
    private IMovitWalletController iMovitWalletController;



    public ForgotTpinController(String strMobile, String strEmail, String strMpin) {
        this.strMobile=strMobile;
        this.strEmail = strEmail;
        this.strMpin = strMpin;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        getKey();

    }

    private void getKey() {
        new GetKeyController().fetchSessionKeyByMobile(strMobile, new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                callAPI();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

                iMovitWalletController.onFailed(errorCode, reason);
            }
        });
    }

    private void callAPI() {

       String xmlParam =   "\t\t<MobileNumber>" + strMobile + "</MobileNumber>\n" +
               "\t\t<Mpin>" + strMpin + "</Mpin>\n" +
               "\t\t<PinCode>"+""+"</PinCode>\n" +
               "\t\t<EmailId>" + strEmail + "</EmailId>";


        RetrofitTask.getInstance().executeTask(xmlParam, "ForgotTpin", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);

                    LogUtils.Verbose("ForgotTpinController",decrypted);

                    ArrayList<String> list = new ArrayList<>();
                    list.add(IntentConstants.ResponseType);
                    list.add(IntentConstants.ErrorCode);
                    list.add(IntentConstants.Reason);


                    list.add("Message");



                    Bundle responseMap = GenericResponseHandler.parseElements(decrypted, list);


                    if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {
                        iMovitWalletController.onSuccess(responseMap);
                    }else{
                        iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(),responseMap.get(IntentConstants.Reason).toString());
                    }

                    }catch (Exception e){
                    e.printStackTrace();

                    iMovitWalletController.onFailed("", e.getMessage());
                }

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }
}
