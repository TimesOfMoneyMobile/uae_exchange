package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 8/8/2017.
 */

public class AddMoneyController implements MovitWalletController {

    String strAmount, strCollaboratorID, strTxnType;
    private IMovitWalletController iMovitWalletController;

    public AddMoneyController(String strAmount) {

        this.strAmount = strAmount;
        this.strCollaboratorID = Constants.NEOCollaboratorId;
        strTxnType = "FUND";


    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();

    }

    private void callApi() {
        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<TxnType>" + strTxnType + "</TxnType>\n" +
                "\t\t<Amount>" + strAmount + "</Amount>\n" +
                "\t\t<CollaboratorID>" + strCollaboratorID + "</CollaboratorID>";


        RetrofitTask.getInstance().executeTask(xmlParam, "PGRequest", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                handleSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }

    private void handleSuccess(String response) {
        try {

            ArrayList<String> list = new ArrayList<>();
            list.add(IntentConstants.ResponseType);
            list.add(IntentConstants.ErrorCode);
            list.add(IntentConstants.Reason);
            list.add(IntentConstants.Message);
            list.add(IntentConstants.PG_MERCHANT_ID);
            list.add(IntentConstants.COLLABORATOR_ID);
            list.add(IntentConstants.ENCRYPTED_REQUEST);
            list.add(IntentConstants.PG_MERCHANT_ORDER_NO);

            Bundle responseMap = GenericResponseHandler.parseElements(response, list);
            if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                iMovitWalletController.onSuccess(responseMap);

            } else {
                iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
            }


        } catch (Exception e) {
            e.printStackTrace();
            iMovitWalletController.onFailed("", e.getMessage());

        }
    }
}
