package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.OfferResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 8/10/2017.
 */

public class OffersController implements MovitWalletController {
    private IMovitWalletController iMovitWalletController;

    public OffersController() {
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callAPI();

    }

    private void callAPI() {

        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>";

        RetrofitTask.getInstance().executeTask(xmlParam, "Offers", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                Serializer serializer = new Persister();
                try {
                    OfferResponse offerResponse = serializer.read(OfferResponse.class, response, false);

                    if (offerResponse.getResponse().getResponseType() != null && offerResponse.getResponse().getResponseType().equalsIgnoreCase("Success")) {
                        //finish();
                        if (offerResponse.getOfferResponseDetails().getOfferList().getListOffers().size() > 0) {
                            iMovitWalletController.onSuccess(offerResponse.getOfferResponseDetails().getOfferList().getListOffers());
                        }
                    }
                    else
                    {
                        iMovitWalletController.onFailed(offerResponse.getOfferResponseDetails().getErrorCode(),offerResponse.getOfferResponseDetails().getReason());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {

            }
        });

    }
}
