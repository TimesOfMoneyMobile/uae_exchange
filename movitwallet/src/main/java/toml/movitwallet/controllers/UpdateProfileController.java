package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 9/7/2017.
 */

public class UpdateProfileController implements MovitWalletController {

    String firstName, lastName, aliasName, emiratsId, dob, emailId, mobileNo, nationality;
    IMovitWalletController iMovitWalletController;
    Map<String, RequestBody> requestBodyMap = new HashMap<>();

    public UpdateProfileController() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getEmiratsId() {
        return emiratsId;
    }

    public void setEmiratsId(String emiratsId) {
        this.emiratsId = emiratsId;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }


    void setMultiPartData() {

        RequestBody rbchannelId = RequestBody.create(MediaType.parse("multipart/form-data"), "APP");
        requestBodyMap.put("channelId", rbchannelId);


        RequestBody rbmobileNo = RequestBody.create(MediaType.parse("multipart/form-data"), mobileNo);
        requestBodyMap.put("mobileNo", rbmobileNo);

        RequestBody rbuserType = RequestBody.create(MediaType.parse("multipart/form-data"), Constants.USER_TYPE);
        requestBodyMap.put("userType", rbuserType);

        RequestBody rbrequestType = RequestBody.create(MediaType.parse("multipart/form-data"), Constants.MANGE_PROFILE_REQUESTTYPE);
        requestBodyMap.put("requestType", rbrequestType);

        RequestBody rbspid = RequestBody.create(MediaType.parse("multipart/form-data"), Constants.SERVICE_PROVIDER);
        requestBodyMap.put("spid", rbspid);

        if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("N")) {
            RequestBody rbfirstname = RequestBody.create(MediaType.parse("multipart/form-data"), firstName);
            requestBodyMap.put("firstName", rbfirstname);

            RequestBody rblastName = RequestBody.create(MediaType.parse("multipart/form-data"), lastName);
            requestBodyMap.put("lastName", rblastName);

            RequestBody rbaliasName = RequestBody.create(MediaType.parse("multipart/form-data"), aliasName);
            requestBodyMap.put("alias", rbaliasName);

            RequestBody rbemailId = RequestBody.create(MediaType.parse("multipart/form-data"), emailId);
            requestBodyMap.put("emailId", rbemailId);
        } else {

            RequestBody rbaliasName = RequestBody.create(MediaType.parse("multipart/form-data"), aliasName);
            requestBodyMap.put("alias", rbaliasName);

            RequestBody rbemailId = RequestBody.create(MediaType.parse("multipart/form-data"), emailId);
            requestBodyMap.put("emailId", rbemailId);
        }


    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        setMultiPartData();
        callUpdateProfile();
    }

    private void callUpdateProfile() {

        RetrofitTask retrofitTask = RetrofitTask.getInstance();

        retrofitTask.executeRegisterCustomer(requestBodyMap, new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                ArrayList<String> list = new ArrayList<>();
                list.add(IntentConstants.ResponseType);
                list.add(IntentConstants.ErrorCode);
                list.add(IntentConstants.Reason);
                list.add(IntentConstants.Message);

                Bundle responseMap = GenericResponseHandler.parseElements(response, list);

                if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {
                    iMovitWalletController.onSuccess(responseMap);

                } else {
                    iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
                }
            }


            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });
    }

}
