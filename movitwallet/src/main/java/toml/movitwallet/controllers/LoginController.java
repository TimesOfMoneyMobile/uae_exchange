package toml.movitwallet.controllers;

import android.content.Context;
import android.text.TextUtils;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.LoginResponse;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by kunalk on 7/19/2017.
 */

public class LoginController implements MovitWalletController {

    private String MobileNumber, MPIN, SocialID, DeviceName;
    private boolean isLoginThroughSocial = false;
    private IMovitWalletController iMovitWalletController;
    private Context context;


    public LoginController(Context context, String mobileNumber, String MPIN, String DeviceName) {
        MobileNumber = mobileNumber;
        this.MPIN = MPIN;
        this.DeviceName = DeviceName;
        this.context = context;

    }

    public LoginController(String socialID) {
        SocialID = socialID;
        isLoginThroughSocial = true;
    }

    private void getKey() {

        if (!isLoginThroughSocial) {
            new GetKeyController().fetchSessionKeyByMobile(MobileNumber, new IMovitWalletController() {
                @Override
                public void onSuccess(Object response) {
                    callAPI();
                }

                @Override
                public void onFailed(String errorCode, String reason) {

                    iMovitWalletController.onFailed(errorCode, reason);
                }
            });
        } else {
            new GetKeyController().fetchSessionKeyBySocial(SocialID, new IMovitWalletController() {
                @Override
                public void onSuccess(Object response) {

                    callAPI();
                }

                @Override
                public void onFailed(String errorCode, String reason) {

                    iMovitWalletController.onFailed(errorCode, reason);
                }
            });
        }

    }

    private void callAPI() {

        String xmlParam = null, reqType = null;

        if (isLoginThroughSocial) {
            xmlParam = "\t\t<SocialId>" + SocialID + "</SocialId>\n" +
                    "\t\t<EmailId></EmailId>";

            reqType = "SocialLogin";

        } else {
            xmlParam = "\t\t<MobileNumber>" + MobileNumber + "</MobileNumber>\n" +
                    "\t\t<Mpin>" + Constants.getEncryptedMpin(MPIN) + "</Mpin>\n" +
//                    "\t\t<AppVersion>AND-1<AppVersion>\n"+
                    "\t\t<DeviceId>" + MovitConsumerApp.getInstance().getIMEINo() + "</DeviceId>\n" +
                    "\t\t<DeviceName>" + DeviceName + "</DeviceName>";

            reqType = "Login";

        }

        RetrofitTask.getInstance().executeTask(xmlParam, reqType, new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                Serializer serializer = new Persister();
                try {
                    LoginResponse loginResponse = serializer.read(LoginResponse.class, response, false);

                    if (loginResponse.getResponse().getResponseType().equals("Success")) {

                        //  If KYC Status is "P" then you wont get Wallets in LoginResponse
                        if (loginResponse.getResponseDetails().getWallet() != null) {

                            MovitConsumerApp.getInstance().setWalletIds(loginResponse.getResponseDetails().getWallet().getWalletId());

                            if (!TextUtils.isEmpty(loginResponse.getResponseDetails().getKycStatus())) {
                                if (loginResponse.getResponseDetails().getKycStatus().equalsIgnoreCase("Y")) {
                                    for (int i = 0; i < loginResponse.getResponseDetails().getWallet().getWalletId().size(); i++) {
                                        if (loginResponse.getResponseDetails().getWallet().getWalletId().get(i).isPrimary.equalsIgnoreCase("Y")) {
                                            MovitConsumerApp.getInstance().setWalletBalance(loginResponse.getResponseDetails().getWallet().getWalletId().get(i).getBalance());
                                        }
                                    }
                                }
                            }

                        }


                        MovitConsumerApp.getInstance().setSessionID(loginResponse.getHeader().getSessionId());
                        MovitConsumerApp.getInstance().setMobileNumber(loginResponse.getResponseDetails().getMobileNumber());
                        MovitConsumerApp.getInstance().setKycStatus(loginResponse.getResponseDetails().getKycStatus());
                        MovitConsumerApp.getInstance().setChangeMPIN(loginResponse.getResponseDetails().getChangeMpin());
                        MovitConsumerApp.getInstance().setOtpAmount(loginResponse.getResponseDetails().getOtpAmount());

                        if (!TextUtils.isEmpty(loginResponse.getResponseDetails().getMinTxnAmount()))
                            MovitConsumerApp.getInstance().setMinAmount(loginResponse.getResponseDetails().getMinTxnAmount());
                        else
                            MovitConsumerApp.getInstance().setMinAmount("10");

                        if (!TextUtils.isEmpty(loginResponse.getResponseDetails().getMaxTxnAmount()))
                            MovitConsumerApp.getInstance().setMaxAmount(loginResponse.getResponseDetails().getMaxTxnAmount());
                        else
                            MovitConsumerApp.getInstance().setMaxAmount("10000");

                        AppSettings.putData(context, AppSettings.FIRST_NAME, loginResponse.getResponseDetails().getFirstName());
                        AppSettings.putData(context, AppSettings.LAST_NAME, loginResponse.getResponseDetails().getLastName());
                        AppSettings.putData(context, AppSettings.EMAIL_ID, loginResponse.getResponseDetails().getEmailID());

                        AppSettings.putData(context, AppSettings.ALIAS, loginResponse.getResponseDetails().getAlias());


                        iMovitWalletController.onSuccess(loginResponse);

                    } else {
                        iMovitWalletController.onFailed(loginResponse.getResponseDetails().getErrorCode(), loginResponse.getResponseDetails().getReason());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {

                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }

    public void init(final IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        getKey();
    }
}
