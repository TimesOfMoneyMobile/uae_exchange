package toml.movitwallet.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import toml.movitwallet.apicalls.RetrofitTaskForJSON;
import toml.movitwallet.jsonmodels.GeneralJSONRequest;
import toml.movitwallet.jsonmodels.JsonRequest;
import toml.movitwallet.jsonmodels.ProviderResponse;
import toml.movitwallet.jsonmodels.RequestDetail;
import toml.movitwallet.models.Header;
import toml.movitwallet.models.Request;
import toml.movitwallet.utils.AESecurity;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 9/25/2017.
 */

public class GetProviderController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;
    private RequestDetail requestDetail;
    private GeneralJSONRequest generalJSONRequest;
    private JsonRequest jsonRequest;
    private String finalJSONString;
    private JSONObject jsonObjectRequest;
    private JSONObject jsonObjectResoponce;
    private ProviderResponse providerResponse;

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callGetProviderApi();

    }

    private void callGetProviderApi() {

        RequestBody jsonRequestBody = formJSONRequest();

        RetrofitTaskForJSON.getInstance().executeTaskForJSON(jsonRequestBody,  new RetrofitTaskForJSON.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                handleSuccess(response);

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(Constants.TIME_OUT_CODE,errorMessage);
            }
        });


    }

    public void handleSuccess(String response) {

        try {
            jsonObjectResoponce = new JSONObject(response);


            String status = jsonObjectResoponce.optString(Constants.JSONRESPONSE_STATUS);
            if (status.equalsIgnoreCase("SUCCESS")) {

                String encryptedResponse = jsonObjectResoponce.optString(Constants.JSONRESPONSE_KEY);

                String decryptedResponse = null;

                decryptedResponse = AESecurity.getInstance().decryptString(encryptedResponse.trim());


                Gson gson2 = new Gson();
                Type type = new TypeToken<ProviderResponse>() {
                }.getType();
                providerResponse = gson2.fromJson(decryptedResponse, type);

                iMovitWalletController.onSuccess(providerResponse);


            } else {
                iMovitWalletController.onFailed("Error Code", providerResponse.getErrorCodes().toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private RequestBody formJSONRequest() {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);

        //Set General Whole JSON
        generalJSONRequest = new GeneralJSONRequest();
        generalJSONRequest.setMobileNo(MovitConsumerApp.getInstance().getMobileNumber());
        generalJSONRequest.setSpId(Constants.SERVICE_PROVIDER);
        generalJSONRequest.setUserType(Constants.USER_TYPE);

        //Set Intermediate JSON
        jsonRequest = new JsonRequest();

        //Set Header
        Header header = new Header();
        header.setChannelId(Constants.CHANNEL_ID);
        header.setTimestamp(sdf.format(calendar.getTime()));
        header.setSessionId(MovitConsumerApp.getInstance().getSessionID());
        header.setServiceProvider(Constants.SERVICE_PROVIDER);
        jsonRequest.setHeader(header);

        Request request = new Request();
        request.setUserType(Constants.USER_TYPE);
        request.setMerchantId(Constants.BILLER_MERCHANT);
        request.setMobileNumber(MovitConsumerApp.getInstance().getMobileNumber());
        request.setRequestType(Constants.GET_PROVIDERS);
        request.setPaymodeType(Constants.PAYMODE_TYPE);
        jsonRequest.setRequest(request);

        requestDetail = new RequestDetail();
        requestDetail.setCountryISOCode("AE");
        jsonRequest.setRequestDetail(requestDetail);

        generalJSONRequest.setJsonRequest(jsonRequest);


        Gson gson = new Gson();
        String jsonRequestString = gson.toJson(jsonRequest);
        LogUtils.Verbose("JSON Plain String", jsonRequestString);

        try {
            String encryptedJSON = AESecurity.getInstance().encryptString(jsonRequestString.trim());

            jsonObjectRequest = new JSONObject();
            jsonObjectRequest.put(Constants.MOBILE_NO_KEY, MovitConsumerApp.getInstance().getMobileNumber());
            jsonObjectRequest.put(Constants.USER_TYPE_KEY, Constants.USER_TYPE);
            jsonObjectRequest.put(Constants.SP_ID_KEY, Constants.SERVICE_PROVIDER);
            jsonObjectRequest.put(Constants.JSONREQUEST_KEY, encryptedJSON);

            //Gson gson1 = new Gson();
            finalJSONString = jsonObjectRequest.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        MediaType type = MediaType.parse("application/json; charset=utf-8");
        RequestBody jsonRequestBody = RequestBody.create(type, finalJSONString);
        return jsonRequestBody;

    }
}
