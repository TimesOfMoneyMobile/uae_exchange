package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.NationalityListResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 9/8/2017.
 */

public class GetNationalityListController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;

        getKey();

       /* if (TextUtils.isEmpty(MovitConsumerApp.getInstance().getKey())) {

            getKey();
        } else {
            getNationaliyList();
        }*/
    }


    private void getKey() {

        new GetKeyController().fetchSessionKeyByMobile(MovitConsumerApp.getInstance().getMobileNumber(), new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {


                getNationaliyList();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

            }
        });

    }


    private void getNationaliyList() {

        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n";


        RetrofitTask.getInstance().executeTask(xmlParam, "ListNationality", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                Serializer serializer = new Persister();


                LogUtils.Verbose("TAG", "In ListNationality Success " + response);

                try {
                    NationalityListResponse nationalityListResponse = serializer.read(NationalityListResponse.class, response, false);

                    if (nationalityListResponse.getResponse().getResponseType() != null && nationalityListResponse.getResponse().getResponseType().equalsIgnoreCase("Success")) {
                        //finish();
                        if (nationalityListResponse.getResponseDetails().getNationalities().getNationalityList().size() > 0) {
                            iMovitWalletController.onSuccess(nationalityListResponse.getResponseDetails().getNationalities().getNationalityList());
                        }
                    } else {
                        iMovitWalletController.onFailed(nationalityListResponse.getResponseDetails().getErrorCode(), nationalityListResponse.getResponseDetails().getReason());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }

}
