package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by kunalk on 7/26/2017.
 */

public class BalanceInquiryController implements MovitWalletController {

    private String walletID;


    public BalanceInquiryController(String walletID) {
        this.walletID = walletID;
    }

    @Override
    public void init(final IMovitWalletController iMovitWalletController) {

        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<WalletId>" + walletID + "</WalletId>";


        RetrofitTask.getInstance().executeTask(xmlParam, "BalanceInquiry", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                ArrayList<String> elements = new ArrayList<String>();
                elements.add(IntentConstants.ResponseType);
                elements.add(IntentConstants.Reason);
                elements.add(IntentConstants.ErrorCode);

                elements.add("Balance");
                elements.add("PromotionalBalance");

                Bundle responseMap = GenericResponseHandler.parseElements(response, elements);

                if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                    iMovitWalletController.onSuccess(responseMap);

                } else {

                    iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
                }
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });
    }
}
