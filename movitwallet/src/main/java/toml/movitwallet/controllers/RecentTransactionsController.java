package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.BenificiaryResponse;
import toml.movitwallet.models.TransactionHistoryResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 9/11/2017.
 */

public class RecentTransactionsController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;
    private String txnType;

    public RecentTransactionsController(String txnType) {
        this.txnType = txnType;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();

    }


    private void callApi() {
        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<TxnType>" + txnType + "</TxnType>\n" +
                "\t\t<Tpin>" + "" + "</Tpin>\n" +
                "\t\t<WalletId>" + MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId() + "</WalletId>\n" +
                "\t\t<FromDate>" + "" + "</FromDate>\n" +
                "\t\t<ToDate>" + "" + "</ToDate>\n";


        String ReqType = "TXNHIST";

        RetrofitTask.getInstance().executeTask(xmlParam, ReqType, new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                Serializer serializer = new Persister();

                try {
                    TransactionHistoryResponse transactionHistoryResponse = serializer.read(TransactionHistoryResponse.class, response, false);

                    if (transactionHistoryResponse.getResponse().getResponseType().equals("Success")) {


                        if (txnType == "P2P") {
                            iMovitWalletController.onSuccess(transactionHistoryResponse.getResponseDetails().getTransactions().getTransactionsList());
                        } else {
                            iMovitWalletController.onSuccess(transactionHistoryResponse);
                        }


                    } else {
                        iMovitWalletController.onFailed(transactionHistoryResponse.getResponseDetails().getErrorCode(), transactionHistoryResponse.getResponseDetails().getReason());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }


            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }

}
