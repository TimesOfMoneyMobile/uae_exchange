package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 9/29/2017.
 */

public class AuthorizeSplitBillController implements MovitWalletController {

    // Note - Here TPIN is OTP
    String TxnId, Tpin = "", Status;
    private IMovitWalletController iMovitWalletController;

    public AuthorizeSplitBillController() {
    }

    public AuthorizeSplitBillController(String txnId, String tpin, String status) {
        TxnId = txnId;
        Tpin = tpin;
        Status = status;
    }


    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callAPI();

    }

    private void callAPI() {


        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                " <TxnId>" + TxnId + "</TxnId>\n" +
                "<Tpin>" + Tpin + "</Tpin>\n" +
                " <WalletId>" + MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId() + "</WalletId>\n" +
                " <Status>" + Status + "</Status>";

        RetrofitTask.getInstance().executeTask(xmlParam, "AuthSplitBill", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                ArrayList<String> list = new ArrayList<>();
                list.add(IntentConstants.ResponseType);
                list.add(IntentConstants.ErrorCode);
                list.add(IntentConstants.Reason);
                list.add(IntentConstants.Message);
                list.add(IntentConstants.TXNID);
                list.add(IntentConstants.AMOUNT);
                list.add(IntentConstants.FEE);
                list.add(IntentConstants.BALANCE);

                Bundle responseMap = GenericResponseHandler.parseElements(response, list);
                if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                    iMovitWalletController.onSuccess(responseMap);

                }else{
                    iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(),responseMap.get(IntentConstants.Reason).toString());
                }

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }
}
