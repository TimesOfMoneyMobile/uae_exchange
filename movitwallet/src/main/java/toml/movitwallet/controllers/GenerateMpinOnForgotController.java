package toml.movitwallet.controllers;

import android.os.Bundle;
import android.text.TextUtils;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 9/19/2017.
 */

public class GenerateMpinOnForgotController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;
    private String MobileNumber, SecurityQuestionResult, OTP;

    public GenerateMpinOnForgotController(String mobileNumber, String securityQuestionResult, String OTP) {
        MobileNumber = mobileNumber;
        SecurityQuestionResult = securityQuestionResult;
        this.OTP = OTP;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        //getKey();

        this.iMovitWalletController = iMovitWalletController;

        if (TextUtils.isEmpty(MovitConsumerApp.getInstance().getKey())) {
            getKey();
        } else {
            callGenMpin();
        }
    }

    private void getKey() {

        new GetKeyController().fetchSessionKeyByMobile(MovitConsumerApp.getInstance().getMobileNumber(), new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                callGenMpin();
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                iMovitWalletController.onFailed(errorCode, reason);
            }
        });

    }

    private void callGenMpin() {

        String xmlParam = "\t\t<MobileNumber>" + MobileNumber + "</MobileNumber>\n" +
                "\t\t<OTP>" + OTP + "</OTP>\n" +
                "\t\t<SecurityQuestions>" + SecurityQuestionResult + "</SecurityQuestions>\n";


        RetrofitTask.getInstance().executeTask(xmlParam, "GenMpin", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                ArrayList<String> list = new ArrayList<>();
                list.add(IntentConstants.ResponseType);
                list.add(IntentConstants.ErrorCode);
                list.add(IntentConstants.Reason);
                list.add(IntentConstants.Message);

                Bundle responseMap = GenericResponseHandler.parseElements(response, list);
                if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                    iMovitWalletController.onSuccess(responseMap);

                }else{
                    iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(),responseMap.get(IntentConstants.Reason).toString());
                }

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });
    }


}
