package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.MoneyRequest;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 8/10/2017.
 */

public class AuthoriseRequestMoneyController implements MovitWalletController {

    String strAmount, strTPIN, strStatus;
    MoneyRequest moneyRequest;
    private IMovitWalletController iMovitWalletController;

    public AuthoriseRequestMoneyController(MoneyRequest moneyRequest, String strTPIN, String strAmount, String strStatus) {

        this.strTPIN = strTPIN;
        this.strAmount = strAmount;
        this.moneyRequest = moneyRequest;
        this.strStatus = strStatus;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();

    }

    private void callApi() {


        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                " <TxnId>" + moneyRequest.getId() + "</TxnId>\n" +
                "<Tpin>" + strTPIN + "</Tpin>\n" +
                " <WalletId>" + MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId() + "</WalletId>\n" +
                " <Status>" + strStatus + "</Status>";


        RetrofitTask.getInstance().executeTask(xmlParam, "AuthReqMoney", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                handleSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }

    private void handleSuccess(String response) {
        try {

            ArrayList<String> list = new ArrayList<>();
            list.add(IntentConstants.ResponseType);
            list.add(IntentConstants.ErrorCode);
            list.add(IntentConstants.Reason);
            list.add(IntentConstants.Message);
            list.add(IntentConstants.TXNID);
            list.add(IntentConstants.AMOUNT);
            list.add(IntentConstants.FEE);
            list.add(IntentConstants.BALANCE);
            list.add(IntentConstants.OTP);
            list.add(IntentConstants.PROMOTIONALBALANCE);


            Bundle responseMap = GenericResponseHandler.parseElements(response, list);
            if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                iMovitWalletController.onSuccess(responseMap);

            } else {
                iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
            }


        } catch (Exception e) {
            e.printStackTrace();
            iMovitWalletController.onFailed("", e.getMessage());

        }
    }
}
