package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 11/9/2017.
 */

public class UnmapDeviceController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;
    String DeviceId;


    public UnmapDeviceController(String deviceId) {
        DeviceId = deviceId;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callAPI();
    }

    private void callAPI() {

        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<DeviceId>" + DeviceId + "</DeviceId>\n";

        RetrofitTask.getInstance().executeTask(xmlParam, "UnMappedDevice", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                try {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(IntentConstants.ResponseType);
                    list.add(IntentConstants.ErrorCode);
                    list.add(IntentConstants.Reason);
                    list.add(IntentConstants.Message);


                    Bundle responseMap = GenericResponseHandler.parseElements(response, list);
                    if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                        iMovitWalletController.onSuccess(responseMap);

                    } else {
                        iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }
            }


            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }
}
