package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.Document;
import toml.movitwallet.models.DocumentResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 8/22/2017.
 */

public class GetIdentificationDocsController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;

        getKey();

       /* if (TextUtils.isEmpty(MovitConsumerApp.getInstance().getKey())) {

            getKey();
        } else {
            getIdentificationDocs();
        }*/
    }

    private void getKey() {

        new GetKeyController().fetchSessionKeyByMobile(MovitConsumerApp.getInstance().getMobileNumber(), new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {


                getIdentificationDocs();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

            }
        });

    }

    private void getIdentificationDocs() {
        /*String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<TxnType>" + strTxnType + "</TxnType>";*/


        RetrofitTask.getInstance().executeTask("", "GetIdentificationDocs", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                Serializer serializer = new Persister();

                LogUtils.Verbose("TAG", " In getIdentificationDocs task " + iMovitWalletController.getClass().getCanonicalName());

                LogUtils.Verbose("TAG", "In GetIdentificationDocs Success " + response);
                try {
                    DocumentResponse documentResponse = serializer.read(DocumentResponse.class, response, false);

                    if (documentResponse.getResponse().getResponseType() != null && documentResponse.getResponse().getResponseType().equalsIgnoreCase("Success")) {
                        //finish();
                        if (documentResponse.getResponseDetails().getDocument().getDocuments_list().size() > 0) {

                            for (Document objDocument : documentResponse.getResponseDetails().getDocument().getDocuments_list())
                            {
                                String docname = objDocument.getName();//.replaceAll("_"," ");
                                objDocument.setName(docname);
                            }

                            iMovitWalletController.onSuccess(documentResponse.getResponseDetails().getDocument().getDocuments_list());
                        }
                    } else {
                        iMovitWalletController.onFailed(documentResponse.getResponseDetails().getErrorCode(), documentResponse.getResponseDetails().getReason());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }

}
