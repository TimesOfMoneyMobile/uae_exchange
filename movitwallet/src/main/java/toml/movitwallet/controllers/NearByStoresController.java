package toml.movitwallet.controllers;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 8/11/2017.
 */

public class NearByStoresController implements MovitWalletController {

    String strlatitude, strlongitude;
    private IMovitWalletController iMovitWalletController;

    public NearByStoresController(String latitude, String longitude) {
        this.strlatitude = latitude;
        this.strlongitude = longitude;
    }


    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        getKey(MovitConsumerApp.getInstance().getMobileNumber());

    }

    private void getKey(String MobileNumber){
        new GetKeyController().fetchSessionKeyByMobile(MobileNumber, new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                callApi();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

                iMovitWalletController.onFailed(errorCode, reason);
            }
        });
    }

    private void callApi() {

        String xmlParam = "<Latitude>" + strlatitude + "</Latitude>\n" +
                "\t\t<Longitude>" + strlongitude + "</Longitude>";


        RetrofitTask.getInstance().executeTask(xmlParam, "NearbyStores", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                iMovitWalletController.onSuccess(response);

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }

        });

    }

}
