package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 8/11/2017.
 */

public class ValidateOfferController  implements MovitWalletController{

    String strPromocode;
    String strAmount;
    private IMovitWalletController iMovitWalletController;

    public ValidateOfferController(String strPromocode, String strAmount) {
        this.strPromocode = strPromocode;
        this.strAmount = strAmount;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();
    }

    private void callApi() {
        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<Amount>" + strAmount + "</Amount>\n" +
                "\t\t<PromoCode>" + strPromocode + "</PromoCode>";

        RetrofitTask.getInstance().executeTask(xmlParam, "Offervalid", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                handleSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });
    }

    private void handleSuccess(String response) {

        ArrayList<String> list = new ArrayList<>();
        list.add(IntentConstants.ResponseType);
        list.add(IntentConstants.Message);
        list.add(IntentConstants.ErrorCode);
        list.add(IntentConstants.Reason);

        Bundle responseMap = GenericResponseHandler.parseElements(response, list);
        if (responseMap.get(IntentConstants.ResponseType).equals(IntentConstants.Success)) {

            iMovitWalletController.onSuccess(responseMap);

        }else{
            iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(),responseMap.get(IntentConstants.Reason).toString());
        }

    }
}
