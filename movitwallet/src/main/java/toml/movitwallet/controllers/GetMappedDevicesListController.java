package toml.movitwallet.controllers;

import android.text.TextUtils;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.MappedDevicesListResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 11/7/2017.
 */

public class GetMappedDevicesListController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;

        // getKey();

        if (TextUtils.isEmpty(MovitConsumerApp.getInstance().getKey())) {
            getKey();
        } else {
            getMappedDevicesList();
        }
    }


    private void getKey() {

        new GetKeyController().fetchSessionKeyByMobile(MovitConsumerApp.getInstance().getMobileNumber(), new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {


                getMappedDevicesList();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

            }
        });

    }


    private void getMappedDevicesList() {

        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n";


        RetrofitTask.getInstance().executeTask(xmlParam, "ListMappedDevices", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                Serializer serializer = new Persister();


                LogUtils.Verbose("TAG", "In ListMappedDevices Success " + response);

                try {
                    MappedDevicesListResponse mappedDevicesListResponse = serializer.read(MappedDevicesListResponse.class, response, false);

                    if (mappedDevicesListResponse.getResponse().getResponseType() != null && mappedDevicesListResponse.getResponse().getResponseType().equalsIgnoreCase("Success")) {
                        //finish();
                        if (mappedDevicesListResponse.getResponseDetails().getDevices().getMappedDeviceList().size() > 0) {
                            iMovitWalletController.onSuccess(mappedDevicesListResponse.getResponseDetails().getDevices().getMappedDeviceList());
                        }
                    } else {
                        iMovitWalletController.onFailed(mappedDevicesListResponse.getResponseDetails().getErrorCode(), mappedDevicesListResponse.getResponseDetails().getReason());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }
}
