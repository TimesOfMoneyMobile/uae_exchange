package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.SplitBillRequestReceiver;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 9/29/2017.
 */

public class ManagedSplitBillController implements MovitWalletController {
    private IMovitWalletController iMovitWalletController;
    private String TxnId, TxnStatus = "ACTIVE", Reminder = "N", ReceiverTxnStatus = "CANCELED";
    private List<SplitBillRequestReceiver> receiverList;

    public ManagedSplitBillController(String txnId, List<SplitBillRequestReceiver> receiverList) {
        TxnId = txnId;
        this.receiverList = receiverList;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callAPI();
    }

    private void callAPI() {

        String receiverDetails = "";

        StringBuilder sb = new StringBuilder(receiverDetails);


        for (int i = 0; i < receiverList.size(); i++) {
            SplitBillRequestReceiver objSplitBillRequestReceiver = receiverList.get(i);
            String s = "<Receiver mobileNumber = \"" + objSplitBillRequestReceiver.getMobileNumber() + "\"" + " status = \"" + ReceiverTxnStatus + "\"" + " sendReminder= \"N\" />\n";
            sb.append(s);
        }


        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<TxnId>" + TxnId + "</TxnId>\n" +
                "\t\t<TxnStatus>" + TxnStatus + "</TxnStatus>\n" +
                "\t\t<Reminder>" + Reminder + "</Reminder>\n" +
                "\t\t<ReceiverDetails>\n" + sb.toString() + "</ReceiverDetails>\n" +
                "\t\t<WalletId>" + MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId() + "</WalletId>";

        String ReqType = "ManagedSplitBill";

        RetrofitTask.getInstance().executeTask(xmlParam, ReqType, new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                try {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(IntentConstants.ResponseType);
                    list.add(IntentConstants.ErrorCode);
                    list.add(IntentConstants.Reason);
                    list.add(IntentConstants.Message);

                    Bundle responseMap = GenericResponseHandler.parseElements(response, list);
                    if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                        iMovitWalletController.onSuccess(responseMap);

                    } else {
                        iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }
}
