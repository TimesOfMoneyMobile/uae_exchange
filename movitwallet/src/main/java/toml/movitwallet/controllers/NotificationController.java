package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.AESecurity;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 8/18/2017.
 */

public class NotificationController implements MovitWalletController {

    IMovitWalletController iMovitWalletController;
   private String strDevicetoken;

    public NotificationController(String token) {
        strDevicetoken = token;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();
    }

    private void callApi() {

        String xmlParam = "<DeviceToken>" + strDevicetoken + "</DeviceToken>\n" +
                "\t\t<DeviceOS>AND</DeviceOS>\t" +
                "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\t";

        RetrofitTask.getInstance().executeTask(xmlParam, "SaveUpdateDeviceToken", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                try {
//                    String decrypted = AESecurity.getInstance().decryptString(response);
//
//                    LogUtils.Verbose("SaveUpdateDeviceToken",decrypted);

                    ArrayList<String> list = new ArrayList<>();
                    list.add(IntentConstants.ResponseType);
                    list.add(IntentConstants.ErrorCode);
                    list.add(IntentConstants.Reason);
                    list.add("Message");

                    Bundle responseMap = GenericResponseHandler.parseElements(response, list);

                    if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {
                        iMovitWalletController.onSuccess(responseMap);
                    }else{
                        iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(),responseMap.get(IntentConstants.Reason).toString());
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }
}
