package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.TransactionHistoryResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 8/10/2017.
 */

public class TransactionHistoryPaginationController implements MovitWalletController {
    private IMovitWalletController iMovitWalletController;

    private String startDate;
    private String endDate;
    private String pageNo;
    private String pageSize = "10";

    public TransactionHistoryPaginationController(String startDate, String endDate, String pageNo) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.pageNo = pageNo;
    }


    public TransactionHistoryPaginationController(String startDate, String endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callAPI();
    }

    public void callAPI() {

        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<WalletId>" + MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId() + "</WalletId>\n" +
                "\t\t<FromDate>" + startDate + "</FromDate>\n" +
                "\t\t<ToDate>" + endDate + "</ToDate>\n" +
                "\t\t<PageNo>" + pageNo + "</PageNo>\n" +
                "\t\t<PageSize>" + pageSize + "</PageSize>";
        ;


        RetrofitTask.getInstance().executeTask(xmlParam, "TxnHistPagination", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {


                Serializer serializer = new Persister();
                try {
                    TransactionHistoryResponse transactionHistoryResponse = serializer.read(TransactionHistoryResponse.class, response, false);


                    if (transactionHistoryResponse.getResponse().getResponseType().equals("Success")) {

                        iMovitWalletController.onSuccess(transactionHistoryResponse);

                    } else {
                        iMovitWalletController.onFailed(transactionHistoryResponse.getResponseDetails().getErrorCode(), transactionHistoryResponse.getResponseDetails().getReason());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });


    }
}
