package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 8/8/2017.
 */

public class AskMoneyController implements MovitWalletController {

    String strName,strMobile,strAmount,strTpin;
    private IMovitWalletController iMovitWalletController;

    public AskMoneyController(String strName, String strMobile, String strAmount, String strTpin) {
        this.strName = strName;
        this.strMobile = strMobile;
        this.strAmount=strAmount;
        this.strTpin = strTpin;

    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();

    }

    private void callApi() {
        String xmlParam =  "\t\t<SenderMobileNumber>" + strMobile + "</SenderMobileNumber>\n" +
                "\t\t<ReceiverMobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber()  + "</ReceiverMobileNumber>\n" +
                "\t\t<Amount>" + strAmount + "</Amount>\n" +
                "\t\t<Tpin>" + strTpin + "</Tpin>\n" +
                "\t\t<WalletId>" + MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId() + "</WalletId>\n" +
                "\t\t<NickName>" +strName+ "</NickName>";


        RetrofitTask.getInstance().executeTask(xmlParam, "ReqMoney", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                handleSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }

    private void handleSuccess(String response) {
            try {

                ArrayList<String> list = new ArrayList<>();
                list.add(IntentConstants.ResponseType);
                list.add(IntentConstants.ErrorCode);
                list.add(IntentConstants.Reason);
                list.add(IntentConstants.Message);
                list.add("TxnId");
                list.add(IntentConstants.AMOUNT);
                list.add(IntentConstants.FEE);
                list.add(IntentConstants.BALANCE);
                list.add(IntentConstants.OTP);
                list.add(IntentConstants.PROMOTIONALBALANCE);

                Bundle responseMap = GenericResponseHandler.parseElements(response, list);
                if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                    iMovitWalletController.onSuccess(responseMap);

                }else{
                    iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(),responseMap.get(IntentConstants.Reason).toString());
                }

            }catch (Exception e){
                e.printStackTrace();
                iMovitWalletController.onFailed("", e.getMessage());

            }
    }
}
