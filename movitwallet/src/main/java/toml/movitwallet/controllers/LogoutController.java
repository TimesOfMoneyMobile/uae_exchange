package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 8/18/2017.
 */

public class LogoutController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();

    }

    private void callApi() {
        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>";

        RetrofitTask.getInstance().executeTask(xmlParam, "Logout", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                try {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(IntentConstants.ResponseType);
                    list.add(IntentConstants.ErrorCode);
                    list.add(IntentConstants.Reason);
                    list.add(IntentConstants.Message);


                    Bundle responseMap = GenericResponseHandler.parseElements(response, list);
                    if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                        iMovitWalletController.onSuccess(responseMap);

                    }else{
                        iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(),responseMap.get(IntentConstants.Reason).toString());
                    }


                }catch (Exception e){
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }
}
