package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.BenificiaryResponse;
import toml.movitwallet.models.SecurityQuestionsResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 9/11/2017.
 */

public class FavouritesController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;
    private String txnType;


    public FavouritesController(String txnType) {
        this.txnType = txnType;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();
    }

    private void callApi() {
        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<TxnType>" + txnType + "</TxnType>\n";

        String ReqType = "ListBene";

        RetrofitTask.getInstance().executeTask(xmlParam, ReqType, new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                Serializer serializer = new Persister();

                try {
                    BenificiaryResponse benificiaryResponse = serializer.read(BenificiaryResponse.class, response, false);

                    if (benificiaryResponse.getResponse().getResponseType().equals("Success")) {

                        iMovitWalletController.onSuccess(benificiaryResponse.getResponseDetails().getBeneficiaries().getBenificiaryList());

                        LogUtils.Verbose("SecurityQuestion", "Done");


                    } else {
                        iMovitWalletController.onFailed(benificiaryResponse.getResponseDetails().getErrorCode(), benificiaryResponse.getResponseDetails().getReason());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }


            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }
}
