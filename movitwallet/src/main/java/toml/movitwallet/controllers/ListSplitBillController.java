package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.SplitBillListResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 9/23/2017.
 */

public class ListSplitBillController implements MovitWalletController {
    private IMovitWalletController iMovitWalletController;
    private String reqType = "";

    public ListSplitBillController(String reqType) {
        this.reqType = reqType;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callAPI();
    }

    private void callAPI() {
        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<WalletId>" + MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId() + "</WalletId>";


        //ListSplitBill

        RetrofitTask.getInstance().executeTask(xmlParam, reqType, new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                Serializer serializer = new Persister();

                try {
                    SplitBillListResponse listSplitBillResponse = serializer.read(SplitBillListResponse.class, response, false);

                    if (listSplitBillResponse.getResponse().getResponseType().equals("Success")) {


                        iMovitWalletController.onSuccess(listSplitBillResponse.getResponseDetails().getsplitBillRequests().getSplitBillRequestList());


                    } else {
                        iMovitWalletController.onFailed(listSplitBillResponse.getResponseDetails().getErrorCode(), listSplitBillResponse.getResponseDetails().getReason());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });


    }


}
