package toml.movitwallet.controllers;

import android.os.Bundle;
import android.text.TextUtils;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.RegistrationRequest;
import toml.movitwallet.models.RegistrationResponse;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by kunalk on 7/19/2017.
 */

public class RegistrationController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;
    private String MobileNumber, FirstName, LastName, EmailId, DateOfBirth, ReferralCode, SocialID, SocialProfile, SecurityQuestionResult, OTP;

    private boolean isRegisterThroughSocial = false;

    public RegistrationController(String MobileNumber, String FirstName, String LastName, String EmailId, String DateOfBirth, String ReferralCode) {

        this.MobileNumber = MobileNumber;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.EmailId = EmailId;
        this.DateOfBirth = DateOfBirth;
        this.ReferralCode = ReferralCode;
    }


    public RegistrationController(RegistrationRequest objRegistrationRequest) {
        MobileNumber = objRegistrationRequest.getMobileNumber();
        FirstName = objRegistrationRequest.getFirstName();
        LastName = objRegistrationRequest.getLastName();
        EmailId = objRegistrationRequest.getEmailId();
        DateOfBirth = objRegistrationRequest.getDateOfBirth();
        ReferralCode = objRegistrationRequest.getReferralCode();
        SecurityQuestionResult = objRegistrationRequest.getSecurityQuestionResult();
        this.OTP = objRegistrationRequest.getOTP();
    }

    public void init(IMovitWalletController iMovitWalletController) {

        this.iMovitWalletController = iMovitWalletController;
        if (TextUtils.isEmpty(MovitConsumerApp.getInstance().getKey())) {

            getKey();
        } else {
            callRegWithSeqQue();
        }
    }

    private void getKey() {

        new GetKeyController().fetchSessionKeyByMobile(MobileNumber, new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {


                //callAPI();

                callRegWithSeqQue();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

            }
        });

    }

    private void callAPI() {

        String xmlParam = null, ReqType = null;


        if (!isRegisterThroughSocial) {
            xmlParam = "\t\t<MobileNumber>" + MobileNumber + "</MobileNumber>\n" +
                    "\t\t<FirstName>" + FirstName + "</FirstName>\n" +
                    "\t\t<LastName>" + LastName + "</LastName>\n" +
                    "\t\t<EmailId>" + EmailId + "</EmailId>\n" +
                    "\t\t<DateOfBirth>" + DateOfBirth + "</DateOfBirth>\n" +
                    "\t\t<ReferralCode>" + ReferralCode + "</ReferralCode>";

            ReqType = "CustomerRegistration";

        } else {

            xmlParam = "\t\t<MobileNumber>" + MobileNumber + "</MobileNumber>\n" +
                    "\t\t<FirstName>" + FirstName + "</FirstName>\n" +
                    "\t\t<LastName>" + LastName + "</LastName>\n" +
                    "\t\t<EmailId>" + EmailId + "</EmailId>\n" +
                    "\t\t<DateOfBirth>" + DateOfBirth + "</DateOfBirth>\n" +
                    "\t\t<ReferralCode>" + ReferralCode + "</ReferralCode>\n" +
                    "\t\t<SocialProfile>" + SocialProfile + "</SocialProfile>\n" +
                    "\t\t<SocialId>" + SocialID + "</SocialId>";

            ReqType = "SocialRegistration";

        }

        RetrofitTask.getInstance().executeTask(xmlParam, ReqType, new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                Serializer serializer = new Persister();
                try {
                    RegistrationResponse registrationResponse = serializer.read(RegistrationResponse.class, response, false);

                    if (registrationResponse.getResponse().getResponseType().equals("Success")) {

                        if (isRegisterThroughSocial) {


                            for (toml.movitwallet.models.Wallet.WalletId walletId : registrationResponse.getResponseDetails().getWallet().getWalletId()) {
                                LogUtils.Verbose("TAG", " Wallet ID " + walletId.getWalletId());
                            }

                            MovitConsumerApp.getInstance().setWalletIds(registrationResponse.getResponseDetails().getWallet().getWalletId());
                            MovitConsumerApp.getInstance().setSessionID(registrationResponse.getHeader().getSessionId());

                            MovitConsumerApp.getInstance().setKey(registrationResponse.getResponseDetails().getSecurityKey());
                            MovitConsumerApp.getInstance().setSalt(registrationResponse.getResponseDetails().getSalt());
                            MovitConsumerApp.getInstance().setSecurityIv(registrationResponse.getResponseDetails().getSecurityIv());


                        }


                        iMovitWalletController.onSuccess(registrationResponse.getResponseDetails().getMessage());


                    } else {
                        iMovitWalletController.onFailed(registrationResponse.getResponseDetails().getErrorCode(), registrationResponse.getResponseDetails().getReason());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {

                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });
    }


    private void callRegWithSeqQue() {
        String xmlParam = "\t\t<MobileNumber>" + MobileNumber + "</MobileNumber>\n" +
                "\t\t<FirstName>" + FirstName + "</FirstName>\n" +
                "\t\t<LastName>" + LastName + "</LastName>\n" +
                "\t\t<EmailId>" + EmailId + "</EmailId>\n" +
                "\t\t<DateOfBirth>" + DateOfBirth + "</DateOfBirth>\n" +
                "\t\t<ReferralCode>" + ReferralCode + "</ReferralCode>\n" +
                "\t\t<OTP>" + OTP + "</OTP>\n" +
                "\t\t<SecurityQuestions>" + SecurityQuestionResult + "</SecurityQuestions>\n";


        RetrofitTask.getInstance().executeTask(xmlParam, "CustRegWithSeqQue", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                handleSuccess(response);

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });
    }

    private void handleSuccess(String response) {

        ArrayList<String> list = new ArrayList<>();
        list.add(IntentConstants.ResponseType);
        list.add(IntentConstants.Message);
        list.add(IntentConstants.ErrorCode);
        list.add(IntentConstants.Reason);

        Bundle responseMap = GenericResponseHandler.parseElements(response, list);
        if (responseMap.get(IntentConstants.ResponseType).equals(IntentConstants.Success)) {

            iMovitWalletController.onSuccess(responseMap);

        } else {
            iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
        }

    }
}