package toml.movitwallet.controllers;

import android.os.Bundle;
import android.text.TextUtils;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 8/10/2017.
 */

public class ChangeMpinController implements MovitWalletController {

    private String strCurrentMpin, strNewMpin, strConfirmMpin;
    private IMovitWalletController iMovitWalletController;


    public ChangeMpinController(String strCurrentMpin, String strNewMpin, String strConfirmMpin) {
        this.strCurrentMpin = strCurrentMpin;
        this.strNewMpin = strNewMpin;
        this.strConfirmMpin = strConfirmMpin;
    }


    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        if (TextUtils.isEmpty(MovitConsumerApp.getInstance().getKey())) {

            getKey();
        } else {
            callAPI();
        }

    }

    private void getKey() {
        new GetKeyController().fetchSessionKeyByMobile(MovitConsumerApp.getInstance().getMobileNumber(), new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                callAPI();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

                iMovitWalletController.onFailed(errorCode, reason);
            }
        });
    }


    private void callAPI() {


        String strOldMPINHash = "";

        if (MovitConsumerApp.getInstance().getSalt() != null && !TextUtils.isEmpty(MovitConsumerApp.getInstance().getSalt())) {
            strOldMPINHash = Constants.getHash(strCurrentMpin, MovitConsumerApp.getInstance().getSalt());
        } else {
            strOldMPINHash = Constants.getHash(strCurrentMpin.getBytes());
        }

        String strNewMPINHash = "";

        if (MovitConsumerApp.getInstance().getSalt() != null && !TextUtils.isEmpty(MovitConsumerApp.getInstance().getSalt())) {
            strNewMPINHash = Constants.getHash(strNewMpin, MovitConsumerApp.getInstance().getSalt());
        } else {
            strNewMPINHash = Constants.getHash(strNewMpin.getBytes());
        }


        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<OldMpin>" + strOldMPINHash + "</OldMpin>\n" +
                "\t\t<NewMpin>" + strNewMPINHash + "</NewMpin>";


        RetrofitTask.getInstance().executeTask(xmlParam, "ChangeMpin", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                try {

                    ArrayList<String> list = new ArrayList<>();
                    list.add(IntentConstants.ResponseType);
                    list.add(IntentConstants.ErrorCode);
                    list.add(IntentConstants.Reason);
                    list.add("Message");

                    Bundle responseMap = GenericResponseHandler.parseElements(response, list);


                    if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {
                        iMovitWalletController.onSuccess(responseMap);
                    } else {
                        iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                    iMovitWalletController.onFailed("", e.getMessage());
                }

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }


}
