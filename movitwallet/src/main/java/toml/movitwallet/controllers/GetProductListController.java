package toml.movitwallet.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

import okhttp3.RequestBody;
import toml.movitwallet.apicalls.RetrofitTaskForJSON;
import toml.movitwallet.jsonmodels.ProductListResponse;
import toml.movitwallet.jsonmodels.ProviderResponse;
import toml.movitwallet.jsonmodels.RequestDetail;
import toml.movitwallet.utils.AESecurity;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 9/25/2017.
 */

public class GetProductListController implements MovitWalletController {


    private IMovitWalletController iMovitWalletController;
    private RequestDetail requestDetail;
    private String requestType;
    private JSONObject jsonObjectResoponce;
    private ProductListResponse productListResponse;

    public GetProductListController(String requestType, String accountNo, String countryISOCode, String providerCode) {

        requestDetail = new RequestDetail();
        requestDetail.setProviderCode(providerCode);
        requestDetail.setCountryISOCode(countryISOCode);
        requestDetail.setAccountNumber(accountNo);
        this.requestType = requestType;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callGetProductList();


    }

    private void callGetProductList() {

        RequestBody jsonRequestBody = Constants.getJSONObjectBiller(requestType,requestDetail);

        RetrofitTaskForJSON.getInstance().executeTaskForJSON(jsonRequestBody,  new RetrofitTaskForJSON.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                try {
                    jsonObjectResoponce = new JSONObject(response);
                    String status = jsonObjectResoponce.optString(Constants.JSONRESPONSE_STATUS);
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        handleSuccess(response);
                    }
                    else
                    {
                        iMovitWalletController.onFailed("Error Code","Oops! Something went wrong. Please try after some time.");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
             //   iMovitWalletController.onFailed("Error Code", productListResponse.getErrorCodes().toString());
                iMovitWalletController.onFailed(Constants.TIME_OUT_CODE,errorMessage);
            }
        });

    }

    private void handleSuccess(String response) {

        try {
            jsonObjectResoponce = new JSONObject(response);


            String status = jsonObjectResoponce.optString(Constants.JSONRESPONSE_STATUS);
            if (status.equalsIgnoreCase("SUCCESS")) {

                String encryptedResponse = jsonObjectResoponce.optString(Constants.JSONRESPONSE_KEY);

                String decryptedResponse = null;

                decryptedResponse = AESecurity.getInstance().decryptString(encryptedResponse.trim());


                Gson gson2 = new Gson();
                Type type = new TypeToken<ProductListResponse>() {
                }.getType();
                productListResponse = gson2.fromJson(decryptedResponse, type);

                iMovitWalletController.onSuccess(productListResponse);


            } else {
                iMovitWalletController.onFailed("Error Code", productListResponse.getErrorCodes().toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
