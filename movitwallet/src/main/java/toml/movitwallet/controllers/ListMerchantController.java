package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.ListMerchantsResponse;
import toml.movitwallet.models.TransactionHistoryResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 10/26/2017.
 */

public class ListMerchantController implements MovitWalletController {

    String strMerchantType;
    private IMovitWalletController iMovitWalletController;

    public ListMerchantController(String strMerchantType)
    {
        this.strMerchantType = strMerchantType;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callListMerchantAPI();
        
    }

    private void callListMerchantAPI() {

        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<MerchantType>" + strMerchantType + "</MerchantType>";

        RetrofitTask.getInstance().executeTask(xmlParam, "ListMerchants", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                Serializer serializer = new Persister();

                try {
                    ListMerchantsResponse listMerchantsResponse = serializer.read(ListMerchantsResponse.class, response, false);

                    if (listMerchantsResponse.getResponse().getResponseType().equals("Success")) {

                        iMovitWalletController.onSuccess(listMerchantsResponse);

                    } else {
                        iMovitWalletController.onFailed(listMerchantsResponse.getResponseDetails().getErrorCode(), listMerchantsResponse.getResponseDetails().getReason());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());
                }
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });
    }
}
