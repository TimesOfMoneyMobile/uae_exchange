package toml.movitwallet.controllers;

import android.os.Bundle;
import android.text.TextUtils;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 8/17/2017.
 */

public class ValidateOTPController implements MovitWalletController {

    String strTxnType, strOTP;

    private IMovitWalletController iMovitWalletController;

    public ValidateOTPController(String strTxnType, String strOTP) {
        this.strTxnType = strTxnType;
        this.strOTP = strOTP;
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;

        if (TextUtils.isEmpty(MovitConsumerApp.getInstance().getKey())) {

            getKey();
        }
        else
        {
            callValidateOTP();
        }


    }


    private void getKey() {

        new GetKeyController().fetchSessionKeyByMobile(MovitConsumerApp.getInstance().getMobileNumber(), new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {


                callValidateOTP();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

            }
        });

    }


    private void callValidateOTP() {
        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<TxnType>" + strTxnType + "</TxnType>\n"+
                "\t\t<OTP>" + strOTP + "</OTP>";;


        RetrofitTask.getInstance().executeTask(xmlParam, "ValidateOTP", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                handleSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });

    }

    private void handleSuccess(String response) {

        ArrayList<String> list = new ArrayList<>();
        list.add(IntentConstants.ResponseType);
        list.add(IntentConstants.Message);
        list.add(IntentConstants.ErrorCode);
        list.add(IntentConstants.Reason);

        Bundle responseMap = GenericResponseHandler.parseElements(response, list);
        if (responseMap.get(IntentConstants.ResponseType).equals(IntentConstants.Success)) {

            iMovitWalletController.onSuccess(responseMap);

        } else {
            iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
        }

    }
}
