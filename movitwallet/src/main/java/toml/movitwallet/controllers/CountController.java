package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 8/10/2017.
 */

public class CountController implements MovitWalletController {
    private IMovitWalletController iMovitWalletController;

    public CountController() {
    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callAPI();

    }

    private void callAPI() {

        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>";

        RetrofitTask.getInstance().executeTask(xmlParam, "PendingRequestCount", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                ArrayList<String> list = new ArrayList<>();
                list.add(IntentConstants.ResponseType);
                list.add(IntentConstants.ErrorCode);
                list.add(IntentConstants.Reason);
                list.add(IntentConstants.PendingRequest);
                list.add(IntentConstants.OffersCount);

                Bundle responseMap = GenericResponseHandler.parseElements(response, list);
                if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                    iMovitWalletController.onSuccess(responseMap);

                } else {
                    iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
                }
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {

            }
        });

    }
}
