package toml.movitwallet.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

import okhttp3.RequestBody;
import toml.movitwallet.apicalls.RetrofitTaskForJSON;
import toml.movitwallet.jsonmodels.RechargeSuccessResponseFromWallet;
import toml.movitwallet.jsonmodels.RechargeSuccessResponsePG;
import toml.movitwallet.jsonmodels.RequestDetail;
import toml.movitwallet.utils.AESecurity;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 9/25/2017.
 */

public class BookRechargeRequestCotroller implements MovitWalletController {


    private IMovitWalletController iMovitWalletController;
    private RequestDetail requestDetail;
    private String requestType;
    private JSONObject jsonObjectResoponce;
    String SkuCode, SendValue, SendCurrencyIso, DistributorRef;
    boolean ValidateOnly;
    RechargeSuccessResponseFromWallet rechargeSuccessResponseFromWallet;
    RechargeSuccessResponsePG rechargeSuccessResponseFromPG;


    public BookRechargeRequestCotroller(String requestType, String SkuCode, String SendValue, String SendCurrencyIso, String accountNumber, String DistributorRef, boolean ValidateOnly, String otpValue, String promocode) {
        requestDetail = new RequestDetail();
        requestDetail.setAccountNumber(Constants.COUNTRY_CODE + accountNumber);
        requestDetail.setSendCurrencyIso(SendCurrencyIso);
        requestDetail.setSendValue(SendValue);
        requestDetail.setSkuCode(SkuCode);
        requestDetail.setDistributorRef(DistributorRef);
        requestDetail.setValidateOnly("true");
        requestDetail.setOTP(otpValue);
        requestDetail.setPromoCode(promocode);
        this.requestType = requestType;


    }


    @Override
    public void init(IMovitWalletController iMovitWalletController) {

        this.iMovitWalletController = iMovitWalletController;
        callBookRequest();

    }

    private void callBookRequest() {

        RequestBody jsonRequestBody = Constants.getJSONObjectBiller(requestType, requestDetail);

        RetrofitTaskForJSON.getInstance().executeBookRequest(jsonRequestBody, new RetrofitTaskForJSON.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                try {
                    jsonObjectResoponce = new JSONObject(response);
                    String status = jsonObjectResoponce.optString(Constants.JSONRESPONSE_STATUS);
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        handleSuccess(response);
                    } else {

                        iMovitWalletController.onFailed(jsonObjectResoponce.optString(Constants.JSON_RESPONSE_CODE), jsonObjectResoponce.optString(Constants.JSON_MESSAGE_RESPONSE));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed("Error Code", errorMessage);
            }
        });
    }

    private void handleSuccess(String response) {

        try {
            jsonObjectResoponce = new JSONObject(response);


            String status = jsonObjectResoponce.optString(Constants.JSONRESPONSE_STATUS);
            if (status.equalsIgnoreCase("SUCCESS")) {

                String encryptedResponse = jsonObjectResoponce.optString(Constants.JSONRESPONSE_KEY);

                String decryptedResponse = null;

                decryptedResponse = AESecurity.getInstance().decryptString(encryptedResponse.trim());


                Gson gson2 = new Gson();
                if (Constants.PAYMODE_TYPE.equalsIgnoreCase("W2W")) {
                    Type type = new TypeToken<RechargeSuccessResponseFromWallet>() {
                    }.getType();
                    rechargeSuccessResponseFromWallet = gson2.fromJson(decryptedResponse, type);

                    iMovitWalletController.onSuccess(rechargeSuccessResponseFromWallet);
                } else {
                    Type type = new TypeToken<RechargeSuccessResponsePG>() {
                    }.getType();
                    rechargeSuccessResponseFromPG = gson2.fromJson(decryptedResponse, type);

                    iMovitWalletController.onSuccess(rechargeSuccessResponseFromPG);

                }


            } else {
                iMovitWalletController.onFailed("Error Code", rechargeSuccessResponseFromWallet.getErrorCodes().toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
