package toml.movitwallet.controllers;

import android.os.Bundle;

import java.util.ArrayList;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 9/25/2017.
 */

public class GetP2MTransactionFee implements MovitWalletController {

    String strMerchantId, strAmount, strPromoCode;
    private IMovitWalletController iMovitWalletController;


    public GetP2MTransactionFee(String strMerchantId, String strAmount, String strPromoCode) {
        this.strMerchantId = strMerchantId;
        this.strAmount = strAmount;
        this.strPromoCode = strPromoCode;

    }

    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();
    }

    private void callApi() {
        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<MerchantId>" + strMerchantId + "</MerchantId>\n" +
                "\t\t<Amount>" + strAmount + "</Amount>\n" +
                "\t\t<PromoCode>" + strPromoCode + "</PromoCode>";


        RetrofitTask.getInstance().executeTask(xmlParam, "P2MFees", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                try {


                    ArrayList<String> list = new ArrayList<>();
                    list.add(IntentConstants.ResponseType);
                    list.add(IntentConstants.ErrorCode);
                    list.add(IntentConstants.Reason);
                    list.add("MerchantName");
                    list.add("TxnFees");
                    list.add("Discount");
                    list.add("DiscountOn");


                    Bundle responseMap = GenericResponseHandler.parseElements(response, list);
                    if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                        iMovitWalletController.onSuccess(responseMap);

                    } else {
                        if (responseMap.containsKey(IntentConstants.ErrorCode)) {
                            iMovitWalletController.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
                        } else {
                            iMovitWalletController.onFailed("", responseMap.get(IntentConstants.Reason).toString());
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    iMovitWalletController.onFailed("", e.getMessage());

                }


            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });


    }
}
