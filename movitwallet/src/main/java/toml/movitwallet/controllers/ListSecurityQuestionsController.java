package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.SecurityQuestionsResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by pankajp on 8/16/2017.
 */

public class ListSecurityQuestionsController implements MovitWalletController {

    private IMovitWalletController iMovitWalletController;
    private String MobileNumber, ListMode;

    public ListSecurityQuestionsController(String mobileNumber, String listMode) {
        ListMode = listMode;
    }

    public void init(IMovitWalletController iMovitWalletController) {

        this.iMovitWalletController = iMovitWalletController;
        getKey();
    }

    private void getKey() {

        new GetKeyController().fetchSessionKeyByMobile(MovitConsumerApp.getInstance().getMobileNumber(), new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {


                callSecurityQuestionsAPI();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

            }
        });

    }


    private void callSecurityQuestionsAPI() {


        String xmlParam = "\t\t<MobileNumber>" + MovitConsumerApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<ListMode>" + ListMode + "</ListMode>\n";

        String ReqType = "ListSecurityQuestions";

        RetrofitTask.getInstance().executeTask(xmlParam, ReqType, new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {

                Serializer serializer = new Persister();

                try {
                    SecurityQuestionsResponse securityQuestionsResponse = serializer.read(SecurityQuestionsResponse.class, response, false);

                    if (securityQuestionsResponse.getResponse().getResponseType().equals("Success")) {

                        iMovitWalletController.onSuccess(securityQuestionsResponse.getResponseDetails().getObjSecurityQuestionList().getQuestionsList());

                        LogUtils.Verbose("SecurityQuestion", "Done");


                    } else {
                        iMovitWalletController.onFailed(securityQuestionsResponse.getResponseDetails().getErrorCode(), securityQuestionsResponse.getResponseDetails().getReason());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {

            }
        });

    }
}
