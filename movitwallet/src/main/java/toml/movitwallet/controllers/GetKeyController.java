package toml.movitwallet.controllers;

import android.os.Bundle;
import android.text.TextUtils;

import java.util.ArrayList;

import toml.movitwallet.BuildConfig;
import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;


/**
 * Created by kunalk on 8/3/17.
 */

public class GetKeyController implements RetrofitTask.IRetrofitTask {

    private IMovitWalletController controller;

    public void fetchSessionKeyByMobile(String mobilenumber, final IMovitWalletController iMovitmVISAController) {

        this.controller = iMovitmVISAController;

        MovitConsumerApp.getInstance().setMobileNumber(mobilenumber);

        String XMLParam = "\t\t<MobileNumber>" + mobilenumber + "</MobileNumber>\n" +
                "<SocialId></SocialId>\n" +
                "\t\t<AppVersion>A-" + BuildConfig.VERSION_NAME + "</AppVersion>";
        RetrofitTask.getInstance().executeTask(XMLParam, "GetKey", this);

    }

    public void fetchSessionKeyBySocial(String SocialID, final IMovitWalletController iMovitmVISAController) {

        this.controller = iMovitmVISAController;
        String XMLParam = "\t\t<MobileNumber></MobileNumber>\n" +
                "<SocialId>" + SocialID + "</SocialId>\n" +
                "\t\t<AppVersion>A-" + BuildConfig.VERSION_NAME + "</AppVersion>";
        RetrofitTask.getInstance().executeTask(XMLParam, "GetKey", this);

    }

    @Override
    public void onSuccess(String response) {


        try {
            ArrayList<String> list = new ArrayList<>();
            list.add(IntentConstants.ResponseType);
            list.add(IntentConstants.ErrorCode);
            list.add(IntentConstants.Reason);
            list.add("SecurityKey");
            list.add("salt");
            list.add("SecurityIv");
            list.add("MobileNumber");


            Bundle responseMap = GenericResponseHandler.parseElements(response, list);

            if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                MovitConsumerApp.getInstance().setKey(responseMap.get("SecurityKey").toString());
                MovitConsumerApp.getInstance().setSalt(responseMap.get("salt").toString());
                MovitConsumerApp.getInstance().setSecurityIv(responseMap.get("SecurityIv").toString());

                if (responseMap.get("MobileNumber") != null && !TextUtils.isEmpty(responseMap.get("MobileNumber").toString()))
                    MovitConsumerApp.getInstance().setMobileNumber(responseMap.get("MobileNumber").toString());

                controller.onSuccess(response);
            } else {

                controller.onFailed(responseMap.get(IntentConstants.ErrorCode).toString(), responseMap.get(IntentConstants.Reason).toString());
            }

        } catch (Exception e) {
            e.printStackTrace();

            controller.onFailed(null, null);
        }
    }

    @Override
    public void onFailed(String errorCode, String errorMessage) {

        controller.onFailed(errorCode, errorMessage);
    }
}