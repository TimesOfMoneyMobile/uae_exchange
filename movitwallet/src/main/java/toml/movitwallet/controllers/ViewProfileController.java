package toml.movitwallet.controllers;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.ViewProfileResponse;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by mayurn on 8/17/2017.
 */

public class ViewProfileController implements MovitWalletController {

    String strMoileNo;
    private IMovitWalletController iMovitWalletController;

    public ViewProfileController(String strMobileNo)
    {
        this.strMoileNo = strMobileNo;
    }
    @Override
    public void init(IMovitWalletController iMovitWalletController) {
        this.iMovitWalletController = iMovitWalletController;
        callApi();
    }

    private void callApi() {
        String xmlParam = "\t\t<MobileNumber>"+ strMoileNo + "</MobileNumber>";

        RetrofitTask.getInstance().executeTask(xmlParam, "ViewProfile", new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {
                handleSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                iMovitWalletController.onFailed(errorCode, errorMessage);
            }
        });
    }

    private void handleSuccess(String response) {

        Serializer serializer = new Persister();
        try {
            ViewProfileResponse viewProfileResponse = serializer.read(ViewProfileResponse.class, response, false);
            iMovitWalletController.onSuccess(viewProfileResponse);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
