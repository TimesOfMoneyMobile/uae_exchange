package toml.movitwallet.models;

/**
 * Created by pankajp on 9/28/2017.
 */

public class ContactsItem extends ContactListItem {

    private ContactsModel objContactsModel;

    public ContactsModel getContactsModel() {
        return objContactsModel;
    }

    public void setContactsModel(ContactsModel objContactsModel) {
        this.objContactsModel = objContactsModel;
    }

    @Override
    public int getType() {
        return ContactListItem.NORMAL;
    }
}
