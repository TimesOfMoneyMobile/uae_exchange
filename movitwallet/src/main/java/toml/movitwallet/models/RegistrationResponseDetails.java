package toml.movitwallet.models;

import org.simpleframework.xml.Element;

/**
 * Created by kunalk on 7/20/2017.
 */

public class RegistrationResponseDetails {

    @Element(required = false)
    String ErrorCode;
    @Element(required = false)
    String Reason;
    @Element(required = false)
    String FirstName;
    @Element(required = false)
    String LastName;
    @Element(required = false)
    String DateOfBirth;
    @Element(required = false)
    String Message;
    @Element(required = false)
    String SecurityKey;

    public String getSecurityKey() {
        return SecurityKey;
    }

    public void setSecurityKey(String securityKey) {
        SecurityKey = securityKey;
    }

    public String getSecurityIv() {
        return SecurityIv;
    }

    public void setSecurityIv(String securityIv) {
        SecurityIv = securityIv;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Element(required = false)
    String SecurityIv;
    @Element(required = false)
    String salt;


    @Element(required = false, name = "Wallets")
    Wallet wallet;

    public String getReason() {
        return Reason;
    }

    public String getLastName() {
        return LastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public String getMessage() {
        return Message;
    }
}
