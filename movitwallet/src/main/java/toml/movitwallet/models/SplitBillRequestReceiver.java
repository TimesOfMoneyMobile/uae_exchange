package toml.movitwallet.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;

import toml.movitwallet.utils.Constants;

/**
 * Created by pankajp on 9/25/2017.
 */

public class SplitBillRequestReceiver implements Parcelable {
    @Attribute(required = false)
    String mobileNumber, splittedAmount = "0.00", status, firstName = "", lastName = "";

    public SplitBillRequestReceiver() {
    }

    public static final Creator<SplitBillRequestReceiver> CREATOR = new Creator<SplitBillRequestReceiver>() {
        @Override
        public SplitBillRequestReceiver createFromParcel(Parcel in) {
            return new SplitBillRequestReceiver(in);
        }

        @Override
        public SplitBillRequestReceiver[] newArray(int size) {
            return new SplitBillRequestReceiver[size];
        }
    };

    protected SplitBillRequestReceiver(Parcel in) {
        mobileNumber = in.readString();
        splittedAmount = in.readString();
        status = in.readString();
        firstName = in.readString();
        lastName = in.readString();


    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mobileNumber);
        parcel.writeString(splittedAmount);
        parcel.writeString(status);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
    }


    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = Constants.getOnlyDigits(mobileNumber);
    }

    public String getSplittedAmount() {
        return splittedAmount;
    }

    public void setSplittedAmount(String splittedAmount) {
        this.splittedAmount = splittedAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
