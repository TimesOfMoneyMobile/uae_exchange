package toml.movitwallet.models;

import android.graphics.Bitmap;
import android.text.TextUtils;

import toml.movitwallet.utils.Constants;

/**
 * Created by vishwanathp on 9/14/2017.
 */

public class ContactsModel {

    String name, number;
    Bitmap image;
    boolean isChecked;


    public String getName() {

        return !TextUtils.isEmpty(name.trim()) ? name : getNumber();
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getNumber() {
        return Constants.getLastNineDigitPhoneNumber(number);
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactsModel that = (ContactsModel) o;

        return number.equals(that.number);

    }

    @Override
    public int hashCode() {
        return number.hashCode();
    }
}
