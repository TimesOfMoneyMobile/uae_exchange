package toml.movitwallet.models;

import org.simpleframework.xml.Element;

/**
 * Created by mayurn on 8/17/2017.
 */

public class ViewProfileResponse {

    @Element(name="Header")
    Header header;

    @Element (name="Request")
    Request request;

    @Element (name="Response")
    Response response;

    @Element (name="ResponseDetails")
    ViewProfileDetails viewProfileDetails;

    public Header getHeader() {
        return header;
    }

    public Request getRequest() {
        return request;
    }

    public Response getResponse() {
        return response;
    }

    public ViewProfileDetails getViewProfileDetails() {
        return viewProfileDetails;
    }


}
