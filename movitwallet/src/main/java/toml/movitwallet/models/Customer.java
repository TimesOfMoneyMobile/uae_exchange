package toml.movitwallet.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;

/**
 * Created by vishwanathp on 8/10/2017.
 */

public class Customer  implements Parcelable {

    @Element(required = false)
    String FirstName, LastName, MobileNumber, CardNumber, SenderCardNumber;

    public Customer() {
    }

    protected Customer(Parcel in) {
        FirstName = in.readString();
        LastName = in.readString();
        MobileNumber = in.readString();
        CardNumber = in.readString();
        SenderCardNumber = in.readString();

    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        if (LastName == null)
            return "";

        return LastName;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public String getCardNumber() {
        return CardNumber;
    }

    public String getSenderCardNumber() {
        return SenderCardNumber;
    }

    @Override
    public String toString() {
        return getMobileNumber();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(FirstName);
        dest.writeString(LastName);
        dest.writeString(MobileNumber);
        dest.writeString(CardNumber);
        dest.writeString(SenderCardNumber);
    }

}