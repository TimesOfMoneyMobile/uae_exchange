package toml.movitwallet.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;

import toml.movitwallet.utils.Constants;

/**
 * Created by vishwanathp on 9/11/2017.
 */

public class Merchant implements Parcelable {

    @Element(required = false)
    private String FirstName,LastName,MobileNumber,CardNumber,SenderCardNumber,MerchantId;

    public Merchant(){}
    public Merchant(Parcel in) {
        FirstName = in.readString();
        LastName = in.readString();
        MobileNumber = in.readString();
        CardNumber = in.readString();
        SenderCardNumber = in.readString();
        MerchantId = in.readString();


    }

    public static final Creator<Merchant> CREATOR = new Creator<Merchant>() {
        @Override
        public Merchant createFromParcel(Parcel in) {
            return new Merchant(in);
        }

        @Override
        public Merchant[] newArray(int size) {
            return new Merchant[size];
        }
    };

    public String getFirstName() {

        if(FirstName==null || FirstName.isEmpty() || FirstName.equalsIgnoreCase("null"))
            return "";

        return Constants.decodeString(FirstName);
    }

    public String getLastName() {
        if(LastName==null || LastName.isEmpty() || LastName.equalsIgnoreCase("null"))
            return "";
        return Constants.decodeString(LastName);
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public String getSenderCardNumber() {
        return SenderCardNumber;
    }

    public String getCardNumber() {
        return CardNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getMerchantId() {
        return MerchantId;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(FirstName);
        dest.writeString(LastName);
        dest.writeString(MobileNumber);
        dest.writeString(CardNumber);
        dest.writeString(SenderCardNumber);
        dest.writeString(MerchantId);

    }
}
