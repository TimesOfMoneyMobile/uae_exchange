package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by pankajp on 9/23/2017.
 */
@Root(name="MpsXml")
public class SplitBillListResponse {
    @Element(name="Header")
    Header header;

    @Element (name="Request")
    Request request;

    @Element (name="Response")
    Response response;

    @Element (name="ResponseDetails")
    SplitBillListResponseDetails responseDetails;


    public Header getHeader() {
        return header;
    }

    public Response getResponse() { return response; }

    public SplitBillListResponseDetails getResponseDetails() {
        return responseDetails;
    }
}
