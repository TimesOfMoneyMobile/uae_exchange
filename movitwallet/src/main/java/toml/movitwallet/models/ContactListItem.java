package toml.movitwallet.models;

/**
 * Created by pankajp on 9/28/2017.
 */

public abstract class ContactListItem {
    public static final int HEADER = 0;
    public static final int NORMAL = 1;

    abstract public int getType();
}
