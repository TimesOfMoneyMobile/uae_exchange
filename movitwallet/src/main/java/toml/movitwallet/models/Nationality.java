package toml.movitwallet.models;

import org.simpleframework.xml.Attribute;

/**
 * Created by pankajp on 9/8/2017.
 */

public class Nationality {

    @Attribute
    String id, name;

    public Nationality() {

    }

    public Nationality(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }


}
