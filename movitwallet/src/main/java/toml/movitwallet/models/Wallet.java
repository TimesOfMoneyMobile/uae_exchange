package toml.movitwallet.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Text;

import java.util.List;

public class Wallet {

        @ElementList(entry = "Wallet", inline = true)
        List<Wallet.WalletId> WalletId;

        public List<Wallet.WalletId> getWalletId() {
            return WalletId;
        }


        public void setWalletId(List<Wallet.WalletId> walletId) {
            WalletId = walletId;
        }


        public static class WalletId {
            @Attribute(required = false)
            public String AccountType, isPrimary, balance,cardNumber,cardHolderName,walletId;


            public void setBalance(String balance) {
                this.balance = balance;
            }

            public String getAccountType() {
                return AccountType;
            }

            public String getIsPrimary() {
                return isPrimary;
            }

            public String getBalance() {
                return balance;
            }

            public String getWalletId() {
                return walletId;
            }

            public String getCardNumber() {
                return cardNumber;
            }

            public String getCardHolderName() {
                return cardHolderName;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                WalletId walletId1 = (WalletId) o;

                return walletId != null ? walletId.equals(walletId1.walletId) : walletId1.walletId == null;

            }

            @Override
            public int hashCode() {
                return walletId != null ? walletId.hashCode() : 0;
            }
        }
    }