package toml.movitwallet.models;

import org.simpleframework.xml.Attribute;

/**
 * Created by mayurn on 9/7/2017.
 */

public class KYCDocument {

    @Attribute
    String id,name,value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
