package toml.movitwallet.models;


import android.app.LauncherActivity;
import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

/**
 * Created by vishwanathp on 8/10/2017.
 */

public class Transaction extends LauncherActivity.ListItem implements Parcelable {

    @Attribute(required = false)
    String id,txnType,amount,type,txnDate,remark,fee,total;

    @Element(name = "OtherDetails",required = false)
    OtherDetail otherDetail;



    private boolean isSectionHeader;

    public Transaction(){}

    protected Transaction(Parcel in) {
        id = in.readString();
        txnType = in.readString();
        amount = in.readString();
        type = in.readString();
        txnDate = in.readString();
        remark = in.readString();
        fee = in.readString();
        total = in.readString();

        otherDetail=in.readParcelable(OtherDetail.class.getClassLoader());
    }


    public boolean isSectionHeader() {
        return isSectionHeader;
    }

    public void setSectionHeader(boolean sectionHeader) {
        isSectionHeader = sectionHeader;
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getTxnType() {
        return txnType;
    }

    public String getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public OtherDetail getOtherDetail() {
        return otherDetail;
    }

    public String getFee() {
        return fee;
    }

    public String getTotal() {
        return total;
    }

    public String getRemark() {
        return remark;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(txnType);
        dest.writeString(amount);
        dest.writeString(type);
        dest.writeString(txnDate);
        dest.writeString(remark);
        dest.writeString(fee);
        dest.writeString(total);
        dest.writeParcelable(otherDetail,1);
    }

    public static class OtherDetail implements Parcelable
    {
        @Element(name = "Customer",required = false)
        public Customer customer;

        @Element(name = "Merchant",required = false)
        public Merchant merchant;



        @Element(name = "Agent",required = false)
        public Agent agent;
//
//        @Element(name = "OriginalTransaction",required = false)
//        public OriginalTransaction originalTransaction;

        protected OtherDetail(Parcel in) {

            customer=in.readParcelable(Customer.class.getClassLoader());
            merchant=in.readParcelable(Merchant.class.getClassLoader());
            agent = in.readParcelable(Agent.class.getClassLoader());

        }

        public OtherDetail(){}

        public static final Creator<OtherDetail> CREATOR = new Creator<OtherDetail>() {
            @Override
            public OtherDetail createFromParcel(Parcel in) {
                return new OtherDetail(in);
            }

            @Override
            public OtherDetail[] newArray(int size) {
                return new OtherDetail[size];
            }
        };

        public Customer getCustomer() {
            return customer;
        }

        public Merchant getMerchant() {
            return merchant;
        }

        public Agent getAgent() { return agent; }

//        public OriginalTransaction getOriginalTransaction() {
//            return originalTransaction;
//        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {

            dest.writeParcelable(customer,1);
            dest.writeParcelable(merchant,1);
            dest.writeParcelable(agent,1);

        }
    }
}
