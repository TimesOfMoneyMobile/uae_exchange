package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by kunalk on 7/20/2017.
 */
@Root(name="MpsXml")
public class RegistrationResponse {

    @Element(name="Header")
    Header header;

    @Element (name="Request")
    Request request;

    @Element (name="Response")
    Response response;

    @Element (name="ResponseDetails")
    RegistrationResponseDetails responseDetails;

    public Response getResponse() {
        return response;
    }

    public RegistrationResponseDetails getResponseDetails() {
        return responseDetails;
    }

    public Header getHeader() {
        return header;
    }
}
