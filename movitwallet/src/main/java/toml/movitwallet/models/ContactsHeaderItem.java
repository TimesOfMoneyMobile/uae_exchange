package toml.movitwallet.models;

/**
 * Created by pankajp on 9/28/2017.
 */

public class ContactsHeaderItem extends ContactListItem {

    private String header;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {

        this.header = header;
    }

    @Override
    public int getType() {
        return ContactListItem.HEADER;
    }
}
