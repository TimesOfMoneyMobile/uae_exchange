package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by kunalk on 8/11/2016.
 */
public class PendingReqResponseDetails {

    @Element(required = false)
    String ErrorCode, Reason;

    @Element(name="MoneyRequests", required = false)
    MoneyRequests moneyRequests;

    public String getErrorCode() {
        return ErrorCode;
    }

    public MoneyRequests getMoneyRequests() {
        return moneyRequests;
    }

    public String getReason() {
        return Reason;
    }

    public static class MoneyRequests
    {
        @ElementList(entry="MoneyRequest", inline = true,required = false)
        List<MoneyRequest> moneyRequestList;

        public List<MoneyRequest> getMoneyRequestList() {
            return moneyRequestList;
        }
    }
}
