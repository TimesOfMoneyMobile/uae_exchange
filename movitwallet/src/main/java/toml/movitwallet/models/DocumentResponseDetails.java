package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;


/**
 * Created by kunalk on 3/10/2016.
 */
public class DocumentResponseDetails {


    @Element(required = false)
    String ErrorCode, Reason;

    @Element(name = "Documents", required = false)
    Documents documents;

    public Documents getDocument() {
        return documents;
    }

    public String getReason() {
        return Reason;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(String errorCode) {
        ErrorCode = errorCode;
    }

    public static class Documents {
        @ElementList(entry = "Document", inline = true, required = false)
        List<Document> documents_list;

        public List<Document> getDocuments_list() {
            return documents_list;
        }
    }
}
