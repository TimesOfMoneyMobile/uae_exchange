package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by pankajp on 9/8/2017.
 */
@Root(name = "MpsXml")
public class NationalityListResponse {
    @Element(name = "Header")
    Header header;

    @Element(name = "Request")
    Request request;

    @Element(name = "Response")
    Response response;

    @Element(name = "ResponseDetails")
    NationalityListResponseDetails responseDetails;

    public Response getResponse() {
        return response;
    }

    public NationalityListResponseDetails getResponseDetails() {
        return responseDetails;
    }

    public Header getHeader() {
        return header;
    }
}
