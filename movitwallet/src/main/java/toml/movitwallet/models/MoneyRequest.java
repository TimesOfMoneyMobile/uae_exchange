package toml.movitwallet.models;

import org.simpleframework.xml.Attribute;

import java.io.Serializable;



/**
 * Created by kunalk on 8/11/2016.
 */
public class MoneyRequest implements Serializable {
    @Attribute
    String id;
    @Attribute
    String amount;
    @Attribute
    String mobileNumber;

    @Attribute
    String firstName;
    @Attribute
    String lastName;
    @Attribute
    String txnDate;

    public String getId() {
        return id;
    }

    public String getAmount() {
        return amount;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getTxnDate() {
        return txnDate;
    }

}
