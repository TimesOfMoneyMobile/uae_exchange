package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by pankajp on 8/16/2017.
 */
@Root(name = "MpsXml")
public class SecurityQuestionsResponse {


    @Element(name = "Header")
    Header header;

    @Element(name = "Request")
    Request request;

    @Element(name = "Response")
    Response response;

    @Element(name = "ResponseDetails")
    SecurityQuestionsResponseDetails responseDetails;

    public Response getResponse() {
        return response;
    }

    public SecurityQuestionsResponseDetails getResponseDetails() {
        return responseDetails;
    }

    public Header getHeader() {
        return header;
    }


}
