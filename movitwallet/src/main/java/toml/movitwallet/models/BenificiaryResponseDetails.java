package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by kunalk on 4/19/2016.
 */
public class BenificiaryResponseDetails {

    @Element(required = false)
    String ErrorCode,Reason;

    @Element(required = false,name="Beneficiaries")
    Beneficiaries beneficiaries;

    public String getErrorCode() {
        return ErrorCode;
    }

    public String getReason() {
        return Reason;
    }

    public Beneficiaries getBeneficiaries() {
        return beneficiaries;
    }

    public static class Beneficiaries
    {
        @ElementList(entry="Beneficiary", inline = true,required = false)
        List<Beneficiary> benificiaryList;

        public List<Beneficiary> getBenificiaryList() {
            return benificiaryList;
        }
    }
}
