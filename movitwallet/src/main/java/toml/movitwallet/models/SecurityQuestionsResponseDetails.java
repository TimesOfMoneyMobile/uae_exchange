package toml.movitwallet.models;

import org.simpleframework.xml.Element;

/**
 * Created by pankajp on 8/16/2017.
 */

public class SecurityQuestionsResponseDetails {



    @Element(name = "SecurityQuestions", required = false)
    SecurityQuestionList objSecurityQuestionList;

    @Element(required = false)
    String ErrorCode;
    @Element(required = false)
    String Reason;
    @Element(name = "Message", required = false)
    String Message;

    public SecurityQuestionList getObjSecurityQuestionList() {
        return objSecurityQuestionList;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public String getReason() {
        return Reason;
    }

    public String getMessage() {
        return Message;
    }
}
