package toml.movitwallet.models;

import org.simpleframework.xml.Element;

/**
 * Created by vishwanathp on 8/1/2017.
 */

class ForgotTpinResponseDetails {

    @Element(required = false)
    String ErrorCode, Reason, FirstName, LastName, DateOfBirth, Message;


    @Element(required = false, name = "Wallets")
    Wallet wallet;

    public String getReason() {
        return Reason;
    }

    public String getLastName() {
        return LastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public String getMessage() {
        return Message;
    }



}
