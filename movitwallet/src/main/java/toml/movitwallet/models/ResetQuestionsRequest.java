package toml.movitwallet.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pankajp on 8/17/2017.
 */

public class ResetQuestionsRequest implements Parcelable {


    String MobileNumber = "";
    String DateOfBirth = "";
    String EmiratesID = "";
    String EmailId = "";
    String KYCDocumentsResult = "";
    String SecurityQuestionResult = "";


    public ResetQuestionsRequest() {
    }

    public ResetQuestionsRequest(Parcel in) {

        try {
            MobileNumber = in.readString();
            DateOfBirth = in.readString();
            EmiratesID = in.readString();
            EmailId = in.readString();
            KYCDocumentsResult = in.readString();
            SecurityQuestionResult = in.readString();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public String getEmiratesID() {
        return EmiratesID;
    }

    public void setEmiratesID(String emiratesID) {
        EmiratesID = emiratesID;
    }

    public String getKYCDocumentsResult() {
        return KYCDocumentsResult;
    }

    public void setKYCDocumentsResult(String KYCDocumentsResult) {
        this.KYCDocumentsResult = KYCDocumentsResult;
    }


    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }


    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }


    public String getSecurityQuestionResult() {
        return SecurityQuestionResult;
    }

    public void setSecurityQuestionResult(String securityQuestionResult) {
        SecurityQuestionResult = securityQuestionResult;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        try {
            parcel.writeString(MobileNumber);
            parcel.writeString(DateOfBirth);
            parcel.writeString(EmiratesID);
            parcel.writeString(EmailId);
            parcel.writeString(KYCDocumentsResult);
            parcel.writeString(SecurityQuestionResult);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static final Creator CREATOR = new Creator() {
        public ResetQuestionsRequest createFromParcel(Parcel in) {
            return new ResetQuestionsRequest(in);
        }

        public ResetQuestionsRequest[] newArray(int size) {
            return new ResetQuestionsRequest[size];
        }
    };

}
