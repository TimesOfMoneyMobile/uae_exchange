package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by kunalk on 4/19/2016.
 */
@Root(name="MpsXml")
public class BenificiaryResponse {

    @Element(name="Header")
    Header header;

    @Element(name="Request")
    Request request;

    @Element(name="Response")
    Response response;

    @Element(name="ResponseDetails")
    BenificiaryResponseDetails responseDetails;

    public Response getResponse() {
        return response;
    }

    public BenificiaryResponseDetails getResponseDetails() {
        return responseDetails;
    }

    public Header getHeader() {
        return header;
    }
}
