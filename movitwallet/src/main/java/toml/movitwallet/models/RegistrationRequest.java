package toml.movitwallet.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pankajp on 8/17/2017.
 */

public class RegistrationRequest implements Parcelable {

    // NON KYC params
    String MobileNumber = "";
    String EmailId = "";
    String FirstName = "";
    String LastName = "";
    String ReferralCode = "";
    String OTP = "";
    String SecurityQuestionResult = "";
    String Alias = "";

    // KYC params
    String EmiratesID = "", Nationality = "", DateOfBirth = "";


    public RegistrationRequest() {
    }

    public RegistrationRequest(Parcel in) {

        try {
            MobileNumber = in.readString();
            EmailId = in.readString();
            FirstName = in.readString();
            LastName = in.readString();
            DateOfBirth = in.readString();
            ReferralCode = in.readString();
            OTP = in.readString();
            SecurityQuestionResult = in.readString();
            EmiratesID = in.readString();
            Nationality = in.readString();
            Alias = in.readString();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public String getEmiratesID() {
        return EmiratesID;
    }

    public void setEmiratesID(String emiratesID) {
        EmiratesID = emiratesID;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getReferralCode() {
        return ReferralCode;
    }

    public void setReferralCode(String referralCode) {
        ReferralCode = referralCode;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }

    public String getSecurityQuestionResult() {
        return SecurityQuestionResult;
    }

    public void setSecurityQuestionResult(String securityQuestionResult) {
        SecurityQuestionResult = securityQuestionResult;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        try {
            parcel.writeString(MobileNumber);
            parcel.writeString(EmailId);
            parcel.writeString(FirstName);
            parcel.writeString(LastName);
            parcel.writeString(DateOfBirth);
            parcel.writeString(ReferralCode);
            parcel.writeString(OTP);
            parcel.writeString(SecurityQuestionResult);
            parcel.writeString(EmiratesID);
            parcel.writeString(Nationality);
            parcel.writeString(Alias);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static final Creator CREATOR = new Creator() {
        public RegistrationRequest createFromParcel(Parcel in) {
            return new RegistrationRequest(in);
        }

        public RegistrationRequest[] newArray(int size) {
            return new RegistrationRequest[size];
        }
    };

}
