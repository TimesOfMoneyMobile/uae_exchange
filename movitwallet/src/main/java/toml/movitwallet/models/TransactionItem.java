package toml.movitwallet.models;

/**
 * Created by pankajp on 9/23/2017.
 */

public class TransactionItem extends ListItem {

    private Transaction objTransaction;

    public Transaction getTransaction() {
        return objTransaction;
    }

    public void setTransaction(Transaction objTransaction) {
        this.objTransaction = objTransaction;
    }

    @Override
    public int getType() {
        return TYPE_GENERAL;
    }
}
