package toml.movitwallet.models;

import android.util.Log;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Text;


import java.util.List;

/**
 * Created by kunalk on 1/29/2016.
 */
public class LoginResponseDetails {

    @Element(required = false)
    String ErrorCode, Reason, FirstName, LastName,Address,MobileNumber,EmailID,ChangeMpin,ChangeTpin,LastLogin,LastTransaction,OneClickFlag,OneClickAmount,KycStatus,Alias,OtpAmount, DateOfBirth, Message,MinTxnAmount,MaxTxnAmount;


    @Element(required = false, name = "Wallets")
    Wallet wallet;

    public String getReason() {
        return Reason;
    }

    public String getAddress() {
        return Address;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public String getEmailID() {
        return EmailID;
    }

    public String getChangeMpin() {
        return ChangeMpin;
    }

    public String getChangeTpin() {
        return ChangeTpin;
    }

    public String getLastLogin() {
        return LastLogin;
    }

    public String getLastTransaction() {
        return LastTransaction;
    }

    public String getOneClickFlag() {
        return OneClickFlag;
    }

    public String getOneClickAmount() {
        return OneClickAmount;
    }

    public String getKycStatus() {
        return KycStatus;
    }

    public String getLastName() {
        return LastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public String getMessage() {
        return Message;
    }


    public String getAlias() {
        return Alias;
    }

    public String getOtpAmount() {
        return OtpAmount;
    }

    public String getMinTxnAmount() {
        return MinTxnAmount;
    }

    public void setMinTxnAmount(String minTxnAmount) {
        MinTxnAmount = minTxnAmount;
    }

    public String getMaxTxnAmount() {
        return MaxTxnAmount;
    }

    public void setMaxTxnAmount(String maxTxnAmount) {
        MaxTxnAmount = maxTxnAmount;
    }


}