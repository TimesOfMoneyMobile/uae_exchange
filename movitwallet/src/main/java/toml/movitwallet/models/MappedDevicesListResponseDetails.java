package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by pankajp on 9/8/2017.
 */

public class MappedDevicesListResponseDetails {
    @Element(required = false)
    String ErrorCode, Reason;

    @Element(name = "Devices", required = false)
    Devices devices;

    public Devices getDevices() {
        return devices;
    }

    public String getReason() {
        return Reason;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(String errorCode) {
        ErrorCode = errorCode;
    }

    public static class Devices {
        @ElementList(entry = "Device", inline = true, required = false)
        List<MappedDevice> mappedDeviceList;

        public List<MappedDevice> getMappedDeviceList() {
            return mappedDeviceList;
        }
    }
}
