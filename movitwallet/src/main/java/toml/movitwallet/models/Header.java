package toml.movitwallet.models;

import org.simpleframework.xml.Element;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import toml.movitwallet.utils.MovitConsumerApp;


/**
 * Created by kunalk on 1/29/2016.
 */
public class Header {


    @Element(required = false)
    String ChannelId="App",Timestamp="TestTime",SessionId="Test",ServiceProvider="mercury";

    public Header()
    {
        Calendar calendar=Calendar.getInstance();
        SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

        Timestamp=sdf.format(calendar.getTime());

        SessionId= MovitConsumerApp.getInstance().getSessionID();

        ServiceProvider = "mercury";
    }

    public String getSessionId() {
        return SessionId;
    }

    public void setChannelId(String channelId) {
        ChannelId = channelId;
    }

    public void setTimestamp(String timestamp) {
        Timestamp = timestamp;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public void setServiceProvider(String serviceProvider) {
        ServiceProvider = serviceProvider;
    }
}