package toml.movitwallet.models;

import org.simpleframework.xml.Attribute;

/**
 * Created by kunalk on 4/19/2016.
 */
public class Beneficiary {

    @Attribute(required = false)
    private String id,nickname,type,value,PAN;

    public String getId() {
        return id;
    }

    public String getNickname() {
        //This has been change since name is already decoded from API
        return nickname;
         //    return Constants.decodeString(nickname);
       /* byte[] data = Base64.decode(nickname, Base64.DEFAULT);
        String nicknameDecoded = new String(data);
        return nicknameDecoded;*/
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public String getPAN() {
        return PAN;
    }
}
