package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by vishwanathp on 8/10/2017.
 */

public class TransactionHistoryResponseDetails {

    @Element(required = false)
    String ErrorCode, Reason,TotalRecords,Balance;



    @Element(name="Transactions",required = false)
    Transactions transactions;

    public Transactions getTransactions() {
        return transactions;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public String getReason() {
        return Reason;
    }

    public String getTotalRecords() {
        return TotalRecords;
    }

    public String getBalance() {
        return Balance;
    }

    public static class Transactions
    {
        @ElementList(entry="Transaction", inline = true,required = false)
        List<Transaction> transactionsList;

        public List<Transaction> getTransactionsList() {
            return transactionsList;
        }
    }




}
