package toml.movitwallet.models;

/**
 * Created by pankajp on 9/23/2017.
 */

public class DateHeaderItem extends ListItem {

    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int getType() {
        return TYPE_DATE;
    }
}
