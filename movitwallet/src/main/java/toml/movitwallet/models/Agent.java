package toml.movitwallet.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;

/**
 * Created by vishwanathp on 9/27/2017.
 */

public class Agent implements Parcelable {



    @Element(required = false)
    private String FirstName, LastName, MobileNumber, CardNumber;

    public Agent() {
    }

    public Agent(Parcel in) {
        FirstName = in.readString();
        LastName = in.readString();
        MobileNumber = in.readString();
        CardNumber = in.readString();

    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        if (LastName == null)
            return "";

        return LastName;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public String getCardNumber() {
        return CardNumber;
    }


    public static final Creator<Agent> CREATOR = new Creator<Agent>() {
        @Override
        public Agent createFromParcel(Parcel in) {
            return new Agent(in);
        }

        @Override
        public Agent[] newArray(int size) {
            return new Agent[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(FirstName);
        parcel.writeString(LastName);
        parcel.writeString(MobileNumber);
        parcel.writeString(CardNumber);
    }
}
