package toml.movitwallet.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by pankajp on 9/23/2017.
 */

public class SplitBillRequest implements Parcelable {
    @Attribute(required = false)
    String id;
    @Attribute(required = false)
    String amount;
    @Attribute(required = false)
    String mobileNumber;
    @Attribute(required = false)
    String firstName;
    @Attribute(required = false)
    String lastName;
    @Attribute(required = false)
    String txnDate;
    @Attribute(required = false)
    String status;

    public List<SplitBillRequestReceiver> getListReceivers() {
        return listReceivers;
    }

    public void setListReceivers(List<SplitBillRequestReceiver> listReceivers) {
        this.listReceivers = listReceivers;
    }

    @ElementList(entry = "Receiver", inline = true, required = false)
    List<SplitBillRequestReceiver> listReceivers;

    //@Element(name = "Receiver", required = false)
    SplitBillRequestReceiver splitBillRequests;


    public SplitBillRequest() {

    }


    protected SplitBillRequest(Parcel in) {
        id = in.readString();
        amount = in.readString();
        txnDate = in.readString();
        mobileNumber = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        status = in.readString();
        splitBillRequests = in.readParcelable(SplitBillRequestReceiver.class.getClassLoader());


    }


    public static final Creator<SplitBillRequest> CREATOR = new Creator<SplitBillRequest>() {
        @Override
        public SplitBillRequest createFromParcel(Parcel in) {
            return new SplitBillRequest(in);
        }

        @Override
        public SplitBillRequest[] newArray(int size) {
            return new SplitBillRequest[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(amount);
        parcel.writeString(txnDate);
        parcel.writeString(mobileNumber);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(status);
        parcel.writeParcelable(splitBillRequests, i);

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public SplitBillRequestReceiver getSplitBillRequestReceiver() {
        return splitBillRequests;
    }

    public void setSplitBillRequestReceiver(SplitBillRequestReceiver splitBillRequests) {
        this.splitBillRequests = splitBillRequests;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
