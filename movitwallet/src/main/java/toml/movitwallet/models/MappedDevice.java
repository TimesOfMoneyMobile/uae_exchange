package toml.movitwallet.models;

import org.simpleframework.xml.Attribute;

/**
 * Created by pankajp on 11/7/2017.
 */

public class MappedDevice {
    @Attribute
    String id, name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MappedDevice() {

    }
}
