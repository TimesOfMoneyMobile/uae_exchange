package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by mayurn on 8/17/2017.
 */

public class ViewProfileDetails {


    @Element(required = false)
    String ErrorCode;
    @Element(required = false)
    String Reason;
    @Element(required = false)
    String Message;
    @Element(required = false)
    String MobileNumber;
    @Element(required = false)
    String EmailID;
    @Element(required = false)
    String FirstName;
    @Element(required = false)
    String LastName;
    @Element(required = false)
    String DateOfBirth;
    @Element(required = false)
    String Gender;
    @Element(required = false)
    String Address1;
    @Element(required = false)
    String Address2;
    @Element(required = false)
    String City;
    @Element(required = false)
    String KycStatus;

    @Element(required = false)
    String Alias;
    @Element(required = false)
    String Nationality;
    @Element(required = false)
    String EmirateId;


    @Element(name = "KYCDocuments",required = false)
    KYCDocumentList kycDocuments;




    public static class KYCDocumentList {
        @ElementList(entry = "Document", inline = true, required = false)
        List<KYCDocument> kycDocumentList;

        public List<KYCDocument> getkycDocumentList() {
            return kycDocumentList;
        }
    }


    public KYCDocumentList getKycDocuments() {
        return kycDocuments;
    }


    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getEmiratesId() {
        return EmirateId;
    }

    public void setEmiratesId(String emiratesId) {
        EmirateId = emiratesId;
    }



    public String getErrorCode() {
        return ErrorCode;
    }

    public String getReason() {
        return Reason;
    }

    public String getMessage() {
        return Message;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public String getEmailId() {
        return EmailID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public String getGender() {
        return Gender;
    }

    public String getAddress1() {
        return Address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public String getCity() {
        return City;
    }

    public String getKycStatus() {
        return KycStatus;
    }
}
