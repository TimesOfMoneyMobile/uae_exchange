package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by kunalk on 8/11/2016.
 */

@Root(name="MpsXml")
public class PendingRequest {

    @Element(name="Header")
    Header header;

    @Element(name="Request")
    Request request;

    @Element(name="Response")
    Response response;

    @Element(name = "ResponseDetails")
    PendingReqResponseDetails responseDetails;

    public PendingRequest(){}

    public PendingRequest(String requestType, PendingReqResponseDetails responseDetails)
    {
        this.header=new Header();
        this.request=new Request(requestType);
        this.responseDetails=responseDetails;
    }

    public PendingReqResponseDetails getResponseDetails() {
        return responseDetails;
    }

    public Response getResponse() {
        return response;
    }
}
