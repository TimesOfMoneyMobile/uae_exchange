package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by pankajp on 9/8/2017.
 */

public class NationalityListResponseDetails {
    @Element(required = false)
    String ErrorCode, Reason;

    @Element(name = "Nationalities", required = false)
    Nationalities nationalities;

    public Nationalities getNationalities() {
        return nationalities;
    }

    public String getReason() {
        return Reason;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(String errorCode) {
        ErrorCode = errorCode;
    }

    public static class Nationalities {
        @ElementList(entry = "Nationality", inline = true, required = false)
        List<Nationality> nationalityList;

        public List<Nationality> getNationalityList() {
            return nationalityList;
        }
    }
}
