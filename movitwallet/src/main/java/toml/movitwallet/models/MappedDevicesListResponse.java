package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by pankajp on 9/8/2017.
 */
@Root(name = "MpsXml")
public class MappedDevicesListResponse {
    @Element(name = "Header")
    Header header;

    @Element(name = "Request")
    Request request;

    @Element(name = "Response")
    Response response;

    @Element(name = "ResponseDetails")
    MappedDevicesListResponseDetails responseDetails;

    public Response getResponse() {
        return response;
    }

    public MappedDevicesListResponseDetails getResponseDetails() {
        return responseDetails;
    }

    public Header getHeader() {
        return header;
    }
}
