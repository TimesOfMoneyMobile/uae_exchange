package toml.movitwallet.models;

import org.simpleframework.xml.Element;

/**
 * Created by kunalk on 1/29/2016.
 */
public class Request {


    @Element
    String RequestType="Test",UserType="AG";
    String MerchantId="381", MobileNumber,PaymodeType;

    public Request(){}
    public Request(String requestType)
    {
        this.RequestType=requestType;
    }

    public void setRequestType(String requestType) {
        RequestType = requestType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public void setMerchantId(String merchantId) {
        MerchantId = merchantId;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public void setPaymodeType(String paymodeType) {
        PaymodeType = paymodeType;
    }

}