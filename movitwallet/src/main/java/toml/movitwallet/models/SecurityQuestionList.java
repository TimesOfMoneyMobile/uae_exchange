package toml.movitwallet.models;

import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by pankajp on 8/16/2017.
 */

public class SecurityQuestionList {

    @ElementList(entry = "SecurityQuestion", inline = true, required = false)
    List<SecurityQuestion> listSecurityQuestions;


    public List<SecurityQuestion> getQuestionsList() {
        return listSecurityQuestions;
    }

}
