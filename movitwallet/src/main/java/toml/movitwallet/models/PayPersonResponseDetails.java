package toml.movitwallet.models;

import org.simpleframework.xml.Element;

/**
 * Created by vishwanathp on 8/2/2017.
 */

class PayPersonResponseDetails {
    @Element(required = false)
    String ErrorCode, Reason, Message, TxnId, Amount, Fee,Balance,OTP,PromotionalBalance;


    public String getErrorCode() {
        return ErrorCode;
    }

    public String getReason() {
        return Reason;
    }

    public String getMessage() {
        return Message;
    }

    public String getTxnId() {
        return TxnId;
    }

    public String getAmount() {
        return Amount;
    }

    public String getFee() {
        return Fee;
    }

    public String getBalance() {
        return Balance;
    }

    public String getOTP() {
        return OTP;
    }

    public String getPromotionalBalance() {
        return PromotionalBalance;
    }



}
