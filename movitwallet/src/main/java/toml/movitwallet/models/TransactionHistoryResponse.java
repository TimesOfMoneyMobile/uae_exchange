package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by vishwanathp on 8/10/2017.
 */
@Root(name="MpsXml")
public class TransactionHistoryResponse {

    @Element(name="Header")
    Header header;

    @Element (name="Request")
    Request request;

    @Element (name="Response")
    Response response;

    @Element (name="ResponseDetails")
    TransactionHistoryResponseDetails responseDetails;


    public Header getHeader() {
        return header;
    }

    public Response getResponse() { return response; }

    public TransactionHistoryResponseDetails getResponseDetails() {
        return responseDetails;
    }



}
