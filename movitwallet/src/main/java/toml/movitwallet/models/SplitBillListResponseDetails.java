package toml.movitwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by pankajp on 9/23/2017.
 */

public class SplitBillListResponseDetails {

    @Element(required = false)
    String ErrorCode, Reason,TotalRecords,Balance;

    @Element(name="SplitBillRequests",required = false)
    SplitBillRequests splitBillRequests;

    public SplitBillRequests getsplitBillRequests() {
        return splitBillRequests;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public String getReason() {
        return Reason;
    }

    public String getTotalRecords() {
        return TotalRecords;
    }

    public String getBalance() {
        return Balance;
    }

    public static class SplitBillRequests
    {
        @ElementList(entry="SplitBillRequest", inline = true,required = false)
        List<SplitBillRequest> splitBillRequestList;

        public List<SplitBillRequest> getSplitBillRequestList() {
            return splitBillRequestList;
        }
    }


}


