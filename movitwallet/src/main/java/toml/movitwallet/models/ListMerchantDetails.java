package toml.movitwallet.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by mayurn on 10/26/2017.
 */

public class ListMerchantDetails {



    @Element(required = false)
    String ErrorCode, Reason;



    @Element(name="Merchants",required = false)
    ListMerchantDetails.Merchants merchants;

    public String getErrorCode() {
        return ErrorCode;
    }

    public String getReason() {
        return Reason;
    }

    public Merchants getMerchants() {
        return merchants;
    }

    public static class Merchants
    {
        @ElementList(entry="Merchant", inline = true,required = false)
        List<MerchantItem> merchantItemList;

        public List<MerchantItem> getMerchantItemList() {
            return merchantItemList;
        }
    }

    public static class MerchantItem {

        @Attribute(required = false)
        String id,externalId,name;

        public String getId() {
            return id;
        }

        public String getExternalId() {
            return externalId;
        }

        public String getName() {
            return name;
        }
    }
}
