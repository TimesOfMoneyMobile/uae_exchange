package toml.uaeexchange.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.TextKeyListener;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;

import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;


/**
 * Created by kunalk on 8/9/2017.
 */

public class NumericInputField extends LinearLayout {


    int TYPE = 1, LENGTH = 4, DIGITS = 1, iEditTextMargin, iEditTextPadding;
    int edtxtBgIamge, rootLayoutBgImage;
    private ArrayList<EditText> editTexts;
    ArrayList<String> pinList;
    private String PASSWORD_PLACEHOLDER = "A";

    public NumericInputField(@NonNull Context context) {
        super(context);

        init();
    }


    public NumericInputField(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.NumericInputField);
        TYPE = t.getInt(R.styleable.NumericInputField_type, 1);
        LENGTH = t.getInt(R.styleable.NumericInputField_length, 4);
        DIGITS = t.getInt(R.styleable.NumericInputField_digits, 1);
        iEditTextMargin = t.getInt(R.styleable.NumericInputField_edittextmargin, 0);
        edtxtBgIamge = t.getResourceId(R.styleable.NumericInputField_edittextbg, 0);


        //rootLayoutBgImage = t.getDrawable(R.styleable.NumericInputField_rootlayoutbg);
        init();

    }


    private void init() {

        editTexts = new ArrayList<>();
        for (int i = 0; i < LENGTH; i++) {
            EditText edit = createEditText(getContext());
            edit.setTag("edit" + i);
            edit.setLongClickable(false);

            edit.setCursorVisible(false);
            editTexts.add(edit);

            this.addView(edit);
        }


        for (int i = 0; i < LENGTH; i++) {

            EditText edtCurr = editTexts.get(i);
            EditText edtPrev = null;
            if (i > 0)
                edtPrev = editTexts.get(i - 1);
            EditText edtNext = null;

            if (i < LENGTH - 1)
                edtNext = editTexts.get(i + 1);


            if (TYPE == 1)
                edtCurr.addTextChangedListener(new NumericTextWatcher(edtCurr, edtPrev, edtNext));
            else
                edtCurr.addTextChangedListener(new NumericTextWatcher(edtCurr, edtPrev, edtNext, i));

        }

    }


    class NumericTextWatcher implements TextWatcher {

        EditText edtCurr, edtNext, edtPrev;

        int index;

        public NumericTextWatcher(EditText ediCurr, EditText edtPrev, EditText edtNext) {

            this.edtCurr = ediCurr;
            this.edtPrev = edtPrev;
            this.edtNext = edtNext;

        }

        public NumericTextWatcher(EditText ediCurr, EditText edtPrev, EditText edtNext, int index) {

            this.edtCurr = ediCurr;
            this.edtPrev = edtPrev;
            this.edtNext = edtNext;

            this.index = index;
            pinList = new ArrayList<>();

            int totalLength = LENGTH * DIGITS;
            for (int i = 0; i < totalLength; i++)
                pinList.add(PASSWORD_PLACEHOLDER);

        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence s, int i, int i1, int i2) {


//            if(TextUtils.isEmpty(s.toString().trim()))
//                return;

            if (s.toString().length() == DIGITS && edtNext != null) {
                edtNext.setEnabled(true);
                edtNext.requestFocus();
            }


            if (s.toString().length() <= 0 && edtPrev != null) {

                if (!edtPrev.isEnabled())
                    edtPrev.setEnabled(true);

                edtPrev.requestFocus();
                edtPrev.setSelection(edtPrev.getText().toString().length());


            }

            if (!TextUtils.isEmpty(s.toString())) {
                edtCurr.setBackgroundResource(R.drawable.otp_view_border_selected);

            } else {

                edtCurr.setBackgroundResource(R.drawable.otp_view_border);
            }


            if (TYPE == 2 && edtCurr != null && !TextUtils.isEmpty(edtCurr.getText().toString())) {


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        edtCurr.removeTextChangedListener(NumericTextWatcher.this);
                        pinList.set(index, edtCurr.getText().toString());
                        edtCurr.setText(edtCurr.getContext().getString(R.string.bullet));
                        edtCurr.addTextChangedListener(NumericTextWatcher.this);
                        edtCurr.setSelection(edtCurr.getText().length());


                    }
                }, 400);
            } else if (TextUtils.isEmpty(edtCurr.getText().toString()) && pinList != null) {
                pinList.set(index, PASSWORD_PLACEHOLDER);
            }








            if (edtNext == null && (s.toString().length() == DIGITS))
                Constants.hideKeyboard(edtCurr.getContext());
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    /**
     * This method is used to clear text of all edit texts.
     */
    public void clearText() {


      /*  int totalLength = LENGTH * DIGITS;

        String input = fillString(totalLength, " ");

        int factor = input.length() / DIGITS;

        String[] subString = new String[factor];

        int index = 0, offset = DIGITS;

        for (int i = 0; i < factor; i++) {
            subString[i] = input.substring(index, offset);
            index = offset;
            offset = offset + DIGITS;
        }*/

      /*  for (int i = editTexts.size()-1; i >= 0; i--) {
            editTexts.get(i).setText(subString[i]);
        }*/

        for (int i = 0; i < editTexts.size(); i++) {

           // editTexts.get(i).addTextChangedListener(null);
            editTexts.get(i).setText("");


        }

        editTexts.get(0).requestFocus();


    }

    public static String fillString(int count, String character) {
        if (count == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder(count);
        for (int i = 0; i < count; i++) {
            sb.append(character);
        }
        return sb.toString();
    }

    public void setText(String input) throws Exception {

        int totalLength = LENGTH * DIGITS;

        if (input.length() != totalLength) {
            throw new Exception("Invalid input length!!!");
        }


        int factor = input.length() / DIGITS;

        String[] subString = new String[factor];

        int index = 0, offset = DIGITS;

        for (int i = 0; i < factor; i++) {
            subString[i] = input.substring(index, offset);
            index = offset;
            offset = offset + DIGITS;
        }

        for (int i = 0; i < editTexts.size(); i++) {

            editTexts.get(i).setText(subString[i]);
            editTexts.get(i).setSelection(DIGITS);

        }

    }


    public String getText() {
        StringBuilder str = new StringBuilder();

        for (EditText edt : editTexts)
            str.append(edt.getText().toString());

        return str.toString();
    }


    public String getPasswordText() {
        if (pinList == null)
            return "";

        StringBuilder ss = new StringBuilder();

        for (String s : pinList)
            ss.append(s);

        return ss.toString();
    }


    public void setEditTextMaxLength(EditText editText, int length) {
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(length);
        editText.setFilters(filterArray);
    }

    private EditText createEditText(Context context) {
        final EditText editText = new EditText(context);

        editText.setKeyListener(TextKeyListener.getInstance());

        float density = getContext().getResources().getDisplayMetrics().density;

        // Calcualate edit text width and height
        int editTextWidthDp = (int) (45 * density);
        int editTextHeightDp = (int) (45 * density);

        // Set edit text width and height
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(editTextWidthDp, editTextHeightDp);
        params.gravity = Gravity.CENTER_VERTICAL;


        editText.setBackgroundResource(edtxtBgIamge);

        // Conversion to support diff densities

        int value = (int) (iEditTextMargin * density); // 5;
        params.setMargins(value, value, value, value);

        editText.setLayoutParams(params);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);

        editText.setGravity(Gravity.CENTER);

        setEditTextMaxLength(editText, DIGITS);


        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    String currentValue = editText.getText().toString();
                    if (currentValue.length() <= 0 && editText.getSelectionStart() <= 0) {

                        if (editText.focusSearch(View.FOCUS_LEFT) != null) {
                            editText.focusSearch(View.FOCUS_LEFT).requestFocus();
                        }

                    }
                }

                return false;
            }
        });


        return editText;

    }
}
