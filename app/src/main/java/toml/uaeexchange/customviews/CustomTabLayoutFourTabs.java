package toml.uaeexchange.customviews;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import toml.uaeexchange.R;

/**
 * Created by kunalk on 4/5/2016.
 */
public class CustomTabLayoutFourTabs implements View.OnClickListener {


    View root_layout, tab_layout, view_one, view_two, view_three, view_four;
    RelativeLayout tab_one, tab_two, tab_three, tab_four;

    TextView tab_one_name, tab_two_name, tab_three_name, tab_four_name;
    private IOnTabClicked callback;


    public CustomTabLayoutFourTabs(View root_layout) {
        this.root_layout = root_layout;
        setupTabs();
    }

    private void setupTabs() {

        tab_layout = root_layout.findViewById(R.id.tab_layout);

        view_one = root_layout.findViewById(R.id.tab_v_one);
        view_two = root_layout.findViewById(R.id.tab_v_two);
        view_three = root_layout.findViewById(R.id.tab_v_three);
        view_four = root_layout.findViewById(R.id.tab_v_four);

        tab_one = (RelativeLayout) root_layout.findViewById(R.id.tab_rl_section_one);
        tab_two = (RelativeLayout) root_layout.findViewById(R.id.tab_rl_section_two);
        tab_three = (RelativeLayout) root_layout.findViewById(R.id.tab_rl_section_three);
        tab_four = (RelativeLayout) root_layout.findViewById(R.id.tab_rl_section_four);

        tab_one_name = (TextView) root_layout.findViewById(R.id.tab_one_tv_name);
        tab_two_name = (TextView) root_layout.findViewById(R.id.tab_two_tv_name);
        tab_three_name = (TextView) root_layout.findViewById(R.id.tab_three_tv_name);
        tab_four_name = (TextView) root_layout.findViewById(R.id.tab_four_tv_name);

        tab_one.setOnClickListener(this);
        tab_two.setOnClickListener(this);
        tab_three.setOnClickListener(this);
        tab_four.setOnClickListener(this);

    }


    public void setTabTexts(String s1, String s2, String s3, String s4) {
        tab_one_name.setText(s1);
        tab_two_name.setText(s2);
        tab_three_name.setText(s3);
        tab_four_name.setText(s4);

    }


    public void setTabTextColor(int textOneColor, int textTwoColor) {
        tab_one_name.setTextColor(textOneColor);
        tab_two_name.setTextColor(textTwoColor);
    }


    public void setTabColor(int color) {
        tab_layout.setBackgroundColor(color);
    }


    public void setCurrentActiveTab(int index, int activeTabColor, int inactiveTabColor) {

        view_one.setVisibility(View.INVISIBLE);
        view_two.setVisibility(View.INVISIBLE);
        view_three.setVisibility(View.INVISIBLE);
        view_four.setVisibility(View.INVISIBLE);
        //add inactiveTabColor below
        tab_one_name.setTextColor(inactiveTabColor);
        tab_two_name.setTextColor(inactiveTabColor);
        tab_three_name.setTextColor(inactiveTabColor);
        tab_four_name.setTextColor(inactiveTabColor);


        switch (index) {
            case 0:

                view_one.setVisibility(View.VISIBLE);
                view_one.setBackgroundColor(activeTabColor);
                tab_one_name.setTextColor(activeTabColor);

                break;
            case 1:
                view_two.setVisibility(View.VISIBLE);
                view_two.setBackgroundColor(activeTabColor);
                tab_two_name.setTextColor(activeTabColor);

                break;

            case 2:
                view_three.setVisibility(View.VISIBLE);
                view_three.setBackgroundColor(activeTabColor);
                tab_three_name.setTextColor(activeTabColor);

                break;

            case 3:
                view_four.setVisibility(View.VISIBLE);
                view_four.setBackgroundColor(activeTabColor);
                tab_four_name.setTextColor(activeTabColor);

                break;


        }
    }

    public void setTabListener(IOnTabClicked callback) {
        this.callback = callback;
    }

    @Override
    public void onClick(View v) {

        if (v == tab_one) {
            callback.onTabClicked(0);
            //setCurrentActiveTab(0);
        } else if (v == tab_two) {
            callback.onTabClicked(1);
            //setCurrentActiveTab(1);

        } else if (v == tab_three) {
            callback.onTabClicked(2);
            //setCurrentActiveTab(1);

        } else if (v == tab_four) {
            callback.onTabClicked(3);
            //setCurrentActiveTab(1);

        }

    }


    public interface IOnTabClicked {
        void onTabClicked(int index);
    }
}
