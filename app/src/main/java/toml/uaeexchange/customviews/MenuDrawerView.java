package toml.uaeexchange.customviews;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.uaeexchange.R;

/**
 * Created by vishwanathp on 8/17/2017.
 */

public class MenuDrawerView {
    Context context;
    View view;

    @BindView(R.id.drawer_tvName)TextView tvName;
    @BindView(R.id.tvEmail)TextView tvEmail;
    @BindView(R.id.tvMyProfile)TextView tvMyProfile;

    public MenuDrawerView(Context context, View view) {
        this.context = context;
        this.view = view;

        initView();
    }

    private void initView() {

        ButterKnife.bind((Activity) context);

    }
}
