package toml.uaeexchange.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;

/**
 * Created by vishwanathp on 8/31/2017.
 */

public class CustomEditTextLayout extends FrameLayout {


    Drawable icon_Normal, icon_Active, icon_Right_Show, icon_Right_Hide;
    private int Max_Length, Input_Type;
    float DENSITY = 1.0f;
    private CustomEditTextLayout.GenericTextWatcher genericTextWatcher;
    private ErrorLabelLayout errorLabelLayout;


    @BindView(R.id.custom_edit_text_rl_for_content)
    RelativeLayout rlForContent;
    @BindView(R.id.custom_edit_text_iv_imgLeft)
    ImageView imgLeft;
    @BindView(R.id.custom_edit_text_v_vertical_divider)
    View verticalDivider;
    @BindView(R.id.custom_edit_text_et_country_code)
    EditText etCountryCode;
    @BindView(R.id.custom_edit_text_et_value)
    EditText etValue;
    @BindView(R.id.custom_edit_text_tv_hint)
    TextView tvHint;
    @BindView(R.id.custom_edit_text_iv_imgRight)
    ImageView imgRight;

    CustomEditTextLayout customEditTextLayout;

    private boolean bIsHintSet = false;


    boolean isRightImageClicked = false;


    public CustomEditTextLayout(Context context) {
        super(context);
        customEditTextLayout = this;
        initView(context);
    }

    public void setErrorLabelLayout(ErrorLabelLayout errorLabelLayout) {
        this.errorLabelLayout = errorLabelLayout;

    }

    public ErrorLabelLayout getErrorLabelLayout() {
        return this.errorLabelLayout;

    }

    public View getVerticalDivider() {
        return verticalDivider;
    }

    public void setVerticalDivider(View verticalDivider) {
        this.verticalDivider = verticalDivider;
    }


    public void setGenericTextWatcher(CustomEditTextLayout.GenericTextWatcher textWatcher) {
        this.genericTextWatcher = textWatcher;
    }


    public RelativeLayout getRlForContent() {
        return rlForContent;
    }

    public void setRlForContent(RelativeLayout rlForContent) {
        this.rlForContent = rlForContent;
    }

    public ImageView getImgLeft() {
        return imgLeft;
    }

    public void setImgLeft(ImageView imgLeft) {
        this.imgLeft = imgLeft;
    }

    public EditText getEtCountryCode() {
        return etCountryCode;
    }

    public void setEtCountryCode(EditText etCountryCode) {
        this.etCountryCode = etCountryCode;
    }

    public EditText getEtValue() {
        return etValue;
    }

    public void setEtValue(EditText etValue) {
        this.etValue = etValue;
    }

    public TextView getTvHint() {
        return tvHint;
    }

    public void setTvHint(TextView tvHint) {
        this.tvHint = tvHint;
    }

    public ImageView getImgRight() {
        return imgRight;
    }

    public void setImgRight(ImageView imgRight) {
        this.imgRight = imgRight;
    }


    public CustomEditTextLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        customEditTextLayout = this;

        TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.CustomEditTextView);
        initView(context);

        icon_Normal = t.getDrawable(R.styleable.CustomEditTextView_iconNormal);
        icon_Active = t.getDrawable(R.styleable.CustomEditTextView_iconActive);
        icon_Right_Show = t.getDrawable(R.styleable.CustomEditTextView_iconRightShow);
        icon_Right_Hide = t.getDrawable(R.styleable.CustomEditTextView_iconRightHide);

        setLayout(t.getString(R.styleable.CustomEditTextView_hint), t.getInteger(R.styleable.CustomEditTextView_maxlength, 1000), t.getString(R.styleable.CustomEditTextView_inputtype));

        t.recycle();
    }

    private void initView(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_edit, null);

        ButterKnife.bind(this, view);
//        rlForContent = (RelativeLayout) view.findViewById(R.id.custom_edit_text_rl_for_content);
//        imgLeft = (ImageView) view.findViewById(R.id.custom_edit_text_iv_imgLeft);
//        verticalDivider =  view.findViewById(R.id.custom_edit_text_v_vertical_divider);
//        etCountryCode = (EditText) view.findViewById(R.id.custom_edit_text_et_country_code);
//        etValue = (EditText) view.findViewById(R.id.custom_edit_text_et_value);
//        tvHint = (TextView) view.findViewById(R.id.custom_edit_text_tv_hint);
//        imgRight = (ImageView)view.findViewById(R.id.custom_edit_text_iv_imgRight);
//
        verticalDivider.setVisibility(VISIBLE);

        addView(view);
    }

    public EditText getEditText() {
        if (etValue != null) {
            return etValue;
        }

        return null;
    }

    public void setEditTextFocus() {
        etValue.requestFocus();
    }

    public void setLayout(String editHint, int maxlength, String inputtype) {
//
//        typeface_regular = Typeface.createFromAsset(getContext().getAssets(), "fonts/Quicksand-Medium.ttf");
//        typeface_italic = Typeface.createFromAsset(getContext().getAssets(), "fonts/Quicksand-Medium.ttf");
//
//        tvHint.setTypeface(typeface_regular);

        Max_Length = maxlength;
        imgLeft.setImageDrawable(icon_Normal);

//        if (icon_Right_Show != null) {
//            imgRight.setVisibility(VISIBLE);
//            imgRight.setImageDrawable(icon_Right_Hide);
//            imgRight.setTag("hide_pin");
//        }
        etValue.setHint("");
        etValue.setMaxLines(1);


        DENSITY = getResources().getDisplayMetrics().density;
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(maxlength);
        etValue.setFilters(FilterArray);

        // Default input type is text
        if (inputtype == null) {
            inputtype = "2";
        }

        int inputMethod = Integer.parseInt(inputtype);

        switch (inputMethod) {
            case 1:
                etValue.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case 2:
                etValue.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            case 3:
                etValue.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                etValue.setTransformationMethod(PasswordTransformationMethod.getInstance());
                etValue.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                etValue.setLongClickable(false);
                break;

            case 5:
                etValue.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;

            case 6:
                //For Amount eg:10.20
                etValue.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                etValue.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});
                etValue.setKeyListener(DigitsKeyListener.getInstance(false, true));
                break;

            case 7:
                etValue.setInputType(InputType.TYPE_CLASS_PHONE);
                //etValue.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});
                etValue.setKeyListener(DigitsKeyListener.getInstance("0123456789-"));
                // To format Emirates ID value
                break;
            case 8:
                etValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                // etValue.setFilters(new InputFilter[]{new AlphaNumericInputFilter()});
                //etValue.setKeyListener(DigitsKeyListener.getInstance("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "));
                //etValue.setFilters(new InputFilter[]{getEditTextFilter()});
                break;
        }
        tvHint.setText(editHint);
        tvHint.setClickable(false);

        etValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

//                if (isRightImageClicked) {
//                    isRightImageClicked = false;
//                    return;
//                } else {

                if (getErrorLabelLayout() != null) {
                    getErrorLabelLayout().clearError();
                    customEditTextLayout.setBackgroundResource(R.drawable.custom_edittext_border);
                    verticalDivider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.bluey_purple));

                }
                //}

                if (TextUtils.isEmpty(s) && !etValue.hasFocus()) {
                    verticalDivider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pinkish_grey));
                }

                if (s.length() >= 1 && !bIsHintSet) {
                    bIsHintSet = true;
                    LogUtils.Verbose("CET", "in animateHintToY() onTextChanged()");
                    animateHintToY(-15, 8);
                }

                if (!TextUtils.isEmpty(s)) {

                    if (s.toString().length() == Max_Length) {
                        if (genericTextWatcher != null)
                            genericTextWatcher.handleCallback();
                    }

                    if (Max_Length == getResources().getInteger(R.integer.mobileNoLength))
                        etCountryCode.setVisibility(View.VISIBLE);


                } else {
                    bIsHintSet = false;
                    etCountryCode.setVisibility(View.GONE);
                    animateHintToY(0, 0);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etValue.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    // verticalDivider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.bluey_purple));
                    imgLeft.setImageDrawable(icon_Active);
                    animateHintToY(-15, 8);

                } else if (TextUtils.isEmpty(etValue.getText().toString().trim())) {
                    // verticalDivider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pinkish_grey));

                    imgLeft.setImageDrawable(icon_Normal);
                    animateHintToY(0, 0);
                } else {
                    //verticalDivider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pinkish_grey));
                }
            }
        });


//        imgRight.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                isRightImageClicked = true;
//
//
//            }
//        });

    }





    public static class AlphaNumericInputFilter implements InputFilter {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {

            // Only keep characters that are alphanumeric
            StringBuilder builder = new StringBuilder();
            for (int i = start; i < end; i++) {
                char c = source.charAt(i);
                if (Character.isLetterOrDigit(c)) {
                    builder.append(c);
                }
            }

            // If all characters are valid, return null, otherwise only return the filtered characters
            boolean allCharactersValid = (builder.length() == end - start);
            return allCharactersValid ? null : builder.toString();
        }
    }


    public void animateHintToY(int offset, int Offset) {

        //LogUtils.Verbose("CET", "in animateHintToY()");

        int animate_offset = (int) (offset * DENSITY);
        int animate_offset_et = (int) (Offset * DENSITY);

        tvHint.animate().translationY(animate_offset).alpha(1).start();
        etCountryCode.animate().translationY(animate_offset_et).alpha(1).start();
        etValue.animate().translationY(animate_offset_et).alpha(1).start();
        if (offset == 0) {
            //tvHint.setTypeface(typeface_regular);
            tvHint.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        } else {
            //tvHint.setTypeface(typeface_italic);
            tvHint.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        }
    }

    public String getText() {

        return etValue.getText().toString();
    }

    public void setText(String text) {

        LogUtils.Verbose("CET", "in setText()");
        etValue.setText(text);
        verticalDivider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pinkish_grey));
        imgLeft.setImageDrawable(icon_Active);
        animateHintToY(-15, 8);
        etValue.setSelection(etValue.getText().length());
    }

    public void clearText() {
        etValue.setText("");
        verticalDivider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pinkish_grey));
        imgLeft.setImageDrawable(icon_Normal);
        customEditTextLayout.setBackgroundResource(R.drawable.custom_edittext_unselected_border);
        animateHintToY(0, 0);
        //etValue.setSelection(etValue.getText().length());
    }


    public interface GenericTextWatcher {
        void handleCallback();
    }


    public class DecimalDigitsInputFilter implements InputFilter {

        Pattern mPattern;

        public DecimalDigitsInputFilter() {
            mPattern = Pattern.compile("[0-9]*+((\\.[0-9]?)?)||(\\.)?");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
//            Matcher matcher = mPattern.matcher(dest);
//            if (!matcher.matches())
//                return "";
//            return null;


            StringBuilder builder = new StringBuilder(dest);
            builder.replace(dstart, dend, source
                    .subSequence(start, end).toString());
            if (!builder.toString().matches(
                    "(([1-9]{1})([0-9]{0,20})?(\\.)?)?([0-9]{0,2})?"

            )) {
                if (source.length() == 0)
                    return dest.subSequence(dstart, dend);
                return "";
            }
            return null;


        }
    }


}
