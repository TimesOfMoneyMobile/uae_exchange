package toml.uaeexchange.customviews;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import toml.uaeexchange.R;

/**
 * Created by kunalk on 4/5/2016.
 */
public class CustomTabLayout implements View.OnClickListener {




    View root_layout,tab_layout, view_one, view_two;
    RelativeLayout tab_one, tab_two;

    TextView tab_one_name, tab_two_name;
    private IOnTabClicked callback;



    public CustomTabLayout(View root_layout) {
        this.root_layout = root_layout;
        setupTabs();
    }

    private void setupTabs() {

        tab_layout = root_layout.findViewById(R.id.tab_layout);
        view_one = root_layout.findViewById(R.id.tab_v_one);
        view_two = root_layout.findViewById(R.id.tab_v_two);
        tab_one = (RelativeLayout) root_layout.findViewById(R.id.tab_rl_section_one);
        tab_two = (RelativeLayout) root_layout.findViewById(R.id.tab_rl_section_two);

        tab_one_name = (TextView) root_layout.findViewById(R.id.tab_one_tv_name);
        tab_two_name = (TextView) root_layout.findViewById(R.id.tab_two_tv_name);
        tab_one.setOnClickListener(this);
        tab_two.setOnClickListener(this);

    }



    public void setTabTexts(String s1, String s2)
    {
        tab_one_name.setText(s1);
        tab_two_name.setText(s2);

    }


    public void setTabTextColor(int textOneColor, int textTwoColor) {
        tab_one_name.setTextColor(textOneColor);
        tab_two_name.setTextColor(textTwoColor);
    }


    public void setTabColor(int color) {
        tab_layout.setBackgroundColor(color);
    }



    public void setCurrentActiveTab(int index,int activeTabColor,int inactiveTabColor) {

        view_one.setVisibility(View.INVISIBLE);
        view_two.setVisibility(View.INVISIBLE);
        //add inactiveTabColor below
        tab_one_name.setTextColor(Color.parseColor("#cabfb7"));
        tab_two_name.setTextColor(Color.parseColor("#cabfb7"));


        switch (index) {
            case 0:

                view_one.setVisibility(View.VISIBLE);
                view_one.setBackgroundColor(activeTabColor);
                tab_one_name.setTextColor(activeTabColor);

                break;
            case 1:
                view_two.setVisibility(View.VISIBLE);
                view_two.setBackgroundColor(activeTabColor);
                tab_two_name.setTextColor(activeTabColor);

                break;


        }
    }

    public void setTabListener(IOnTabClicked callback)
    {
        this.callback=callback;
    }

    @Override
    public void onClick(View v) {

        if(v==tab_one)
        {
            callback.onTabClicked(0);
            //setCurrentActiveTab(0);
        }else
        {
            callback.onTabClicked(1);
            //setCurrentActiveTab(1);

        }
    }



    public interface IOnTabClicked {
        void onTabClicked(int index);
    }
}
