package toml.uaeexchange.whatsappp2p;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.AskMoneyController;
import toml.movitwallet.controllers.BalanceInquiryController;
import toml.movitwallet.controllers.GenerateOTPController;
import toml.movitwallet.controllers.PayPersonController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.dialogs.ValidateOTPDialog;
import toml.uaeexchange.utilities.Constants;


/**
 * Created by kunalk on 8/2/17.
 */

public class OverlayDashboard extends BaseActivity implements BaseActivity.ClickableSpanListener {

    private static final String TXN_TYPE = "P2P";
    @BindView(R.id.root_layout)
    RelativeLayout layout_root;

    @BindView(R.id.layout_send)
    LinearLayout layout_send;
    @BindView(R.id.layout_request)
    LinearLayout layout_request;
    @BindView(R.id.show_balance)
    LinearLayout layout_balance;

    @BindView(R.id.txtsend)
    TextView txtSend;
    @BindView(R.id.txtrequest)
    TextView txtRequest;
    @BindView(R.id.txtBalance)
    TextView txtBalance;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtMobile)
    TextView txtMobile;

    @BindView(R.id.refresh)
    ProgressBar balance_progress;


    String name, amount, number, strTpin;
    private boolean isSpanClicked;

    ValidateOTPDialog otpDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overlay_dashboard);
        ButterKnife.bind(this);

        setClickableSpanListener(this);
        layout_balance.setVisibility(View.GONE);

        name = getIntent().getStringExtra("name");
        number = getIntent().getStringExtra("number");
        amount = getIntent().getStringExtra("amount");

        txtName.setText(name);
        txtMobile.setText(number);
        txtSend.setText("Send AED " + toml.movitwallet.utils.Constants.formatAmount(amount));
        txtRequest.setText("Request AED " + toml.movitwallet.utils.Constants.formatAmount(amount));


        callBalanceAPI();


    }


    @OnClick({R.id.layout_send, R.id.layout_request, R.id.root_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layout_send:
                isSpanClicked = false;
                if ((Double.valueOf(amount) < Double.valueOf("0"))) {
                    showError(getString(R.string.please_enter_amount_greater_than) + " " + Constants.formatAmount(MovitConsumerApp.getInstance().getMinAmount()));
                } else if (Double.valueOf(amount) > Double.valueOf(MovitConsumerApp.getInstance().getWalletBalance())) {
                    showError(getString(R.string.amount_should_be_less_than_balance));
                } else if ((Double.valueOf(amount) > Double.valueOf(MovitConsumerApp.getInstance().getMaxAmount()))) {
                    showError(getString(R.string.please_enter_amount_less_than) + " " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount()));
                } else if ((Double.valueOf(amount) < Double.valueOf(MovitConsumerApp.getInstance().getMinAmount()))) {
                    showError(getString(R.string.please_enter_amount_greater_than) + " " + Constants.formatAmount(MovitConsumerApp.getInstance().getMinAmount()));
                } else if (Constants.checkToShowOtpDialog(OverlayDashboard.this, amount)) {
                    callGenerateOTP(isSpanClicked);
                } else {
                    callP2PApi();
                }
                break;
            case R.id.layout_request:
                //callRequestMoneyAPI();
                break;
            case R.id.root_layout:
                finish();
                break;
        }
    }


    @Override
    public void onSpanClick(String strText) {
        isSpanClicked = true;
        callGenerateOTP(isSpanClicked);
    }

    private void callGenerateOTP(final boolean isSpanClicked) {
        showProgress();

        MovitWalletController generateOTPController = new GenerateOTPController(TXN_TYPE);

        generateOTPController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                dismissProgress();


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String code = "";
                        try {
                            if (isSpanClicked) {
                                if (otpDialog.isShowing()) {
                                    otpDialog.refreshOnResend();
                                }
                            } else {
                                showOTPDialog();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }, 200);


            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                showError(reason);

            }
        });
        if (isSpanClicked) {
            this.isSpanClicked = false;
        }
    }


    private void showOTPDialog() {

        otpDialog = new ValidateOTPDialog(this, new ValidateOTPDialog.IValidateOTPDialogCallBack() {


            @Override
            public void resendOTPCallBack(String otpValue) {

//                strTpin = otpValue;
//                callP2PApi();
//                otpDialog.cancel();
                //verifyOTP(otpValue);
            }

            @Override
            public void verifyOTPCallBack(String otpValue) {

                strTpin = otpValue;
                callP2PApi();
                // otpDialog.cancel();
                //verifyOTP(otpValue);
            }
        });

        otpDialog.show();

    }


    private void callP2PApi() {
        showProgress();

        MovitWalletController movitWalletController = new PayPersonController("", number, amount, strTpin);
        movitWalletController.init(new IMovitWalletController<Bundle>() {


            @Override
            public void onSuccess(Bundle response) {
                showError(response.get("Message") + "");

                if (otpDialog != null)
                    otpDialog.dismiss();

                dismissProgress();

                finish();
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                // showError(reason);

                new OkDialog(OverlayDashboard.this, null, reason, new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                    }
                });


            }
        });
        //}


    }

    private void callBalanceAPI() {


        if (MovitConsumerApp.getInstance().getPrimaryWallet() != null) {

            balance_progress.setVisibility(View.VISIBLE);
            layout_balance.setVisibility(View.GONE);


            final String walletID = MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId();
            MovitWalletController controller = new BalanceInquiryController(walletID);

            controller.init(new IMovitWalletController<Bundle>() {
                @Override
                public void onSuccess(Bundle response) {

                    balance_progress.setVisibility(View.GONE);
                    layout_balance.setVisibility(View.VISIBLE);

                    MovitConsumerApp.getInstance().setWalletBalanceByID(walletID, response.getString("Balance"));

                    txtBalance.setText(getString(R.string.AED) + " " + response.getString("Balance"));


                }

                @Override
                public void onFailed(String errorCode, String reason) {

                    balance_progress.setVisibility(View.GONE);
                    layout_balance.setVisibility(View.VISIBLE);

                    if (txtBalance != null) {
                        txtBalance.setText(getString(R.string.dummyAmount));
                    }

                    Toast.makeText(OverlayDashboard.this, reason, Toast.LENGTH_SHORT).show();

                }
            });
        } else {

            balance_progress.setVisibility(View.GONE);
            layout_balance.setVisibility(View.VISIBLE);
        }
    }


    private void callRequestMoneyAPI() {
        showProgress();

        String tpin = "111111";

        AskMoneyController askMoneyController = new AskMoneyController("", number, amount, tpin);
        askMoneyController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                LogUtils.Verbose("" + OverlayDashboard.this.getClass().getName(), "Request Money Response");
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                showError(reason);

            }
        });
    }


    // utility function for encryption of MPIN
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }


}
