package toml.uaeexchange.whatsappp2p;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import toml.uaeexchange.R;


/**
 * Created by kunalk on 30/1/17.
 */

public class OverlayShowingService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    String amount, name, number;

    @Override
    public void onCreate() {
        super.onCreate();


        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        final View view = inflater.inflate(R.layout.overlay, null);

        LinearLayout overlay_layout = (LinearLayout) view.findViewById(R.id.overlay);


        final WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams topLeftParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.TYPE_SYSTEM_ALERT, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, PixelFormat.TRANSLUCENT);
        topLeftParams.gravity = Gravity.TOP | Gravity.RIGHT;
        wm.addView(view, topLeftParams);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                wm.removeView(view);
                stopSelf();
            }
        });

        overlay_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wm.removeView(view);
                Intent intent = new Intent(OverlayShowingService.this, OverlayLoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("name", name);
                intent.putExtra("amount", amount);
                // This regex will mark all characters that are not digits, and replace them with an empty string.
                // String phoneNumber = number.replaceAll("\\D+", "");
                String phoneNumber = number.replaceAll("[^0-9]", "");
                intent.putExtra("number", number);
                startActivity(intent);

                stopSelf();
            }
        });
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        amount = intent.getStringExtra("amount");
        name = intent.getStringExtra("name");
        number = intent.getStringExtra("number");

        return super.onStartCommand(intent, flags, startId);

    }
}
