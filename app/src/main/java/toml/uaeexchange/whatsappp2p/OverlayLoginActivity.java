package toml.uaeexchange.whatsappp2p;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jaredrummler.android.device.DeviceName;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.LoginController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;

public class OverlayLoginActivity extends BaseActivity implements IMovitWalletController {


    @BindView(R.id.root_layout)
    RelativeLayout rlRootLayout;

    @BindView(R.id.error_label_MobileNumber)
    ErrorLabelLayout errorLabelMobileNumber;
    @BindView(R.id.etMobileNumber)
    CustomEditTextLayout etMobileNumber;

    @BindView(R.id.error_label_Mpin)
    ErrorLabelLayout errorLabelMpin;
    @BindView(R.id.etMpin)
    CustomEditTextLayout etMpin;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    GenericValidations genericValidations;

    String mobileNumber, mpin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overlay_login);
        ButterKnife.bind(this);
        genericValidations = new GenericValidations(OverlayLoginActivity.this);

        etMobileNumber.setErrorLabelLayout(errorLabelMobileNumber);
        etMobileNumber.getEditText().addTextChangedListener(etMobileTextWatcher);
        etMpin.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etMobileNumber.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etMpin.requestFocus();
                    return true;
                }
                return false;
            }
        });


        etMpin.setErrorLabelLayout(errorLabelMpin);
        etMpin.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        etMpin.getEditText().addTextChangedListener(etMobileTextWatcher);


        if (!TextUtils.isEmpty(MovitConsumerApp.getInstance().getMobileNumber())) {
            etMobileNumber.setText(MovitConsumerApp.getInstance().getMobileNumber());
            etMobileNumber.setEnabled(true);
            etMobileNumber.setFocusable(false);
            etMpin.requestFocus();
        } else {
            etMobileNumber.requestFocus();
        }


    }

    @OnClick({R.id.btnLogin, R.id.root_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:

                mobileNumber = etMobileNumber.getText();
                mpin = etMpin.getText();


                if (genericValidations.validateLogin(mobileNumber, etMobileNumber, mpin, etMpin)) {
                    showProgress();
                    MovitWalletController movitWalletController = new LoginController(OverlayLoginActivity.this,  mobileNumber, mpin, DeviceName.getDeviceName());
                    movitWalletController.init(OverlayLoginActivity.this);
                }


                break;
            case R.id.root_layout:
                finish();
                break;
        }
    }


    private TextWatcher etMobileTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.toString().length() == getResources().getInteger(R.integer.mobileNoLength)) {
                etMpin.requestFocus();
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{etMobileNumber, etMpin})) {
                btnLogin.setEnabled(true);
            } else {
                btnLogin.setEnabled(false);

            }


        }
    };


//    private TextView.OnEditorActionListener editAction = new TextView.OnEditorActionListener() {
//        @Override
//        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//            if (actionId == EditorInfo.IME_ACTION_NEXT) {
//               etMpin.requestFocus();
//                return true;
//            }
//            return false;
//        }
//    };


    @Override
    public void onSuccess(Object response) {
        dismissProgress();

        Intent dashboard = new Intent(OverlayLoginActivity.this, OverlayDashboard.class);
        dashboard.putExtra("name", getIntent().getStringExtra("name"));
        dashboard.putExtra("amount", getIntent().getStringExtra("amount"));
        dashboard.putExtra("number", getIntent().getStringExtra("number"));
        startActivity(dashboard);

        finish();
    }

    @Override
    public void onFailed(String errorCode, String reason) {

        dismissProgress();
        showError(reason);

    }


}
