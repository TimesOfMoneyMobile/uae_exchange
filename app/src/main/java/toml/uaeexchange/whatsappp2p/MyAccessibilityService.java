package toml.uaeexchange.whatsappp2p;

import android.accessibilityservice.AccessibilityService;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by kunalk on 30/1/17.
 */

public class MyAccessibilityService extends AccessibilityService {

    private String name, number,amount, text;


    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();

    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {

        final int eventType = accessibilityEvent.getEventType();

        Log.v("TAG", " Type is " + eventType);


        switch (eventType) {


//            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED: {
//                try {
//                    if (!TextUtils.isEmpty(AppSettings.getData(this, AppSettings.LINK))) {
//
//                        Log.v("TAG", " Got window state changed");
//
//
//                        AccessibilityNodeInfo nodeInfo = accessibilityEvent.getSource();
//                        Log.i(TAG, "ACC::onAccessibilityEvent: nodeInfo=" + nodeInfo);
//
//                        for (int i = 0; i < nodeInfo.getChildCount(); i++) {
//                            AccessibilityNodeInfo info = nodeInfo.getChild(i);
//
//                            if (info.getClassName().equals("android.widget.EditText")) {
//                                Bundle arguments = new Bundle();
//                                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE,
//                                        "Link");
//                                info.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
//                                AppSettings.putData(this, AppSettings.LINK, "");
//                                break;
//                            }
//                        }
//
//
//                    }
//                } catch (Exception e) {
//
//                }
//
//                break;
//            }

            case AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED: {

                Log.v("TAG", "TYPE_VIEW_TEXT_CHANGED");

                Log.v("TAG", " edit text is " + accessibilityEvent.getText() + " got text " + name);

                if (accessibilityEvent.getText().size() > 0) {

                    text = accessibilityEvent.getText().get(0).toString();
                    Log.v("TAG", " text text is " +text);


                    if (text.matches("^\\d+(\\.\\d{1,2})?WAYVE")){ //|| text.contains("uae") || text.contains("UAe")|| text.contains("UaE")|| text.contains("uAE")|| text.contains("uaE")|| text.contains("Uae")|| text.contains("uAe")) {

                        Log.v("TAG", " Got text " + text);
                        if (!TextUtils.isEmpty(number) && !TextUtils.isEmpty(name)) {
                            try {
                                amount = text.substring(0, text.indexOf("WAYVE"));
                                Log.v("TAG", " AMount is " + amount + " Name is " + name);

                                Intent svc = new Intent(this, OverlayShowingService.class);
                                svc.putExtra("amount", amount);
                                svc.putExtra("name", name);
                                svc.putExtra("number", number);
                                startService(svc);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                break;


            }
            case AccessibilityEvent.TYPE_VIEW_CLICKED: {

                Log.v("TAG", "TYPE_VIEW_CLICKED");

                try {


                    if (!TextUtils.isEmpty(accessibilityEvent.getText().get(0).toString())) {

                      /*  if (accessibilityEvent.getText().get(0).toString().equals(text))
                            break;*/

                        if (accessibilityEvent.getText().get(0).toString() != null && accessibilityEvent.getText().get(0).toString().equals("Type a message"))
                            return;

                        String temp = null;
                        if (TextUtils.isEmpty(name)) {
                            Log.v("TAG", "Temp is " + temp);
                            name = accessibilityEvent.getText().get(0).toString();
                        }else {
                            //name=null;
                            temp = accessibilityEvent.getText().get(0).toString();

                            Log.v("TAG", "Temp is " + temp);
                        }

                        ContentResolver cr = getContentResolver();
                        Cursor cur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + "='" + (temp == null ? name : temp) + "'", null, null);
                        try {

                            if (cur.getCount() > 0) {
                                // Log.v("TAG", " Size" + cur.getCount());


                                while (cur.moveToNext()) {

                                    /*String display_name = cur.getString(cur.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                                    Log.v("TAG", " Found display name " + display_name);

                                    if (name != null && !name.equalsIgnoreCase(display_name)) {
                                        continue;
                                    } else if (temp != null && !temp.equalsIgnoreCase(display_name)) {
                                        continue;
                                    }*/

                                    number = cur.getString(cur.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("\\s+", "");
                                    number = toml.movitwallet.utils.Constants.getLastNineDigitPhoneNumber(number);
//                                    if (number.startsWith("+91"))
//                                        number = number.substring(3, number.length());
//                                    else if(number.startsWith("+971"))
//                                        number = toml.movitwallet.utils.Constants.getLastNineDigitPhoneNumber(number);

                                    if (temp != null) {
                                        Log.v("TAG", " Found new name " + temp);
                                        name = temp;
                                        temp = null;
                                    }


                                    Log.v("TAG", " number is " + number);


                                    break;
                                }
                            } else {
                                //name = null;
                                // number = null;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {

                }
                break;

            }

        }


        //
    }


    @Override
    public void onInterrupt() {

    }


}
