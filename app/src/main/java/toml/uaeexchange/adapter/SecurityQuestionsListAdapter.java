package toml.uaeexchange.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import toml.movitwallet.models.SecurityQuestion;
import toml.uaeexchange.R;

/**
 * Created by pankajp on 9/5/2017.
 */

public class SecurityQuestionsListAdapter extends BaseAdapter {

    private Context mContext;
    private List<SecurityQuestion> securityQuestionList;
    private LayoutInflater layoutInflater;

    public SecurityQuestionsListAdapter(Context mContext, List<SecurityQuestion> securityQuestionList) {
        this.mContext = mContext;
        this.securityQuestionList = securityQuestionList;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return securityQuestionList.size();
    }

    @Override
    public Object getItem(int i) {
        return securityQuestionList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.security_questions_list_item, null);
            holder = new ViewHolder();
            holder.txtvwQuestion = (TextView) convertView.findViewById(R.id.txtvwQuestion);
            holder.txtvwLine = (TextView) convertView.findViewById(R.id.txtvwLine);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        if (!securityQuestionList.get(pos).question.contains("?")) {
            holder.txtvwQuestion.setText(securityQuestionList.get(pos).question + "?");
        } else {
            holder.txtvwQuestion.setText(securityQuestionList.get(pos).question);
        }


        if (pos == getCount() - 1) {
            holder.txtvwLine.setVisibility(View.GONE);
            // holder.txtvwLine.setBackgroundColor(mContext.getResources().getColor(R.color.blueColor));
        }

        return convertView;
    }

    static class ViewHolder {
        TextView txtvwQuestion;
        TextView txtvwLine;
    }

}
