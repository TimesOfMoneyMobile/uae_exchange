package toml.uaeexchange.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import ru.rambler.libs.swipe_layout.SwipeLayout;
import toml.movitwallet.models.MoneyRequest;
import toml.movitwallet.utils.IntentConstants;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.PendingRequestActivity;
import toml.uaeexchange.activities.ReviewAndConfirmActivity;
import toml.uaeexchange.utilities.Constants;


/**
 * Created by kunalk on 8/11/2016.
 */
public class PendingRequestAdapter extends BaseAdapter {

    List<MoneyRequest> moneyRequestList;
   // Context mContext;
    Activity mActivity;
    LayoutInflater inflater;
    Intent intent;
    String TAG = "PendingRequestAdapter";

    public PendingRequestAdapter(Activity mActivity, List<MoneyRequest> moneyRequestList) {
        //this.mContext = context;
        this.mActivity = mActivity;
        this.moneyRequestList = moneyRequestList;
        inflater = (LayoutInflater) mActivity.getSystemService(mActivity.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return moneyRequestList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.pending_request_listview_item, parent, false);

            holder.linlayParent = (LinearLayout) convertView.findViewById(R.id.parent);
            holder.swipeLayout = (SwipeLayout) convertView.findViewById(R.id.swipeLayout);
            holder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            holder.txtvwRequestID = (TextView) convertView.findViewById(R.id.txtvwRequestID);
            holder.txtDate = (TextView) convertView.findViewById(R.id.txtDate);
            holder.txtAMount = (TextView) convertView.findViewById(R.id.stramount);
            holder.frmLayoutRejectView = (FrameLayout) convertView.findViewById(R.id.right_view);
            holder.frmLayoutAcceptView = (FrameLayout) convertView.findViewById(R.id.left_view);

            convertView.setTag(holder);

        }

        holder = (ViewHolder) convertView.getTag();

        final MoneyRequest moneyRequest = moneyRequestList.get(position);
        holder.swipeLayout.setTag(R.id.IS_OPENED, false);
        final ViewHolder finalHolder = holder;


        holder.frmLayoutAcceptView.setOnClickListener(new View.OnClickListener() {
                                                          @Override
                                                          public void onClick(View v) {

                                                              finalHolder.swipeLayout.animateReset();
                                                              Bundle bundle = new Bundle();
                                                              Intent intent = new Intent(mActivity, ReviewAndConfirmActivity.class);
                                                              bundle.putString("amount", moneyRequest.getAmount());
                                                              bundle.putString("mobile_number", moneyRequest.getMobileNumber());
                                                              bundle.putSerializable("money_req", moneyRequest);
                                                              bundle.putString("nick_name", "");
                                                              bundle.putString("status", "AUTHORIZE");
                                                              bundle.putBoolean("isFav", false);
                                                              bundle.putString(IntentConstants.FROM_ACTIVITY, PendingRequestActivity.class.getCanonicalName());
                                                              intent.putExtras(bundle);
                                                              mActivity.startActivityForResult(intent, 1);
                                                          }
                                                      }
        );


        holder.frmLayoutRejectView.setOnClickListener(new View.OnClickListener() {
                                                          @Override
                                                          public void onClick(View v) {

                                                              finalHolder.swipeLayout.animateReset();
                                                              Bundle bundle = new Bundle();
                                                              Intent intent = new Intent(mActivity, ReviewAndConfirmActivity.class);
                                                              bundle.putString("amount", moneyRequest.getAmount());
                                                              bundle.putString("mobile_number", moneyRequest.getMobileNumber());
                                                              bundle.putSerializable("money_req", moneyRequest);
                                                              bundle.putString("nick_name", "");
                                                              bundle.putString("status", "REJECT");
                                                              bundle.putBoolean("isFav", false);
                                                              bundle.putString(IntentConstants.FROM_ACTIVITY, PendingRequestActivity.class.getCanonicalName());
                                                              intent.putExtras(bundle);
                                                              mActivity.startActivityForResult(intent, 1);
                                                          }
                                                      }
        );


        holder.swipeLayout.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
            @Override
            public void onBeginSwipe(SwipeLayout swipeLayout, boolean moveToRight) {
                //toggleRow(swipeLayout, parent);
            }

            @Override
            public void onSwipeClampReached(SwipeLayout swipeLayout, boolean moveToRight) {

            }

            @Override
            public void onLeftStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
                swipeLayout.setTag(R.id.IS_OPENED, true);
                toggleRow(swipeLayout, parent);
            }

            @Override
            public void onRightStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
                swipeLayout.setTag(R.id.IS_OPENED, true);
                toggleRow(swipeLayout, parent);
            }
        });



    /*    holder.layout_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context,AuthoriseDeclineRequestActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(IntentConstants.MONEYREQUEST, moneyRequest);
                bundle.putString(IntentConstants.STATUS, "Authorize");
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });*/

      /*  holder.layout_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context,AuthoriseDeclineRequestActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(IntentConstants.MONEYREQUEST, moneyRequest);
                bundle.putString(IntentConstants.STATUS, "Reject");
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });*/

        holder.txtName.setText(moneyRequest.getFirstName() + " " + moneyRequest.getLastName());
        holder.txtvwRequestID.setText("Request ID: " + moneyRequest.getId());
        try {
            holder.txtDate.setText(Constants.formatToYesterdayOrToday(moneyRequest.getTxnDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.txtAMount.setText("AED " + moneyRequest.getAmount());


        return convertView;
    }


   /* private void toggleRow(View v, ViewGroup parent) {

        boolean isOpened = (Boolean) v.getTag(R.id.IS_OPENED);

        LogUtils.Verbose(TAG, " isOpened " + isOpened);

        if (isOpened) {
            v.animate().translationX(0).start();
            v.setTag(R.id.IS_OPENED, false);

        } else {
            int ANIM_OFFSET = 0;
            RelativeLayout rl_top = null;
            for (int i = 0; i < parent.getChildCount(); i++) {
                View view = parent.getChildAt(i);

                rl_top = (RelativeLayout) view.findViewById(R.id.rl_top);
                if (rl_top != null) {
                    rl_top.setTranslationX(0);
                    rl_top.setTag(R.id.IS_OPENED, false);


                }
            }


            v.animate().translationX(-ANIM_OFFSET).start();
            v.setTag(R.id.IS_OPENED, true);
        }
    }*/


    private void toggleRow(SwipeLayout swipeLayout, ViewGroup parent) {

        //CardView cardView = (CardView) parent.getChildAt(0);

        boolean isOpen = (Boolean) swipeLayout.getTag(R.id.IS_OPENED);

        if (isOpen) {

        } else {


            for (int i = 0; i < parent.getChildCount(); i++) {
                View view = parent.getChildAt(0);
                View cardview = ((ViewGroup) view).getChildAt(0);
                View mSwipeLayout = ((ViewGroup) cardview).getChildAt(0);

                ((SwipeLayout) mSwipeLayout).animateReset();

              /*  if (mSwipeLayout.getTag().toString().equalsIgnoreCase("open")) {
                    mSwipeLayout.ani

                } else {
                    ((SwipeLayout) mSwipeLayout).animateReset();
                }*/


            }
        }
    }

    class ViewHolder {
        LinearLayout linlayParent;
        TextView txtName, txtvwRequestID, txtDate, txtAMount;
        FrameLayout frmLayoutRejectView, frmLayoutAcceptView;
        SwipeLayout swipeLayout;
        //  LinearLayout layout_accept, layout_decline, layout_expand, layout_behind;
    }

}
