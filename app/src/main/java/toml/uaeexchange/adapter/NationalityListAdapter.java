package toml.uaeexchange.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import toml.movitwallet.models.Nationality;
import toml.uaeexchange.R;

/**
 * Created by pankajp on 9/5/2017.
 */

public class NationalityListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Nationality> nationalityList;
    private LayoutInflater layoutInflater;

    public NationalityListAdapter(Context mContext, List<Nationality> nationalityList) {
        this.mContext = mContext;
        this.nationalityList = nationalityList;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return nationalityList.size();
    }

    @Override
    public Object getItem(int i) {
        return nationalityList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.security_questions_list_item, null);
            holder = new ViewHolder();
            holder.txtvwName = (TextView) convertView.findViewById(R.id.txtvwQuestion);
            holder.txtvwLine = (TextView) convertView.findViewById(R.id.txtvwLine);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtvwName.setText(nationalityList.get(pos).getName());

        if (pos == getCount() - 1) {
            holder.txtvwLine.setVisibility(View.GONE);
            // holder.txtvwLine.setBackgroundColor(mContext.getResources().getColor(R.color.blueColor));
        }

        return convertView;
    }

    static class ViewHolder {
        TextView txtvwName;
        TextView txtvwLine;
    }

}
