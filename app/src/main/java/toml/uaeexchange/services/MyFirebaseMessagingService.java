package toml.uaeexchange.services;

/**
 * Created by mayurn on 15/3/17.
 */

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.LoginActivity;
import toml.uaeexchange.activities.NotificationActivity;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        if (remoteMessage != null) {
            LogUtils.Verbose(TAG, remoteMessage.toString());
            LogUtils.Verbose(TAG, "txnType" + remoteMessage.getData().get("txnType") + "---From: " + remoteMessage.getFrom() + " BODY " + remoteMessage.getData().get("body"));


            toml.uaeexchange.models.Notification notification = new toml.uaeexchange.models.Notification();
            if (!TextUtils.isEmpty(remoteMessage.getData().get("body")))
                notification.setMessage(remoteMessage.getData().get("body"));

            SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd", Locale.ENGLISH);
            SimpleDateFormat sdf3 = new SimpleDateFormat("MMM", Locale.ENGLISH);
            SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy", Locale.ENGLISH);
            SimpleDateFormat sdf5 = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
            SimpleDateFormat sdf6 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

            Date d1 = null;
            try {
                if (!TextUtils.isEmpty(remoteMessage.getData().get("timestamp"))) {
                    LogUtils.Verbose("TAG", " Notification time " + remoteMessage.getData().get("timestamp"));
                    d1 = sdf1.parse(remoteMessage.getData().get("timestamp"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (d1 != null) {
                notification.setTimestamp(sdf6.format(d1));
                // notification.setTimestamp(sdf2.format(d1) + " " + sdf3.format(d1) + "\n" + sdf4.format(d1));
                notification.setTime(sdf5.format(d1));
            }
            //if (!TextUtils.isEmpty(remoteMessage.getData().get("txnType"))) {
            notification.setTxnType(remoteMessage.getData().get("txnType"));
            //}

            if (AppSettings.getBooleanData(this, AppSettings.NOTIFICATION_ACTIVE)) {
                // This check to avoid blank notifications
                if (!TextUtils.isEmpty(notification.getMessage())) {
                    saveMessage(notification);
                }
            }

        }
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }


    private void saveMessage(toml.uaeexchange.models.Notification notification) {
        Gson gson = new Gson();
        String json = AppSettings.getData(this, AppSettings.NOTIFICATIONS);
        Type type = new TypeToken<ArrayList<toml.uaeexchange.models.Notification>>() {
        }.getType();
        List<toml.uaeexchange.models.Notification> notificationList = null;

        try {
            notificationList = gson.fromJson(json, type);
        } catch (Exception e) {
            notificationList = new ArrayList<>();
        }

        if (notificationList == null)
            notificationList = new ArrayList<>();

        notificationList.add(0, notification);

        String c = (AppSettings.getData(this, AppSettings.NOTIFICATION_COUNT).equals("")) ? "0" : AppSettings.getData(this, AppSettings.NOTIFICATION_COUNT);
        int count = Integer.parseInt(c);
        count = count + 1;

        AppSettings.putData(this, AppSettings.NOTIFICATION_COUNT, count + "");


        json = gson.toJson(notificationList);
        LogUtils.Verbose("TAG", " Notification JSON " + json);
        AppSettings.putData(this, AppSettings.NOTIFICATIONS, json);
        LogUtils.Verbose("TAG", " notification.getMessage " + notification.getMessage());
        handleDataMessage(notification.getMessage());

        Intent intent = new Intent("message");
        intent.putExtra("type", 2);
        sendBroadcast(intent);

    }


    private void handleDataMessage(String messageBody) {

        LogUtils.Verbose(TAG, "push json: " + messageBody);
        Intent intent = null;

        if (!isAppIsInBackground(this)) {
            intent = new Intent(this, LoginActivity.class);
        } else {
            intent = new Intent(this, NotificationActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);


        Notification notification = mBuilder.setSmallIcon(R.drawable.ic_launcher).setTicker(getString(R.string.app_name)).setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentIntent(pendingIntent)
                .setGroup(getString(R.string.app_name))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
                .setContentText(messageBody).build();


//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.mipmap.mwallet_logo)
//                .setContentTitle(GlobalVariables.NOTIFICATION_TITLE)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);

        int id = ((int) System.currentTimeMillis());
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (AppSettings.getBooleanData(this, AppSettings.NOTIFICATION_ACTIVE)) {
            notificationManager.notify(id, notification);
        } else {
            notificationManager.cancel(id);
        }

    }


    // [END receive_message]


    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = true;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = true;
            }
        }

        return isInBackground;
    }
}
