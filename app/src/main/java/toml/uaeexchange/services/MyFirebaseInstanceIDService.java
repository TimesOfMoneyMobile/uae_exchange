package toml.uaeexchange.services;

import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.simpleframework.xml.Serializer;

import java.util.Map;


import toml.movitwallet.controllers.NotificationController;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.dialogs.OkDialog;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {



    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    private Serializer serializer;
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]


    @Override
    public void onCreate() {
        super.onCreate();

        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        LogUtils.Verbose(TAG,"refreshedToken"+refreshedToken);



        if(refreshedToken!=null)
        {
            Runnable r=new Runnable() {
                @Override
                public void run() {
                    sendRegistrationToServer(refreshedToken);
                }
            };

            new Thread(r).start();
        }

    }


    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.

        MovitWalletController movitWalletController = new NotificationController(token);
        movitWalletController.init(new IMovitWalletController<Bundle>() {
            @Override
            public void onSuccess(Bundle response) {
                AppSettings.putData(MyFirebaseInstanceIDService.this, AppSettings.isTOKENSENDTOSERVER, "true");
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                //((BaseActivity)getApplicationContext()).showError(reason);
                //new OkDialog(getBaseContext(),reason,null,null);
            }
        });

    }
}