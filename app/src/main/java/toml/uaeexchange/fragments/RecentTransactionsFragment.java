package toml.uaeexchange.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import toml.movitwallet.controllers.RecentTransactionsController;
import toml.movitwallet.models.DateHeaderItem;
import toml.movitwallet.models.ListItem;
import toml.movitwallet.models.Transaction;
import toml.movitwallet.models.TransactionHistoryResponse;
import toml.movitwallet.models.TransactionItem;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.adapters.NewTransactionsHistoryAdapter;
import toml.uaeexchange.adapters.RecentTransactionsAdapter;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 9/8/2017.
 */

public class RecentTransactionsFragment extends Fragment {

    @BindView(R.id.frag_rt_lv_transactions)
    ListView lvTransactions;

    @BindView(R.id.frag_rt_rv_transactions)
    RecyclerView rvTransactions;

    @BindView(R.id.fraf_rt_tv_no_transactions)
    TextView tvNoTransactions;
    @BindView(R.id.frag_rt_pb_progressBar)
    ProgressBar pbProgress;

    private Context mContext;
    private View mView;
    Unbinder unbinder;
    RecentTransactionsAdapter recentTransactionsAdapter;


    RecentTransactionsFragment recentTransactionsFragment;
    private int mIndex;


    List<ListItem> consolidatedList = new ArrayList<>();


    public RecentTransactionsFragment() {
    }


    public RecentTransactionsFragment(int index) {
        mIndex = index;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_recent_transactions, null, false);
        recentTransactionsFragment = this;
        unbinder = ButterKnife.bind(this, mView);
        return mView;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handleUIOnCreate();
    }

    private void handleUIOnCreate() {
        pbProgress.setVisibility(View.VISIBLE);
        tvNoTransactions.setVisibility(View.GONE);

        // All
        if (mIndex == 1) {
            callAllRecentTransactions();
        }
        // Send
        else if (mIndex == 2) {
            callSentRecentTransactions();
        }
        // Received
        else if (mIndex == 3) {
            callReceivedRecentTransactions();
        }
        // Added
        else if (mIndex == 4) {
            callAddedRecentTransactions();
        }
        // Refresh Txn History API;
        else if (mIndex == 100) {
            callRefreshTransactionsApi();
        }
        // Send Money Fragment
        else if (mIndex == 99) {
            callRecentTransactionsApi();
        }
    }

    private void callRefreshTransactionsApi() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                callAllRecentTransactions();
            }
        }, 200);

    }


    /**
     * All transactions
     */
    public void callAllRecentTransactions() {
        MovitWalletController movitWalletController = new RecentTransactionsController("");
        movitWalletController.init(new IMovitWalletController<TransactionHistoryResponse>() {

            @Override
            public void onSuccess(TransactionHistoryResponse response) {
                setTransactionHistoryAdapter(response, "", "");
            }

            @Override
            public void onFailed(String errorCode, String reason) {

                setTransactionHistoryAdapter(null, errorCode, reason);
            }
        });
    }

    /**
     * Sent transactions
     */
    public void callSentRecentTransactions() {
        MovitWalletController movitWalletController = new RecentTransactionsController("SEND");
        movitWalletController.init(new IMovitWalletController<TransactionHistoryResponse>() {

            @Override
            public void onSuccess(TransactionHistoryResponse response) {
                setTransactionHistoryAdapter(response, "", "");
            }

            @Override
            public void onFailed(String errorCode, String reason) {

                setTransactionHistoryAdapter(null, errorCode, reason);
            }
        });
    }

    /**
     * Received transactions
     */
    public void callReceivedRecentTransactions() {
        MovitWalletController movitWalletController = new RecentTransactionsController("RECEIVED");
        movitWalletController.init(new IMovitWalletController<TransactionHistoryResponse>() {

            @Override
            public void onSuccess(TransactionHistoryResponse response) {
                setTransactionHistoryAdapter(response, "", "");
            }

            @Override
            public void onFailed(String errorCode, String reason) {

                setTransactionHistoryAdapter(null, errorCode, reason);
            }
        });
    }

    /**
     * Added transactions
     */
    public void callAddedRecentTransactions() {
        MovitWalletController movitWalletController = new RecentTransactionsController("ADD");
        movitWalletController.init(new IMovitWalletController<TransactionHistoryResponse>() {


            @Override
            public void onSuccess(TransactionHistoryResponse response) {
                setTransactionHistoryAdapter(response, "", "");
            }

            @Override
            public void onFailed(String errorCode, String reason) {

                setTransactionHistoryAdapter(null, errorCode, reason);
            }
        });
    }


    private void callRecentTransactionsApi() {
        MovitWalletController movitWalletController = new RecentTransactionsController("P2P");
        movitWalletController.init(new IMovitWalletController<List<Transaction>>() {

            @Override
            public void onSuccess(List<Transaction> response) {

                if (isAdded())
                    recentTransactionsFragment.setAdapter(response, "", "");
            }

            @Override
            public void onFailed(String errorCode, String reason) {

                if (isAdded())
                    recentTransactionsFragment.setAdapter(null, errorCode, reason);
            }
        });
    }


    public void setAdapter(List<Transaction> response, String errorCode, String reason) {
        if (response != null) {
            pbProgress.setVisibility(View.GONE);
            tvNoTransactions.setVisibility(View.GONE);

            recentTransactionsAdapter = new RecentTransactionsAdapter(mContext, recentTransactionsFragment, response);
            lvTransactions.setAdapter(recentTransactionsAdapter);

        } else {

            pbProgress.setVisibility(View.GONE);
            tvNoTransactions.setVisibility(View.VISIBLE);
            tvNoTransactions.setText(reason);

        }

    }


    public void setTransactionHistoryAdapter(TransactionHistoryResponse response, String errorCode, String reason) {
        if (isAdded()) {
            if (response != null) {
                pbProgress.setVisibility(View.GONE);
                tvNoTransactions.setVisibility(View.GONE);


                Map<String, List<Transaction>> groupedHashMap = groupDataIntoHashMap(response.getResponseDetails().getTransactions().getTransactionsList());

                if (consolidatedList != null && consolidatedList.size() > 0) {
                    consolidatedList.clear();
                }

                for (String date : groupedHashMap.keySet()) {
                    DateHeaderItem dateItem = new DateHeaderItem();
                    dateItem.setDate(date);
                    consolidatedList.add(dateItem);


                    for (Transaction objTransaction : groupedHashMap.get(date)) {
                        TransactionItem transactionItem = new TransactionItem();
                        transactionItem.setTransaction(objTransaction);//setBookingDataTabs(bookingDataTabs);
                        consolidatedList.add(transactionItem);
                    }
                }

                NewTransactionsHistoryAdapter newadapter = new NewTransactionsHistoryAdapter(mContext, consolidatedList);
                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                rvTransactions.setLayoutManager(layoutManager);
                rvTransactions.setAdapter(newadapter);
                rvTransactions.setVisibility(View.VISIBLE);

            } else {

                pbProgress.setVisibility(View.GONE);

                tvNoTransactions.setVisibility(View.VISIBLE);
                tvNoTransactions.setText(reason);

            }
        }

    }

    private Map<String, List<Transaction>> groupDataIntoHashMap(List<Transaction> inputList) {

        Map<String, List<Transaction>> groupedHashMap = new TreeMap<>();


        for (Transaction objTransaction : inputList) {


            String hashMapKey = Constants.getFormattedDate(Constants.SERVER_DATE_FROMAT, "MMMM yyyy", objTransaction.getTxnDate());

            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the transaction object
                // against the existing key.
                groupedHashMap.get(hashMapKey).add(objTransaction);
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                List<Transaction> list = new ArrayList<>();
                list.add(objTransaction);
                groupedHashMap.put(hashMapKey, list);
            }

        }

        return groupedHashMap;


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
