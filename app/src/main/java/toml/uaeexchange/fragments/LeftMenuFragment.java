package toml.uaeexchange.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import toml.movitwallet.controllers.CountController;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.AboutWayve;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.activities.ChangeMpinActivity;
import toml.uaeexchange.activities.DashboardActivity;
import toml.uaeexchange.activities.LoginActivity;
import toml.uaeexchange.activities.OffersActivity;
import toml.uaeexchange.activities.PendingRequestActivity;
import toml.uaeexchange.activities.ProfileActivity;
import toml.uaeexchange.activities.RegisterActivity;
import toml.uaeexchange.activities.RequestMoneyActivity;
import toml.uaeexchange.activities.SettingsActivity;
import toml.uaeexchange.activities.TransactionHistoryActivity;
import toml.uaeexchange.dialogs.YesNoDialog;
import toml.uaeexchange.nearbystores.NearByActivity;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 8/28/2017.
 */

public class LeftMenuFragment extends Fragment {

    //Navigation View
    @BindView(R.id.rlGoBackHome)
    RelativeLayout rlGoBackHome;

    @BindView(R.id.rlUserProfile)
    RelativeLayout rlUserProfile;

    @BindView(R.id.rl_for_name_email)
    RelativeLayout rlForNameEmail;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.tvUserEmail)
    TextView tvUserEmail;

    @BindView(R.id.rl_for_profile_completion)
    RelativeLayout rlForProfieCompletion;
    @BindView(R.id.ivProfieCompletion)
    ImageView ivProfieCompletion;

    @BindView(R.id.rl_for_login)
    RelativeLayout rlForLogin;

    @BindView(R.id.btnSignUp)
    Button btnSignUp;
    @BindView(R.id.btnLogin)
    Button btnLogin;


    //Sub-List
    @BindView(R.id.menu_rl_my_profile)
    RelativeLayout rlMyProfile;
    @BindView(R.id.menu_rl_manage_beneficaries)
    RelativeLayout rlManageBeneficaries;
    @BindView(R.id.menu_rl_transaction_history)
    RelativeLayout rlTransactionHistory;
    @BindView(R.id.menu_rl_request_money)
    RelativeLayout rlRequestMoney;
    @BindView(R.id.menu_rl_pending_requests)
    RelativeLayout rlPendingRequets;
    @BindView(R.id.menu_rl_offers)
    RelativeLayout rlOffers;
    @BindView(R.id.menu_rl_favourites)
    RelativeLayout rlFavourites;
    @BindView(R.id.menu_rl_change_mpin)
    RelativeLayout rlChangeMpin;
    @BindView(R.id.menu_rl_near_by)
    RelativeLayout rlNearBy;
    @BindView(R.id.menu_rl_about_arapay)
    RelativeLayout rlAboutArapay;
    @BindView(R.id.menu_rl_settings)
    RelativeLayout rlSettings;
    @BindView(R.id.menu_rl_terms_and_conditions)
    RelativeLayout rlTermsAndConditions;
    @BindView(R.id.menu_rl_logout)
    RelativeLayout rlLogout;
    @BindView(R.id.tvLogout)
    TextView tvLogout;
    @BindView(R.id.txtvwPendingRequestCount)
    TextView txtvwPendingRequestCount;
    @BindView(R.id.txtvwOfferCount)
    TextView txtvwOfferCount;

    private View mView;
    Unbinder unbinder;
    private Context mContext;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_left_menu, null, false);
        unbinder = ButterKnife.bind(this, mView);


        return mView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        handleLayout();
    }

    private void handleLayout() {

        if (Constants.isUserLoggedIn(mContext)) {
            tvUserName.setText(AppSettings.getData(mContext, AppSettings.FIRST_NAME) + " " + AppSettings.getData(mContext, AppSettings.LAST_NAME));
            tvUserEmail.setVisibility(View.VISIBLE);
            tvUserEmail.setText(AppSettings.getData(mContext, AppSettings.EMAIL_ID));
            rlForProfieCompletion.setVisibility(View.VISIBLE);

            rlForLogin.setVisibility(View.GONE);
            rlMyProfile.setVisibility(View.VISIBLE);
            rlTransactionHistory.setVisibility(View.VISIBLE);
            rlRequestMoney.setVisibility(View.VISIBLE);
            rlPendingRequets.setVisibility(View.VISIBLE);
            rlFavourites.setVisibility(View.VISIBLE);
            rlChangeMpin.setVisibility(View.VISIBLE);
            rlNearBy.setVisibility(View.VISIBLE);
            rlLogout.setVisibility(View.VISIBLE);

            if (!TextUtils.isEmpty(MovitConsumerApp.getInstance().getKycStatus())) {
                if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("N")) {
                    ivProfieCompletion.setImageResource(R.drawable.thirty_three);
                    handleUIForNonKYC();
                } else if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("P")) {
                    ivProfieCompletion.setImageResource(R.drawable.sixty_six);
                    handleUIForNonKYC();
                }
            }


            callGetCountAPI();


        } else {
            tvUserName.setText(getString(R.string.welcome_guest));
            rlForLogin.setVisibility(View.VISIBLE);

            tvUserEmail.setVisibility(View.GONE);
            rlForProfieCompletion.setVisibility(View.GONE);
            rlMyProfile.setVisibility(View.GONE);
            rlTransactionHistory.setVisibility(View.GONE);
            rlRequestMoney.setVisibility(View.GONE);
            rlPendingRequets.setVisibility(View.GONE);
            rlFavourites.setVisibility(View.GONE);
            rlChangeMpin.setVisibility(View.GONE);
            rlNearBy.setVisibility(View.GONE);
            rlLogout.setVisibility(View.GONE);
          //  rlOffers.setVisibility(View.GONE);
           // rlSettings.setVisibility(View.GONE);

        }

    }

    private void handleUIForNonKYC() {
        rlMyProfile.setClickable(false);
        rlMyProfile.setEnabled(false);

        rlTransactionHistory.setClickable(false);
        rlTransactionHistory.setEnabled(false);

        rlRequestMoney.setClickable(false);
        rlRequestMoney.setEnabled(false);

        rlPendingRequets.setClickable(false);
        rlPendingRequets.setEnabled(false);

        rlFavourites.setClickable(false);
        rlFavourites.setEnabled(false);


//        if (!TextUtils.isEmpty(MovitConsumerApp.getInstance().getKycStatus())) {
//            if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("N")) {
//                ivProfieCompletion.setImageResource(R.drawable.thirty_three);
//                rlChangeMpin.setClickable(true);
//                rlChangeMpin.setEnabled(true);
//            } else if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("P")) {
//                rlChangeMpin.setClickable(false);
//                rlChangeMpin.setEnabled(false);
//
//            }
//        }



        rlNearBy.setClickable(false);
        rlNearBy.setEnabled(false);

        rlSettings.setClickable(true);
        rlSettings.setEnabled(true);


    }


    private void callGetCountAPI() {

        MovitWalletController movitWalletController = new CountController();
        movitWalletController.init(new IMovitWalletController<Bundle>() {
            @Override
            public void onSuccess(Bundle response) {

                if (response != null) {

                    if (Integer.parseInt(response.getString(IntentConstants.PendingRequest)) > 0) {
                        txtvwPendingRequestCount.setVisibility(View.VISIBLE);
                        txtvwPendingRequestCount.setText(response.getString(IntentConstants.PendingRequest));
                    }
                    if (Integer.parseInt(response.getString(IntentConstants.OffersCount)) > 0) {
                        txtvwOfferCount.setVisibility(View.VISIBLE);
                        txtvwOfferCount.setText(response.getString(IntentConstants.OffersCount));
                    }
                } else {
                    txtvwPendingRequestCount.setVisibility(View.GONE);
                    txtvwOfferCount.setVisibility(View.GONE);
                }


            }

            @Override
            public void onFailed(String errorCode, String reason) {
                txtvwPendingRequestCount.setVisibility(View.GONE);
                txtvwOfferCount.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handleLayout();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.rlGoBackHome,
            R.id.btnLogin,
            R.id.btnSignUp,
            R.id.menu_rl_my_profile,
            R.id.menu_rl_manage_beneficaries,
            R.id.menu_rl_transaction_history,
            R.id.menu_rl_request_money,
            R.id.menu_rl_pending_requests,
            R.id.menu_rl_offers,
            R.id.menu_rl_change_mpin,
            R.id.menu_rl_near_by,
            R.id.menu_rl_about_arapay,
            R.id.menu_rl_settings,
            R.id.menu_rl_terms_and_conditions,
            R.id.menu_rl_logout
    })
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.rlGoBackHome:
                getActivity().onBackPressed();

                break;

            case R.id.btnLogin:
                BaseActivity.openNewActivity(mContext, LoginActivity.class.getCanonicalName(), false);
                break;

            case R.id.btnSignUp:
                BaseActivity.openNewActivity(mContext, RegisterActivity.class.getCanonicalName(), false);
                break;

            case R.id.menu_rl_my_profile:
                BaseActivity.openNewActivity(mContext, ProfileActivity.class.getCanonicalName(), true);
                break;

            case R.id.menu_rl_manage_beneficaries:

                break;

            case R.id.menu_rl_transaction_history:
                BaseActivity.openNewActivity(mContext, TransactionHistoryActivity.class.getCanonicalName(), true);

                break;

            case R.id.menu_rl_request_money:
                BaseActivity.openNewActivity(mContext, RequestMoneyActivity.class.getCanonicalName(), true);
                break;

            case R.id.menu_rl_pending_requests:
                BaseActivity.openNewActivity(mContext, PendingRequestActivity.class.getCanonicalName(), true);
                break;

            case R.id.menu_rl_offers:
                BaseActivity.openNewActivity(mContext, OffersActivity.class.getCanonicalName(), false);
                break;

            case R.id.menu_rl_change_mpin:
                BaseActivity.openNewActivity(mContext, ChangeMpinActivity.class.getCanonicalName(), true);
                break;

            case R.id.menu_rl_near_by:
                BaseActivity.openNewActivity(mContext, NearByActivity.class.getCanonicalName(), true);
                break;


            case R.id.menu_rl_about_arapay:
                BaseActivity.openNewActivity(mContext, AboutWayve.class.getCanonicalName(), false);

                break;

            case R.id.menu_rl_settings:
                BaseActivity.openNewActivity(mContext, SettingsActivity.class.getCanonicalName(), false);
                break;

            case R.id.menu_rl_terms_and_conditions:

                Intent intent = new Intent(mContext, AboutWayve.class);
                intent.putExtra("terms", true);
                mContext.startActivity(intent);

                break;

            case R.id.menu_rl_logout:
                if (tvLogout.getText().equals(getString(R.string.logout))) {
                    new YesNoDialog(mContext, mContext.getString(R.string.are_you_sure_you_want_to_logout), null, new YesNoDialog.IYesNoDialogCallback() {
                        @Override
                        public void handleResponse(int responsecode) {
                            if (responsecode == 1) {
                                ((DashboardActivity) getActivity()).handleLogout();
                            }
                        }
                    });

                } else {
                    BaseActivity.openNewActivity(mContext, LoginActivity.class.getCanonicalName(), false);
                }
                break;
        }

    }


}
