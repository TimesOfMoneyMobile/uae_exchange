package toml.uaeexchange.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import toml.movitwallet.controllers.FavouritesController;
import toml.movitwallet.models.Beneficiary;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.adapters.FavouritesAdapter;

/**
 * Created by vishwanathp on 9/8/2017.
 */

public class FavouritesFragment extends Fragment {

    private Context mContext;
    private View mView;
    Unbinder unbinder;

    @BindView(R.id.frag_fav_lv_favourites)
    ListView lvFavourites;
    @BindView(R.id.fraf_fav_tv_no_favourites)
    TextView tvNoFavourites;
    @BindView(R.id.frag_fav_pb_progressBar)
    ProgressBar pbProgress;

    FavouritesAdapter favouritesAdapter;

    FavouritesFragment favouritesFragment;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_favourites, null, false);
        favouritesFragment = this;
        unbinder = ButterKnife.bind(this, mView);
        return mView;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        pbProgress.setVisibility(View.VISIBLE);
        tvNoFavourites.setVisibility(View.GONE);


        if (isAdded())
            callFavouritesApi();

    }

    private void callFavouritesApi() {
        MovitWalletController movitWalletController = new FavouritesController("P2P");
        movitWalletController.init(new IMovitWalletController<List<Beneficiary>>() {


            @Override
            public void onSuccess(List<Beneficiary> response) {
                favouritesFragment.setFavAdapter(response, "", "");
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                favouritesFragment.setFavAdapter(null, errorCode, reason);

            }
        });
    }


    public void setFavAdapter(List<Beneficiary> response, String errorCode, String reason) {

        if (isAdded()) {
            LogUtils.Verbose("Beneficiary setFavAdapter", response + "");
            if (response != null) {


                LogUtils.Verbose("Beneficiary setFavAdapter", "if");
                pbProgress.setVisibility(View.GONE);
                tvNoFavourites.setVisibility(View.GONE);

                favouritesAdapter = new FavouritesAdapter(mContext, favouritesFragment, response);
                lvFavourites.setAdapter(favouritesAdapter);

            } else {

                LogUtils.Verbose("Beneficiary setFavAdapter", "else");
                pbProgress.setVisibility(View.GONE);
                tvNoFavourites.setVisibility(View.VISIBLE);
                tvNoFavourites.setText(reason);

            }
        }

    }

//    public static FavouritesFragment getInstance(){
//        return favouritesFragment;
//    }

    @Override
    public void onDetach() {
        LogUtils.Verbose("FavouritesFragment", "onDetach");
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
