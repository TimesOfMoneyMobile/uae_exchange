package toml.uaeexchange.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import toml.movitwallet.controllers.GetIdentificationDocsController;
import toml.movitwallet.controllers.GetNationalityListController;
import toml.movitwallet.models.Document;
import toml.movitwallet.models.Nationality;
import toml.movitwallet.models.RegistrationRequest;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.KYCRegistrationActivity;
import toml.uaeexchange.adapter.NationalityListAdapter;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;

import static toml.uaeexchange.R.id.edtxtNationalitySearch;


public class KYCRegistrationStep1Fragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.edtxtLayoutEmitatesID)
    CustomEditTextLayout edtxtLayoutEmitatesID;
    @BindView(R.id.errorLabelEmiratesID)
    ErrorLabelLayout errorLabelEmiratesID;
    @BindView(R.id.edtxtLayoutNationality)
    CustomEditTextLayout edtxtLayoutNationality;
    @BindView(R.id.errorLabelNationality)
    ErrorLabelLayout errorLabelNationality;
    @BindView(R.id.edtxtLayoutDateOfBirth)
    CustomEditTextLayout edtxtLayoutDateOfBirth;
    @BindView(R.id.errorLabelDateOfBirth)
    ErrorLabelLayout errorLabelDateOfBirth;
    @BindView(R.id.btnContinue)
    Button btnContinue;

    Context mContext;
    @BindView(R.id.toolbar_pay)
    Toolbar toolbarPay;


    private EditText edtEmiratesID;
    private EditText edtNationality;
    private EditText edtDOB;
    private ProgressDialog progressDialog;


    List<Nationality> allNationalityList = new ArrayList<>();
    List<Nationality> searchNationalityList = new ArrayList<>();
    NationalityListAdapter nationalityListAdapter;

    String selectedeDate = "";
    RegistrationRequest objRegistrationRequest;


    public KYCRegistrationStep1Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment KYCRegistrationStep1Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static KYCRegistrationStep1Fragment newInstance(String param1, String param2) {
        KYCRegistrationStep1Fragment fragment = new KYCRegistrationStep1Fragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kycregistration_step1, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //MovitConsumerApp.getInstance().setMobileNumber("1234567890");

        ((KYCRegistrationActivity) getActivity()).setToolbar(toolbarPay, "KYC Registration", R.drawable.icon_back, 0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().onBackPressed();

            }
        }, null);

        progressBar.setProgress(5);




      /*  if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

            Drawable drawableProgress = DrawableCompat.wrap(progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(drawableProgress, ContextCompat.getColor(getContext(), R.color.aqua_green));
            progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(drawableProgress));

        } else {
            progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(),  R.color.aqua_green), PorterDuff.Mode.SRC_IN);
        }*/


        // Setting text watcher here
        edtxtLayoutEmitatesID.getEditText().addTextChangedListener(textWatcher);
        edtxtLayoutNationality.getEditText().addTextChangedListener(textWatcher);
        edtxtLayoutDateOfBirth.getEditText().addTextChangedListener(textWatcher);


        edtxtLayoutEmitatesID.getEditText().addTextChangedListener(new PatternedTextWatcher("###-####-#######-#"));


        /* edtxtLayoutEmitatesID.getEditText().addTextChangedListener(new AutoAddTextWatcher(edtxtLayoutEmitatesID.getEditText(), "-", new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                LogUtils.Verbose("TAG", "Fragment onTextChanged()");

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        }, 3, 8, 16));*/

        // Setting error labels here
        edtxtLayoutEmitatesID.setErrorLabelLayout(errorLabelEmiratesID);
        edtxtLayoutNationality.setErrorLabelLayout(errorLabelNationality);
        edtxtLayoutDateOfBirth.setErrorLabelLayout(errorLabelDateOfBirth);


        edtEmiratesID = edtxtLayoutEmitatesID.getEditText();
        edtNationality = edtxtLayoutNationality.getEditText();
        edtDOB = edtxtLayoutDateOfBirth.getEditText();

       /* int maxEmiratedIDLength = 15;
        edtEmiratesID.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxEmiratedIDLength)});*/


        edtxtLayoutEmitatesID.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutEmitatesID.getImgLeft().setVisibility(View.GONE);

        edtxtLayoutNationality.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutNationality.getImgLeft().setVisibility(View.GONE);
        edtxtLayoutNationality.getImgRight().setVisibility(View.VISIBLE);
        edtxtLayoutNationality.getImgRight().setImageResource(R.drawable.down_arrow_unselected_reg);

        edtxtLayoutDateOfBirth.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutDateOfBirth.getImgLeft().setVisibility(View.GONE);

        edtxtLayoutDateOfBirth.getImgRight().setVisibility(View.VISIBLE);
        edtxtLayoutDateOfBirth.getImgRight().setImageResource(R.drawable.date_of_birth_icon);

        edtNationality.setInputType(InputType.TYPE_NULL);
        edtNationality.setFocusable(false);

        edtNationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showNationalityDialog();


            }
        });

        edtxtLayoutNationality.getImgRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNationalityDialog();
            }
        });


        edtDOB.setInputType(InputType.TYPE_NULL);
        edtDOB.setFocusable(false);
        edtDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtxtLayoutDateOfBirth.performClick();
            }
        });
        edtxtLayoutDateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar cal = Calendar.getInstance(Locale.ENGLISH);
              /* // This is alternative
                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 18);*/
                cal.add(Calendar.YEAR, -18);
                DatePickerDialog datePicker = new DatePickerDialog(mContext, R.style.CustomDatePickerDialogTheme,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                                month = month + 1;
                                String strMonth = month > 9 ? month + "" : "0" + month;
                                String strDay = dayOfMonth > 9 ? dayOfMonth + "" : "0" + dayOfMonth;

                                selectedeDate = strDay + "-" + strMonth + "-" + year;

                                String formattedDate = "";//Constants.getFormattedDate("dd-MM-yyyy", "dd MMMM, yyyy", date);

                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d");
                                String day = null;
                                try {
                                    day = simpleDateFormat.format(sdf.parse(selectedeDate));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                                if (day.endsWith("1"))
                                    simpleDateFormat = new SimpleDateFormat("d'st' MMMM, yyyy");
                                else if (day.endsWith("2"))
                                    simpleDateFormat = new SimpleDateFormat("d'nd' MMMM, yyyy");
                                else if (day.endsWith("3"))
                                    simpleDateFormat = new SimpleDateFormat("d'rd' MMMM, yyyy");
                                else
                                    simpleDateFormat = new SimpleDateFormat("d'th' MMMM, yyyy");

                                try {
                                    formattedDate = simpleDateFormat.format(sdf.parse(selectedeDate));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                edtxtLayoutDateOfBirth.setText(selectedeDate);
                                edtxtLayoutDateOfBirth.getImgRight().setImageResource(R.drawable.date_of_birth_selected);
                            }
                        },
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());

                datePicker.show();

            }
        });


        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String emiratesID = edtEmiratesID.getText().toString().trim();
                emiratesID = emiratesID.replaceAll("-", "");

                if (emiratesID.length() < 15) {
                    edtxtLayoutEmitatesID.getErrorLabelLayout().setError("Please enter valid Emirates ID.");
                    edtxtLayoutEmitatesID.setBackgroundResource(R.drawable.custom_edittext_border_red);
                    edtxtLayoutEmitatesID.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(mContext, R.color.grapefruit));

                } else if (TextUtils.isEmpty(edtNationality.getText().toString().trim())) {
                    edtxtLayoutNationality.getErrorLabelLayout().setError("Please select your Nationality.");
                    edtxtLayoutNationality.setBackgroundResource(R.drawable.custom_edittext_border_red);
                    edtxtLayoutNationality.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(mContext, R.color.grapefruit));

                } else if (TextUtils.isEmpty(selectedeDate)) {
                    edtxtLayoutDateOfBirth.getErrorLabelLayout().setError("Please select your Date of Birth.");
                    edtxtLayoutDateOfBirth.setBackgroundResource(R.drawable.custom_edittext_border_red);
                    edtxtLayoutDateOfBirth.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(mContext, R.color.grapefruit));

                } else {

                    objRegistrationRequest.setEmiratesID(emiratesID);
                    objRegistrationRequest.setNationality(edtNationality.getText().toString().trim());
                    objRegistrationRequest.setDateOfBirth(selectedeDate);
                    //LogUtils.Verbose("EID", objRegistrationRequest.getEmiratesID());

                    KYCRegistrationActivity.thisActivity.getPager().setCurrentItem(1);
                }

              /*  if (edtEmirateID.getText().toString().trim().equalsIgnoreCase("test")) {
                    emiratesIDWrapper.setError("Please enter emirated ID.");
                    //edtEmirateID.setError("Please enter emirated ID.");
                } else {

                    KYCRegistrationActivity.thisActivity.getPager().setCurrentItem(1);
                }*/

            }
        });

        showProgress();


        GetNationalityListController nationalityListController = new GetNationalityListController();
        nationalityListController.init(new IMovitWalletController<List<Nationality>>() {
            @Override
            public void onSuccess(List<Nationality> list) {

                //  dismissProgress();

                LogUtils.Verbose("TAG", "in callNationalityList()");
                allNationalityList.clear();
                allNationalityList.addAll(list);

                callGetDocsAPI();

            }

            @Override
            public void onFailed(String errorCode, String reason) {

                dismissProgress();

                Toast.makeText(mContext, reason, Toast.LENGTH_LONG).show();

            }
        });


    }

    private void showNationalityDialog() {
        searchNationalityList.clear();
        searchNationalityList.addAll(allNationalityList);

        openNationalityListDialog(searchNationalityList);
    }

    @Override
    public void onResume() {
        super.onResume();

        // This is to resolve text overlapping issue in CustomTextLayout

        objRegistrationRequest = ((KYCRegistrationActivity) getActivity()).getRegistrationRequestObject();

        if (objRegistrationRequest != null) {
            if (!TextUtils.isEmpty(objRegistrationRequest.getEmiratesID())) {
                edtxtLayoutEmitatesID.setText(objRegistrationRequest.getEmiratesID());
            }
            if (!TextUtils.isEmpty(objRegistrationRequest.getNationality())) {
                edtxtLayoutNationality.setText(objRegistrationRequest.getNationality());
            }
            if (!TextUtils.isEmpty(objRegistrationRequest.getEmiratesID())) {
                edtxtLayoutDateOfBirth.setText(objRegistrationRequest.getDateOfBirth());
            }

        }

    }

    private void callGetDocsAPI() {
        MovitWalletController getIdentificationDocsController = new GetIdentificationDocsController();

        getIdentificationDocsController.init(new IMovitWalletController<List<Document>>() {
            @Override
            public void onSuccess(List<Document> response) {

                dismissProgress();

                if (response.size() > 0) {


                    ArrayList<Document> documents = (ArrayList<Document>) response;
                    //Replace underscore(_) from doc name
                    for (Document document : documents) {
                        document.setName(document.getName().replaceAll("_", " "));
                    }
                    KYCRegistrationActivity.thisActivity.setDocuments(documents);

                } else {
                    // txtvwNoOffers.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailed(String errorCode, String reason) {

                dismissProgress();

            }
        });
    }

    public void setToolbar(String titleText, int leftImageResourceId, int rightImageResourceId, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener) {
        //  Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_pay);
        getActivity().setTitle(null);
        if (!TextUtils.isEmpty(titleText)) {
            TextView title = (TextView) toolbarPay.findViewById(R.id.title_pay);
            title.setText(titleText);
        } else if (titleText.equalsIgnoreCase(getString(R.string.menu_nearby))) {
            RelativeLayout relSettings = (RelativeLayout) getActivity().findViewById(R.id.relSettings);
            relSettings.setVisibility(View.VISIBLE);
        }

        // ((KYCRegistrationActivity) getActivity()).setSupportActionBar(toolbar);

        ImageView imgvwLeft, imgvwRight;

        imgvwLeft = (ImageView) getActivity().findViewById(R.id.imgvwLeft);
        imgvwRight = (ImageView) getActivity().findViewById(R.id.imgvwRight);

        // For left image

        if (leftImageResourceId > 0) {

            imgvwLeft.setImageResource(leftImageResourceId);

            if (imgvwLeft != null) {
                imgvwLeft.setOnClickListener(leftClickListener);
            }
        } else {
            imgvwLeft.setVisibility(View.INVISIBLE);
        }

        // For right  image

        if (rightImageResourceId > 0) {

            imgvwRight.setImageResource(rightImageResourceId);

            if (imgvwRight != null) {
                imgvwRight.setOnClickListener(rightClickListener);
            }
        } else {
            imgvwRight.setVisibility(View.INVISIBLE);
        }


    }

    public void openNationalityListDialog(final List<Nationality> listData) {


        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_nationality_list, null);

        nationalityListAdapter = new NationalityListAdapter(mContext, listData);

        final Dialog nationalityDialog = new Dialog(mContext);

        EditText edtxtSearch;
        ListView lstvwSecurityQuestions;

        lstvwSecurityQuestions = (ListView) view.findViewById(R.id.lstvwNationality);
        edtxtSearch = (EditText) view.findViewById(edtxtNationalitySearch);

        TextView title = (TextView) view.findViewById(R.id.title_pay);
        ImageView imgvwLeft = (ImageView) view.findViewById(R.id.imgvwLeft);
        ImageView imgvwRight = (ImageView) view.findViewById(R.id.imgvwRight);

        title.setText("Select Nationality");

        if (imgvwRight != null) {
            imgvwRight.setVisibility(View.GONE);
        }

        imgvwLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nationalityDialog.dismiss();
            }
        });

        lstvwSecurityQuestions.setAdapter(nationalityListAdapter);

        lstvwSecurityQuestions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                edtxtLayoutNationality.setText(listData.get(pos).getName());
                edtxtLayoutNationality.getImgRight().setImageResource(R.drawable.down_arrow_selected_reg);
                nationalityDialog.dismiss();


            }
        });


        edtxtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    searchNationalityList.clear();
                    searchNationalityList.addAll(allNationalityList);
                    nationalityListAdapter.notifyDataSetChanged();
                } else {
                    search(s.toString());
                }
            }
        });

        nationalityDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before setcontentview
        nationalityDialog.setContentView(view);
        nationalityDialog.setCancelable(true);
        nationalityDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        nationalityDialog.getWindow().setGravity(Gravity.CENTER);
        nationalityDialog.show();

    }

    @SuppressLint("DefaultLocale")
    private void search(String text) {
        searchNationalityList.clear();
        for (Nationality nationality : allNationalityList) {
            if (nationality.getName().toLowerCase(Locale.ENGLISH).contains(text.toLowerCase())) {
                searchNationalityList.add(nationality);
            }
        }
        nationalityListAdapter.notifyDataSetChanged();
    }


    public void showProgress() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setCanceledOnTouchOutside(false);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = getActivity().getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        progressDialog.show();

    }

    public void dismissProgress() {

        if (progressDialog != null && progressDialog.isShowing()) {
            LogUtils.Verbose("inside if", "dismissProgress......");
            progressDialog.dismiss();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;


    }


    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {


            if (checkForNonEmpty(new CustomEditTextLayout[]{edtxtLayoutEmitatesID, edtxtLayoutNationality, edtxtLayoutDateOfBirth})) {
                btnContinue.setEnabled(true);
                progressBar.setProgress(15);
            } else {
                btnContinue.setEnabled(false);
                progressBar.setProgress(5);
            }


        }
    };

    /**
     * This method returns true if any of the given edit texts has text. (length > 0)
     *
     * @param fields
     * @return
     */

    private boolean checkForNonEmpty(CustomEditTextLayout[] fields) {
        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i].getEditText();
            if (currentField.getText().toString().length() > 0) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
