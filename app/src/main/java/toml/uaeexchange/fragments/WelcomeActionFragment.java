package toml.uaeexchange.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import toml.uaeexchange.BuildConfig;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.activities.DashboardActivity;
import toml.uaeexchange.activities.LoginActivity;
import toml.uaeexchange.activities.RegisterActivity;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;


public class WelcomeActionFragment extends Fragment {


    Context mContext;

    @BindView(R.id.tvSigninLater)
    TextView tvSigninLater;
    Unbinder unbinder;
    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.btnSignUp)
    Button btnSignUp;


    public WelcomeActionFragment() {
        // Required empty public constructor
    }

    public static WelcomeActionFragment newInstance(String param1, String param2) {
        WelcomeActionFragment fragment = new WelcomeActionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_welcome_action, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);


        if (Constants.isDeviceRooted() && !BuildConfig.DEBUG) {
            if (isVisible() && isAdded()) {
                new OkDialog(mContext, getString(R.string.rooted_device_error), new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {
                        getActivity().finish();
                    }
                });

            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnSignIn, R.id.btnSignUp, R.id.tvSigninLater})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.btnSignUp:
                //  BaseActivity.openNewActivity(mContext, RegisterActivity.class);
                BaseActivity.openNewActivity(mContext, RegisterActivity.class.getCanonicalName(), false);

                // close  activity
                //getActivity().finish();
                break;
            case R.id.btnSignIn:
                BaseActivity.openNewActivity(mContext, LoginActivity.class.getCanonicalName(), false);
                //getActivity().finish();
                break;
            case R.id.tvSigninLater:
                BaseActivity.openNewActivity(mContext, DashboardActivity.class.getCanonicalName(), false);
                // getActivity().finish();
                break;
        }
    }
}
