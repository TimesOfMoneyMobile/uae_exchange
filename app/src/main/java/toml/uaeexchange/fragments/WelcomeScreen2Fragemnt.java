package toml.uaeexchange.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import toml.uaeexchange.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WelcomeScreen2Fragemnt#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WelcomeScreen2Fragemnt extends Fragment {

    Context mContext;

    public WelcomeScreen2Fragemnt() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WelcomeScreen1Fragemnt.
     */
    // TODO: Rename and change types and number of parameters
    public static WelcomeScreen2Fragemnt newInstance(String param1, String param2) {
        WelcomeScreen2Fragemnt fragment = new WelcomeScreen2Fragemnt();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_welcome_screen2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

}
