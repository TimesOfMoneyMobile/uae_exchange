package toml.uaeexchange.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 9/7/2017.
 */

public class SocialPaymentFragment extends Fragment {


    @BindView(R.id.frag_sp_tv_type_blink)
    TextView tvTypeBlink;

    @BindView(R.id.frag_sp_rl_copy_blink)
    RelativeLayout rlCopyBlink;
    @BindView(R.id.frag_sp_tv_copy_code)
    TextView tvCopyCode;

    @BindView(R.id.frag_sp_tv_text)
    TextView tvText;

    private Context mContext;
    private View mView;
    Unbinder unbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_social_payment, null, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        String strText = getString(R.string.type_blink_in_whatsapp);
//        SpannableString spanStrText = new SpannableString(strText);
//        ((BaseActivity) getActivity()).setColorAndClick(tvTypeBlink, strText, spanStrText, "‘Wayve’", ContextCompat.getColor(mContext, R.color.black));


//        String strBlinkHelps = getString(R.string.blink_helps_you_conveniently);
//        SpannableString spanStrBlinkHelps = new SpannableString(strBlinkHelps);
//        ((BaseActivity) getActivity()).setColorAndClick(tvText, strBlinkHelps, spanStrBlinkHelps, "Whatsapp!", ContextCompat.getColor(mContext, R.color.black));


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        if (mContext != null)
            Constants.hideKeyboard(mContext);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.frag_sp_rl_copy_blink, R.id.frag_sp_tv_copy_code})
    public void OnClickOfCopyText() {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(mContext.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(getString(R.string.wayve), "WAYVE");
        clipboard.setPrimaryClip(clip);


        Toast.makeText(mContext, getString(R.string.text_copied_to_clipbaord), Toast.LENGTH_SHORT).show();
    }


}
