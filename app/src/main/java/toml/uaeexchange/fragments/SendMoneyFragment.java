package toml.uaeexchange.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import toml.movitwallet.controllers.PayPersonController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.activities.ContactsListActivity;
import toml.uaeexchange.activities.P2MQRCodeScanActivity;
import toml.uaeexchange.activities.ReviewAndConfirmActivity;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.CustomTabLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;
import toml.uaeexchange.utilities.TwoDecimalInputFilter;

/**
 * Created by vishwanathp on 9/7/2017.
 */

public class SendMoneyFragment extends Fragment {

    private static final int REQUEST_CODE = 1;
    private String staticAmount = "500";
    private Context mContext;
    private View mView;
    Unbinder unbinder;
    GenericValidations genericValidations;

    String strMobileNumber, strAmount, strNickname, strSource;
    String favNumber, favAmount, favNickName;

    @BindView(R.id.frag_sm_sv_for_content)
    ScrollView svForContent;

    @BindView(R.id.frag_sm_el_MobileNumber)
    ErrorLabelLayout elMobileNumber;
    @BindView(R.id.frag_sm_et_MobileNumber)
    CustomEditTextLayout etMobileNumber;

    @BindView(R.id.frag_sm_el_Amount)
    ErrorLabelLayout elAmount;
    @BindView(R.id.frag_sm_et_Amount)
    CustomEditTextLayout etAmount;

    @BindView(R.id.frag_sm_el_NickName)
    ErrorLabelLayout elNickName;
    @BindView(R.id.frag_sm_et_NickName)
    CustomEditTextLayout etNickName;

    @BindView(R.id.frag_sm_rl_favourite)
    RelativeLayout rlFavourite;
    @BindView(R.id.frag_sm_iv_favourite)
    ImageView ivFavourite;
    boolean isFavChecked;

    @BindView(R.id.frag_sm_cb_proceed)
    Button cbProceed;


    @BindView(R.id.frag_sm_tab_layout)
    LinearLayout tabLayout;
    @BindView(R.id.frag_sm_viewpager)
    ViewPager viewPager;

    CustomTabLayout customTabLayout;

    FavouritesFragment favouritesFragment;
    RecentTransactionsFragment recentTransactionsFragment;
    private boolean perfromOnlyOnce;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_send_money, null, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        handleUI();
    }


    private void handleUI() {

        genericValidations = new GenericValidations(mContext);


        etMobileNumber.getImgLeft().setVisibility(View.GONE);
        etMobileNumber.getVerticalDivider().setVisibility(View.GONE);

        etMobileNumber.getImgRight().setImageResource(R.drawable.contacts);
        etMobileNumber.getImgRight().setVisibility(View.VISIBLE);

        etMobileNumber.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etMobileNumber.setErrorLabelLayout(elMobileNumber);
        etMobileNumber.getEditText().addTextChangedListener(textWatcher);

        etMobileNumber.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etAmount.getEditText().requestFocus();
                    return true;
                }
                return false;
            }
        });

        etMobileNumber.getImgRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show contacts list
                // SendMoneyFragmentPermissionsDispatcher.showContactListWithCheck(SendMoneyFragment.this);

                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_CONTACTS},
                            2);
                } else {
                    showContactList();
                }

            }
        });


        etAmount.getImgLeft().setVisibility(View.GONE);
        etAmount.getVerticalDivider().setVisibility(View.GONE);

        etAmount.setErrorLabelLayout(elAmount);
        if (elNickName.getVisibility() == View.GONE) {
            etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        }
        etAmount.getEditText().addTextChangedListener(textWatcherAmount);
        etAmount.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etNickName.requestFocus();
                    return true;
                }
                return false;
            }
        });
        if (!TextUtils.isEmpty(MovitConsumerApp.getInstance().getMaxAmount())) {
            etAmount.getEditText().setFilters(new InputFilter[]{new TwoDecimalInputFilter(MovitConsumerApp.getInstance().getMaxAmount().length())
            });
        }


        etNickName.getImgLeft().setVisibility(View.GONE);
        etNickName.setErrorLabelLayout(elNickName);
        etNickName.getVerticalDivider().setVisibility(View.GONE);
        etNickName.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        etNickName.getEditText().addTextChangedListener(textWatcherNickName);


        customTabLayout = new CustomTabLayout(tabLayout);

        customTabLayout.setTabListener(new CustomTabLayout.IOnTabClicked() {
            @Override
            public void onTabClicked(int index) {

                viewPager.setCurrentItem(index);
            }

        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                customTabLayout.setCurrentActiveTab(position, ContextCompat.getColor(mContext, R.color.clear_blue), 0);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        customTabLayout.setTabTexts(getString(R.string.favourites), getString(R.string.recent_transactions));
        customTabLayout.setTabTextColor(ContextCompat.getColor(mContext, R.color.clear_blue), ContextCompat.getColor(mContext, R.color.clear_blue));
        customTabLayout.setTabColor(ContextCompat.getColor(mContext, R.color.white));
        customTabLayout.setCurrentActiveTab(0, ContextCompat.getColor(mContext, R.color.clear_blue), 0);


        viewPager.setAdapter(new SampleFragmentPagerAdapter(getChildFragmentManager()));

    }


    public void setValuesToEditText(String number, String amount, String nickname, boolean isFav, String source) {


        perfromOnlyOnce = true;

        if (!TextUtils.isEmpty(number)) {
            favNumber = number;
            etMobileNumber.setText(favNumber);
        }


        if (!TextUtils.isEmpty(amount.trim()) && source.equalsIgnoreCase("recentTransactions"))
            etAmount.setText(amount);
        else {
            etAmount.setText("");
            etAmount.requestFocus();
        }

        if (!TextUtils.isEmpty(nickname) && source.equalsIgnoreCase("favourites"))
            favNickName = nickname;
        else
            favNickName = "";


        if (!TextUtils.isEmpty(source))
            strSource = source;


//        ivFavourite.setImageResource(R.drawable.favourites_unselected);
//        isFavChecked = false;
//        strNickname = "";
        etMobileNumber.clearFocus();
        etAmount.clearFocus();
        etNickName.clearFocus();
        if (isFav) {


            etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
            elNickName.setVisibility(View.VISIBLE);
            etNickName.setText(favNickName);
            ivFavourite.setImageResource(R.drawable.favourites_selected);
            rlFavourite.setEnabled(false);
            isFavChecked = true;


            svForContent.post(new Runnable() {
                @Override
                public void run() {
                    svForContent.setSmoothScrollingEnabled(true);
                    svForContent.smoothScrollTo(0, (int) cbProceed.getY());
                }
            });

        } else {
            etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);

            etNickName.setText(favNickName);
            elNickName.setVisibility(View.GONE);

            ivFavourite.setImageResource(R.drawable.favourites_unselected);
            rlFavourite.setEnabled(true);
            isFavChecked = false;
        }

    }

    @OnClick({R.id.frag_sm_rl_favourite, R.id.frag_sm_cb_proceed})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.frag_sm_rl_favourite:

                etMobileNumber.clearFocus();
                etAmount.clearFocus();
                etNickName.clearFocus();

                if (isFavChecked) {
                    elNickName.setVisibility(View.GONE);
                    // if (TextUtils.isEmpty(strSource)) {
                    etNickName.setText(null);
                    //}

                    etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
                    isFavChecked = false;
                    ivFavourite.setImageResource(R.drawable.favourites_unselected);
                    strNickname = "";

                } else {





                    etNickName.requestFocus();
                    etNickName.setText(favNickName);
                    elNickName.setVisibility(View.VISIBLE);
                    etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
                    ivFavourite.setImageResource(R.drawable.favourites_selected);
                    isFavChecked = true;
                    strNickname = etNickName.getText();



                    svForContent.post(new Runnable() {
                        @Override
                        public void run() {
                            svForContent.setSmoothScrollingEnabled(true);
                            svForContent.smoothScrollTo(0, (int) cbProceed.getY());
                        }
                    });
                }

                break;
            case R.id.frag_sm_cb_proceed:

                strMobileNumber = etMobileNumber.getEditText().getText().toString();
                strAmount = etAmount.getEditText().getText().toString();
                strNickname = etNickName.getEditText().getText().toString();


                if (isFavChecked) {
                    if (genericValidations.valdiatePayPerson(strNickname, etNickName, strMobileNumber, etMobileNumber, strAmount, etAmount, "")) {
                        //  BaseActivity.openNewActivity(mContext,ReviewAndConfirmActivity.class.getCanonicalName(),false);

//                        if ((Float.valueOf(strAmount) > Float.valueOf(MovitConsumerApp.getInstance().getMaxAmount()))) {
//                            etAmount.getErrorLabelLayout().setError("Please enter amount less than " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount()));
//                           // new OkDialog(mContext, "", "Please enter amount less than " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount()), null);
//                        } else if ((Float.valueOf(strAmount) < Float.valueOf(MovitConsumerApp.getInstance().getMinAmount()))) {
//                            etAmount.getErrorLabelLayout().setError("Please enter amount greater than " + MovitConsumerApp.getInstance().getMinAmount());
//                           // new OkDialog(mContext, "", "Please enter amount greater than " + MovitConsumerApp.getInstance().getMinAmount(), null);
//                        } else


//                        if (Double.valueOf(strAmount) > Double.valueOf(MovitConsumerApp.getInstance().getWalletBalance())) {
//                            new OkDialog(mContext, "", getString(R.string.amount_should_be_less_than_balance), null);
//                        } else {
                            Bundle bundle = new Bundle();
                            Intent intent = new Intent(mContext, ReviewAndConfirmActivity.class);
                            bundle.putString("txnType", "P2P");
                            bundle.putString("amount", strAmount);
                            bundle.putString("mobile_number", strMobileNumber);
                            bundle.putString("nick_name", strNickname);
                            bundle.putBoolean("isFav", isFavChecked);
                            intent.putExtras(bundle);
                            mContext.startActivity(intent);
                        //}
                    }

                } else {


                    if (genericValidations.valdiatePayPerson("", strMobileNumber, etMobileNumber, strAmount, etAmount, "")) {

//                        if ((Float.valueOf(strAmount) > Float.valueOf(MovitConsumerApp.getInstance().getMaxAmount()))) {
//                            new OkDialog(mContext, "", "Please enter amount less than " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount()), null);
//                        } else if ((Float.valueOf(strAmount) < Float.valueOf(MovitConsumerApp.getInstance().getMinAmount()))) {
//                            new OkDialog(mContext, "", "Please enter amount greater than " + MovitConsumerApp.getInstance().getMinAmount(), null);
//                        } else

//                        if (Double.valueOf(strAmount) > Double.valueOf(MovitConsumerApp.getInstance().getWalletBalance())) {
//                            new OkDialog(mContext, "", getString(R.string.amount_should_be_less_than_balance), null);
//                        } else {
                            Bundle bundle = new Bundle();
                            Intent intent = new Intent(mContext, ReviewAndConfirmActivity.class);
                            bundle.putString("txnType", "P2P");
                            bundle.putString("amount", strAmount);
                            bundle.putString("mobile_number", strMobileNumber);
                            bundle.putString("nick_name", strNickname);
                            bundle.putBoolean("isFav", isFavChecked);
                            intent.putExtras(bundle);
                            mContext.startActivity(intent);
                        //}
                    }
                }

                break;
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                setValuesToEditText(data.getStringExtra("number"), "", data.getStringExtra("name"), data.getBooleanExtra("isFav", false), data.getStringExtra("source"));

            }
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (TextUtils.isEmpty(charSequence)) {
                etMobileNumber.setBackgroundResource(R.drawable.pinkish_grey_border);
            }

            if (charSequence.toString().length() == getResources().getInteger(R.integer.mobileNoLength)) {
                etAmount.requestFocus();
            } else {


                if (perfromOnlyOnce) {
                    perfromOnlyOnce = false;

                    if (mContext != null)
                        Constants.hideKeyboard(mContext);


                    etMobileNumber.clearFocus();
                    etAmount.clearFocus();


                    etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);

                    ivFavourite.setImageResource(R.drawable.favourites_unselected);
                    rlFavourite.setEnabled(true);
                    isFavChecked = false;
                    favNickName = "";
                    elNickName.setVisibility(View.GONE);
                    etNickName.setText(favNickName);

                }

            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{etMobileNumber, etAmount, etNickName})) {
                cbProceed.setEnabled(true);
            } else {
                cbProceed.setEnabled(false);

            }


        }
    };

    private TextWatcher textWatcherAmount = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            if (!TextUtils.isEmpty(charSequence)) {
                etAmount.getEtCountryCode().setText(getString(R.string.AED));
                etAmount.getEtCountryCode().setVisibility(View.VISIBLE);

            } else {
                etAmount.getEtCountryCode().setVisibility(View.GONE);
                etAmount.setBackgroundResource(R.drawable.pinkish_grey_border);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{etMobileNumber, etAmount, etNickName})) {
                cbProceed.setEnabled(true);
            } else {
                cbProceed.setEnabled(false);

            }

        }
    };

    private TextWatcher textWatcherNickName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            if (TextUtils.isEmpty(charSequence)) {
                etNickName.setBackgroundResource(R.drawable.pinkish_grey_border);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{etMobileNumber, etAmount, etNickName})) {
                cbProceed.setEnabled(true);
            } else {
                cbProceed.setEnabled(false);

            }

        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void showContactList() {
        Intent intent = new Intent(mContext, ContactsListActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }


    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;

        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public int getCount() {
            return PAGE_COUNT;
        }


        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                if (favouritesFragment == null)
                    favouritesFragment = new FavouritesFragment();
                return favouritesFragment;
            } else {
                if (recentTransactionsFragment == null)
                    recentTransactionsFragment = new RecentTransactionsFragment(99);
                return recentTransactionsFragment;
            }


        }
    }
}
