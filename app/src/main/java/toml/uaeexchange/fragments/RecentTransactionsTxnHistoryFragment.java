package toml.uaeexchange.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.AddMoneyActivity;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.activities.TransactionHistoryActivity;
import toml.uaeexchange.adapters.RecentTransactionsAdapter;
import toml.uaeexchange.customviews.CustomTabLayoutFourTabs;

/**
 * Created by vishwanathp on 9/8/2017.
 */

public class RecentTransactionsTxnHistoryFragment extends Fragment {

    /*  @BindView(R.id.frag_rt_lv_transactions)
      ListView lvTransactions;
      @BindView(R.id.fraf_rt_tv_no_transactions)
      TextView tvNoTransactions;
      @BindView(R.id.frag_rt_pb_progressBar)
      ProgressBar pbProgress;*/
    @BindView(R.id.txtvwBalanceValue)
    TextView txtvwBalanceValue;
    @BindView(R.id.txtvwAddMoney)
    TextView txtvwAddMoney;
    @BindView(R.id.linlayBalanceAddMoneyContainer)
    LinearLayout linlayBalanceAddMoneyContainer;
    @BindView(R.id.recent_transactions_tab_layout)
    LinearLayout tabLayout;
    @BindView(R.id.recent_transactions_viewpager)
    ViewPager viewPager;
    private Context mContext;
    private View mView;
    Unbinder unbinder;

    RecentTransactionsAdapter recentTransactionsAdapter;
    RecentTransactionsFragment recentTransactionsFragment;
    RecentTransactionsHistoryFragmentPagerAdapter recentTransactionsHistoryFragmentPagerAdapter;

    CustomTabLayoutFourTabs customTabLayoutFourTabs;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_recent_transactions_txn_history, null, false);

        unbinder = ButterKnife.bind(this, mView);
        return mView;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //  pbProgress.setVisibility(View.VISIBLE);
        //  tvNoTransactions.setVisibility(View.GONE);

        ((TransactionHistoryActivity) getActivity()).callRefreshApi(0);
        recentTransactionsHistoryFragmentPagerAdapter = new RecentTransactionsHistoryFragmentPagerAdapter(getChildFragmentManager());

        if (!TextUtils.isEmpty(MovitConsumerApp.getInstance().getWalletBalance())) {
            txtvwBalanceValue.setText(MovitConsumerApp.getInstance().getWalletBalance());
        } else {
            txtvwBalanceValue.setText("N.A.");
        }

        customTabLayoutFourTabs = new CustomTabLayoutFourTabs(tabLayout);

        txtvwAddMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseActivity.openNewActivity(mContext, AddMoneyActivity.class.getCanonicalName(), false);
            }
        });

        customTabLayoutFourTabs.setTabListener(new CustomTabLayoutFourTabs.IOnTabClicked() {
            @Override
            public void onTabClicked(int index) {

                viewPager.setCurrentItem(index);
            }

        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                customTabLayoutFourTabs.setCurrentActiveTab(position, ContextCompat.getColor(mContext, R.color.clear_blue), ContextCompat.getColor(mContext, R.color.battleship_grey));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        customTabLayoutFourTabs.setTabTexts(getString(R.string.all), getString(R.string.sent), getString(R.string.received), getString(R.string.added));
        customTabLayoutFourTabs.setTabTextColor(ContextCompat.getColor(mContext, R.color.battleship_grey), ContextCompat.getColor(mContext, R.color.battleship_grey));
        customTabLayoutFourTabs.setCurrentActiveTab(0, ContextCompat.getColor(mContext, R.color.clear_blue), ContextCompat.getColor(mContext, R.color.battleship_grey));
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(recentTransactionsHistoryFragmentPagerAdapter);


    }


    public void reLoadFragment() {

        recentTransactionsHistoryFragmentPagerAdapter.notifyDataSetChanged();
    }


    public class RecentTransactionsHistoryFragmentPagerAdapter extends FragmentStatePagerAdapter {
        final int PAGE_COUNT = 4;

        public RecentTransactionsHistoryFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public int getCount() {
            return PAGE_COUNT;
        }


        @Override
        public Fragment getItem(int position) {

            return new RecentTransactionsFragment(position + 1);


        }

        @Override
        public int getItemPosition(Object object) {
            // Causes adapter to reload all Fragments when
            // notifyDataSetChanged is called
            return POSITION_NONE;
        }
    }


   /* public void setAdapter(List<Transaction> response, String errorCode, String reason) {
        if (response != null) {
            pbProgress.setVisibility(View.GONE);
            tvNoTransactions.setVisibility(View.GONE);

            recentTransactionsAdapter = new RecentTransactionsAdapter(mContext, recentTransactionsFragment, response);
            lvTransactions.setAdapter(recentTransactionsAdapter);

        } else {
            pbProgress.setVisibility(View.GONE);
            tvNoTransactions.setVisibility(View.VISIBLE);
            tvNoTransactions.setText(reason);
        }

    }
*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
