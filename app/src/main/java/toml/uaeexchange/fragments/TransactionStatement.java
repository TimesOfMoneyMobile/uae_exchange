package toml.uaeexchange.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import toml.movitwallet.controllers.TransactionHistoryPaginationController;
import toml.movitwallet.models.DateHeaderItem;
import toml.movitwallet.models.ListItem;
import toml.movitwallet.models.Transaction;
import toml.movitwallet.models.TransactionHistoryResponse;
import toml.movitwallet.models.TransactionItem;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.TransactionHistoryActivity;
import toml.uaeexchange.adapters.NewTransactionsHistoryAdapter;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by kunalk on 4/12/2016.
 */
public class TransactionStatement extends Fragment {

    View view;
    public static final int Transaction_Statement_1 = 0;
    public static final int Transaction_Statement_2 = 1;
    TransactionStatement transactionStatement1;
    TransactionStatement transactionStatement2;
    @BindView(R.id.edtxtLayoutStartDate)
    CustomEditTextLayout edtxtLayoutStartDate;
    @BindView(R.id.errorLabelStartDate)
    ErrorLabelLayout errorLabelStartDate;
    @BindView(R.id.edtxtLayoutEndDate)
    CustomEditTextLayout edtxtLayoutEndDate;
    @BindView(R.id.errorLabelEndDate)
    ErrorLabelLayout errorLabelEndDate;
    Unbinder unbinder;
    Context mContext;
    @BindView(R.id.btnProceed)
    CustomButton btnProceed;
    @BindView(R.id.linlayDateSelectionContainer)
    LinearLayout linlayDateSelectionContainer;
    @BindView(R.id.linlayListPageContainer)
    LinearLayout linlayListPageContainer;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.txtvwNoDataFound)
    TextView txtvwNoDataFound;

    int totalRecords;
    @BindView(R.id.imgvwBackArrow)
    ImageView imgvwBackArrow;
    @BindView(R.id.txtvwSelectedDate)
    TextView txtvwSelectedDate;
    @BindView(R.id.spinnerPageNumber)
    Spinner spinnerPageNumber;
    @BindView(R.id.linlaySelectedDatePageContainer)
    LinearLayout linlaySelectedDatePageContainer;

    int pageSize = 10;

    @BindView(R.id.recyclerViewTransactionStatement)
    RecyclerView recyclerViewTransactionStatement;
    private ProgressDialog progressDialog;
    String selectedStartDate = "", selectedEndDate = "", formattedStartDate = "", formattedEndDate = "";

    List<ListItem> consolidatedList = new ArrayList<>();

    int iPageNo = 1;
    int iTotalPagesCount = 0;
    private boolean isUserTouch = false;

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.transaction_statement, container, false);
        unbinder = ButterKnife.bind(this, view);
        imgvwBackArrow.performClick();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        edtxtLayoutStartDate.setErrorLabelLayout(errorLabelStartDate);
        edtxtLayoutEndDate.setErrorLabelLayout(errorLabelEndDate);


        edtxtLayoutStartDate.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutStartDate.getImgLeft().setVisibility(View.GONE);

        edtxtLayoutStartDate.getImgRight().setVisibility(View.VISIBLE);
        edtxtLayoutStartDate.getImgRight().setImageResource(R.drawable.date_of_birth_icon);


        edtxtLayoutEndDate.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutEndDate.getImgLeft().setVisibility(View.GONE);

        edtxtLayoutEndDate.getImgRight().setVisibility(View.VISIBLE);
        edtxtLayoutEndDate.getImgRight().setImageResource(R.drawable.date_of_birth_icon);


        edtxtLayoutStartDate.getEditText().setInputType(InputType.TYPE_NULL);
        edtxtLayoutStartDate.getEditText().setFocusable(false);
        edtxtLayoutStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate("start");
            }
        });

        edtxtLayoutStartDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate("start");
            }
        });

        edtxtLayoutEndDate.getEditText().setInputType(InputType.TYPE_NULL);
        edtxtLayoutEndDate.getEditText().setFocusable(false);
        edtxtLayoutEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate("end");
                btnProceed.setEnabled(true);
            }
        });
        edtxtLayoutEndDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setDate("end");
                btnProceed.setEnabled(true);
            }
        });


        imgvwBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateSelectionView();
            }
        });


        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edtxtLayoutStartDate.getText())) {
                    edtxtLayoutStartDate.getErrorLabelLayout().setError("Please select Start Date.");
                    edtxtLayoutStartDate.setBackgroundResource(R.drawable.custom_edittext_border_red);
                } else if (TextUtils.isEmpty(edtxtLayoutEndDate.getText())) {
                    edtxtLayoutEndDate.getErrorLabelLayout().setError("Please select End Date.");
                    edtxtLayoutEndDate.setBackgroundResource(R.drawable.custom_edittext_border_red);
                } else {
                    try {
                        Date from = sdf.parse(edtxtLayoutStartDate.getText().toString());
                        Date to = sdf.parse(edtxtLayoutEndDate.getText().toString());

                        if (to.before(from)) {
                            new OkDialog(mContext, getString(R.string.txnDateStartEndDateEroor), new OkDialog.IOkDialogCallback() {
                                @Override
                                public void handleResponse() {

                                }
                            });


                            return;
                        }

                        checkAndCallApi();


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }


            }
        });


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showDateSelectionView();
        }
    }

    public void checkAndCallApi() {
        consolidatedList.clear();
        spinnerPageNumber.setAdapter(null);
        showListView();
        iPageNo = 1;
        isUserTouch = false;
        callTransactionStatementAPI();
    }

    private void setDate(final String value) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);

      /*  if (value.equalsIgnoreCase("end")) {
            cal.add(Calendar.MONTH, -6);
        }*/


        DatePickerDialog datePicker = new DatePickerDialog(mContext, R.style.CustomDatePickerDialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        month = month + 1;
                        String strMonth = month > 9 ? month + "" : "0" + month;
                        String strDay = dayOfMonth > 9 ? dayOfMonth + "" : "0" + dayOfMonth;


                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d");
                        String day = null;


                        switch (value) {
                            case "start":
                                edtxtLayoutStartDate.setText(strDay + "-" + strMonth + "-" + year);
                                selectedStartDate = strDay + "-" + strMonth + "-" + year;


                                try {
                                    day = simpleDateFormat.format(sdf.parse(selectedStartDate));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                                if (day.endsWith("1"))
                                    simpleDateFormat = new SimpleDateFormat("d'st' MMMM, yyyy");
                                else if (day.endsWith("2"))
                                    simpleDateFormat = new SimpleDateFormat("d'nd' MMMM, yyyy");
                                else if (day.endsWith("3"))
                                    simpleDateFormat = new SimpleDateFormat("d'rd' MMMM, yyyy");
                                else
                                    simpleDateFormat = new SimpleDateFormat("d'th' MMMM, yyyy");

                                try {
                                    formattedStartDate = simpleDateFormat.format(sdf.parse(selectedStartDate));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                edtxtLayoutStartDate.getImgRight().setImageResource(R.drawable.date_of_birth_selected);


                                break;

                            case "end":

                                edtxtLayoutEndDate.setText(strDay + "-" + strMonth + "-" + year);

                                selectedEndDate = strDay + "-" + strMonth + "-" + year;

                                try {
                                    day = simpleDateFormat.format(sdf.parse(selectedEndDate));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                                if (day.endsWith("1"))
                                    simpleDateFormat = new SimpleDateFormat("d'st' MMMM, yyyy");
                                else if (day.endsWith("2"))
                                    simpleDateFormat = new SimpleDateFormat("d'nd' MMMM, yyyy");
                                else if (day.endsWith("3"))
                                    simpleDateFormat = new SimpleDateFormat("d'rd' MMMM, yyyy");
                                else
                                    simpleDateFormat = new SimpleDateFormat("d'th' MMMM, yyyy");


                                try {
                                    formattedEndDate = simpleDateFormat.format(sdf.parse(selectedEndDate));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                edtxtLayoutEndDate.getImgRight().setImageResource(R.drawable.date_of_birth_selected);
                                break;
                        }

                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

        datePicker.setCancelable(false);
        datePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());

        datePicker.show();


    }

    private void showDateSelectionView() {
        linlayDateSelectionContainer.setVisibility(View.VISIBLE);
        linlayListPageContainer.setVisibility(View.GONE);
        edtxtLayoutStartDate.clearText();
        edtxtLayoutEndDate.clearText();
        ((TransactionHistoryActivity) getActivity()).callRefreshApi(1);
    }

    private void showListView() {
        linlayDateSelectionContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        txtvwNoDataFound.setVisibility(View.GONE);
        linlayListPageContainer.setVisibility(View.VISIBLE);
        ((TransactionHistoryActivity) getActivity()).callRefreshApi(2);


    }

    public void showProgress() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setCanceledOnTouchOutside(false);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = getActivity().getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        progressDialog.show();

    }

    public void dismissProgress() {

        if (progressDialog != null && progressDialog.isShowing()) {
            LogUtils.Verbose("inside if", "dismissProgress......");
            progressDialog.dismiss();
        }
    }

    private void callTransactionStatementAPI() {

        LogUtils.Verbose("TAG", "In callTransactionStatementAPI() - " + iPageNo);


       /* if (iPageNo > iTotalPagesCount) {
            dismissProgress();
            // Toast.makeText(mContext, "All records are loaded.", Toast.LENGTH_LONG).show();
            return;
        }*/
        showProgress();
        MovitWalletController movitWalletController = new TransactionHistoryPaginationController(edtxtLayoutStartDate.getText(), edtxtLayoutEndDate.getText(), "" + iPageNo);
        movitWalletController.init(new IMovitWalletController<TransactionHistoryResponse>() {
            @Override
            public void onSuccess(TransactionHistoryResponse response) {

                spinnerPageNumber.setVisibility(View.VISIBLE);
                dismissProgress();

                totalRecords = Integer.parseInt(response.getResponseDetails().getTotalRecords());

                LogUtils.Verbose("TAG", "totalRecords - " + totalRecords);


                Date date1 = Constants.stringToDate(selectedStartDate, "dd-MM-yyyy");
                Date date2 = Constants.stringToDate(selectedEndDate, "dd-MM-yyyy");

                /*if (Constants.isSameYear(date1, date2)) {
                    String s1 = Constants.removeLastFourChar(formattedStartDate);
                    // To remove comma from date string
                    s1 = s1.replaceAll(", ", "");
                    txtvwSelectedDate.setText(s1 + " - " + formattedEndDate);
                } else {
                    txtvwSelectedDate.setText(formattedStartDate + " - " + formattedEndDate);
                }*/

                txtvwSelectedDate.setText(selectedStartDate + " to " + selectedEndDate);


                if (spinnerPageNumber.getAdapter() == null) {

                    ArrayList<String> listPageNo = new ArrayList<>();
                    iTotalPagesCount = (totalRecords + pageSize - 1) / pageSize;

                    for (int i = 1; i <= iTotalPagesCount; i++) {
                        listPageNo.add("Page " + i + " of " + iTotalPagesCount);
                    }

                    String[] pageNoArray = listPageNo.toArray(new String[listPageNo.size()]);


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                            android.R.layout.simple_spinner_item, pageNoArray);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerPageNumber.setAdapter(adapter);

                    spinnerPageNumber.setSelection(0, false);


                    spinnerPageNumber.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View view, MotionEvent motionEvent) {
                            isUserTouch = true;
                            return false;
                        }
                    });

                    spinnerPageNumber.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            if (isUserTouch) {
                                iPageNo = position + 1;
                                callTransactionStatementAPI();
                            }


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }

                setDataAdapter(response);

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                recyclerViewTransactionStatement.setAdapter(null);
                txtvwNoDataFound.setVisibility(View.VISIBLE);
                txtvwNoDataFound.setText(reason);
                txtvwSelectedDate.setText(selectedStartDate + " to " + selectedEndDate);
                spinnerPageNumber.setVisibility(View.GONE);
                //Toast.makeText(mContext, reason, Toast.LENGTH_LONG).show();
            }
        });

    }


    /**
     * This mrthod set data to recycler view
     *
     * @param response
     */

    public void setDataAdapter(TransactionHistoryResponse response) {
        Map<String, List<Transaction>> groupedHashMap = groupDataIntoHashMap(response.getResponseDetails().getTransactions().getTransactionsList());

        if (consolidatedList != null && consolidatedList.size() > 0) {
            consolidatedList.clear();
        }

        for (String date : groupedHashMap.keySet()) {
            DateHeaderItem dateItem = new DateHeaderItem();
            dateItem.setDate(date);
            consolidatedList.add(dateItem);


            for (Transaction objTransaction : groupedHashMap.get(date)) {
                TransactionItem transactionItem = new TransactionItem();
                transactionItem.setTransaction(objTransaction);//setBookingDataTabs(bookingDataTabs);
                consolidatedList.add(transactionItem);
            }
        }

        NewTransactionsHistoryAdapter newadapter = new NewTransactionsHistoryAdapter(mContext, consolidatedList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewTransactionStatement.setLayoutManager(layoutManager);
        recyclerViewTransactionStatement.setAdapter(newadapter);

    }


    private Map<String, List<Transaction>> groupDataIntoHashMap(List<Transaction> inputList) {

        Map<String, List<Transaction>> groupedHashMap = new TreeMap<>();


        for (Transaction objTransaction : inputList) {


            String hashMapKey = Constants.getFormattedDate(Constants.SERVER_DATE_FROMAT, "MMMM yyyy", objTransaction.getTxnDate());

            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the transaction object
                // against the existing key.
                groupedHashMap.get(hashMapKey).add(objTransaction);
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                List<Transaction> list = new ArrayList<>();
                list.add(objTransaction);
                groupedHashMap.put(hashMapKey, list);
            }

        }

        return groupedHashMap;


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
