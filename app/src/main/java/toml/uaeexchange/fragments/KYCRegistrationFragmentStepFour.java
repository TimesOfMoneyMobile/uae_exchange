package toml.uaeexchange.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import toml.movitwallet.utils.IntentConstants;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.activities.KYCRegistrationActivity;
import toml.uaeexchange.activities.LoginActivity;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.nearbystores.NearByActivity;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 8/21/2017.
 */

public class KYCRegistrationFragmentStepFour extends Fragment {


    Unbinder unbinder;
    @BindView(R.id.toolbar_pay)
    Toolbar toolbarPay;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.btnLogin)
    CustomButton btnLogin;
    @BindView(R.id.btnLocateAgents)
    CustomButton btnLocateAgents;


    private View mView;
    KYCRegistrationActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_kyc_registration_step_four, null, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ((KYCRegistrationActivity) getActivity()).setToolbar(toolbarPay, "Almost There!", R.drawable.icon_back, 0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnLogin.performClick();

            }
        }, null);

        progressBar.setProgress(100);


        activity = (KYCRegistrationActivity) getActivity();


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnLogin, R.id.btnLocateAgents})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:

                Intent intent = new Intent(activity, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                // intent.putExtra(IntentConstants.FROM_ACTIVITY, DashboardActivity.class.getCanonicalName());
                intent.putExtra(IntentConstants.FROM_ACTIVITY_REGISTRATION, Constants.RegistrationSuccessActivity);
                activity.startActivity(intent);

                activity.finish();

                break;

            case R.id.btnLocateAgents:

                BaseActivity.openNewActivity(activity, NearByActivity.class.getCanonicalName(), false);

                break;
        }
    }


}
