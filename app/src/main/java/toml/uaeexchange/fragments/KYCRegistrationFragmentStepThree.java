package toml.uaeexchange.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.KYCRegistrationActivity;

/**
 * Created by vishwanathp on 8/21/2017.
 */

public class KYCRegistrationFragmentStepThree extends Fragment {


    @BindView(R.id.frag_krst_btn_locate)
    Button fragKrstBtnLocate;
    @BindView(R.id.layout_cr_tv_title)
    TextView tvTitle;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.layout_cr_tv_steps)
    TextView tvSteps;
    @BindView(R.id.layout_cr_im_icon)
    ImageView imIcon;

    Unbinder unbinder;


    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_kyc_registration_step_three, null, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvTitle.setText(getString(R.string.find_nearest_agent));
        tvSteps.setText("Step 3 of 4");
        progressBar.setProgress(75);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.frag_krst_btn_locate)
    public void onViewClicked(View view) {
        switch (view.getId()){
            case R.id.frag_krst_btn_locate:
                KYCRegistrationActivity.thisActivity.getPager().setCurrentItem(4);
                break;
        }
    }
}
