package toml.uaeexchange.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aviadmini.quickimagepick.PickCallback;
import com.aviadmini.quickimagepick.PickSource;
import com.aviadmini.quickimagepick.PickTriggerResult;
import com.aviadmini.quickimagepick.QiPick;
import com.aviadmini.quickimagepick.UriUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import fr.ganfra.materialspinner.MaterialSpinner;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import toml.movitwallet.models.Document;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.KYCRegistrationActivity;
import toml.uaeexchange.customviews.CustomButton;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.facebook.login.widget.ProfilePictureView.TAG;

@RuntimePermissions
public class KYCRegistrationStep2Fragment extends Fragment {


    private static final String DIR_NAME = "UAExchange Photos";
    @PickTriggerResult
    int triggerResult;

    Context mContext;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    Unbinder unbinder;
    @BindView(R.id.spinnerDocType)
    MaterialSpinner spinnerDocType;
    @BindView(R.id.imgvwFirstImage)
    ImageView imgvwFirstImage;
    @BindView(R.id.btnTakePicture)
    Button btnTakePicture;
    @BindView(R.id.btnChooseFromGallery)
    Button btnChooseFromGallery;
    @BindView(R.id.btnNext)
    CustomButton btnNext;
    @BindView(R.id.toolbar_pay)
    Toolbar toolbarPay;


    private ProgressDialog progressDialog;

    private Uri mSelectedImageUri;

    Map<Document, Map<String, Bitmap>> documentMap;
    ArrayList<Document> documents;
    Document selectedDocument;
    KYCRegistrationActivity activity;

    Typeface fontBold;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kycregistration_step2, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity = (KYCRegistrationActivity) getActivity();

        fontBold = Typeface.createFromAsset(activity.getAssets(), "fonts/Quicksand-Bold.ttf");

        documentMap = activity.getDocumentMap();
        selectedDocument = activity.getSelectedDocument();

        ((KYCRegistrationActivity) getActivity()).setToolbar(toolbarPay, "Scan & Upload", R.drawable.icon_back, 0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().onBackPressed();

            }
        }, null);

        progressBar.setProgress(25);


        //showProgress();
        spinnerDocType.setHint("Choose Document Type");


        spinnerDocType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position >= 0) {

                    LogUtils.Verbose("Pos", "" + position + " "
                            + documents.get(position).getName());

                    TextView selectedView = (TextView) spinnerDocType.getSelectedView();
                    if (selectedView != null) {
                        selectedView.setTypeface(fontBold);
                        selectedView.setTextColor(Color.BLACK);
                    }

                    selectedDocument = documents.get(position);

                    ((KYCRegistrationActivity) getActivity()).setSelectedDocument(selectedDocument);

                    selectedDocument = documents.get(position);
                    if (!documentMap.containsKey(selectedDocument)) {
                        HashMap<String, Bitmap> bitmapHashMap = new HashMap<String, Bitmap>();
                        documentMap.put(selectedDocument, bitmapHashMap);

                    }

                    btnTakePicture.setEnabled(true);
                    btnChooseFromGallery.setEnabled(true);
                } else {
                    btnTakePicture.setEnabled(false);
                    btnChooseFromGallery.setEnabled(false);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        final View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View pView) {
                KYCRegistrationStep2FragmentPermissionsDispatcher.onViewClickedWithCheck(KYCRegistrationStep2Fragment.this, pView);
            }
        };


        btnTakePicture.setOnClickListener(clickListener);
        btnChooseFromGallery.setOnClickListener(clickListener);


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            documents = activity.getDocuments();

            if (documents != null) {
                ArrayAdapter<Document> dataAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, documents);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerDocType.setAdapter(dataAdapter);

                selectedDocument = documents.get(0);
                ((KYCRegistrationActivity) getActivity()).setSelectedDocument(selectedDocument);
             //   selectedDocument = activity.getSelectedDocument();

                if (documents.indexOf(selectedDocument) >= 0) {
                    // Note - Here we adding 1 in index due to hint
                    spinnerDocType.setSelection(documents.indexOf(selectedDocument) + 1);
                    // spinnerDocType.setEnabled(false);
                }

            }
        } else {

        }
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    private PickCallback mCallback = new PickCallback() {
        @Override
        public void onImagePicked(@NonNull PickSource pickSource, int requestType, @NonNull Uri imageUri) {

            mSelectedImageUri = imageUri;
            final Context context = getApplicationContext();

            final boolean exists = UriUtils.contentExists(context, imageUri);

            if (!exists) {

                Toast.makeText(context, "Image does not exist.", Toast.LENGTH_SHORT)
                        .show();

                return;
            }

            final String extension = UriUtils.getFileExtension(context, imageUri);
            Log.i(TAG, "Picked: " + imageUri.toString() + "\nMIME type: " + UriUtils.getMimeType(context,
                    imageUri) + "\nFile extension: " + extension + "\nRequest type: " + requestType);

            // Do something with Uri, for example load image into an ImageView
            Glide.with(context)
                    .load(mSelectedImageUri)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(imgvwFirstImage);


            try {


                Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(KYCRegistrationActivity.thisActivity.getContentResolver(), imageUri);

                // imageBitmap = Bitmap.createBitmap(imageBitmap, 0, 0, 100, 100);

                // imageBitmap = Bitmap.createScaledBitmap(imageBitmap, 300, 300, false);

                HashMap<String, Bitmap> bitmapHashMap = (HashMap<String, Bitmap>) documentMap.get(selectedDocument);
                String img_id = selectedDocument.getId() + "_" + "0";
                Log.v("TAG", " Image ID " + img_id);
                bitmapHashMap.put(img_id, imageBitmap);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 20, stream);

                ((KYCRegistrationActivity) getActivity()).getImageByteStreamMap().put(img_id, stream);

                // ((KYCRegistrationActivity) getActivity()).getImageByteHashMap().put(img_id, imageBitmap);

                // String filePath = getRealPathFromURI(mSelectedImageUri);

               /* String path = mSelectedImageUri.getPath().substring(0, mSelectedImageUri.getPath().indexOf(":"));
                File file = new File(path + "/" + getFileName(mSelectedImageUri));
                ((KYCRegistrationActivity) getActivity()).getFileHashMap().put(img_id, file);*/


            } catch (Exception e) {
                e.printStackTrace();
            }


            try {

               /* final String ext = extension == null ? "" : "." + extension;
                final File outDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DIR_NAME);
                final File file = new File(outDir, "temp" + ext);

                //noinspection ResultOfMethodCallIgnored
                outDir.mkdirs();

                // DO NOT do this on main thread. This is only for reference
                UriUtils.saveContentToFile(context, imageUri, file);

                Toast.makeText(context, "Save complete", Toast.LENGTH_SHORT);//.show();
*/
                btnTakePicture.setText("Take Another Picture");
                btnTakePicture.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                btnTakePicture.setBackgroundColor(ContextCompat.getColor(mContext, R.color.aqua_green));
                btnChooseFromGallery.setText("Edit Picture");
                //btnNext.setVisibility(View.VISIBLE);
                btnNext.setEnabled(true);
                progressBar.setProgress(40);

            } catch (final Exception e) {
                Toast.makeText(context, "Save failed: " + e.getMessage(), Toast.LENGTH_SHORT)
                        .show();
            }

        }

        @Override
        public void onMultipleImagesPicked(int i, @NonNull List<Uri> list) {

        }

        @Override
        public void onError(@NonNull PickSource pickSource, int i, @NonNull String s) {

        }

        @Override
        public void onCancel(@NonNull PickSource pickSource, int i) {

        }
    };


    public KYCRegistrationStep2Fragment() {
        // Required empty public constructor
    }


    public static KYCRegistrationStep2Fragment newInstance(String param1, String param2) {
        KYCRegistrationStep2Fragment fragment = new KYCRegistrationStep2Fragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    public void showProgress() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCanceledOnTouchOutside(false);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = getActivity().getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        progressDialog.show();

    }

    public void dismissProgress() {

        if (progressDialog != null && progressDialog.isShowing()) {
            LogUtils.Verbose("inside if", "dismissProgress......");
            progressDialog.dismiss();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnTakePicture, R.id.btnChooseFromGallery, R.id.btnNext})
    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})

    public void onViewClicked(View view) {

        final File outDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DIR_NAME);
        Log.d(TAG, outDir.getAbsolutePath() + ", can write: " + outDir.canWrite());

        switch (view.getId()) {
            case R.id.btnTakePicture:


                triggerResult = QiPick.in(this)
                        .withAllImageMimeTypesAllowed()
                        .withCameraPicsDirectory(outDir)
                        .withRequestType(3)
                        .fromCamera();

                this.solveTriggerResult(triggerResult);


                break;
            case R.id.btnChooseFromGallery:


                triggerResult = QiPick.in(this)
                        .allowOnlyLocalContent(true)
                        .withAllowedMimeTypes(QiPick.MIME_TYPE_IMAGE_JPEG, QiPick.MIME_TYPE_IMAGE_WEBP)
                        .withCameraPicsDirectory(outDir)
                        .withRequestType(1)
                        .fromMultipleSources("Select image from", PickSource.DOCUMENTS, PickSource.GALLERY);

                this.solveTriggerResult(triggerResult);
                break;

            case R.id.btnNext:

                KYCRegistrationActivity.thisActivity.getPager().setCurrentItem(2);

                break;

        }
    }


    private void solveTriggerResult(final @PickTriggerResult int pTriggerResult) {

        switch (pTriggerResult) {

            case PickTriggerResult.TRIGGER_PICK_ERR_CAM_FILE: {

                Toast.makeText(mContext, "Could not create file to save Camera image. Make sure camera pics dir is writable", Toast.LENGTH_SHORT)
                        .show();

                break;
            }

            case PickTriggerResult.TRIGGER_PICK_ERR_NO_ACTIVITY: {

                Toast.makeText(mContext, "There is no Activity that can pick requested file :(", Toast.LENGTH_SHORT)
                        .show();

                break;
            }

            case PickTriggerResult.TRIGGER_PICK_ERR_NO_PICK_SOURCES: {

                Toast.makeText(mContext, "Dear dev, multiple source request needs at least one source!", Toast.LENGTH_SHORT)
                        .show();

                break;
            }

            case PickTriggerResult.TRIGGER_PICK_OK: {
                break;// all good, do nothing
            }

        }

    }


    @Override
    public void onActivityResult(final int pRequestCode, final int pResultCode, final Intent pData) {

        if (!QiPick.handleActivityResult(getApplicationContext(), pRequestCode, pResultCode, pData, this.mCallback)) {
            super.onActivityResult(pRequestCode, pResultCode, pData);
        }

    }

    @Override
    public void onRequestPermissionsResult(final int pRequestCode, @NonNull final String[] pPermissions, @NonNull final int[] pGrantResults) {
        super.onRequestPermissionsResult(pRequestCode, pPermissions, pGrantResults);

        KYCRegistrationStep2FragmentPermissionsDispatcher.onRequestPermissionsResult(KYCRegistrationStep2Fragment.this, pRequestCode, pGrantResults);

    }


}
