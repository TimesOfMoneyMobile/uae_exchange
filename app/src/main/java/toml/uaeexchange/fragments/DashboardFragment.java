package toml.uaeexchange.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import toml.movitwallet.controllers.BalanceInquiryController;
import toml.movitwallet.controllers.OffersController;
import toml.movitwallet.models.Offer;
import toml.movitwallet.utils.AnimationUtils;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.AddMoneyActivity;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.activities.LoginActivity;
import toml.uaeexchange.activities.NotificationActivity;
import toml.uaeexchange.activities.P2MQRCodeScanActivity;
import toml.uaeexchange.activities.PayPersonActivity;
import toml.uaeexchange.activities.RechargeActivity;
import toml.uaeexchange.activities.RegisterActivity;
import toml.uaeexchange.activities.SplitAndShareActivity;
import toml.uaeexchange.utilities.Constants;


/**
 * Created by kunalk on 7/6/2017.
 */

public class DashboardFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {


    Context mContext;
    LinearLayout layout_payShop;
    LinearLayout layout_balance;

    @BindView(R.id.rlHamIcon)
    RelativeLayout rlHamIcon;
    @BindView(R.id.toolbar_dashboard_rl_for_notification)
    RelativeLayout rlNotification;
    @BindView(R.id.toolbar_dashboard_tv_notification_count)
    TextView tvNotificationCount;


    @BindView(R.id.txtBalance)
    TextView txtBalance;
    @BindView(R.id.ivPayPerson)
    ImageView ivPayPerson;
    @BindView(R.id.layout_p2p)
    LinearLayout layoutP2p;
    @BindView(R.id.ivRequestMoney)
    ImageView ivRequestMoney;
    @BindView(R.id.ivPayShop)
    ImageView ivPayShop;
    @BindView(R.id.layout_p2m)
    LinearLayout layoutP2m;

    @BindView(R.id.slider)
    SliderLayout mDemoSlider;
    @BindView(R.id.cardoffers)
    CardView cardoffers;

    @BindView(R.id.tvYourBalance)
    TextView tvYourBalance;
    @BindView(R.id.tvExcitingFeaturelable)
    TextView tvExcitingFeaturelable;
    @BindView(R.id.ivRecharge)
    ImageView ivRecharge;
    @BindView(R.id.layout_recharge)
    LinearLayout layoutRecharge;
    @BindView(R.id.ivEventTkt)
    ImageView ivEventTkt;
    @BindView(R.id.layout_event_tkt)
    LinearLayout layoutEventTkt;
    @BindView(R.id.ivGifting)
    ImageView ivGifting;
    @BindView(R.id.layout_gifting)
    LinearLayout layoutGifting;
    @BindView(R.id.ivEntertainment)
    ImageView ivEntertainment;
    @BindView(R.id.layout_entertainment)
    LinearLayout layoutEntertainment;
    @BindView(R.id.llBillers)
    LinearLayout llBillers;
    @BindView(R.id.vBiller)
    View vBiller;
    @BindView(R.id.navigation_toolbar)
    Toolbar navigationToolbar;
    @BindView(R.id.btnAddMoney)
    TextView btnAddMoney;
    @BindView(R.id.rlWithLogin)
    RelativeLayout rlWithLogin;
    @BindView(R.id.btnSignUp)
    Button btnSignUp;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.rlWithoutLogin)
    RelativeLayout rlWithoutLogin;
    @BindView(R.id.llBalance)
    LinearLayout llBalance;
    @BindView(R.id.llPayments)
    LinearLayout llPayments;
    @BindView(R.id.tvWelcomeTitle)
    TextView tvWelcomeTitle;
    @BindView(R.id.ivRefresh)
    ImageView ivRefresh;
    @BindView(R.id.vExciting)
    View vExciting;
    @BindView(R.id.rellayRoot)
    RelativeLayout rellayRoot;
    @BindView(R.id.tvSpotLight)
    TextView tvSpotLight;
    @BindView(R.id.linlayDim)
    LinearLayout linlayDim;
    @BindView(R.id.layout_splitandshare)
    LinearLayout layoutSplitandshare;
    private View mView;

    Unbinder unbinder;
    // CircleImageView imageViewProfile;


    LeftMenuFragment leftMenuFragment;
    private AnimationUtils rotateanimation;
    private HashMap<String, String> url_maps;

    public static DashboardFragment getInstance(Bundle bundle) {
        DashboardFragment dashboardFragment = new DashboardFragment();
        if (bundle != null)
            dashboardFragment.setArguments(bundle);
        return dashboardFragment;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.dashboard_fragment, null, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!TextUtils.isEmpty(MovitConsumerApp.getInstance().getKycStatus())) {
            if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("N")) {
                handleUiForNonKYC();
                ViewCompat.setElevation(cardoffers, 0);
            } else if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("P")) {
                handleUiForNonKYC();
                // linlayDim.setVisibility(View.VISIBLE);
                ViewCompat.setElevation(cardoffers, 0);
            } else {
                //  linlayDim.setVisibility(View.GONE);
                ViewCompat.setElevation(cardoffers, 10);
                //cardoffers.setElevation(10);
            }
        }
        else if(!AppSettings.getBooleanData(mContext,AppSettings.IS_FIRSTTIMELOGIN))
        {
            handleUIForGuestLogin();
        }

        url_maps = new HashMap<>();
        //url_maps.put("Coming Soon", "https://qamovit.timesofmoney.in/mps/FileViewer?type=promocode&fileName=banner4.png&spId=uaexchange");
        //url_maps.put("Coming Soon", "https://qamovit.timesofmoney.in/mps/FileViewer?type=promocode&fileName=banner4.png&spId=uaexchange");
      /*  url_maps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        url_maps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg")*/
        ;


    }

    private void setSliderData(HashMap<String, String> url_maps) {
        mDemoSlider.removeAllSliders();
        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(mContext);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setOnSliderClickListener(DashboardFragment.this);


            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(DashboardFragment.this);
    }

    private void handleUIForGuestLogin() {
        layoutP2p.setEnabled(false);
        layoutP2p.setClickable(false);

        layoutSplitandshare.setEnabled(false);
        layoutSplitandshare.setClickable(false);

        layoutP2m.setEnabled(false);
        layoutP2m.setClickable(false);

        layoutRecharge.setEnabled(false);
        layoutRecharge.setClickable(false);

        rlNotification.setClickable(false);
        rlNotification.setEnabled(false);

        ivRefresh.setClickable(false);
        ivRefresh.setEnabled(false);
    }

    private void handleUiForNonKYC() {

        layoutP2p.setEnabled(false);
        layoutP2p.setClickable(false);

        layoutSplitandshare.setEnabled(false);
        layoutSplitandshare.setClickable(false);

        layoutP2m.setEnabled(false);
        layoutP2m.setClickable(false);

        //layoutRecharge.setEnabled(false);
        //layoutRecharge.setClickable(false);

        rlNotification.setClickable(false);
        rlNotification.setEnabled(false);

        ivRefresh.setClickable(false);
        ivRefresh.setEnabled(false);

        btnAddMoney.setClickable(false);
        btnAddMoney.setEnabled(false);
    }

    private void handleUIOnCreate() {
        //Glide.with(DashboardFragment.this).load(getArguments().getString(IntentConstants.PHOTO_URL)).placeholder(R.mipmap.ic_launcher).into(imageViewProfile);
        if (Constants.isUserLoggedIn(mContext)) {
            rlWithLogin.setVisibility(View.VISIBLE);
            rlWithoutLogin.setVisibility(View.GONE);
        } else {
            rlWithLogin.setVisibility(View.GONE);
            rlWithoutLogin.setVisibility(View.VISIBLE);
        }

        if (txtBalance != null && !TextUtils.isEmpty(MovitConsumerApp.getInstance().getWalletBalance())) {
            txtBalance.setText(getString(R.string.AED) + " " + Constants.formatAmount(MovitConsumerApp.getInstance().getWalletBalance()));
        }


        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (Constants.isUserLoggedIn(mContext)) {
            if (timeOfDay >= 0 && timeOfDay < 12) {
                llBalance.setBackgroundResource(R.drawable.morning_dashboard);
                tvWelcomeTitle.setText(getString(R.string.goodmorningtext) + "" + Constants.getUserNameForWelcome(mContext));
            } else if (timeOfDay >= 12 && timeOfDay < 16) {
                llBalance.setBackgroundResource(R.drawable.afternoon_dashboard);
                tvWelcomeTitle.setText(getString(R.string.goodafternoontext) + "" + Constants.getUserNameForWelcome(mContext));
            } else if (timeOfDay >= 16 && timeOfDay < 21) {
                llBalance.setBackgroundResource(R.drawable.night_dashboard);
                tvWelcomeTitle.setText(getString(R.string.goodeveningtext) + "" + Constants.getUserNameForWelcome(mContext));

            } else if (timeOfDay >= 21 && timeOfDay < 24) {
                llBalance.setBackgroundResource(R.drawable.night_dashboard);
                tvWelcomeTitle.setText(getString(R.string.goodeveningtext) + "" + Constants.getUserNameForWelcome(mContext));
            }
        } else {
            if (timeOfDay >= 0 && timeOfDay < 12) {
                llBalance.setBackgroundResource(R.drawable.morning_dashboard);
                tvWelcomeTitle.setText(getString(R.string.welcome_guest));
            } else if (timeOfDay >= 12 && timeOfDay < 16) {
                llBalance.setBackgroundResource(R.drawable.afternoon_dashboard);
                tvWelcomeTitle.setText(getString(R.string.welcome_guest));
            } else if (timeOfDay >= 16 && timeOfDay < 21) {
                llBalance.setBackgroundResource(R.drawable.night_dashboard);
                tvWelcomeTitle.setText(getString(R.string.welcome_guest));

            } else if (timeOfDay >= 21 && timeOfDay < 24) {
                llBalance.setBackgroundResource(R.drawable.night_dashboard);
                tvWelcomeTitle.setText(getString(R.string.welcome_guest));
            }

        }


    }

    @Override
    public void onResume() {
        super.onResume();
        handleUIOnCreate();
        handleNotificationCount();


        if (Constants.isUserLoggedIn(mContext)) {
            callBalanceAPI(false);
        } else {
            // progressBar.setVisibility(View.GONE);
            // layoutBalance.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {
        //  Toast.makeText(mContext, slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.rlHamIcon, R.id.toolbar_dashboard_rl_for_notification, R.id.layout_p2m, R.id.layout_p2p, R.id.layout_splitandshare, R.id.layout_recharge, R.id.layout_event_tkt, R.id.layout_gifting, R.id.layout_entertainment, R.id.ivRefresh, R.id.btnSignUp, R.id.btnLogin, R.id.btnAddMoney})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.rlHamIcon:
                leftMenuFragment = new LeftMenuFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_down, R.anim.slide_out_down, R.anim.slide_in_up, R.anim.slide_out_up);
                fragmentTransaction.replace(R.id.frame, leftMenuFragment).addToBackStack(null).commit();
                break;

            case R.id.toolbar_dashboard_rl_for_notification:

                BaseActivity.openNewActivity(mContext, NotificationActivity.class.getCanonicalName(), true);

                break;

            case R.id.layout_p2m:

                int permission = ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA);

                if (permission != PackageManager.PERMISSION_GRANTED) {
                    // We don't have permission so prompt the user
//                    ActivityCompat.requestPermissions(getActivity(),
//                            new String[]{Manifest.permission.CAMERA},
//                            1);
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            1);

                } else {
                    BaseActivity.openNewActivity(mContext, P2MQRCodeScanActivity.class.getCanonicalName(), true);
                }


                break;
            case R.id.layout_p2p:
                BaseActivity.openNewActivity(mContext, PayPersonActivity.class.getCanonicalName(), true);
                break;
            case R.id.layout_splitandshare:

                openSplitShareInitialDialog();

                break;
            case R.id.btnAddMoney:
                BaseActivity.openNewActivity(mContext, AddMoneyActivity.class.getCanonicalName(), false);
                break;
            case R.id.btnSignUp:
                BaseActivity.openNewActivity(mContext, RegisterActivity.class.getCanonicalName(), false);
                break;

            case R.id.btnLogin:
                BaseActivity.openNewActivity(mContext, LoginActivity.class.getCanonicalName(), false);
                break;

            case R.id.layout_recharge:
                BaseActivity.openNewActivity(mContext, RechargeActivity.class.getCanonicalName(), true);
                break;
            case R.id.layout_event_tkt:

                break;
            case R.id.layout_gifting:
                break;
            case R.id.layout_entertainment:
                break;
            case R.id.ivRefresh:
                callBalanceAPI(true);
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            BaseActivity.openNewActivity(mContext, P2MQRCodeScanActivity.class.getCanonicalName(), true);

        } else {
            ((BaseActivity) getActivity()).showError("Please accept permission to send money to merchant.");
        }
    }


    public void openSplitShareInitialDialog() {


        View view = getActivity().getLayoutInflater().inflate(R.layout.split_share_initial_dialog, null);

        final Dialog dialog = new Dialog(mContext);

        TextView txtvwContinue = ButterKnife.findById(view, R.id.txtvwContinue);
        ImageView imgvwClose = ButterKnife.findById(view, R.id.imgvwClose);

        txtvwContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                BaseActivity.openNewActivity(mContext, SplitAndShareActivity.class.getCanonicalName(), true);
            }
        });

        imgvwClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before setcontentview
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

    }


    public void handleNotificationCount() {
        String c = (AppSettings.getData(mContext, AppSettings.NOTIFICATION_COUNT).equals("")) ? "0" : AppSettings.getData(mContext, AppSettings.NOTIFICATION_COUNT);
        int count = Integer.parseInt(c);

        if (count > 0 && Constants.isUserLoggedIn(mContext)) {
            tvNotificationCount.setText(count + "");
            tvNotificationCount.setVisibility(View.VISIBLE);
        } else {
            tvNotificationCount.setVisibility(View.GONE);

        }
    }


    private void callBalanceAPI(final boolean isRefreshClicked) {


        if (MovitConsumerApp.getInstance().getPrimaryWallet() != null) {

            //   layoutBalance.setVisibility(View.GONE);
            //progressBarBal.setVisibility(View.VISIBLE);
            if (isRefreshClicked) {
                rotateanimation = new AnimationUtils();
                rotateanimation.rotateView(ivRefresh, 0, 360, ivRefresh.getWidth() / 2, ivRefresh.getHeight() / 2, null);
            }


            final String walletID = MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId();
            MovitWalletController controller = new BalanceInquiryController(walletID);

            controller.init(new IMovitWalletController<Bundle>() {
                @Override
                public void onSuccess(Bundle response) {

                    MovitConsumerApp.getInstance().setWalletBalanceByID(walletID, response.getString("Balance"));

                    if (getActivity() != null) {
                        MovitConsumerApp.getInstance().setWalletBalance(response.getString("Balance"));
                        if (txtBalance != null) {
                            txtBalance.setText(getString(R.string.AED) + " " + Constants.formatAmount(response.getString("Balance")));
                        }
                        if (isRefreshClicked) {
                            rotateanimation.stopRotateAnimation(ivRefresh);
                        }
                    }
                    callOffersList();

                }

                @Override
                public void onFailed(String errorCode, String reason) {

                    if (getActivity() != null) {
                        // progressBar.setVisibility(View.GONE);
                        //  layoutBalance.setVisibility(View.VISIBLE);
                        MovitConsumerApp.getInstance().setWalletBalance("");
                        if (txtBalance != null) {
                            txtBalance.setText(getString(R.string.dummyAmount));
                        }
                        if (rotateanimation != null && ivRefresh != null) {
                            rotateanimation.stopRotateAnimation(ivRefresh);
                        }
                        // Toast.makeText(getActivity(), reason, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {

            //  progressBar.setVisibility(View.GONE);
            //  layoutBalance.setVisibility(View.VISIBLE);
        }
    }

    private void callOffersList() {
        OffersController offersController = new OffersController();

        //  showProgress();

        offersController.init(new IMovitWalletController<List<Offer>>() {
            @Override
            public void onSuccess(List<Offer> response) {
                //  dismissProgress();

                if (response.size() > 0) {
                    for (int i = 0; i < response.size(); i++) {
                        url_maps.put(response.get(i).getOfferName(), MovitConsumerApp.getInstance().getBaseURL() + response.get(i).getImageURL());
                    }
                    setSliderData(url_maps);
                } else {
                    // txtvwNoOffers.setVisibility(View.VISIBLE);
                }

            }


            @Override
            public void onFailed(String errorCode, String reason) {
                //dismissProgress();
                //txtvwNoOffers.setVisibility(View.VISIBLE);
            }
        });
    }

}
