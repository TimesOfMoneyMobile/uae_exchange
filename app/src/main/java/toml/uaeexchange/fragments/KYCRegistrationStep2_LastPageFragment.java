package toml.uaeexchange.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aviadmini.quickimagepick.PickCallback;
import com.aviadmini.quickimagepick.PickSource;
import com.aviadmini.quickimagepick.PickTriggerResult;
import com.aviadmini.quickimagepick.QiPick;
import com.aviadmini.quickimagepick.UriUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import fr.ganfra.materialspinner.MaterialSpinner;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import toml.movitwallet.apicalls.RetrofitTask;
import toml.movitwallet.models.Document;
import toml.movitwallet.models.RegistrationRequest;
import toml.movitwallet.utils.AESecurity;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.GenericResponseHandler;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.KYCRegistrationActivity;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.MultipartUtility;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.facebook.login.widget.ProfilePictureView.TAG;


@RuntimePermissions
public class KYCRegistrationStep2_LastPageFragment extends Fragment {


    private static final String DIR_NAME = "UAExchange Photos";
    @PickTriggerResult
    int triggerResult;

    Context mContext;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    Unbinder unbinder;
    @BindView(R.id.spinnerDocType)
    MaterialSpinner spinnerDocType;
    @BindView(R.id.btnTakePicture)
    Button btnTakePicture;
    @BindView(R.id.btnChooseFromGallery)
    Button btnChooseFromGallery;

    @BindView(R.id.imgvwLastImage)
    ImageView imgvwLastImage;
    @BindView(R.id.btnSubmit)
    CustomButton btnSubmit;
    @BindView(R.id.toolbar_pay)
    Toolbar toolbarPay;

    private ProgressDialog progressDialog;

    private Uri mSelectedImageUri;
    Map<Document, Map<String, Bitmap>> documentMap;
    private HashMap<String, Bitmap> imageByteHashMap;
    KYCRegistrationActivity activity;
    ArrayList<Document> documents;
    Document selectedDocument;
    HashMap<String, File> fileHashMap = null;

    String boundry = "text/plain";

    String strResponse = "";
    Typeface fontBold;

    public KYCRegistrationStep2_LastPageFragment() {
        // Required empty public constructor
    }


    public static KYCRegistrationStep2_LastPageFragment newInstance(String param1, String param2) {
        KYCRegistrationStep2_LastPageFragment fragment = new KYCRegistrationStep2_LastPageFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kycregistration_step2_last_stage, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((KYCRegistrationActivity) getActivity()).setToolbar(toolbarPay, "Scan & Upload", R.drawable.icon_back, 0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().onBackPressed();

            }
        }, null);

        progressBar.setProgress(45);

        activity = (KYCRegistrationActivity) getActivity();

        fontBold = Typeface.createFromAsset(activity.getAssets(), "fonts/Quicksand-Bold.ttf");


        documentMap = activity.getDocumentMap();
        documents = activity.getDocuments();

        spinnerDocType.setHint("Choose Document Type");


        if (documents != null && documents.size() > 0) {
            ArrayAdapter<Document> dataAdapter = new ArrayAdapter<Document>(getActivity(), android.R.layout.simple_spinner_item, documents);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerDocType.setAdapter(dataAdapter);
        }


        spinnerDocType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position >= 0) {

                    LogUtils.Verbose("Pos", "" + position + " "
                            + documents.get(position).getName());

                    TextView selectedView = (TextView) spinnerDocType.getSelectedView();
                    if (selectedView != null) {
                        selectedView.setTypeface(fontBold);
                        selectedView.setTextColor(Color.BLACK);
                    }

                    selectedDocument = documents.get(position);

                    selectedDocument = documents.get(position);
                    if (!documentMap.containsKey(selectedDocument)) {
                        HashMap<String, Bitmap> bitmapHashMap = new HashMap<String, Bitmap>();
                        documentMap.put(selectedDocument, bitmapHashMap);

                    }

                    btnTakePicture.setEnabled(true);
                    btnChooseFromGallery.setEnabled(true);
                } else {
                    btnTakePicture.setEnabled(false);
                    btnChooseFromGallery.setEnabled(false);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        final View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View pView) {
                KYCRegistrationStep2_LastPageFragmentPermissionsDispatcher.onViewClickedWithCheck(KYCRegistrationStep2_LastPageFragment.this, pView);
            }
        };


        btnTakePicture.setOnClickListener(clickListener);
        btnChooseFromGallery.setOnClickListener(clickListener);


    }

    @Override
    public void onResume() {
        super.onResume();

        selectedDocument = activity.getSelectedDocument();

        if (documents != null) {
            if (documents.indexOf(selectedDocument) >= 0) {
                // Note - Here we adding 1 in index due to hint
                spinnerDocType.setSelection(documents.indexOf(selectedDocument) + 1);
                spinnerDocType.setEnabled(false);


            }
        }
    }


    private PickCallback mCallback = new PickCallback() {
        @Override
        public void onImagePicked(@NonNull PickSource pickSource, int requestType, @NonNull Uri imageUri) {
            mSelectedImageUri = imageUri;

            final Context context = getApplicationContext();

            final boolean exists = UriUtils.contentExists(context, imageUri);

            if (!exists) {

                Toast.makeText(context, "Image does not exist.", Toast.LENGTH_SHORT)
                        .show();

                return;
            }

            final String extension = UriUtils.getFileExtension(context, imageUri);
            Log.i(TAG, "Picked: " + imageUri.toString() + "\nMIME type: " + UriUtils.getMimeType(context,
                    imageUri) + "\nFile extension: " + extension + "\nRequest type: " + requestType);

            // Do something with Uri, for example load image into an ImageView
            Glide.with(context)
                    .load(mSelectedImageUri)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(imgvwLastImage);


            try {
                //byte [] imageBytes = activity.readBytesFromURI(mSelectedImageUri);

                Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), imageUri);
                // imageBitmap = Bitmap.createBitmap(imageBitmap, 0, 0, 100, 100);

                // imageBitmap = Bitmap.createScaledBitmap(imageBitmap, 300, 300, false);

                HashMap<String, Bitmap> bitmapHashMap = (HashMap<String, Bitmap>) documentMap.get(selectedDocument);
                String img_id = selectedDocument.getId() + "_" + "1";
                Log.v("TAG", " Image ID " + img_id);
                bitmapHashMap.put(img_id, imageBitmap);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 20, stream);

                ((KYCRegistrationActivity) getActivity()).getImageByteStreamMap().put(img_id, stream);

                //((KYCRegistrationActivity) getActivity()).getImageByteHashMap().put(img_id, imageBitmap);




               /* String path = mSelectedImageUri.getPath().substring(0, mSelectedImageUri.getPath().indexOf(":"));

                File file = new File(path + "/" + getFileName(mSelectedImageUri));
                ((KYCRegistrationActivity) getActivity()).getFileHashMap().put(img_id, file);*/


            } catch (Exception e) {
                e.printStackTrace();
            }


            try {

              /*  final String ext = extension == null ? "" : "." + extension;
                final File outDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DIR_NAME);
                final File file = new File(outDir, "qip_temp" + ext);

                //noinspection ResultOfMethodCallIgnored
                outDir.mkdirs();

                // DO NOT do this on main thread. This is only for reference
                UriUtils.saveContentToFile(context, imageUri, file);

                Toast.makeText(context, "Save complete", Toast.LENGTH_SHORT);//.show();*/

                btnTakePicture.setText("Take Another Picture");
                btnTakePicture.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                btnTakePicture.setBackgroundColor(ContextCompat.getColor(mContext, R.color.aqua_green));
                btnChooseFromGallery.setText("Edit Picture");
                //btnNext.setVisibility(View.VISIBLE);
                btnSubmit.setEnabled(true);
                progressBar.setProgress(60);

            } catch (final Exception e) {
                Toast.makeText(context, "Save failed: " + e.getMessage(), Toast.LENGTH_SHORT)
                        .show();
            }

        }

        @Override
        public void onMultipleImagesPicked(int i, @NonNull List<Uri> list) {

        }

        @Override
        public void onError(@NonNull PickSource pickSource, int i, @NonNull String s) {

        }

        @Override
        public void onCancel(@NonNull PickSource pickSource, int i) {

        }
    };


    public void showProgress() {

        progressDialog = new ProgressDialog(mContext);
        progressDialog.setCanceledOnTouchOutside(false);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = getActivity().getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        progressDialog.show();

    }

    public void dismissProgress() {

        if (progressDialog != null && progressDialog.isShowing()) {
            LogUtils.Verbose("inside if", "dismissProgress......");
            progressDialog.dismiss();
        }
    }

    private void setData() {


        RegistrationRequest objRegistrationRequest = ((KYCRegistrationActivity) getActivity()).getRegistrationRequestObject();

        fileHashMap = null;

        try {
            fileHashMap = createFileArray();

            //fileHashMap = ((KYCRegistrationActivity) getActivity()).getFileHashMap();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (fileHashMap.size() == 0) {
            new OkDialog(getActivity(), getString(R.string.app_name), null, null);
            return;
        }

        Map<String, RequestBody> requestBodyMap = new HashMap<>();

        for (Document document : documentMap.keySet()) {
            int count = documentMap.get(document).size();
            String countStr = document.getId() + "_count";

            Log.v("TAG", " doc_Count " + countStr + " " + count);

            RequestBody doc_count = RequestBody.create(MediaType.parse(boundry), count + "");
            requestBodyMap.put(countStr, doc_count);
            //TODO Need to add docID_Value here
            //requestBodyMap.put(countStr, value);
        }
        for (String filename : fileHashMap.keySet()) {

            Log.v("TAG", " Adding file to body");
            RequestBody requestBody =
                    RequestBody.create(MediaType.parse("image/jpeg"), fileHashMap.get(filename));

            // RequestBody re = new MultipartBody.Builder().build();

          /*  RequestBody multirequestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("image", "android.jpeg",
                            requestBody)
                    .build();*/


            // MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);

            requestBodyMap.put(filename, requestBody);
        }


        RequestBody emiratesid = RequestBody.create(MediaType.parse(boundry), objRegistrationRequest.getEmiratesID());
        requestBodyMap.put(selectedDocument.getId() + "_value ", emiratesid);


        RequestBody firstname = RequestBody.create(MediaType.parse(boundry), objRegistrationRequest.getFirstName());
        requestBodyMap.put("firstName", firstname);

        RequestBody lastName = RequestBody.create(MediaType.parse(boundry), objRegistrationRequest.getLastName());
        requestBodyMap.put("lastName", lastName);

        /*RequestBody gender = RequestBody.create(MediaType.parse("multipart/form-data"), addCustomerPOJO.getGender());
        requestBodyMap.put("gender", gender);*/

        RequestBody dob = RequestBody.create(MediaType.parse(boundry), objRegistrationRequest.getDateOfBirth());
        requestBodyMap.put("dob", dob);

        RequestBody emailId = RequestBody.create(MediaType.parse(boundry), objRegistrationRequest.getEmailId());
        requestBodyMap.put("emailId", emailId);

        RequestBody mobileNo = RequestBody.create(MediaType.parse(boundry), objRegistrationRequest.getMobileNumber());
        requestBodyMap.put("mobileNo", mobileNo);

       /* RequestBody address = RequestBody.create(MediaType.parse("multipart/form-data"), objRegistrationRequest.getAddress());
        requestBodyMap.put("address", address);

        RequestBody city = RequestBody.create(MediaType.parse("multipart/form-data"), addCustomerPOJO.getCity());
        requestBodyMap.put("city", city);*/

        RequestBody userType = RequestBody.create(MediaType.parse(boundry), Constants.USER_TYPE);
        requestBodyMap.put("userType", userType);

        RequestBody requestType = RequestBody.create(MediaType.parse(boundry), "ManagedProfile");
        requestBodyMap.put("requestType", requestType);

        RequestBody spid = RequestBody.create(MediaType.parse(boundry), Constants.SERVICE_PROVIDER);
        requestBodyMap.put("spid", spid);

        RequestBody channelID = RequestBody.create(MediaType.parse(boundry), "App");
        requestBodyMap.put("channelId", channelID);

       /* RequestBody emiratesId = RequestBody.create(MediaType.parse("multipart/form-data"), objRegistrationRequest.getEmiratesID());
        requestBodyMap.put("emiratesId", emiratesId);*/

        RequestBody nationality = RequestBody.create(MediaType.parse(boundry), objRegistrationRequest.getNationality());
        requestBodyMap.put("nationality", nationality);

        RequestBody alias = RequestBody.create(MediaType.parse(boundry), "");
        requestBodyMap.put("alias", alias);

      /*  RequestBody AgentId = RequestBody.create(MediaType.parse("multipart/form-data"), AgentApplication.getInstance().getAgentID());
        requestBodyMap.put("agentId", AgentId);*/


        activity.setRequestBodyMap(requestBodyMap);
        activity.setFileHashMap(fileHashMap);

        // callRegistrationWithMultipart();

        //callUploadTask();

        callRegistrationWithMultipartLatest();

    }


    private void callRegistrationWithMultipart() {

        Map<String, RequestBody> requestBodyMap = activity.getRequestBodyMap();

      /*  RequestBody TPIN = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        //requestBodyMap.put("tpin ", TPIN);*/

        RetrofitTask retrofitTask = RetrofitTask.getInstance();

        retrofitTask.executeRegisterCustomer(requestBodyMap, new RetrofitTask.IRetrofitTask() {
            @Override
            public void onSuccess(String response) {


                ArrayList<String> list = new ArrayList<>();
                list.add(IntentConstants.ResponseType);
                list.add(IntentConstants.ErrorCode);
                list.add(IntentConstants.Reason);
                list.add(IntentConstants.Message);

                Bundle responseMap = GenericResponseHandler.parseElements(response, list);

                if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                    //  Toast.makeText(mContext, "Registration Success!!!", Toast.LENGTH_LONG).show();

                    activity.deleteCacheFiles();

                    KYCRegistrationActivity.thisActivity.getPager().setCurrentItem(3);


                } else

                {
                    Toast.makeText(mContext, responseMap.get(IntentConstants.Reason).toString(), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                Toast.makeText(mContext, getString(R.string.error_in_registation), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();

            }
        });


    }


    private void callUploadTask() {
        String charset = "UTF-8";

        try {

            RegistrationRequest objRegistrationRequest = ((KYCRegistrationActivity) getActivity()).getRegistrationRequestObject();
            MultipartUtility objMultipartUtility = new MultipartUtility("https://dw.uaeexchange.com/mps/CustomerRegistration", charset);

            for (Document document : documentMap.keySet()) {
                int count = documentMap.get(document).size();
                String countStr = document.getId() + "_count";

                Log.v("TAG", " doc_Count " + countStr + " " + count);

                objMultipartUtility.addFormField(countStr, "" + count);


                //TODO Need to add docID_Value here
                //requestBodyMap.put(countStr, value);
            }


            for (String filename : fileHashMap.keySet()) {

                Log.v("TAG", " Adding file to body");
               /* RequestBody requestBody =
                        RequestBody.create(MediaType.parse("image/jpeg"), fileHashMap.get(filename));*/

                objMultipartUtility.addFilePart(filename, fileHashMap.get(filename));


            }


            for (String img_name : ((KYCRegistrationActivity) getActivity()).getImageByteHashMap().keySet()) {

                Log.v("TAG", " Image Name " + img_name);
                Bitmap bitmap = ((KYCRegistrationActivity) getActivity()).getImageByteHashMap().get(img_name);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                byte[] byteArray = stream.toByteArray();
                ByteArrayBody imageBody = new ByteArrayBody(byteArray, img_name + ".jpeg");
                //objMultipartUtility.addFilePart(img_name, imageBody);
                // builder.addPart(img_name, imageBody);
                bitmap.recycle();
            }

            objMultipartUtility.addFormField("" + selectedDocument.getId() + "_value ", "" + objRegistrationRequest.getEmiratesID());
            objMultipartUtility.addFormField("firstName", "" + objRegistrationRequest.getFirstName());
            objMultipartUtility.addFormField("lastName", "" + objRegistrationRequest.getLastName());
            objMultipartUtility.addFormField("dob", "" + objRegistrationRequest.getDateOfBirth());
            objMultipartUtility.addFormField("emailId", "" + objRegistrationRequest.getEmailId());
            objMultipartUtility.addFormField("mobileNo", "" + objRegistrationRequest.getMobileNumber());
            objMultipartUtility.addFormField("userType", "" + Constants.USER_TYPE);
            objMultipartUtility.addFormField("requestType", "ManagedProfile");
            objMultipartUtility.addFormField("spid", Constants.SERVICE_PROVIDER);
            objMultipartUtility.addFormField("channelId", "APP");
            objMultipartUtility.addFormField("nationality", objRegistrationRequest.getNationality());
            objMultipartUtility.addFormField("alias", "");


            final List<String> response = objMultipartUtility.finish();

            System.out.println("SERVER REPLIED:");

            for (String line : response) {
                strResponse = line;

            }


            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    dismissProgress();


                    ArrayList<String> list = new ArrayList<>();
                    list.add(IntentConstants.ResponseType);
                    list.add(IntentConstants.ErrorCode);
                    list.add(IntentConstants.Reason);
                    list.add(IntentConstants.Message);


                    String apiResponse = strResponse;


                    try {
                        apiResponse = AESecurity.getInstance().decryptString(apiResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    LogUtils.Verbose("API Response ", apiResponse);


                    Bundle responseMap = GenericResponseHandler.parseElements(apiResponse, list);

                    if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                        //  Toast.makeText(mContext, "Registration Success!!!", Toast.LENGTH_LONG).show();

                        activity.deleteCacheFiles();

                        KYCRegistrationActivity.thisActivity.getPager().setCurrentItem(3);


                    } else

                    {
                        Toast.makeText(mContext, responseMap.get(IntentConstants.Reason).toString(), Toast.LENGTH_LONG).show();
                    }

                }
            });


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void callRegistrationWithMultipartLatest() {

        String responseString = "";

        RegistrationRequest objRegistrationRequest = ((KYCRegistrationActivity) getActivity()).getRegistrationRequestObject();
        HttpClient httpclient = new DefaultHttpClient();

        HttpPost httppost = new HttpPost(MovitConsumerApp.getInstance().getBaseURL() + "CustomerRegistration");

        String boundary = "-------------" + System.currentTimeMillis();
        httppost.setHeader("Content-type", "multipart/form-data; boundary=" + boundary);

        try {
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            StringBody emiratesID = new StringBody(objRegistrationRequest.getEmiratesID(), ContentType.TEXT_PLAIN);
            StringBody firstName = new StringBody(objRegistrationRequest.getFirstName(), ContentType.TEXT_PLAIN);
            StringBody lastName = new StringBody(objRegistrationRequest.getLastName(), ContentType.TEXT_PLAIN);
            // StringBody gender = new StringBody(profile.getGender(), ContentType.TEXT_PLAIN);
            StringBody dob = new StringBody(objRegistrationRequest.getDateOfBirth(), ContentType.TEXT_PLAIN);
            StringBody emailId = new StringBody(objRegistrationRequest.getEmailId(), ContentType.TEXT_PLAIN);
            StringBody mobileNo = new StringBody(objRegistrationRequest.getMobileNumber(), ContentType.TEXT_PLAIN);
            // StringBody address = new StringBody(o.getAddress(), ContentType.TEXT_PLAIN);
            // StringBody city = new StringBody(obj.getCity(), ContentType.TEXT_PLAIN);
            StringBody userType = new StringBody("CU", ContentType.TEXT_PLAIN);
            StringBody channelID = new StringBody("APP", ContentType.TEXT_PLAIN);

            StringBody requestType = new StringBody("ManagedProfile", ContentType.TEXT_PLAIN);
            StringBody spid = new StringBody("uaexchange", ContentType.TEXT_PLAIN);
            StringBody otp = new StringBody("", ContentType.TEXT_PLAIN);
            StringBody alias = new StringBody("", ContentType.TEXT_PLAIN);
            StringBody nationality = new StringBody("Egyptian", ContentType.TEXT_PLAIN);


            for (Document document : documentMap.keySet()) {

                int count = documentMap.get(document).size();
                String countStr = document.getId() + "_count";

                Log.v("TAG", " doc_Count " + countStr + " " + count);
                StringBody doc_count_Body = new StringBody(count + "", ContentType.TEXT_PLAIN);
                builder.addPart(countStr, doc_count_Body);


            }


            for (String img_name : ((KYCRegistrationActivity) getActivity()).getImageByteStreamMap().keySet()) {

                Log.v("TAG", " Image Name " + img_name);
                //Bitmap bitmap = ((KYCRegistrationActivity) getActivity()).getIm().get(img_name);

               /* int bytes = bitmap.getByteCount();

                ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
                bitmap.copyPixelsToBuffer(buffer); //Move the byte data to the buffer

                byte[] array = buffer.array(); //Get the underlying array containing the data.*/

                //  Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
                // ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                byte[] byteArray = ((KYCRegistrationActivity) getActivity()).getImageByteStreamMap().get(img_name).toByteArray();

                //image/jpeg
                ByteArrayBody imageBody = new ByteArrayBody(byteArray, "image/jpeg", img_name + ".jpeg");
                builder.addPart(img_name, imageBody);
                // bitmap.recycle();
            }
            builder.addPart(selectedDocument.getId() + "_value ", emiratesID);
            builder.addPart("firstName", firstName);
            builder.addPart("lastName", lastName);
            // builder.addPart("gender", gender);
            builder.addPart("dob", dob);
            builder.addPart("emailId", emailId);
            builder.addPart("mobileNo", mobileNo);
            // builder.addPart("address", address);
            //  builder.addPart("city", city);
            builder.addPart("userType", userType);
            builder.addPart("requestType", requestType);
            builder.addPart("spid", spid);
            builder.addPart("OTP", otp);
            builder.addPart("alias", alias);
            builder.addPart("nationality", nationality);
            builder.addPart("channelId", channelID);


            builder.setBoundary(boundary);

            final HttpEntity entity = builder.build();
            Log.v("Entity ", entity.toString());
            httppost.setEntity(entity);

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();


            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {

                responseString = EntityUtils.toString(r_entity);

                //  EntityUtilsHC4.consume(r_entity);
                Log.d("TAG67", "responseString=" + responseString);


                final String finalResponseString = responseString;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        dismissProgress();


                        ArrayList<String> list = new ArrayList<>();
                        list.add(IntentConstants.ResponseType);
                        list.add(IntentConstants.ErrorCode);
                        list.add(IntentConstants.Reason);
                        list.add(IntentConstants.Message);

                        String apiResponse = finalResponseString;

                        try {
                            apiResponse = AESecurity.getInstance().decryptString(apiResponse);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        LogUtils.Verbose("API Response ", apiResponse);


                        Bundle responseMap = GenericResponseHandler.parseElements(apiResponse, list);

                        if (responseMap.get(IntentConstants.ResponseType).equals("Success")) {

                            //  Toast.makeText(mContext, "Registration Success!!!", Toast.LENGTH_LONG).show();

                            activity.deleteCacheFiles();

                            KYCRegistrationActivity.thisActivity.getPager().setCurrentItem(3);


                        } else

                        {
                            Toast.makeText(mContext, responseMap.get(IntentConstants.Reason).toString(), Toast.LENGTH_LONG).show();
                        }

                    }
                });
            } else {
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;

                Log.d("TAG67", "responseString=" + responseString);
            }


            if (r_entity != null) {
                try {
                    r_entity.consumeContent();
                } catch (IOException e) {
                    Log.e("TAG", "", e);
                }
            }

            // Safe close

   /* if (httpclient != null && httpclient.getConnectionManager() != null) {

        httpclient.getConnectionManager().shutdown();
    }*/


        } catch (ClientProtocolException e) {
            responseString = e.toString();
        } catch (IOException e) {
            responseString = e.toString();
        }

    }

    private HashMap<String, File> createFileArray() throws IOException {

        HashMap<String, File> fileHashMap = new HashMap<>();
        File storageDir = getActivity().getCacheDir();

        //  File storageDir = new File(Environment.getExternalStorageDirectory() + "/UAEX/");

        if (!storageDir.exists()) {
            boolean storage = storageDir.mkdir();
            Log.v("TAG", " Created Dir " + storage + " PAth" + storageDir.getAbsolutePath());

        }

        for (Document document : documentMap.keySet()) {
            HashMap<String, Bitmap> bitmapHashMap = (HashMap<String, Bitmap>) documentMap.get(document);

            for (String img_name : bitmapHashMap.keySet()) {

                Bitmap bitmap = bitmapHashMap.get(img_name);
                File image = new File(storageDir, img_name + ".jpeg");
                OutputStream os = new BufferedOutputStream(new FileOutputStream(image));
                bitmap.compress(Bitmap.CompressFormat.JPEG, 20, os);
                os.close();

                Log.v("TAG", " Creating file " + img_name + "   " + image.getAbsolutePath());
                Log.v("TAG", "file size " + img_name + "   " + Integer.parseInt(String.valueOf(image.length() / 1024)));
                fileHashMap.put(img_name, image);

                bitmap.recycle();
            }
        }

        return fileHashMap;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnTakePicture, R.id.btnChooseFromGallery, R.id.btnSubmit})
    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})

    public void onViewClicked(View view) {

        final File outDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DIR_NAME);
        Log.d(TAG, outDir.getAbsolutePath() + ", can write: " + outDir.canWrite());

        switch (view.getId()) {
            case R.id.btnTakePicture:


                triggerResult = QiPick.in(this)
                        .withAllImageMimeTypesAllowed()
                        .withCameraPicsDirectory(outDir)
                        .withRequestType(3)
                        .fromCamera();

                this.solveTriggerResult(triggerResult);


                break;
            case R.id.btnChooseFromGallery:

                triggerResult = QiPick.in(this)
                        .allowOnlyLocalContent(true)
                        .withAllowedMimeTypes(QiPick.MIME_TYPE_IMAGE_JPEG, QiPick.MIME_TYPE_IMAGE_WEBP)
                        .withCameraPicsDirectory(outDir)
                        .withRequestType(1)
                        .fromMultipleSources("Select image from", PickSource.DOCUMENTS, PickSource.GALLERY);

                this.solveTriggerResult(triggerResult);
                break;

            case R.id.btnSubmit:

                showProgress();


                Runnable r = new Runnable() {
                    @Override
                    public void run() {

                        setData();

                    }
                };

                new Thread(r).start();
                break;

        }
    }


    private void solveTriggerResult(final @PickTriggerResult int pTriggerResult) {

        switch (pTriggerResult) {

            case PickTriggerResult.TRIGGER_PICK_ERR_CAM_FILE: {

                Toast.makeText(mContext, "Could not create file to save Camera image. Make sure camera pics dir is writable", Toast.LENGTH_SHORT)
                        .show();

                break;
            }

            case PickTriggerResult.TRIGGER_PICK_ERR_NO_ACTIVITY: {

                Toast.makeText(mContext, "There is no Activity that can pick requested file :(", Toast.LENGTH_SHORT)
                        .show();

                break;
            }

            case PickTriggerResult.TRIGGER_PICK_ERR_NO_PICK_SOURCES: {

                Toast.makeText(mContext, "Dear dev, multiple source request needs at least one source!", Toast.LENGTH_SHORT)
                        .show();

                break;
            }

            case PickTriggerResult.TRIGGER_PICK_OK: {
                break;// all good, do nothing
            }

        }

    }


    @Override
    public void onActivityResult(final int pRequestCode, final int pResultCode, final Intent pData) {

        if (!QiPick.handleActivityResult(getApplicationContext(), pRequestCode, pResultCode, pData, this.mCallback)) {
            super.onActivityResult(pRequestCode, pResultCode, pData);
        }

    }

    @Override
    public void onRequestPermissionsResult(final int pRequestCode, @NonNull final String[] pPermissions, @NonNull final int[] pGrantResults) {
        super.onRequestPermissionsResult(pRequestCode, pPermissions, pGrantResults);

        KYCRegistrationStep2_LastPageFragmentPermissionsDispatcher.onRequestPermissionsResult(KYCRegistrationStep2_LastPageFragment.this, pRequestCode, pGrantResults);

    }


}
