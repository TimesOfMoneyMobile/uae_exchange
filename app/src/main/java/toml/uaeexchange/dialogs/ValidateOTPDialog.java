package toml.uaeexchange.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by mayurn on 9/13/2017.
 */

public class ValidateOTPDialog extends Dialog {


    LinearLayout linlayTimerContainer;
    TextView txtvwReceiveSMSText, txtvwTimerValue, tvOtptext;
   /* @BindView(R.id.etOTP)
    EditText etOTP;*/

    TextView tvResendOTP;
    Spannable spannable;
    TextView tvDone;
    BaseActivity mActivity;
    IValidateOTPDialogCallBack callBack;
    MyCountDownTimer objMyCountDownTimer;
    boolean isSpanClicked;
    ErrorLabelLayout errorLabelLayoutOTP;
    CustomEditTextLayout edtxtLayoutOTP;
    private Spannable spannableMobile;

    private SmsVerifyCatcher smsVerifyCatcher;

    public void setSpanClicked(boolean spanClicked) {
        isSpanClicked = spanClicked;
    }

    public boolean isSpanClicked() {
        return isSpanClicked;
    }

    public ValidateOTPDialog(@NonNull Context context, final IValidateOTPDialogCallBack callBack) {
        super(context);
        mActivity = (BaseActivity) context;
        this.callBack = callBack;

        initUI();


        //this.show();
    }

    private void initUI() {


        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.dialog_validate_otp);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //ButterKnife.bind(mActivity);

        tvOtptext = (TextView) findViewById(R.id.tvOtptext);
        //tvOtptext.setText(mActivity.getString(R.string.verifyMobileInfo)+  " "+ MovitConsumerApp.getInstance().getMobileNumber());

        String userNumber = mActivity.getString(R.string.country_code) + " " + MovitConsumerApp.getInstance().getMobileNumber();
        String sourceString = mActivity.getString(R.string.verifyMobileInfo) + " " + userNumber;

        int i = sourceString.indexOf(userNumber);

        SpannableStringBuilder str = new SpannableStringBuilder(sourceString);
        str.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.black)), i, i + userNumber.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvOtptext.setText(str);


        tvResendOTP = (TextView) findViewById(R.id.tvResendOTP);
        tvDone = (TextView) findViewById(R.id.tvDone);
        // etOTP = (EditText) findViewById(R.id.etOTP);
        linlayTimerContainer = (LinearLayout) findViewById(R.id.linlayTimerContainer);
        txtvwReceiveSMSText = (TextView) findViewById(R.id.txtvwReceiveSMSText);
        txtvwTimerValue = (TextView) findViewById(R.id.txtvwTimerValue);

        errorLabelLayoutOTP = (ErrorLabelLayout) findViewById(R.id.errorLabelOTP);
        edtxtLayoutOTP = (CustomEditTextLayout) findViewById(R.id.edtxtLayoutOTP);
edtxtLayoutOTP.getEditText().setLongClickable(false);
        edtxtLayoutOTP.setErrorLabelLayout(errorLabelLayoutOTP);
        edtxtLayoutOTP.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutOTP.getImgLeft().setVisibility(View.GONE);

        //edtxtLayoutOTP.getErrorLabelLayout().getErrorLabelView().setVisibility(View.);

        tvResendOTP.setHighlightColor(Color.TRANSPARENT);
        String fullText = "Didn’t receive OTP? Resend OTP";
        spannable = new SpannableString(fullText);
        mActivity.setColorAndClick(tvResendOTP, fullText, spannable, "Resend OTP", ContextCompat.getColor(mActivity, R.color.clear_blue));
        handleClicks();

        smsVerifyCatcher = new SmsVerifyCatcher(mActivity, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = Constants.parseSMSForOTP(message);//Parse verification code
                try {
                    edtxtLayoutOTP.setText(code);//set code in edit text
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //then you can send verification code to server
            }
        });

    }

    public void refreshOnResend() {
        objMyCountDownTimer = new MyCountDownTimer(Constants.RESEND_OTP_TIME_IN_MILIS, 1000);
        objMyCountDownTimer.start();

    }

    public void setOTPText(String OTP) {

        if (edtxtLayoutOTP != null) {
            edtxtLayoutOTP.setText(OTP);
            edtxtLayoutOTP.getEditText().setSelection(OTP.length());
        }
    }

    private void handleClicks() {

        tvResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.resendOTPCallBack(edtxtLayoutOTP.getText().toString());
                edtxtLayoutOTP.setText("");
            }
        });
        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!TextUtils.isEmpty(edtxtLayoutOTP.getText().toString()) && edtxtLayoutOTP.getText().toString().trim().length() == mActivity.getResources().getInteger(R.integer.OTPLength)) {
                    errorLabelLayoutOTP.clearError();

                    callBack.verifyOTPCallBack(edtxtLayoutOTP.getText().toString());
                } else {
                    errorLabelLayoutOTP.setError(mActivity.getString(R.string.errorEmptyOTP));
                    edtxtLayoutOTP.setBackgroundResource(R.drawable.custom_edittext_border_red);
                    // etOTP.setError(mActivity.getString(R.string.errorEmptyOTP));
                }
            }
        });
    }

    public interface IValidateOTPDialogCallBack {
        public void resendOTPCallBack(String otpValue);

        public void verifyOTPCallBack(String otpValue);

    }


    private class MyCountDownTimer extends CountDownTimer {
        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            linlayTimerContainer.setVisibility(View.VISIBLE);
            txtvwReceiveSMSText.setVisibility(View.VISIBLE);
            txtvwTimerValue.setVisibility(View.VISIBLE);

            txtvwReceiveSMSText.setText(mActivity.getString(R.string.waitforOTP) + " " + mActivity.getString(R.string.country_code) + " " + MovitConsumerApp.getInstance().getMobileNumber() + " in ");
            txtvwTimerValue.setText("" + (Constants.RESEND_OTP_TIME_IN_MILIS / 1000) + " Secs");
            //etOTP.setText("");
            tvResendOTP.setEnabled(false);
            tvResendOTP.setAlpha(0.5f);

        }

        @Override
        public void onTick(long millisUntilFinished) {

            txtvwTimerValue.setText((millisUntilFinished / 1000) > 1 ? (millisUntilFinished / 1000) + " Secs" : (millisUntilFinished / 1000) + " Sec");

        }

        @Override
        public void onFinish() {
            tvResendOTP.setEnabled(true);
            tvResendOTP.setAlpha(1.0f);
            linlayTimerContainer.setVisibility(View.GONE);
            //txtvwTimerValue.setVisibility(View.GONE);
            // txtvwReceiveSMSText.setVisibility(View.GONE);
            objMyCountDownTimer.cancel();
        }
    }
}
