package toml.uaeexchange.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.Window;

import toml.uaeexchange.R;


/**
 * Created by kunalk on 10/26/2015.
 */
public class OkDialog {

    private AlertDialog.Builder builder;
    AlertDialog dialog;

    public OkDialog(final Context context, String title, String message, final IOkDialogCallback callback) {

        builder = new AlertDialog.Builder(context);
        if (title == null || TextUtils.isEmpty(title))
            //  title=context.getString(R.string.error);
            title = "";
        builder.setMessage(message)
                .setTitle(title);

        //if block is executed when this constructor is called from Login Activity,GetKey api.
//        if(title.equals(Constants.FORCEFULL_UPDATE_ERROR_CODE)){
//            builder.setTitle("QNB ALAHLI");
//            builder.setPositiveButton(context.getString(R.string.update), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                  dialogInterface.dismiss();
//
//                }
//            });
//
//        }else{
        builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        //}


        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if (callback != null)
                    callback.handleResponse();
            }
        });

        if (!((Activity) context).isFinishing())
            dialog.show();


    }

    // This Constructor added to avoid title
    public OkDialog(Context context, String message, final IOkDialogCallback callback) {

        builder = new AlertDialog.Builder(context);

        builder.setMessage(message)
                .setTitle("");
        builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if (callback != null)
                    callback.handleResponse();
            }
        });

        if (!((Activity) context).isFinishing())
            dialog.show();

    }


    public boolean isShowing() {
        return dialog.isShowing();
    }

    public void dismissDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public interface IOkDialogCallback {
        public void handleResponse();
    }
}
