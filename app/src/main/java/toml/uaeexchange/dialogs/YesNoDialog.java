package toml.uaeexchange.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.Window;

import toml.uaeexchange.R;

/**
 * Created by kunalk on 3/11/2016.
 */
public class YesNoDialog {

    private AlertDialog.Builder builder;
    AlertDialog dialog;
    int responsecode = 0;


    public YesNoDialog(Context context, String message, String title, final IYesNoDialogCallback callback) {

        builder = new AlertDialog.Builder(context);
        builder.setMessage(message);

        builder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                responsecode = 1;
                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if (callback != null)
                    callback.handleResponse(responsecode);
            }
        });

        if (!((Activity) context).isFinishing())
            dialog.show();

    }


    public YesNoDialog(final Context context, String title, String message, String positiveButton, String negativeButton, final IYesNoDialogCallback callback) {

        builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setCancelable(false);

        builder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                responsecode = 1;
                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if (callback != null)
                    callback.handleResponse(responsecode);
            }
        });

        if (!((Activity) context).isFinishing())
            dialog.show();


    }

    public boolean isShowing() {
        return dialog.isShowing();
    }

    public void dismissDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public interface IYesNoDialogCallback {
        public void handleResponse(int responsecode);
    }
}
