package toml.uaeexchange.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;

/**
 * Created by pankajp on 9/5/2017.
 */

public class SecurityQuestionsDialog extends Dialog {
    public SecurityQuestionsDialog(@NonNull Context context) {
        super(context);
    }

    public SecurityQuestionsDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected SecurityQuestionsDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }


    public interface onCancelClickListener
    {
        void onItemClicked();
        void onCancelClicked();
    }
}
