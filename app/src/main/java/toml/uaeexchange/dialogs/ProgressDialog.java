package toml.uaeexchange.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.widget.ProgressBar;

import toml.uaeexchange.R;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by kunalk on 2/3/2016.
 */
public class ProgressDialog extends Dialog {

    AnimationDrawable myAnimationDrawable;


    public ProgressDialog(Context context, String message) {
        super(context);
        // setMessage(message);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.progress_loader);
//
//        ImageView imageView = (ImageView) findViewById(R.id.progressBar);
//        imageView.setBackgroundResource(R.drawable.loader);

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

//        myAnimationDrawable = (AnimationDrawable) imageView.getBackground();
//
//        myAnimationDrawable.start();


        setCanceledOnTouchOutside(false);
        Constants.hideKeyboard(context);


    }


}
