package toml.uaeexchange.models;

import org.simpleframework.xml.Element;

/**
 * Created by kunalk on 4/18/2016.
 */
public class P2PRequest {
    @Element(required = false)
    String RequestType="",UserType="",Paymode="";

    public P2PRequest(){}
    public P2PRequest(String requestType)
    {
        this.RequestType=requestType;
    }
}
