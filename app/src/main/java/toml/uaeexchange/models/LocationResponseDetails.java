package toml.uaeexchange.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by vishwanathp on 20/12/16.
 */
public class LocationResponseDetails {

    @Element(required = false)
    String ErrorCode,Reason;

    @Element(required = false,name="NearbyStores")
    NearbyStores nearbyStores;

    public String getErrorCode() {
        return ErrorCode;
    }

    public String getReason() {
        return Reason;
    }

    public NearbyStores getNearbyStores() {
        return nearbyStores;
    }


    public static class NearbyStores {
        @ElementList(entry="Store", inline = true,required = false)
        List<Store> storeList;

        public List<Store> getStoreList() {
            return storeList;
        }


    }
}
