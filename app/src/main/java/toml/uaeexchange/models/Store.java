package toml.uaeexchange.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import org.simpleframework.xml.Attribute;

import toml.uaeexchange.nearbystores.NearByActivity;


/**
 * Created by vishwanathp on 20/12/16.
 */
public class Store implements ClusterItem {

    @Attribute(required = false)
    private String Id;
    @Attribute(required = false)
    private String Name;
    @Attribute(required = false)
    private String Latitude;
    @Attribute(required = false)
    private String Longitude;
    @Attribute(required = false)
    private String Address;
    @Attribute(required = false)
    private String MCC_Code;
    @Attribute(required = false)
    private String Type;
    @Attribute(required = false)
    private String DistanceInKm;
    LatLng latLng;

    private int positionValue;

    public String getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getLatitude() {
        return Latitude;
    }

    public String getAddress() {
        return Address;
    }

    public String getLongitude() {
        return Longitude;
    }

    public String getMCC_Code() {
        return MCC_Code;
    }

    public String getType() {
        return Type;
    }


    public String getDistanceInKm() {
        return DistanceInKm;
    }

    public int getPositionValue() {
        return positionValue;
    }

    public void setPositionValue(int positionValue) {
        this.positionValue = positionValue;
    }

    @Override
    public LatLng getPosition() {
        if (latLng == null) {

            latLng = new LatLng(Double.parseDouble(getLatitude()), Double.parseDouble(getLongitude()));
            NearByActivity.boundsBuilder.include(latLng);
        }
        return latLng;
    }
}
