package toml.uaeexchange.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import toml.movitwallet.models.Header;
import toml.movitwallet.models.Response;


/**
 * Created by vishwanathp on 20/12/16.
 */
@Root(name="MpsXml")
public class LocationResponse {

    @Element(name="Header")
    Header header;

    @Element(name="Request")
    P2PRequest request;

    @Element(name="Response")
    Response response;

    @Element(name="ResponseDetails")
    LocationResponseDetails responseDetails;

    public Response getResponse() {
        return response;
    }

    public LocationResponseDetails getResponseDetails() {
        return responseDetails;
    }

    public Header getHeader() {
        return header;
    }
}
