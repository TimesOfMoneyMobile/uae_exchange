package toml.uaeexchange.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import toml.movitwallet.models.ContactListItem;
import toml.movitwallet.models.ContactsHeaderItem;
import toml.movitwallet.models.ContactsItem;
import toml.movitwallet.models.ContactsModel;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.MultipleContactsListActivity;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 9/13/2017.
 */

public class NewMultipleContactsSelectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mContext;
    List<ContactListItem> consolidatedList = new ArrayList<>();
    List<ContactsModel> listContacts = new ArrayList<>();




    UpdateTextListener objUpdateTextListener;


    public NewMultipleContactsSelectionAdapter(Activity mContext, List<ContactListItem> consolidatedList, List<ContactsModel> listContacts) {
        this.mContext = mContext;
        this.consolidatedList = consolidatedList;
        this.listContacts = listContacts;
    }

    public interface UpdateTextListener {
        void onTextChanged();
    }

    public void setUpdateTextListener(UpdateTextListener objUpdateTextListener) {
        this.objUpdateTextListener = objUpdateTextListener;
    }

    public void setConsolidatedList(List<ContactListItem> consolidatedList) {
        this.consolidatedList = consolidatedList;
    }

    public void setContactsList(List<ContactsModel> listContacts) {
        this.listContacts = listContacts;
    }


  /*  public ArrayList<ContactsModel> getCheckedItems() {


        ArrayList<ContactsModel> mTempArry = new ArrayList<>();

        *//*ContactsModel objContactsModel = new ContactsModel();
        objContactsModel.setName((AppSettings.getData(context, AppSettings.FIRST_NAME) + " " + AppSettings.getData(context, AppSettings.LAST_NAME) + " (You) "));
        mTempArry.add(objContactsModel);*//*

        for (int i = 0; i < consolidatedList.size(); i++) {
            if (consolidatedList.get(i) instanceof ContactsItem) {

                ContactsModel contactsModel = ((ContactsItem) consolidatedList.get(i)).getContactsModel();
                if (contactsModel.isChecked()) {

                    if (!mTempArry.contains(contactsModel)) {
                        mTempArry.add(((ContactsItem) consolidatedList.get(i)).getContactsModel());
                    }

                }
            }


        }


        return mTempArry;


    }*/


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {

            case ContactListItem.NORMAL:
                View v1 = inflater.inflate(R.layout.list_item_contact, parent,
                        false);
                viewHolder = new ContactsViewHolder(v1);
                break;

            case ContactListItem.HEADER:
                View v2 = inflater.inflate(R.layout.contact_header_item, parent, false);
                viewHolder = new HeaderViewHolder(v2);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        switch (viewHolder.getItemViewType()) {
            case ContactListItem.NORMAL:

                ContactsItem contactListItem = (ContactsItem) consolidatedList.get(position);
                final ContactsViewHolder contactsViewHolder = (ContactsViewHolder) viewHolder;
                //transactionViewHolder.txtTitle.setText(generalItem.getPojoOfJsonArray().getName());


                final ContactsModel objContactsModel = contactListItem.getContactsModel();

                contactsViewHolder.tvName.setText(objContactsModel.getName());
                contactsViewHolder.tvNumber.setText(objContactsModel.getNumber());

                if (objContactsModel.isChecked()) {
                    contactsViewHolder.imgvwRight.setVisibility(View.VISIBLE);
                    contactsViewHolder.imgvwRight.setImageResource(R.drawable.friend_selected_checkmark);
                } else {
                    contactsViewHolder.imgvwRight.setVisibility(View.GONE);
                }


                if (objContactsModel.getImage() != null) {
                    try {

                        contactsViewHolder.ivUserImage.setVisibility(View.VISIBLE);
                        contactsViewHolder.ivUserImage.setImageBitmap(objContactsModel.getImage());

                        // holder.ivImage.setVisibility(View.GONE);
                    } catch (Exception e) {
                        // holder.ivUserImage.setVisibility(View.GONE);

                        contactsViewHolder.ivUserImage.setVisibility(View.VISIBLE);
                        contactsViewHolder.ivUserImage.setImageResource(Constants.getRandomImage());

                    }
                } else {
                    // holder.ivUserImage.setVisibility(View.GONE);

                    contactsViewHolder.ivUserImage.setVisibility(View.VISIBLE);
                    contactsViewHolder.ivUserImage.setImageResource(Constants.getRandomImage());
                }

                contactsViewHolder.rlRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (contactsViewHolder.imgvwRight.getVisibility() == View.GONE) {
                            contactsViewHolder.imgvwRight.setVisibility(View.VISIBLE);
                            contactsViewHolder.imgvwRight.setImageResource(R.drawable.friend_selected_checkmark);
                            objContactsModel.setChecked(true);

                            ((MultipleContactsListActivity)mContext).listSelectedContacts.add(objContactsModel);

                            objUpdateTextListener.onTextChanged();
                        } else {
                            contactsViewHolder.imgvwRight.setVisibility(View.GONE);
                            objContactsModel.setChecked(false);

                            if (((MultipleContactsListActivity)mContext).listSelectedContacts.contains(objContactsModel)) {
                                ((MultipleContactsListActivity)mContext).listSelectedContacts.remove(objContactsModel);
                            }

                            objUpdateTextListener.onTextChanged();
                        }


                    }
                });


                break;

            case ContactListItem.HEADER:

                ContactsHeaderItem headerItem = (ContactsHeaderItem) consolidatedList.get(position);
                HeaderViewHolder dateViewHolder = (HeaderViewHolder) viewHolder;

                dateViewHolder.txtvwHeader.setText(headerItem.getHeader());


                break;

        }

    }

    @Override
    public int getItemViewType(int position) {
        return consolidatedList.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return consolidatedList != null ? consolidatedList.size() : 0;
    }

    // ViewHolder for date row item
    class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtvwHeader)
        TextView txtvwHeader;

        public HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


    class ContactsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.row_ca_rl_row)
        LinearLayout rlRow;
        @BindView(R.id.row_ca_iv_image)
        ImageView ivImage;
        @BindView(R.id.row_ca_civ_user_image)
        CircleImageView ivUserImage;
        @BindView(R.id.row_ca_tv_name)
        TextView tvName;
        @BindView(R.id.row_ca_tv_number)
        TextView tvNumber;
        @BindView(R.id.imgvwRight)
        ImageView imgvwRight;

        ContactsViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }
}
