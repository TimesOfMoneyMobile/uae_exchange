package toml.uaeexchange.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.models.Transaction;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.movitwallet.models.DateHeaderItem;
import toml.movitwallet.models.ListItem;
import toml.movitwallet.models.TransactionItem;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by pankajp on 9/23/2017.
 */

public class NewTransactionsHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    List<ListItem> consolidatedList = new ArrayList<>();

    public NewTransactionsHistoryAdapter(Context mContext, List<ListItem> consolidatedList) {
        this.mContext = mContext;
        this.consolidatedList = consolidatedList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {

            case ListItem.TYPE_GENERAL:
                View v1 = inflater.inflate(R.layout.transaction_history_item, parent,
                        false);
                viewHolder = new TransactionViewHolder(v1);
                break;

            case ListItem.TYPE_DATE:
                View v2 = inflater.inflate(R.layout.section_header_txn_hst, parent, false);
                viewHolder = new DateViewHolder(v2);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        switch (viewHolder.getItemViewType()) {

            case ListItem.TYPE_GENERAL:

                TransactionItem transactionItem = (TransactionItem) consolidatedList.get(position);
                TransactionViewHolder transactionViewHolder = (TransactionViewHolder) viewHolder;
                //transactionViewHolder.txtTitle.setText(generalItem.getPojoOfJsonArray().getName());


                final Transaction transaction = transactionItem.getTransaction();

                if ((transaction.getTxnType().equalsIgnoreCase("P2P") && transaction.getType().equalsIgnoreCase("D"))) {
                    transactionViewHolder.txtvwTxnType.setText(mContext.getString(R.string.payaperson));
                    transactionViewHolder.imgvwTxnType.setImageResource(R.drawable.ic_txn_hst_p2p);
                } else if (transaction.getTxnType().equalsIgnoreCase("P2P") && transaction.getType().equalsIgnoreCase("C")) {
                    transactionViewHolder.txtvwTxnType.setText("Money Received");
                    transactionViewHolder.imgvwTxnType.setImageResource(R.drawable.ic_txn_hst_money_received);
                } else if (transaction.getTxnType().equalsIgnoreCase("P2M")) {
                    transactionViewHolder.txtvwTxnType.setText(mContext.getString(R.string.payamerchant));
                    transactionViewHolder.imgvwTxnType.setImageResource(R.drawable.pay_merchant_txn);
                } else if (transaction.getTxnType().equalsIgnoreCase("FUND")) {
                    transactionViewHolder.txtvwTxnType.setText("Load Money");
                    transactionViewHolder.imgvwTxnType.setImageResource(R.drawable.ic_txn_hst_p2p);
                } else if (transaction.getTxnType().equalsIgnoreCase("SPLITBILL")) {

                    if (transaction.getType().equalsIgnoreCase("C")) {
                        transactionViewHolder.txtvwTxnType.setText("Split bill Credit");
                    } else if (transaction.getType().equalsIgnoreCase("D")) {
                        transactionViewHolder.txtvwTxnType.setText("Split bill Debit");
                    }
                    transactionViewHolder.imgvwTxnType.setImageResource(R.drawable.split_n_share_dashboard);
                } else if (transaction.getTxnType().equalsIgnoreCase("P2MREV")) {
                    transactionViewHolder.txtvwTxnType.setText(mContext.getString(R.string.merchantRefunded));
                    transactionViewHolder.imgvwTxnType.setImageResource(R.drawable.ic_txn_hst_money_received);
                } else if (transaction.getTxnType().equalsIgnoreCase("WITH")) {
                    transactionViewHolder.txtvwTxnType.setText("Money Withdrawal");
                    transactionViewHolder.imgvwTxnType.setImageResource(R.drawable.ic_txn_hst_money_received);
                } else {
                    transactionViewHolder.txtvwTxnType.setText(transaction.getTxnType());
                    transactionViewHolder.imgvwTxnType.setImageResource(R.drawable.pay_merchant_txn);
                }
                try {
                    transactionViewHolder.txtvwTxnTime.setText(Constants.formatToYesterdayOrToday(transaction.getTxnDate()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                if (transaction.getOtherDetail() != null) {

                    String firstName = "", lastName = "", mobileNumber = "";
                    // Receiver is Customer
                    if (transaction.getTxnType().equalsIgnoreCase("P2P")) {
                        firstName = transaction.getOtherDetail().getCustomer().getFirstName().trim();
                        lastName = transaction.getOtherDetail().getCustomer().getLastName().trim();
                        mobileNumber = transaction.getOtherDetail().getCustomer().getMobileNumber().trim();
                    }
                    // Receiver is Merchant
                    else if (transaction.getTxnType().equalsIgnoreCase("P2M")) {
                        firstName = transaction.getOtherDetail().getMerchant().getFirstName().trim();
                        lastName = transaction.getOtherDetail().getMerchant().getLastName().trim();
                        mobileNumber = transaction.getOtherDetail().getMerchant().getMobileNumber().trim();
                    } else if (transaction.getTxnType().equalsIgnoreCase("FUND")) {
                        firstName = transaction.getOtherDetail().getAgent().getFirstName().trim();
                        lastName = transaction.getOtherDetail().getAgent().getLastName().trim();
                        mobileNumber = transaction.getOtherDetail().getAgent().getMobileNumber().trim();
                    } else if (transaction.getTxnType().equalsIgnoreCase("SPLITBILL")) {
                        firstName = transaction.getOtherDetail().getCustomer().getFirstName().trim();
                        lastName = transaction.getOtherDetail().getCustomer().getLastName().trim();
                        mobileNumber = transaction.getOtherDetail().getCustomer().getMobileNumber().trim();
                    } else if (transaction.getTxnType().equalsIgnoreCase("P2MREV")) {
                        firstName = transaction.getOtherDetail().getMerchant().getFirstName().trim();
                        lastName = transaction.getOtherDetail().getMerchant().getLastName().trim();
                        mobileNumber = transaction.getOtherDetail().getMerchant().getMobileNumber().trim();
                    } else if (transaction.getTxnType().equalsIgnoreCase("REQ")) {
                        firstName = transaction.getOtherDetail().getCustomer().getFirstName().trim();
                        lastName = transaction.getOtherDetail().getCustomer().getLastName().trim();
                        mobileNumber = transaction.getOtherDetail().getCustomer().getMobileNumber().trim();
                    }

                    if (transaction.getTxnType().equalsIgnoreCase("P2M")) {
                        transactionViewHolder.txtvwReceiverDetails.setText(firstName + " " + lastName);
                    } else {
                        transactionViewHolder.txtvwReceiverDetails.setText(firstName + " " + lastName + ", " + mContext.getString(R.string.country_code) + " " + mobileNumber);
                    }

                } else {

                    if (transaction.getTxnType().equalsIgnoreCase("WITH") || transaction.getTxnType().equalsIgnoreCase("FUND")) {
                        transactionViewHolder.txtvwReceiverDetails.setText(AppSettings.getData(mContext, AppSettings.FIRST_NAME) + " " + AppSettings.getData(mContext, AppSettings.LAST_NAME) + ", " + "+971" + " " + MovitConsumerApp.getInstance().getMobileNumber());
                    } else {
                        transactionViewHolder.txtvwReceiverDetails.setText("N.A.");
                    }
                }


                transactionViewHolder.txtvwTxnAmount.setText(mContext.getString(R.string.AED) + " " + Constants.formatAmount(transaction.getAmount()));

                // + and - sign
                if (transaction.getType().equalsIgnoreCase("C")) {
                    transactionViewHolder.txtvwTxnCreditDebit.setText("+");
                    transactionViewHolder.txtvwTxnCreditDebit.setTextColor(ContextCompat.getColor(mContext, R.color.aqua_green));
                } else if (transaction.getType().equalsIgnoreCase("D")) {
                    transactionViewHolder.txtvwTxnCreditDebit.setText("-");
                    transactionViewHolder.txtvwTxnCreditDebit.setTextColor(ContextCompat.getColor(mContext, R.color.grapefruit));
                }


                break;

            case ListItem.TYPE_DATE:
                DateHeaderItem dateItem = (DateHeaderItem) consolidatedList.get(position);
                DateViewHolder dateViewHolder = (DateViewHolder) viewHolder;

                dateViewHolder.txtvwHeaderDate.setText(dateItem.getDate());
                // Populate date item data here

                break;
        }

    }

    // ViewHolder for date row item
    class DateViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtvwHeaderDate)
        TextView txtvwHeaderDate;

        public DateViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    // View holder for general row item
    class TransactionViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtvwTxnType)
        TextView txtvwTxnType;
        @BindView(R.id.txtvwReceiverDetails)
        TextView txtvwReceiverDetails;
        @BindView(R.id.txtvwTxnTime)
        TextView txtvwTxnTime;
        @BindView(R.id.txtvwTxnCreditDebit)
        TextView txtvwTxnCreditDebit;
        @BindView(R.id.txtvwTxnAmount)
        TextView txtvwTxnAmount;
        @BindView(R.id.imgvwTxnType)
        ImageView imgvwTxnType;

        TransactionViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    @Override
    public int getItemViewType(int position) {
        return consolidatedList.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return consolidatedList != null ? consolidatedList.size() : 0;
    }

}
