package toml.uaeexchange.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.models.Transaction;
import toml.uaeexchange.R;
import toml.uaeexchange.fragments.RecentTransactionsFragment;
import toml.uaeexchange.fragments.SendMoneyFragment;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 9/8/2017.
 */

public class RecentTransactionsAdapter extends BaseAdapter {


    Context context;
    List<Transaction> transactionsList;
    LayoutInflater inflater;
    SendMoneyFragment sendMoneyFragment;
    String strDisplayType;
    String number;

    public RecentTransactionsAdapter(Context context, RecentTransactionsFragment recentTransactionsFragment, List<Transaction> transactionsList) {
        this.context = context;
        this.transactionsList = transactionsList;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (recentTransactionsFragment != null) {
            sendMoneyFragment = ((SendMoneyFragment) recentTransactionsFragment.getParentFragment());
        }
    }

    public void setDisplayType(String type) {
        strDisplayType = type;
    }


    @Override
    public int getCount() {
        return transactionsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.recent_transaction_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }


        final Transaction transaction = transactionsList.get(position);

        if (transaction.getOtherDetail() != null) {
            // Receiver is Customer
            if (transaction.getTxnType().equalsIgnoreCase("P2P")) {
                if (!TextUtils.isEmpty(transaction.getOtherDetail().getCustomer().getMobileNumber())) {
                    number = transaction.getOtherDetail().getCustomer().getMobileNumber();
                    holder.tvNumber.setText(context.getString(R.string.country_code) + " " + number);
                }
                holder.tvName.setText(transaction.getOtherDetail().getCustomer().getFirstName() + " " + transaction.getOtherDetail().getCustomer().getLastName());
            }
            // Receiver is Merchant
            else if (transaction.getTxnType().equalsIgnoreCase("P2M")) {
                if (!TextUtils.isEmpty(transaction.getOtherDetail().getMerchant().getMobileNumber()))
                    holder.tvNumber.setText(transaction.getOtherDetail().getMerchant().getMobileNumber());
                holder.tvName.setText(transaction.getOtherDetail().getMerchant().getFirstName() + " " + transaction.getOtherDetail().getMerchant().getLastName());
            }


        } else {
            holder.tvNumber.setText("N.A.");
            holder.tvName.setText("N.A.");
        }


        holder.tvAmount.setText(context.getString(R.string.AED) + " " + transaction.getAmount());

        try {
            holder.tvTimeStamp.setText(Constants.formatToYesterdayOrToday(transaction.getTxnDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.cvRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sendMoneyFragment != null) {
                    sendMoneyFragment.setValuesToEditText(number, transaction.getAmount(), holder.tvName.getText().toString(), false,"recentTransactions");
                }
            }
        });


        return convertView;
    }

    public class ViewHolder {
        @BindView(R.id.row_rt_cv_row)
        CardView cvRow;
        @BindView(R.id.row_rt_tv_name)
        TextView tvName;
        @BindView(R.id.row_rt_tv_number)
        TextView tvNumber;
        @BindView(R.id.row_rt_tv_time_stamp)
        TextView tvTimeStamp;
        @BindView(R.id.row_rt_tv_amount)
        TextView tvAmount;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
