package toml.uaeexchange.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import toml.movitwallet.models.ContactsModel;
import toml.uaeexchange.R;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 9/13/2017.
 */

public class ContactsAdapter extends BaseAdapter {

    private Activity context;
    private LayoutInflater inflater;
    //private Cursor cursor = null;
    ArrayList<ContactsModel> contactsModelArrayList;

    public ContactsAdapter(Activity context, Cursor cursor, ArrayList<ContactsModel> allContactsList) {
        this.context = context;
        // this.cursor = cursor;
        contactsModelArrayList = allContactsList;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return contactsModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_contact, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // cursor.moveToPosition(position);
        ContactsModel contactsModel = contactsModelArrayList.get(position);


        holder.tvName.setText(contactsModel.getName());

        // String phoneNumber = toml.movitwallet.utils.Constants.getOnlyDigits(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
        holder.tvNumber.setText(contactsModel.getNumber());


        if (contactsModel.getImage() != null) {
            try {
                // Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI))));
                holder.ivUserImage.setVisibility(View.VISIBLE);
                holder.ivUserImage.setImageBitmap(contactsModel.getImage());

                // holder.ivImage.setVisibility(View.GONE);
            } catch (Exception e) {
                // holder.ivUserImage.setVisibility(View.GONE);

                holder.ivUserImage.setVisibility(View.VISIBLE);
                holder.ivUserImage.setImageResource(Constants.getRandomImage());

            }
        } else {
            // holder.ivUserImage.setVisibility(View.GONE);

            holder.ivUserImage.setVisibility(View.VISIBLE);
            holder.ivUserImage.setImageResource(Constants.getRandomImage());
        }


        holder.rlRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //cursor.moveToPosition(position);

                Intent intent = new Intent();
                Bundle bundle = new Bundle();


                String no = "";


                //String phoneNumber = toml.movitwallet.utils.Constants.getOnlyDigits(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                no = holder.tvNumber.getText().toString();
                //toml.movitwallet.utils.Constants.getLastNineDigitPhoneNumber(phoneNumber);

                if (no != null) {
                    if (no.startsWith("+91"))
                        bundle.putString("number", no.replace("+91", ""));
                    else if (no.startsWith("+84"))
                        bundle.putString("number", no.replace("+84", ""));
                    else if (no.startsWith("+971"))
                        bundle.putString("number", no.replace("+971", ""));
                    else
                        bundle.putString("number", no);
                }
                bundle.putString("name", holder.tvName.getText().toString());//cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                bundle.putBoolean("isFav", false);
                bundle.putString("source", "contacts");
                intent.putExtras(bundle);
                context.setResult(Activity.RESULT_OK, intent);
                context.finish();
            }
        });

        return convertView;
    }


    public class ViewHolder {
        @BindView(R.id.row_ca_rl_row)
        LinearLayout rlRow;
        @BindView(R.id.row_ca_iv_image)
        ImageView ivImage;

        @BindView(R.id.row_ca_civ_user_image)
        CircleImageView ivUserImage;


        @BindView(R.id.row_ca_tv_name)
        TextView tvName;
        @BindView(R.id.row_ca_tv_number)
        TextView tvNumber;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
