package toml.uaeexchange.adapters;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import toml.uaeexchange.R;
import toml.movitwallet.models.Offer;
import toml.uaeexchange.customviews.CustomButton;

public class OffersListAdapter extends BaseAdapter {


    Context context;
    List<Offer> offersList;
    LayoutInflater inflater;
    IPromoCodeClickListener iPromoCodeClickListener;
    boolean isOffersActivity;

    public OffersListAdapter(Context context, List<Offer> offersList, boolean isOffersActivity, IPromoCodeClickListener iPromoCodeClickListener) {
        this.context = context;
        this.offersList = offersList;
        this.iPromoCodeClickListener = iPromoCodeClickListener;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.isOffersActivity = isOffersActivity;
    }

    public interface IPromoCodeClickListener {
        public void onPromoCodeClick(String promoCode,int position,View view);
    }


    @Override
    public int getCount() {
        return offersList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.offer_row_new, parent, false);

            holder.txtvwPromoCode = (TextView) convertView.findViewById(R.id.tvPromocodeName);
            // holder.txtvwOfferName = (TextView) convertView.findViewById(R.id.txtvwOfferName);
            // holder.txtvwValidaity = (TextView) convertView.findViewById(R.id.txtvwOfferValidity);
            holder.txtvwDescription = (TextView) convertView.findViewById(R.id.tvDescription);
            holder.btnAppyPromoCode = (CustomButton) convertView.findViewById(R.id.btnAppyPromoCode);
            holder.rlOfferView = (RelativeLayout) convertView.findViewById(R.id.rlOfferView);

            convertView.setTag(holder);
        }

        holder = (ViewHolder) convertView.getTag();

        final Offer objOffer = offersList.get(position);

        holder.txtvwPromoCode.setText(objOffer.getPromoCodeName());
        // holder.txtvwOfferName.setText(objOffer.getOfferName());
        /*String formattedFromDate = formattedDateFromString("yyyy-MM-dd HH:mm:ss.s", "MMM dd yyyy", objOffer.getFromDate());
        String formattedToDate = formattedDateFromString("yyyy-MM-dd HH:mm:ss.s", "MMM dd yyyy", objOffer.getToDate());*/
        //  holder.txtvwValidaity.setText("Valid from " + formattedFromDate + " to " + formattedToDate);
        holder.txtvwDescription.setText(objOffer.getDescription());

        if (isOffersActivity) {
            holder.btnAppyPromoCode.setText("Copy");

        } else {
            holder.btnAppyPromoCode.setText("Apply");
        }

        final ViewHolder finalHolder = holder;
       /* holder.txtvwPromoCode.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    final android.content.ClipboardManager clipboardManager = (android.content.ClipboardManager) context
                            .getSystemService(Context.CLIPBOARD_SERVICE);
                    final android.content.ClipData clipData = android.content.ClipData
                            .newPlainText("PROMOCODE", finalHolder.txtvwPromoCode.getText());
                    clipboardManager.setPrimaryClip(clipData);
                } else {
                    final android.content.ClipboardManager clipboardManager = (android.content.ClipboardManager) context
                            .getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboardManager.setText(finalHolder.txtvwPromoCode.getText());
                }
              //  Toast.makeText(context, "Copied to clipboard", Toast.LENGTH_SHORT).show();
                return false;
            }
*/
        holder.btnAppyPromoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (iPromoCodeClickListener != null)
                    iPromoCodeClickListener.onPromoCodeClick(objOffer.getPromoCodeName(),position,view);
            }
        });
        holder.rlOfferView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (iPromoCodeClickListener != null)
                    iPromoCodeClickListener.onPromoCodeClick(objOffer.getPromoCodeName(),position,view);
            }
        });



        return convertView;
    }


    class ViewHolder {
        TextView txtvwPromoCode, txtvwDescription;
        // TextView txtvwOfferName, txtvwValidaity;
        CustomButton btnAppyPromoCode;
        RelativeLayout rlOfferView;
    }

    public String formattedDateFromString(String inputFormat, String outputFormat, String inputDate) {
        if (inputFormat.equals("")) { // if inputFormat = "", set a default input format.
            inputFormat = "yyyy-MM-dd hh:mm:ss";
        }
        if (outputFormat.equals("")) {
            outputFormat = "EEEE d 'de' MMMM 'del' yyyy"; // if inputFormat = "", set a default output format.
        }
        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        // You can set a different Locale, This example set a locale of Country Mexico.
        //SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, new Locale("es", "MX"));
        //SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, new Locale("es", "MX"));

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (Exception e) {
            Log.e("formattedDateFromString", "Exception in formateDateFromstring(): " + e.getMessage());
        }
        return outputDate;

    }

}
