package toml.uaeexchange.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import toml.uaeexchange.R;
import toml.uaeexchange.models.Notification;

/**
 * Created by vishwanathp on 8/14/2017.
 */

public class NotificationAdapter extends BaseAdapter {

    Context context;
    List<Notification> notificationList;
    LayoutInflater inflater;

    public NotificationAdapter(Context context, List<Notification> agentNotificationList) {
        this.context = context;
        this.notificationList = agentNotificationList;
        inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return notificationList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView==null)
        {
            holder=new ViewHolder();

            convertView=inflater.inflate(R.layout.notification_row,parent,false);

            holder.txtDate= (TextView) convertView.findViewById(R.id.txtDate);
            holder.txtMessage= (TextView) convertView.findViewById(R.id.txtMessage);
            holder.txtTime= (TextView) convertView.findViewById(R.id.txtTime);
            holder.imgIcon = (ImageView)convertView.findViewById(R.id.imgIcon);

            convertView.setTag(holder);
        }

        holder= (ViewHolder) convertView.getTag();

        Notification notification= notificationList.get(position);

        holder.txtDate.setText(notification.getTimestamp());
        holder.txtMessage.setText(notification.getMessage());
        holder.txtTime.setText(notification.getTimestamp());
        //holder.txtTime.setText(notification.getTime());

        if(notification.getTxnType()!=null) {
            if (notification.getTxnType().equalsIgnoreCase("P2P"))
                holder.imgIcon.setImageResource(R.drawable.notifications_1);
            else if (notification.getTxnType().equalsIgnoreCase("P2M"))
                holder.imgIcon.setImageResource(R.drawable.notifications_2);
            else if (notification.getTxnType().equalsIgnoreCase("Add Money"))
                holder.imgIcon.setImageResource(R.drawable.notifications_3);
            else if (notification.getTxnType().equalsIgnoreCase("Refund"))
                holder.imgIcon.setImageResource(R.drawable.notifications_4);
        }


        return convertView;
    }

    class ViewHolder
    {
        TextView txtDate,txtMessage,txtTime;
        ImageView imgIcon;

    }
}
