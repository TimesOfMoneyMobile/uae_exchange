package toml.uaeexchange.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import toml.movitwallet.models.ContactListItem;
import toml.movitwallet.models.ContactsModel;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 9/13/2017.
 */

public class MultipleContactsSelectionAdapter extends BaseAdapter {

    private Activity context;
    private LayoutInflater inflater;
    //   private Cursor cursor = null;
    List<ContactsModel> listContacts = new ArrayList<>();

    List<ContactListItem> consolidatedList = new ArrayList<>();
    SparseBooleanArray mSparseBooleanArray;

    UpdateTextListener objUpdateTextListener;


    public interface UpdateTextListener {
        void onTextChanged();
    }

    public void setUpdateTextListener(UpdateTextListener objUpdateTextListener) {
        this.objUpdateTextListener = objUpdateTextListener;
    }

    public MultipleContactsSelectionAdapter(Activity context, List<ContactsModel> listContacts) {
        this.context = context;
        //  this.cursor = cursor;
        this.listContacts = listContacts;
        // this.cursor.moveToFirst();
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSparseBooleanArray = new SparseBooleanArray();
    }

    public void resetCheckmarkListView() {
        if (MovitConsumerApp.getInstance().getListSelectedContacts() != null && MovitConsumerApp.getInstance().getListSelectedContacts().size() > 0) {

        }
    }


    public ArrayList<ContactsModel> getCheckedItems() {


        ArrayList<ContactsModel> mTempArry = new ArrayList<>();

        /*ContactsModel objContactsModel = new ContactsModel();
        objContactsModel.setName((AppSettings.getData(context, AppSettings.FIRST_NAME) + " " + AppSettings.getData(context, AppSettings.LAST_NAME) + " (You) "));
        mTempArry.add(objContactsModel);*/

        for (int i = 0; i < listContacts.size(); i++) {


            if (listContacts.get(i).isChecked()) {

                mTempArry.add(listContacts.get(i));

            }


        }


        return mTempArry;


    }


    @Override
    public int getCount() {
        return listContacts.size();
    }

    @Override
    public Object getItem(int position) {
        return listContacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_contact, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        final ContactsModel objContactsModel = listContacts.get(position);

        //  cursor.moveToPosition(position);


        holder.tvName.setText(objContactsModel.getName());
        holder.tvNumber.setText(objContactsModel.getNumber());

        if (objContactsModel.isChecked()) {
            holder.imgvwRight.setVisibility(View.VISIBLE);
            holder.imgvwRight.setImageResource(R.drawable.friend_selected_checkmark);
        } else {
            holder.imgvwRight.setVisibility(View.GONE);
        }


        if (objContactsModel.getImage() != null) {
            try {

                holder.ivUserImage.setVisibility(View.VISIBLE);
                holder.ivUserImage.setImageBitmap(objContactsModel.getImage());

                // holder.ivImage.setVisibility(View.GONE);
            } catch (Exception e) {
                // holder.ivUserImage.setVisibility(View.GONE);

                holder.ivUserImage.setVisibility(View.VISIBLE);
                holder.ivUserImage.setImageResource(Constants.getRandomImage());

            }
        } else {
            // holder.ivUserImage.setVisibility(View.GONE);

            holder.ivUserImage.setVisibility(View.VISIBLE);
            holder.ivUserImage.setImageResource(Constants.getRandomImage());
        }

        holder.rlRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.imgvwRight.getVisibility() == View.GONE) {
                    holder.imgvwRight.setVisibility(View.VISIBLE);
                    holder.imgvwRight.setImageResource(R.drawable.friend_selected_checkmark);
                    objContactsModel.setChecked(true);
                    mSparseBooleanArray.put(position, true);
                    objUpdateTextListener.onTextChanged();
                } else {
                    holder.imgvwRight.setVisibility(View.GONE);
                    mSparseBooleanArray.put(position, false);
                    objContactsModel.setChecked(false);
                    objUpdateTextListener.onTextChanged();
                }


            }
        });


        return convertView;
    }



    public class ViewHolder {
        @BindView(R.id.row_ca_rl_row)
        LinearLayout rlRow;
        @BindView(R.id.row_ca_iv_image)
        ImageView ivImage;
        @BindView(R.id.row_ca_civ_user_image)
        CircleImageView ivUserImage;
        @BindView(R.id.row_ca_tv_name)
        TextView tvName;
        @BindView(R.id.row_ca_tv_number)
        TextView tvNumber;
        @BindView(R.id.imgvwRight)
        ImageView imgvwRight;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
