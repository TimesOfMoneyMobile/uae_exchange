package toml.uaeexchange.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.uaeexchange.R;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by mayurn on 9/27/2017.
 */

public class ProviderAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<String> providerNamesToDisplay;

    LayoutInflater inflater;

    ISpinnerItemClickLiner iSpinnerItemClickLiner;

    public interface ISpinnerItemClickLiner {
        public void onSpinnerItemClick(int position);
    }

    public ProviderAdapter(Context context, ArrayList<String> providerNamesToDisplay,ISpinnerItemClickLiner iSpinnerItemClickLiner) {
        this.mContext = context;
        this.providerNamesToDisplay = providerNamesToDisplay;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.iSpinnerItemClickLiner = iSpinnerItemClickLiner;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return this.providerNamesToDisplay.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        return getCustomView(position, convertView, parent);

    }

    public View getCustomView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ProviderAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_provider, null);
            holder = new ProviderAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ProviderAdapter.ViewHolder) convertView.getTag();
        }

        holder.tvProviderName.setText(providerNamesToDisplay.get(position));

        if (position == 0) {
            holder.ivProviderIcon.setVisibility(View.GONE);
            holder.tvProviderName.setVisibility(View.GONE);
        }

        if (providerNamesToDisplay.get(position).contains(Constants.DU)) {
            holder.ivProviderIcon.setImageResource(R.drawable.du_icon);
            holder.ivProviderIcon.setVisibility(View.VISIBLE);
            holder.tvProviderName.setVisibility(View.VISIBLE);

        } else if (providerNamesToDisplay.get(position).contains(Constants.ETISALAT)) {
            holder.ivProviderIcon.setImageResource(R.drawable.etisalat_icon);
            holder.ivProviderIcon.setVisibility(View.VISIBLE);
            holder.tvProviderName.setVisibility(View.VISIBLE);
        }

        /*holder.tvProviderName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //   iSpinnerItemClickLiner.onSpinnerItemClick(position);
            }
        });*/
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.ivProviderIcon)
        ImageView ivProviderIcon;
        @BindView(R.id.tvProviderName)
        TextView tvProviderName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
