package toml.uaeexchange.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.jsonmodels.ProductListItem;
import toml.uaeexchange.R;

/**
 * Created by mayurn on 9/26/2017.
 */

public class RechargeProductAdapter extends BaseAdapter {


    private Context mContext;
    private List<String> productListItems;
    private LayoutInflater inflater;
    private IRechargeProductClickLiner iRechargeProductClickLiner;
    private static int positionSelected = -1;

    public interface IRechargeProductClickLiner {
        public void onRechargeProductClick(int onRechargeProductClick);
    }
    // Keep all Images in array


    // Constructor
    public RechargeProductAdapter(Context mContext, List<String> productListItems, IRechargeProductClickLiner iRechargeProductClickLiner) {
        this.mContext = mContext;
        this.productListItems = productListItems;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.iRechargeProductClickLiner = iRechargeProductClickLiner;
    }

    @Override
    public int getCount() {
        return productListItems.size();
    }

    @Override
    public String getItem(int position) {
        return productListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_recharge_product, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        if (positionSelected >= 0) {
            if (!(positionSelected == position)) {
                holder.tvProductRecharge.setBackgroundResource(R.drawable.aqua_green_border);
                holder.tvProductRecharge.setTextColor(ContextCompat.getColor(mContext,R.color.aqua_green));
                holder.tvProductRecharge.setText("AED " + productListItems.get(position));
            } else {
                holder.tvProductRecharge.setText("AED " + productListItems.get(position));
            }
        }
        else
        {
            holder.tvProductRecharge.setText("AED " + productListItems.get(position));
        }

        holder.tvProductRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positionSelected = position;
                iRechargeProductClickLiner.onRechargeProductClick(position);
                holder.tvProductRecharge.setBackgroundResource(R.drawable.aqua_green_selected_border);
                holder.tvProductRecharge.setTextColor(ContextCompat.getColor(mContext,R.color.white));
                notifyDataSetChanged();
            }


        });

        return convertView;
    }


    class ViewHolder {
        @BindView(R.id.tvProductRecharge)
        TextView tvProductRecharge;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
