package toml.uaeexchange.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.models.Beneficiary;

import toml.uaeexchange.R;
import toml.uaeexchange.activities.RequestMoneyActivity;
import toml.uaeexchange.fragments.FavouritesFragment;
import toml.uaeexchange.fragments.SendMoneyFragment;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 9/8/2017.
 */

public class FavouritesAdapter extends BaseAdapter {

    private Context context;
    private List<Beneficiary> favouritesList;
    private LayoutInflater inflater;
    private SendMoneyFragment sendMoneyFragment;
    private RequestMoneyActivity requestMoneyActivity;

    Beneficiary beneficiary;

    public FavouritesAdapter(Context context, FavouritesFragment favouritesFragment, List<Beneficiary> favouritesList) {
        this.context = context;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.favouritesList = favouritesList;
        sendMoneyFragment = ((SendMoneyFragment) favouritesFragment.getParentFragment());
    }

    public FavouritesAdapter(Context context, List<Beneficiary> favouritesList) {
        this.context = context;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.favouritesList = favouritesList;
        requestMoneyActivity = (RequestMoneyActivity) context;

    }


    @Override
    public int getCount() {
        return favouritesList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.favourites_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }


        beneficiary = favouritesList.get(position);
        holder.tvName.setText(beneficiary.getNickname());

        holder.tvNumber.setText(context.getString(R.string.country_code) + " " + beneficiary.getValue());
        holder.ivImage.setImageResource(Constants.getRandomBigImage());


        holder.cvRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sendMoneyFragment != null)
                    sendMoneyFragment.setValuesToEditText(toml.movitwallet.utils.Constants.getLastNineDigitPhoneNumber(holder.tvNumber.getText().toString()), "", holder.tvName.getText().toString(), true, "favourites");
                else
                    requestMoneyActivity.setValuesToEditText(toml.movitwallet.utils.Constants.getLastNineDigitPhoneNumber(holder.tvNumber.getText().toString()), "", holder.tvName.getText().toString(), true, "favourites");


            }
        });

        return convertView;
    }

    public class ViewHolder {
        @BindView(R.id.row_fav_cv_row)
        CardView cvRow;
        @BindView(R.id.row_fav_iv_image)
        ImageView ivImage;
        @BindView(R.id.row_fav_tv_name)
        TextView tvName;
        @BindView(R.id.row_fav_tv_number)
        TextView tvNumber;
        @BindView(R.id.row_fav_tv_select)
        TextView tvSelect;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
