package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.uaeexchange.R;

public class SettingsActivity extends BaseActivity {


    @BindView(R.id.rlMangeNotification)
    RelativeLayout rlMangeNotification;
    @BindView(R.id.ivManageNotification)
    ImageView ivManageNotification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        //setToolbar(getString(R.string.title_settings));

        setToolbarWithBalance(getString(R.string.title_settings), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);

    }


    @OnClick({R.id.rlMangeDevices, R.id.rlMangeNotification})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.rlMangeDevices:
                intent = new Intent(this, ManageDevicesActivity.class);
                startActivity(intent);
                break;
            case R.id.rlMangeNotification:
                intent = new Intent(this, ManageNotificationActivity.class);
                startActivity(intent);
                break;

        }
    }

}
