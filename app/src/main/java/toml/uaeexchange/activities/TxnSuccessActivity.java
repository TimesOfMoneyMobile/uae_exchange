package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;

/**
 * Created by vishwanathp on 9/12/2017.
 */

public class TxnSuccessActivity extends BaseActivity {

    @BindView(R.id.act_ts_tv_txn_type)
    TextView tvTxnType;
    @BindView(R.id.act_ts_tv_trans_id)
    TextView tvTransId;
    @BindView(R.id.act_ts_tv_thanks_msg)
    TextView tvThanksMsg;
    @BindView(R.id.act_ts_cb_back_to_dashboard)
    Button cbBackToDashBoard;

    Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_txn_success);
        unbinder = ButterKnife.bind(this);


        Bundle bundle = getIntent().getExtras();

        if (bundle.getString("txnType").equalsIgnoreCase("req")) {
            tvTxnType.setText(getString(R.string.request_placed_successful));
            if (!TextUtils.isEmpty(bundle.getString(IntentConstants.TXNID))) {
                tvTransId.setText(getString(R.string.request_id) + " " + bundle.getString(IntentConstants.TXNID));
            }


            tvThanksMsg.setText(getString(R.string.message_for_request_txn));


        }else {
            tvTxnType.setText(getString(R.string.transaction_successful));
            if (!TextUtils.isEmpty(bundle.getString(IntentConstants.TXNID))) {
                tvTransId.setText(getString(R.string.transaction_id) + " " + bundle.getString(IntentConstants.TXNID));
            }

            // if (!TextUtils.isEmpty(bundle.getString("username"))) {

            String transferAmount = getString(R.string.AED) + " " + toml.uaeexchange.utilities.Constants.formatAmount(bundle.getString(IntentConstants.TRANSFER_AMOUNT));
            String sourceString = getString(R.string.thanks_part_pp_1) + " " + transferAmount + " " + getString(R.string.thanks_part_pp_2);

            int i = sourceString.indexOf(transferAmount);

            SpannableStringBuilder str = new SpannableStringBuilder(sourceString);
            str.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TxnSuccessActivity.this, R.color.black)), i, i + transferAmount.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvThanksMsg.setText(str);


//            } else {
//                String receiverMobile = bundle.getString("receiverMobile");
//                String sourceString = getString(R.string.thanks_part_1) + " " + receiverMobile + "." + getString(R.string.thanks_part_2);
//
//                int i = sourceString.indexOf(receiverMobile);
//
//                SpannableStringBuilder str = new SpannableStringBuilder(sourceString);
//                str.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TxnSuccessActivity.this, R.color.black)), i, i + receiverMobile.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tvThanksMsg.setText(str);
//
//            }

        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        BaseActivity.openDashboardActivity(TxnSuccessActivity.this);
        finish();
    }

    @OnClick({R.id.act_ts_cb_back_to_dashboard})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.act_ts_cb_back_to_dashboard:
                BaseActivity.openDashboardActivity(TxnSuccessActivity.this);
                finish();
                break;
        }
    }
}
