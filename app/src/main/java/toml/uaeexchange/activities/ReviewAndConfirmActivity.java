package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import toml.movitwallet.controllers.AuthoriseRequestMoneyController;
import toml.movitwallet.controllers.AskMoneyController;
import toml.movitwallet.controllers.GenerateOTPController;
import toml.movitwallet.controllers.PayPersonController;
import toml.movitwallet.controllers.ValidateOTPController;
import toml.movitwallet.models.MoneyRequest;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.dialogs.ValidateOTPDialog;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.whatsappp2p.OverlayDashboard;

/**
 * Created by vishwanathp on 9/12/2017.
 */

public class ReviewAndConfirmActivity extends BaseActivity implements BaseActivity.ClickableSpanListener {

    private static final String TXN_TYPE = "P2P";
    @BindView(R.id.act_rac_tv_amount)
    TextView tvAmount;
    @BindView(R.id.act_rac_rl_edit)
    RelativeLayout rlEdit;
    @BindView(R.id.act_rac_iv_user_image)
    ImageView ivUserImage;
    @BindView(R.id.act_rac_tv_user_name)
    TextView tvUserName;
    @BindView(R.id.act_rac_tv_user_number)
    TextView tvUserNumber;
    @BindView(R.id.act_rac_tv_amount_to_sent)
    TextView tvAmountToSent;
    @BindView(R.id.act_rac_tv_total_payable_amount)
    TextView tvTotalPayAmount;
    @BindView(R.id.act_rac_cb_confirm)
    Button cbConfirm;

    Unbinder unbinder;
    ValidateOTPDialog otpDialog;

    boolean isFavChecked;
    private boolean isSpanClicked;
    String strTxnType = "";
    String strAmount, strUserNumber, strUserName, strOTP = "", strStatus, strFromActivity = "";
    MoneyRequest objMoneyRequest;

    private SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_and_confirm);
        unbinder = ButterKnife.bind(this);
        setToolbarWithBalance(getString(R.string.review_and_send), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);
        setClickableSpanListener(this);

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = Constants.parseSMSForOTP(message);//Parse verification code
                try {
                    if (otpDialog != null && otpDialog.isShowing()) {
                        otpDialog.setOTPText(code);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //then you can send verification code to server
            }
        });


        Bundle bundle = getIntent().getExtras();

        if (!TextUtils.isEmpty(bundle.getString("amount"))) {
            tvAmount.setText(getString(R.string.AED) + " " + Constants.formatAmount(bundle.getString("amount")));
            tvAmountToSent.setText(getString(R.string.AED) + " " + Constants.formatAmount(bundle.getString("amount")));
            tvTotalPayAmount.setText(getString(R.string.AED) + " " + Constants.formatAmount(bundle.getString("amount")));
            strAmount = bundle.getString("amount");
        }


        if (!TextUtils.isEmpty(bundle.getString("mobile_number"))) {
            strUserNumber = bundle.getString("mobile_number");
            tvUserNumber.setText(getString(R.string.country_code) + " " + bundle.getString("mobile_number"));
        }


        if (!TextUtils.isEmpty(bundle.getString("nick_name"))) {

            strUserName = bundle.getString("nick_name");
            tvUserName.setText(bundle.getString("nick_name"));
        }else{
            tvUserName.setVisibility(View.GONE);
        }

        if (bundle.getBoolean("isFav")) {
            isFavChecked = true;
        }


        if (bundle.containsKey("txnType")) {
            if (!TextUtils.isEmpty(bundle.getString("txnType"))) {
                strTxnType = bundle.getString("txnType");
            }
        }
        if (bundle.containsKey("money_req")) {

            objMoneyRequest = (MoneyRequest) bundle.getSerializable("money_req");

        }

        if (bundle.containsKey("status")) {

            strStatus = bundle.getString("status");

        }

        if (bundle.containsKey(IntentConstants.FROM_ACTIVITY)) {
            strFromActivity = bundle.getString(IntentConstants.FROM_ACTIVITY);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (smsVerifyCatcher != null)
            smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (smsVerifyCatcher != null)
            smsVerifyCatcher.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    @OnClick({R.id.act_rac_cb_confirm, R.id.act_rac_rl_edit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.act_rac_cb_confirm:
                isSpanClicked = false;

                if (strTxnType.equalsIgnoreCase("req")) {
                    callRequestMoneyApi();
                } else {
                    if (Constants.checkToShowOtpDialog(ReviewAndConfirmActivity.this, strAmount)) {
                        callGenerateOTP(isSpanClicked);
                    } else {
                        if (strFromActivity.equalsIgnoreCase(PendingRequestActivity.class.getCanonicalName())) {
                            callAuthoriseReqMoney();
                        } else {
                            callP2PApi();
                        }
                    }
                }

                break;
            case R.id.act_rac_rl_edit:
                onBackPressed();
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (smsVerifyCatcher != null)
            smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onSpanClick(String strText) {
        isSpanClicked = true;
        callGenerateOTP(isSpanClicked);
    }

    private void callGenerateOTP(final boolean isSpanClicked) {
        showProgress();

        MovitWalletController generateOTPController = new GenerateOTPController(strTxnType);

        generateOTPController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                dismissProgress();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String code = "";
                        try {
                            if (isSpanClicked) {
                                if (otpDialog.isShowing()) {
                                    otpDialog.refreshOnResend();
                                }
                            } else {
                                showOTPDialog();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, 200);
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                showError(reason);

            }
        });
        if (isSpanClicked) {
            this.isSpanClicked = false;
        }
    }


    private void showOTPDialog() {

        otpDialog = new ValidateOTPDialog(this, new ValidateOTPDialog.IValidateOTPDialogCallBack() {

            @Override
            public void resendOTPCallBack(String otpValue) {
               // Toast.makeText(ReviewAndConfirmActivity.this, "resend Clicked" + otpValue, Toast.LENGTH_SHORT).show();
//                strTpin = otpValue;
//                callP2PApi();
//                otpDialog.cancel();
                //verifyOTP(otpValue);
            }

            @Override
            public void verifyOTPCallBack(String otpValue) {

                strOTP = otpValue;
                if (strFromActivity.equalsIgnoreCase(PendingRequestActivity.class.getCanonicalName())) {
                    callAuthoriseReqMoney();
                } else {
                    callP2PApi();
                }


                // otpDialog.cancel();
                //verifyOTP(otpValue);
            }
        });

        otpDialog.show();

    }


    private void verifyOTP(String otpValue) {

        if (otpValue.length() == getResources().getInteger(R.integer.OTPLength)) {

            showProgress();

            MovitWalletController validateOTPController = new ValidateOTPController(TXN_TYPE, otpValue);

            validateOTPController.init(new IMovitWalletController() {
                @Override
                public void onSuccess(Object response) {

                    dismissProgress();
                   // Toast.makeText(ReviewAndConfirmActivity.this, "Verified Success", Toast.LENGTH_SHORT).show();
                    callP2PApi();
                    otpDialog.cancel();

                }

                @Override
                public void onFailed(String errorCode, String reason) {
                    dismissProgress();
                    showError(reason);
                }
            });

        } else {
          //  showError("Please enter " + getResources().getInteger(R.integer.OTPLength) + " digit OTP.");
            showError(getString(R.string.invalidOtpError));
        }

    }

    private void callRequestMoneyApi() {
        showProgress();

        MovitWalletController movitWalletController;

        if (isFavChecked) {
            movitWalletController = new AskMoneyController(strUserName, strUserNumber, strAmount, strOTP);
            movitWalletController.init(new IMovitWalletController<Bundle>() {

                @Override
                public void onSuccess(Bundle response) {

                    showSuccessScreen(response);
                }

                @Override
                public void onFailed(String errorCode, String reason) {
                    dismissProgress();
                    // showError(reason);

                    new OkDialog(ReviewAndConfirmActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
                        @Override
                        public void handleResponse() {

                        }
                    });
                }
            });

        } else {
            movitWalletController = new AskMoneyController("", strUserNumber, strAmount, strOTP);
            movitWalletController.init(new IMovitWalletController<Bundle>() {

                @Override
                public void onSuccess(Bundle response) {
                    showSuccessScreen(response);
                }

                @Override
                public void onFailed(String errorCode, String reason) {
                    dismissProgress();
                    // showError(reason);

                    new OkDialog(ReviewAndConfirmActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
                        @Override
                        public void handleResponse() {

                        }
                    });
                }
            });
        }


    }

    private void callP2PApi() {
        showProgress();

        MovitWalletController movitWalletController;


        if (isFavChecked) {
            movitWalletController = new PayPersonController(strUserName, strUserNumber, strAmount, strOTP);
            movitWalletController.init(new IMovitWalletController<Bundle>() {

                @Override
                public void onSuccess(Bundle response) {

                    showSuccessScreen(response);
                }

                @Override
                public void onFailed(String errorCode, String reason) {
                    dismissProgress();
                   // showError(reason);

                    new OkDialog(ReviewAndConfirmActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
                        @Override
                        public void handleResponse() {

                        }
                    });
                }
            });

        } else {
            movitWalletController = new PayPersonController("", strUserNumber, strAmount, strOTP);
            movitWalletController.init(new IMovitWalletController<Bundle>() {

                @Override
                public void onSuccess(Bundle response) {
                    showSuccessScreen(response);
                }

                @Override
                public void onFailed(String errorCode, String reason) {
                    dismissProgress();
                   // showError(reason);

                    new OkDialog(ReviewAndConfirmActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
                        @Override
                        public void handleResponse() {

                        }
                    });
                }
            });
        }


    }

    private void callAuthoriseReqMoney() {

        showProgress();

        AuthoriseRequestMoneyController authoriseRequestMoneyController = new AuthoriseRequestMoneyController(objMoneyRequest, strOTP, strAmount, strStatus);
        authoriseRequestMoneyController.init(new IMovitWalletController<Bundle>() {
            @Override
            public void onSuccess(Bundle response) {
                dismissProgress();
                handleAuthorizeResponse(response);
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                showError(reason);

            }
        });

    }

    private void handleAuthorizeResponse(Bundle response) {

        if (otpDialog != null)
            otpDialog.dismiss();

        new OkDialog(ReviewAndConfirmActivity.this, response.getString(IntentConstants.Message), new OkDialog.IOkDialogCallback() {
            @Override
            public void handleResponse() {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    private void showSuccessScreen(Bundle response) {

        dismissProgress();

        Intent intent = new Intent(ReviewAndConfirmActivity.this, TxnSuccessActivity.class);
        Bundle b = new Bundle();
        b.putString("txnType", strTxnType);
        b.putString(IntentConstants.TXNID, response.get("TxnId") + "");
        b.putString("username", strUserName);
        b.putString("receiverMobile", getString(R.string.country_code) + " " + strUserNumber);
        b.putString(IntentConstants.TRANSFER_AMOUNT, strAmount);
        intent.putExtras(b);
        startActivity(intent);

        if (otpDialog != null)
            otpDialog.cancel();

    }


}
