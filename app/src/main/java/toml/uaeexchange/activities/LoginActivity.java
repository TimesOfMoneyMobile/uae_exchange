package toml.uaeexchange.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.android.device.DeviceName;

import java.security.MessageDigest;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import toml.movitwallet.controllers.BalanceInquiryController;
import toml.movitwallet.controllers.LoginController;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.BuildConfig;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.services.MyFirebaseInstanceIDService;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;


public class LoginActivity extends BaseActivity implements IMovitWalletController {


    @BindView(R.id.error_label_MobileNumber)
    ErrorLabelLayout errorLabelMobileNumber;
    @BindView(R.id.etMobileNumber)
    CustomEditTextLayout etMobileNumber;
    @BindView(R.id.error_label_Mpin)
    ErrorLabelLayout errorLabelMpin;
    @BindView(R.id.etMpin)
    CustomEditTextLayout etMpin;

    @BindView(R.id.tvForgotPassword)
    TextView tvForgotPassword;
    @BindView(R.id.tvCreateAccount)
    TextView tvCreateAccount;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.root_layout)
    LinearLayout rootLayout;

    GenericValidations genericValidations;
    Unbinder unbinder;

    String mobileNumber, mpin;
    private String fromActivity = "";
    private static final int FETCH_DEVICE_ID = 100;
    private boolean sentToSettings = false;
    private static final int REQUEST_PERMISSION_SETTING = 101;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // setupUI(findViewById(R.id.root_layout));
        unbinder = ButterKnife.bind(this);
        genericValidations = new GenericValidations(LoginActivity.this);

        checkForPhoneReadPermission();
//Device Root Check
        if (Constants.isDeviceRooted() && !BuildConfig.DEBUG) {
            new OkDialog(this, getString(R.string.rooted_device_error), new OkDialog.IOkDialogCallback() {
                @Override
                public void handleResponse() {
                    finish();
                }
            });


        }

        fromActivity = getIntent().getStringExtra(IntentConstants.FROM_ACTIVITY_REGISTRATION);

        etMobileNumber.setErrorLabelLayout(errorLabelMobileNumber);
        etMobileNumber.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etMobileNumber.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etMpin.requestFocus();
                    return true;
                }
                return false;
            }
        });

        etMpin.setErrorLabelLayout(errorLabelMpin);
        etMpin.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);


        etMobileNumber.getEditText().addTextChangedListener(textWatcher);
        etMpin.getEditText().addTextChangedListener(etMpinTextWatcher);

        // etMobileNumber.setText("998800003");

        // etMpin.setText("101");


        etMpin.getImgRight().setTag("hide_pin");
        etMpin.getImgRight().setImageResource(R.drawable.hide_pin);
        etMpin.getImgRight().setVisibility(View.VISIBLE);
        etMpin.getImgRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag().toString().equalsIgnoreCase("show_pin")) {
                    etMpin.getImgRight().setTag("hide_pin");
                    etMpin.getImgRight().setImageResource(R.drawable.hide_pin);
                    etMpin.getEditText().setTransformationMethod(new PasswordTransformationMethod());
                    //etValue.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    etMpin.getEditText().setSelection(etMpin.getEditText().getText().length());
                } else {
                    etMpin.getImgRight().setTag("show_pin");
                    etMpin.getImgRight().setImageResource(R.drawable.show);
                    etMpin.getEditText().setTransformationMethod(null);
                    //etValue.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    etMpin.getEditText().setSelection(etMpin.getEditText().getText().length());
                }

            }
        });


        String strText = getString(R.string.create_account);
        SpannableString spanStrText = new SpannableString(strText);
        tvCreateAccount.setHighlightColor(Color.TRANSPARENT);
        setColorAndClick(tvCreateAccount, strText, spanStrText, "Click here", ContextCompat.getColor(LoginActivity.this, R.color.clear_blue));


//        if (getIntent() != null)
//            ACTIVITY_TO_OPEN = getIntent().getStringExtra(IntentConstants.ACTIVITY_TO_OPEN);


        rootLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Constants.hideKeyboard(LoginActivity.this);
                return false;
            }
        });

        imgLogo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Constants.hideKeyboard(LoginActivity.this);
                return false;
            }
        });
    }

    private void checkForPhoneReadPermission() {

        if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.READ_PHONE_STATE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, FETCH_DEVICE_ID);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (AppSettings.getBooleanData(LoginActivity.this, Manifest.permission.READ_PHONE_STATE)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Need Phone Permission");
                builder.setMessage("This app needs phone permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        // Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, FETCH_DEVICE_ID);
            }


            AppSettings.putBooleanData(LoginActivity.this, Manifest.permission.READ_PHONE_STATE, true);


        } else {
            //You already have the permission, just go ahead.
            proceedAfterPermission();
        }


//        if (ContextCompat.checkSelfPermission(LoginActivity.this,
//                Manifest.permission.READ_PHONE_STATE)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(LoginActivity.this,
//                    new String[]{Manifest.permission.READ_PHONE_STATE},
//                    1);
//
//        }
    }

    private void proceedAfterPermission() {
        //We've got the permission, now we can proceed further
        //Toast.makeText(getBaseContext(), "We got the Storage Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(LoginActivity.this, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                proceedAfterPermission();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == FETCH_DEVICE_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                proceedAfterPermission();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.READ_PHONE_STATE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, FETCH_DEVICE_ID);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    //Toast.makeText(getBaseContext(),"Unable to get Permission",Toast.LENGTH_LONG).show();
                }
            }
        }


//        if (grantResults.length > 0
//                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//        } else {
//            finish();
//        }
    }


    @OnClick({R.id.tvForgotPassword,
            R.id.tvCreateAccount,
            R.id.btnLogin,
            R.id.imgLogo

    })
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.tvForgotPassword:
                BaseActivity.openNewActivity(LoginActivity.this, ForgotMpinActivity.class.getCanonicalName(), false);

                // BaseActivity.openNewActivity(LoginActivity.this, ForgotSecurityAnswerActivity.class.getCanonicalName(), false);
                break;

            case R.id.tvCreateAccount:
                BaseActivity.openNewActivity(LoginActivity.this, RegisterActivity.class.getCanonicalName(), false);
                break;

            case R.id.btnLogin:

                    callLoginApi();

                break;
            case R.id.imgLogo:
                if (BuildConfig.DEBUG) {
                    if (toml.movitwallet.utils.Constants.DEVICE_ID) {
                        toml.movitwallet.utils.Constants.DEVICE_ID = false;
                        //showError("TEST DEVICE ID");
                    } else {
                        toml.movitwallet.utils.Constants.DEVICE_ID = true;
                        //showError("REAL DEVICE ID");
                    }
                }

                break;

        }

    }

    private void callLoginApi() {

        mobileNumber = etMobileNumber.getText();
        mpin = etMpin.getText();

        if (genericValidations.validateLogin(mobileNumber, etMobileNumber, mpin, etMpin)) {
            int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

            if (permission == PackageManager.PERMISSION_GRANTED) {
                showProgress();
                MovitWalletController movitWalletController = new LoginController(LoginActivity.this, mobileNumber, mpin,DeviceName.getDeviceName());
                movitWalletController.init(LoginActivity.this);
            }else{
                checkForPhoneReadPermission();
            }
        }
    }


    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.toString().length() == getResources().getInteger(R.integer.mobileNoLength)) {
                etMpin.requestFocus();
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{etMobileNumber, etMpin})) {
                btnLogin.setEnabled(true);
            } else {
                btnLogin.setEnabled(false);

            }


        }
    };

    private TextWatcher etMpinTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.toString().length() == getResources().getInteger(R.integer.mpinLength)) {
                // callLoginApi();
                toml.movitwallet.utils.Constants.hideKeyboard(LoginActivity.this);
                // callLoginApi();
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{etMobileNumber, etMpin})) {
                btnLogin.setEnabled(true);
            } else {
                btnLogin.setEnabled(false);

            }


        }
    };


    private void generateKeyHash() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "toml.movitconsumerwallet",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.v("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Login API Response Should be handle here


    @Override
    public void onSuccess(Object response) {
        dismissProgress();

        callBalanceAPI();

        callSaveDeviceTokenAPI();
        AppSettings.putBooleanData(this, AppSettings.IS_FIRSTTIMELOGIN, true);
        if (MovitConsumerApp.getInstance().getChangeMPIN().equalsIgnoreCase("Y"))
            BaseActivity.openNewActivity(LoginActivity.this, ChangeMpinActivity.class.getCanonicalName(), false);
        else if (getIntent().hasExtra(IntentConstants.ACTIVITY_TO_OPEN))
            BaseActivity.openNewActivity(LoginActivity.this, getIntent().getExtras().getString(IntentConstants.ACTIVITY_TO_OPEN), true);
        else if (getIntent().hasExtra(IntentConstants.FROM_ACTIVITY))
            BaseActivity.openNewActivity(LoginActivity.this, getIntent().getExtras().getString(IntentConstants.FROM_ACTIVITY), true);
        else
            BaseActivity.openNewActivity(LoginActivity.this, DashboardActivity.class.getCanonicalName(), false);


        finish();
    }

    private void callSaveDeviceTokenAPI() {

        //if (!AppSettings.getData(getApplicationContext(), AppSettings.isTOKENSENDTOSERVER).equals("true")) {
        Intent intent = new Intent(getApplicationContext(), MyFirebaseInstanceIDService.class);
        startService(intent);
        // }

    }

    @Override
    public void onFailed(String errorCode, String reason) {
        dismissProgress();
        showError(reason);

    }

    private void callBalanceAPI() {


        if (MovitConsumerApp.getInstance().getPrimaryWallet() != null) {


            final String walletID = MovitConsumerApp.getInstance().getPrimaryWallet().getWalletId();
            MovitWalletController controller = new BalanceInquiryController(walletID);

            controller.init(new IMovitWalletController<Bundle>() {
                @Override
                public void onSuccess(Bundle response) {

                    MovitConsumerApp.getInstance().setWalletBalanceByID(walletID, response.getString("Balance"));

                    //if (getActivity() != null) {
                    LogUtils.Verbose("Balance", response.getString("Balance"));
                    MovitConsumerApp.getInstance().setWalletBalance(response.getString("Balance"));

                    //}

                }

                @Override
                public void onFailed(String errorCode, String reason) {

                    //if (getActivity() != null) {
                    MovitConsumerApp.getInstance().setWalletBalance("");
                    Toast.makeText(LoginActivity.this, reason, Toast.LENGTH_SHORT).show();
                    // }
                }
            });
        } else {

            //  progressBar.setVisibility(View.GONE);
            //  layoutBalance.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();

    }

    private void openQuitDialog() {


        AlertDialog.Builder quitDialog
                = new AlertDialog.Builder(LoginActivity.this);
        quitDialog.setTitle("Confirm to Quit?");

        quitDialog.setPositiveButton("OK, Quit!", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                //WelcomeActivity.super.onBackPressed();
                finish();

            }
        });

        quitDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        });

        quitDialog.show();
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();


        //This handling for Login User where IS_FIRSTTIMELOGIN is true once user login
        if (AppSettings.getBooleanData(this, AppSettings.IS_FIRSTTIMELOGIN)) {

            openQuitDialog();
        } else {
            if (!TextUtils.isEmpty(fromActivity)) {
                if (fromActivity.equalsIgnoreCase(Constants.RegistrationSuccessActivity)) {

                    Intent intent = new Intent(LoginActivity.this, WelcomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra(IntentConstants.FROM_ACTIVITY_REGISTRATION, Constants.RegistrationSuccessActivity);
                    startActivity(intent);
                    finish();

                } else {
                    finish();
                }
            } else {
                finish();
            }
        }
    }
}
