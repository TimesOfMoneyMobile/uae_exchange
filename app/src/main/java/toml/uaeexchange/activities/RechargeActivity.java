package toml.uaeexchange.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.GetProductListController;
import toml.movitwallet.controllers.GetProviderController;
import toml.movitwallet.controllers.ListMerchantController;
import toml.movitwallet.jsonmodels.ProductListResponse;
import toml.movitwallet.jsonmodels.ProviderItems;
import toml.movitwallet.jsonmodels.ProviderResponse;
import toml.movitwallet.models.ListMerchantDetails;
import toml.movitwallet.models.ListMerchantsResponse;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.adapters.ProviderAdapter;
import toml.uaeexchange.adapters.RechargeProductAdapter;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.GenericValidations;
import toml.uaeexchange.utilities.TwoDecimalInputFilter;

public class RechargeActivity extends BaseActivity implements ProviderAdapter.ISpinnerItemClickLiner {


    @BindView(R.id.etMobile)
    CustomEditTextLayout etMobile;
    @BindView(R.id.ellMobileno)
    ErrorLabelLayout ellMobileno;
    @BindView(R.id.tvUserRegisterNo)
    TextView tvUserRegisterNo;
    @BindView(R.id.etAmount)
    CustomEditTextLayout etAmount;
    @BindView(R.id.ellAmount)
    ErrorLabelLayout ellAmount;
    @BindView(R.id.tvHavePromoCode)
    TextView tvHavePromoCode;
    @BindView(R.id.btnProceed)
    CustomButton btnProceed;
    @BindView(R.id.spinnerProvider)
    Spinner spinnerProvider;
    @BindView(R.id.llRechargeProduct)
    LinearLayout llRechargeProduct;
    @BindView(R.id.llRecharge)
    LinearLayout llRecharge;
    @BindView(R.id.gvRechargeProduct)
    GridView gvRechargeProduct;
    @BindView(R.id.ivCouponCancel)
    ImageView ivCouponCancel;
    @BindView(R.id.tvPromocodeName)
    TextView tvPromocodeName;
    @BindView(R.id.ivCoupon)
    ImageView ivCoupon;
    @BindView(R.id.rlPromocode)
    RelativeLayout rlPromocode;
    private ProviderAdapter providerAdapter;
    private ArrayList<String> providerCodes;
    private ProductListResponse productListResponse;
    private String skuCode;
    private String sendCurrencyISO;
    private String senderMobile;
    private String sendValue;
    private TextView[] tvRechargeProduct;
    private List<String> rechargeAmounts;
    private ArrayList<String> providerNamesDisplay;
    private boolean isFirstTime = false;
    private String min = "";
    private String max = "";
    private static final int CONTACT_REQUEST_CODE = 1;
    private static boolean isCallProductList = true;
    private String strPromoCode = "";
    private int PROMCODE_REQUEST_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);
        ButterKnife.bind(this);

        setToolbarWithBalance(getString(R.string.recharge), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, true);

        handleUI();
    }

    private void handleUI() {
        etMobile.getVerticalDivider().setVisibility(View.GONE);
        etAmount.getVerticalDivider().setVisibility(View.GONE);
        etMobile.getImgLeft().setVisibility(View.GONE);
        etAmount.getImgLeft().setVisibility(View.GONE);
        etAmount.getEditText().addTextChangedListener(textWatcher);

        etMobile.getEditText().addTextChangedListener(etMobileTextWatcher);

        etMobile.getImgRight().setImageResource(R.drawable.contacts);
        etMobile.getImgRight().setVisibility(View.VISIBLE);


        etMobile.getImgRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show contacts list
                // SendMoneyFragmentPermissionsDispatcher.showContactListWithCheck(SendMoneyFragment.this);

                if (ContextCompat.checkSelfPermission(RechargeActivity.this,
                        Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(RechargeActivity.this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            1);

                } else {
                    showContactList();
                }

            }
        });

        spinnerProvider.setEnabled(false);
        callBillerMerchant();

        //callGetProvider();

        spinnerProvider.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isCallProductList = true;
                return false;
            }
        });

        spinnerProvider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (isFirstTime && position > 0) {
                    String providerCode = providerCodes.get(position);
                    //callGetProductList(providerCode, mobileNoWitCountryCode, "", position);
                    //spinnerProvider.setSelection(1, false);
                    if (isCallProductList)
                        callGetProductList(providerCode, "", Constants.COUNTRY_ISO_CODE, position);

                } else {
                    isFirstTime = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void callBillerMerchant() {

        showProgress();
        MovitWalletController movitWalletController = new ListMerchantController(Constants.BILLER);
        movitWalletController.init(new IMovitWalletController<ListMerchantsResponse>() {
            @Override
            public void onSuccess(ListMerchantsResponse response) {

                dismissProgress();
                List<ListMerchantDetails.MerchantItem> listOfMerchants = response.getResponseDetails().getMerchants().getMerchantItemList();
                for (int i = 0; i < listOfMerchants.size(); i++) {
                    if (listOfMerchants.get(i).getName().equalsIgnoreCase("Ding")) {
                        Constants.BILLER_MERCHANT = listOfMerchants.get(i).getId();
                    }
                }
                callGetProvider();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

            }
        });
    }

    public void showContactList() {
        Intent intent = new Intent(this, ContactsListActivity.class);
        startActivityForResult(intent, CONTACT_REQUEST_CODE);
        // BaseActivity.openNewActivity(mContext, ContactsListActivity.class.getCanonicalName(),false);

    }


    public void setValuesToEditText(String number, String amount, String nickname, boolean isFav, String source) {
        if (!TextUtils.isEmpty(number))
            etMobile.setText(number);


    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            if (!TextUtils.isEmpty(charSequence)) {
                etAmount.getEtCountryCode().setText(getString(R.string.AED));
                etAmount.getEtCountryCode().setVisibility(View.VISIBLE);

            } else {
                etAmount.getEtCountryCode().setVisibility(View.GONE);
                etAmount.setBackgroundResource(R.drawable.pinkish_grey_border);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (toml.uaeexchange.utilities.Constants.checkForFocusActive(new CustomEditTextLayout[]{etMobile, etAmount}) && !TextUtils.isEmpty(skuCode) && toml.uaeexchange.utilities.Constants.checkForNonEmpty(new CustomEditTextLayout[]{etAmount})) {
                btnProceed.setEnabled(true);
            } else {
                btnProceed.setEnabled(false);
            }

        }
    };


    private String mobileNoWitCountryCode = "";
    private TextWatcher etMobileTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.toString().length() == getResources().getInteger(R.integer.mobileNoLength)) {
                mobileNoWitCountryCode = "971" + etMobile.getText();
                callGetProductList("", mobileNoWitCountryCode, Constants.COUNTRY_ISO_CODE, 0);
                if (providerCodes != null && providerCodes.size() > 0) {
                   /* providerAdapter.notifyDataSetChanged();
                    spinnerProvider.setSelection(1);*/
                    spinnerProvider.setEnabled(true);
                }
            } else {
                spinnerProvider.setEnabled(false);
                spinnerProvider.setSelection(0);
                etAmount.setText("");
                gvRechargeProduct.setVisibility(View.GONE);
                btnProceed.setEnabled(false);
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void callGetProvider() {
        //showProgress();
        MovitWalletController movitWalletController = new GetProviderController();
        movitWalletController.init(new IMovitWalletController<ProviderResponse>() {
            @Override
            public void onSuccess(ProviderResponse providerResponse) {
                // dismissProgress();
                List<ProviderItems> items = providerResponse.getItems();
                providerCodes = new ArrayList<String>();
                providerNamesDisplay = new ArrayList<String>();
                providerCodes.add("");
                providerNamesDisplay.add("--Select--");
                for (ProviderItems item : items) {
                    providerCodes.add(item.getProviderCode());
                    providerNamesDisplay.add(item.getName());
                }

                providerAdapter = new ProviderAdapter(RechargeActivity.this, providerNamesDisplay, new ProviderAdapter.ISpinnerItemClickLiner() {
                    @Override
                    public void onSpinnerItemClick(int position) {
                        /*spinnerProvider.performClick();

                        String providerCode = providerCodes.get(position);
                        callGetProductList(providerCode, "", Constants.COUNTRY_ISO_CODE, position);

                        try {
                            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                            method.setAccessible(true);
                            method.invoke(spinnerProvider);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/
                    }
                });
                //providerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerProvider.setAdapter(providerAdapter);
                spinnerProvider.setEnabled(false);


            }

            @Override
            public void onFailed(String errorCode, String reason) {
                //dismissProgress();
                    showError(getString(R.string.somethingWentWrong));


            }
        });

    }

    private void callGetProductList(final String providerCode, final String accountNoReciever, String countryISOCode, final int pos) {
        showProgress();

        String accountNo = "";
        MovitWalletController movitWalletController = new GetProductListController(Constants.GET_PRODUCT_LIST, accountNoReciever, countryISOCode, providerCode);
        movitWalletController.init(new IMovitWalletController<ProductListResponse>() {
            @Override
            public void onSuccess(ProductListResponse productListResponse) {
                dismissProgress();
                handleProductListSuccess(productListResponse, accountNoReciever, pos);

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                // showError(reason);
                showError(getString(R.string.somethingWentWrong));

            }
        });


    }

    private void handleProductListSuccess(ProductListResponse productListResponse, String accountNoReciever, int pos) {

        if (productListResponse.getProductListItem() != null && productListResponse.getProductListItem().size() == 0) {
            gvRechargeProduct.setVisibility(View.GONE);
            spinnerProvider.setEnabled(false);
            spinnerProvider.setSelection(0);
            isCallProductList = false;
            etAmount.getEditText().setEnabled(false);
            btnProceed.setEnabled(false);
            Toast.makeText(this, getString(R.string.oprater_not_supported), Toast.LENGTH_SHORT).show();
        } else {
            gvRechargeProduct.setVisibility(View.VISIBLE);
          //  btnProceed.setEnabled(true);
            etAmount.getEditText().setEnabled(true);
            skuCode = productListResponse.getProductListItem().get(0).getSkuCode();
            sendCurrencyISO = productListResponse.getProductListItem().get(0).getMaximum().getSendCurrencyIso();
            rechargeAmounts = productListResponse.getProductListItem().get(0).getList_item();
            min = productListResponse.getProductListItem().get(0).getMin();
            max = productListResponse.getProductListItem().get(0).getMax();

            if (!TextUtils.isEmpty(max)) {
                etAmount.getEditText().setFilters(new InputFilter[]{new TwoDecimalInputFilter(max.length())
                });
            }


            if (!TextUtils.isEmpty(accountNoReciever)) {
                String allocatedProvider = productListResponse.getProductListItem().get(0).getProviderCode();
                if (providerCodes != null && providerCodes.size() > 0) {
                    for (int i = 0; i < providerCodes.size(); i++) {
                        if (allocatedProvider.equalsIgnoreCase(providerCodes.get(i))) {
                            spinnerProvider.setSelection(i);
                            isCallProductList = false;
                            spinnerProvider.setEnabled(true);
                            providerAdapter.notifyDataSetChanged();

                            //etAmount.requestFocus();
                        }
                    }
                }
            } else {
                if (providerCodes != null && providerCodes.size() > 0) {
                    isCallProductList = true;
                    providerAdapter.notifyDataSetChanged();
                    spinnerProvider.setSelection(pos);
                    spinnerProvider.setEnabled(true);
                    providerAdapter.notifyDataSetChanged();
                    etAmount.requestFocus();
                }
            }

            if (rechargeAmounts != null) {
                drawProductList();
            }
        }
    }

    public void drawProductList() {


        gvRechargeProduct.setVisibility(View.VISIBLE);

        gvRechargeProduct.setAdapter(new RechargeProductAdapter(this, rechargeAmounts, new RechargeProductAdapter.IRechargeProductClickLiner() {
            @Override
            public void onRechargeProductClick(int position) {
                etAmount.setText(rechargeAmounts.get(position));
               // btnProceed.setEnabled(true);

            }
        }));


    }


    @OnClick({R.id.btnProceed, R.id.tvUserRegisterNo,R.id.tvHavePromoCode,R.id.ivCouponCancel})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.btnProceed:
                proceed();
                break;

            case R.id.tvUserRegisterNo:
                etMobile.setText(MovitConsumerApp.getInstance().getMobileNumber());

            case R.id.tvHavePromoCode:
                onPromocodeClick();
                break;
            case R.id.ivCouponCancel:
                onCancelPromocode();
                break;

        }


    }
    private void onCancelPromocode() {
        strPromoCode = "";
        tvHavePromoCode.setVisibility(View.VISIBLE);
        rlPromocode.setVisibility(View.GONE);
    }


    private void onPromocodeClick() {
       /* tvHavePromoCodeP2M.setVisibility(View.GONE);
        rlPromocode.setVisibility(View.VISIBLE);*/

        sendValue = etAmount.getText();
        if (!TextUtils.isEmpty(sendValue)) {
            Intent intent = new Intent(this, AddPromoActivity.class);
            intent.putExtra(IntentConstants.AMOUNT, sendValue);
            startActivityForResult(intent, PROMCODE_REQUEST_CODE);
        } else {
            showError(getString(R.string.please_enter_valid_amount));
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == PROMCODE_REQUEST_CODE) {
                strPromoCode = data.getStringExtra(IntentConstants.PROMOCODE);
                tvHavePromoCode.setVisibility(View.GONE);
                rlPromocode.setVisibility(View.VISIBLE);
                tvPromocodeName.setText(strPromoCode + " applied");

            } else if (requestCode == CONTACT_REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    setValuesToEditText(data.getStringExtra("number"), "", data.getStringExtra("name"), data.getBooleanExtra("isFav", false), data.getStringExtra("source"));

                }

            }
        }
    }

    private void proceed() {

        senderMobile = etMobile.getText().toString();
        sendValue = etAmount.getText().toString();
        if (validate()) {
            Intent intent = new Intent(this, RechargeConfirmationActivity.class);
            intent.putExtra(IntentConstants.ACCOUNT_NO, senderMobile);
            intent.putExtra(IntentConstants.SEND_VALUE, sendValue);
            intent.putExtra(IntentConstants.SEND_CURRENCY_ISO, sendCurrencyISO);
            intent.putExtra(IntentConstants.SKU_CODE, skuCode);
            intent.putExtra(IntentConstants.DISTRIBUTER_REF, "");
            intent.putExtra(IntentConstants.VALIDATE_ONLY, true);
            intent.putExtra(IntentConstants.PROMOCODE, strPromoCode);
            startActivity(intent);
        }
    }

    private boolean validate() {

        GenericValidations genericValidations = new GenericValidations(this);

        try {


            if (!genericValidations.isValidMobile(senderMobile)) {
                showError(genericValidations.getErrorMessage());
                return false;
            } else if (!genericValidations.isValidAmount(sendValue)) {
                showError(genericValidations.getErrorMessage());
                return false;
            } else if (!TextUtils.isEmpty(max) && !TextUtils.isEmpty(min)) {

                if (Float.parseFloat(sendValue) > Float.parseFloat(max) || Float.parseFloat(sendValue) < Float.parseFloat(min)) {
                    //String errormessage = "Recharge amount should be between AED " + min + " to " + "AED" + max + "";

                    String errormessage = "You'll be able to recharge for amounts between AED " + min + " and " + "AED " + max + ". Please enter the amount or choose from the options provided.";
                    new OkDialog(this, errormessage, new OkDialog.IOkDialogCallback() {
                        @Override
                        public void handleResponse() {
                            etAmount.requestFocus();
                        }
                    });
                    return false;
                }

            } else if (Float.parseFloat(MovitConsumerApp.getInstance().getWalletBalance()) < Float.parseFloat(sendValue)) {
                showError(getString(R.string.amount_should_be_less_than_balance));
                return false;
            } else if (Float.parseFloat(sendValue) <= 0) {
                showError(getString(R.string.errorValidAmount));
                return false;
            }
        } catch (NumberFormatException ex) {
            showError(getString(R.string.please_enter_valid_amount));
            return false;
        }
        return true;

    }

    @Override
    public void onSpinnerItemClick(int position) {

    }
}
