package toml.uaeexchange.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import toml.movitwallet.utils.DecodeQRCode;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;


public class P2MQRCodeScanActivity extends BaseActivity implements QRCodeReaderView.OnQRCodeReadListener {

    /*    @BindView(R.id.ivBack)
        ImageView ivBack;*/
    /*@BindView(R.id.toolBarP2M)
    Toolbar toolBarP2M;*/
    @BindView(R.id.etMerchantId)
    EditText etMerchantId;
    @BindView(R.id.civOr)
    CircleImageView civOr;
    @BindView(R.id.llScanFromGallery)
    LinearLayout llScanFromGallery;
    @BindView(R.id.qrdecoderview)
    QRCodeReaderView qrdecoderview;
    private Handler handler;
    private boolean shouldExecuteOnResume;
    GenericValidations genericValidations;

    boolean isErrorShown;
    private static boolean onceExecute = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p2_mqrcode_scan);

        setToolbarWithBalance(getString(R.string.payamerchant), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, true);
        ButterKnife.bind(this);
        genericValidations = new GenericValidations(this);
        loadQRCode();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (onceExecute) {
            onceExecute = false;
        }

    }

    private void loadQRCode() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showhideQRCOde(true);
            }
        }, 300);
        qrdecoderview.setOnQRCodeReadListener(this);
    }

    @OnClick(R.id.etMerchantId)
    public void etMerchantId() {
       /* Intent intent = new Intent(this, P2MAmountActivity.class);
        startActivity(intent);*/
        // finish();
    }

    public void showhideQRCOde(boolean isSHow) {
        try {
            if (isSHow) {


//                if (ContextCompat.checkSelfPermission(this,
//                        Manifest.permission.CAMERA)
//                        == PackageManager.PERMISSION_GRANTED) {
//
//                    qrdecoderview.setVisibility(View.VISIBLE);
//
//                    qrdecoderview.startCamera();
//
//                    //qrdecoderview.getCameraManager().startPreview();
//                } else {
//                    ActivityCompat.requestPermissions(this,
//                            new String[]{Manifest.permission.CAMERA},
//                            1);
//                }
                qrdecoderview.setVisibility(View.VISIBLE);

            } else {

                if (qrdecoderview != null) {
                    //  qrdecoderview.getCameraManager().stopPreview();
                    qrdecoderview.stopCamera();
                    qrdecoderview.setVisibility(View.GONE);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*@OnClick(R.id.ivBack)
    public void ivBack() {
        finish();
    }*/

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        LogUtils.Verbose("QRCode String", text);

        Map<String, String> map = null;

        try {
            map = DecodeQRCode.decodeQR(text);

            if (!onceExecute) {
                if (isValidQRCode(map)) {
                    onceExecute = true;
                    LogUtils.Verbose("QR Code Map", "" + map);
                    qrdecoderview.stopCamera();
                    Intent intent = new Intent(this, P2MAmountActivity.class);
                    intent.putExtra(IntentConstants.EXTERNAL_MERCHANTID, map.get("EXTERNAL_ID"));
                    intent.putExtra(IntentConstants.MERCHANTID, map.get("MERCHANT_ID"));
                    intent.putExtra(IntentConstants.MERCHANTNAME, map.get("MERCHANT_NAME"));
                    intent.putExtra(IntentConstants.QR_CODE_AMOUNT, map.get("AMOUNT"));
                    intent.putExtra(IntentConstants.MOBILE_NUMBER, map.get("MobileNo"));
                    startActivity(intent);
                }
                else
                {
                    onceExecute = true;
                    new OkDialog(this, getString(R.string.errorInvalidQR), null, new OkDialog.IOkDialogCallback() {
                        @Override
                        public void handleResponse() {

                            onceExecute = false;
                        }
                    });
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if (!isErrorShown) {
                isErrorShown = true;
                new OkDialog(this, getString(R.string.errorInvalidQR), null, new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                        isErrorShown = false;
                    }
                });
            }

            isErrorShown = true;
        }

    }

    public boolean isValidQRCode(Map<String, String> map) {
        String errorMessage = null;

        if (TextUtils.isEmpty(map.get(IntentConstants.MERCHANTID))) {
            errorMessage = this.getString(R.string.errorEmptyMerchantID);
            return false;
        } else if (TextUtils.isEmpty(map.get(IntentConstants.MERCHANTNAME))) {
            errorMessage = this.getString(R.string.errorEmptyMerchantName);
            return false;
        }


        if (errorMessage != null) {

            if (!isErrorShown) {
                isErrorShown = true;
                new OkDialog(this, errorMessage, null, new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                        isErrorShown = false;
                    }
                });
            }

            return false;
        } else
            return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


            onReceivePermission(true);
        } else {
            onReceivePermission(false);
        }
    }

    public void onReceivePermission(boolean flag) {
        if (flag) {
            qrdecoderview.setVisibility(View.VISIBLE);

            // instruction.setText("");
        } else {
            Toast.makeText(this, getString(R.string.cameraPermissionDenied), Toast.LENGTH_SHORT).show();
        }
    }
}
