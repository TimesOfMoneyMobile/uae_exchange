package toml.uaeexchange.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.IndicatorController;

import java.util.ArrayList;
import java.util.List;

import toml.movitwallet.utils.IntentConstants;
import toml.uaeexchange.R;
import toml.uaeexchange.fragments.WelcomeActionFragment;
import toml.uaeexchange.fragments.WelcomeScreen1Fragemnt;
import toml.uaeexchange.fragments.WelcomeScreen2Fragemnt;
import toml.uaeexchange.utilities.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class WelcomeActivity extends AppIntro {

    public static WelcomeActivity thisActivity;
    private String fromActivity = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_welcome);

        //addSlide(AppIntroFragment.newInstance("Slide 1", "", "Desc 1", "", R.mipmap.ic_launcher, Color.parseColor("#FFFFFF"), Color.parseColor("#000000"), Color.parseColor("#000000")));
        //addSlide(AppIntroFragment.newInstance("Slide 2", "", "Desc 2", "", R.mipmap.ic_launcher, Color.parseColor("#FFFFFF"), Color.parseColor("#000000"), Color.parseColor("#000000")));

        //setSeparatorColor(Color.parseColor("#0000FF"));

        thisActivity = this;


        if (!(TextUtils.isEmpty(getIntent().getStringExtra(IntentConstants.FROM_ACTIVITY_REGISTRATION)))) {
            fromActivity = getIntent().getStringExtra(IntentConstants.FROM_ACTIVITY_REGISTRATION);
        } else {
            fromActivity = "";
        }


        if (!TextUtils.isEmpty(fromActivity)) {
            if (fromActivity.equalsIgnoreCase(Constants.RegistrationSuccessActivity)) {
                addSlide(new WelcomeActionFragment());
                setCustomIndicator(null);
                showPagerIndicator(false);
                showSeparator(false);
            }
        } else {
            addSlide(new WelcomeScreen1Fragemnt());
            addSlide(new WelcomeScreen2Fragemnt());
            addSlide(new WelcomeActionFragment());
            setCustomIndicator(new CustomIndicatorController());
            setIndicatorColor(ContextCompat.getColor(WelcomeActivity.this, R.color.colorPrimary), ContextCompat.getColor(WelcomeActivity.this, R.color.darkGreyColor));

            showSeparator(false);

            // Hide Skip/Done button.
            showSkipButton(false);
            showDoneButton(false);

        }

        getPager().addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 2) {

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    // pass context to Calligraphy
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }


    private class CustomIndicatorController implements IndicatorController {
        private static final int FIRST_PAGE_NUM = 0;
        private TextView mTextView;
        private int mSlideCount;
        private List<ImageView> mDots;
        int selectedDotColor = DEFAULT_COLOR;
        int unselectedDotColor = DEFAULT_COLOR;
        private Context mContext;
        private LinearLayout parent, mDotLayout;
        int mCurrentPosition;

        @Override
        public View newInstance(@NonNull Context context) {
            mContext = context;
            mDotLayout = (LinearLayout) View.inflate(context, R.layout.page_indicator_dot_container, null);
            mTextView = (TextView) View.inflate(context, R.layout.custom_indicator, null);
            return mDotLayout;
        }

        @Override
        public void initialize(int slideCount) {
            mSlideCount = slideCount;

            mDots = new ArrayList<>();
            mSlideCount = slideCount;
            selectedDotColor = -1;
            unselectedDotColor = -1;

            for (int i = 0; i < slideCount; i++) {
                ImageView dot = new ImageView(mContext);

                dot.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.indicator_dot_grey));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.setMargins(5, 0, 5, 0);
                dot.setPadding(0, 10, 0, 10);
                params.gravity = Gravity.CENTER;
                //mDotLayout.setBackgroundColor(ContextCompat.getColor(WelcomeActivity.this, android.R.color.holo_red_dark));
                mDotLayout.addView(dot, params);
                mDots.add(dot);


            }


            selectPosition(FIRST_PAGE_NUM);
        }

        @Override
        public void selectPosition(int index) {
            //   mTextView.setText(String.format("%d/%d", index + 1, mSlideCount));

            mCurrentPosition = index;
            for (int i = 0; i < mSlideCount; i++) {
                int drawableId = (i == index) ?
                        (R.drawable.next_welcome) : (R.drawable.oval_copy);
                Drawable drawable = ContextCompat.getDrawable(mContext, drawableId);
               /* if (selectedDotColor != DEFAULT_COLOR && i == index)
                    drawable.mutate().setColorFilter(selectedDotColor, PorterDuff.Mode.SRC_IN);
                if (unselectedDotColor != DEFAULT_COLOR && i != index)
                    drawable.mutate().setColorFilter(unselectedDotColor, PorterDuff.Mode.SRC_IN);*/
                mDots.get(i).setImageDrawable(drawable);
            }

            if (index == 2) {
                if (this.mDotLayout != null)
                    this.mDotLayout.setVisibility(View.GONE);
            } else if (index >= 0) {
                if (this.mDotLayout != null)
                    this.mDotLayout.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void setSelectedIndicatorColor(int color) {
            selectedDotColor = color;
            selectPosition(mCurrentPosition);
        }

        @Override
        public void setUnselectedIndicatorColor(int color) {
            unselectedDotColor = color;
            selectPosition(mCurrentPosition);

        }
    }


    @Override
    public void onBackPressed() {

        if (!TextUtils.isEmpty(fromActivity)) {
            if (fromActivity.equalsIgnoreCase(Constants.RegistrationSuccessActivity)) {
                if (getPager().getCurrentItem() == 0) {
                    openQuitDialog();
                }
            }
        } else {
            if (getPager().getCurrentItem() == 2) {
                openQuitDialog();
            } else {
                super.onBackPressed();
            }
        }


    }

    private void openQuitDialog() {


        AlertDialog.Builder quitDialog
                = new AlertDialog.Builder(WelcomeActivity.this);
        quitDialog.setTitle("Confirm to Quit?");

        quitDialog.setPositiveButton("OK, Quit!", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                //WelcomeActivity.super.onBackPressed();
                finish();

            }
        });

        quitDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        });

        quitDialog.show();
    }
}
