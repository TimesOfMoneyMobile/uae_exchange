package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.GenerateOTPController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 8/3/2017.
 */

public class ForgotMpinActivity extends BaseActivity {


    @BindView(R.id.edtxtLayoutMobileNumber)
    CustomEditTextLayout edtxtLayoutMobileNumber;
    @BindView(R.id.errorLabelMobileNumber)
    ErrorLabelLayout errorLabelMobileNumber;
    @BindView(R.id.btnProceed)
    CustomButton btnProceed;
    @BindView(R.id.root_layout)
    RelativeLayout rootLayout;


    private String TXN_TYPE = "FORGOT";
    Spannable spannable;
    private boolean isSpanClicked;

    private SmsVerifyCatcher smsVerifyCatcher;

    private String otpValue, mobileNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_mpin);

        setToolbarWithBalance(getString(R.string.forgot_mpin), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);

        ButterKnife.bind(this);

        // Setting text watcher here
        edtxtLayoutMobileNumber.getEditText().addTextChangedListener(textWatcher);
        edtxtLayoutMobileNumber.setErrorLabelLayout(errorLabelMobileNumber);


        rootLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Constants.hideKeyboard(ForgotMpinActivity.this);
                return false;
            }
        });

    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.toString().length() == getResources().getInteger(R.integer.mobileNoLength)) {
                Constants.hideKeyboard(ForgotMpinActivity.this);
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{edtxtLayoutMobileNumber})) {
                btnProceed.setEnabled(true);
            } else {
                btnProceed.setEnabled(false);
            }


        }
    };


    private void callGenerateOTP() {
        showProgress();

        MovitConsumerApp.getInstance().setMobileNumber(mobileNumber);

        MovitWalletController generateOTPController = new GenerateOTPController(TXN_TYPE);

        generateOTPController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                dismissProgress();

                Intent intent = new Intent(ForgotMpinActivity.this, ForgotMpinOTPActivity.class);
                intent.putExtra(IntentConstants.MOBILE_NUMBER, edtxtLayoutMobileNumber.getText().toString());
                startActivity(intent);


            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();

                edtxtLayoutMobileNumber.getErrorLabelLayout().setError(reason);
                edtxtLayoutMobileNumber.setBackgroundResource(R.drawable.custom_edittext_border_red);
                edtxtLayoutMobileNumber.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(ForgotMpinActivity.this, R.color.grapefruit));


                // showError(reason);

            }
        });
    }


    @OnClick(R.id.btnProceed)
    public void onViewClicked() {

        mobileNumber = edtxtLayoutMobileNumber.getText().toString().trim();

        if (!TextUtils.isEmpty(mobileNumber)) {
            if (mobileNumber.length() == getResources().getInteger(R.integer.mobileNoLength)) {

                callGenerateOTP();

            } else {

                edtxtLayoutMobileNumber.getErrorLabelLayout().setError(getString(R.string.errorInvalidMobile));
                edtxtLayoutMobileNumber.setBackgroundResource(R.drawable.custom_edittext_border_red);
                edtxtLayoutMobileNumber.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(ForgotMpinActivity.this, R.color.grapefruit));

                //showError(getString(R.string.errorInvalidMobile));
            }
        } else {
            //showError(getString(R.string.errorEmptyMobile));

            edtxtLayoutMobileNumber.getErrorLabelLayout().setError(getString(R.string.errorEmptyMobile));
            edtxtLayoutMobileNumber.setBackgroundResource(R.drawable.custom_edittext_border_red);
            edtxtLayoutMobileNumber.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(ForgotMpinActivity.this, R.color.grapefruit));
        }

    }
}
