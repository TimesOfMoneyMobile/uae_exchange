package toml.uaeexchange.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.paolorotolo.appintro.AppIntro;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import toml.movitwallet.models.Document;
import toml.movitwallet.models.RegistrationRequest;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;
import toml.uaeexchange.fragments.KYCRegistrationFragmentStepFour;
import toml.uaeexchange.fragments.KYCRegistrationStep1Fragment;
import toml.uaeexchange.fragments.KYCRegistrationStep2Fragment;
import toml.uaeexchange.fragments.KYCRegistrationStep2_LastPageFragment;
import toml.uaeexchange.utilities.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class KYCRegistrationActivity extends AppIntro {

    public static KYCRegistrationActivity thisActivity;

    RegistrationRequest objRegistrationRequest;
    private Map<String, RequestBody> requestBodyMap;
    public ArrayList<Document> documents;
    public Map<Document, Map<String, Bitmap>> documentMap;
    HashMap<String, File> fileHashMap;
    private ProgressDialog progressDialog;
    private Document selectedDocument;
    private String fromActivity;
    private HashMap<String, Bitmap> imageByteHashMap;
    private HashMap<String, ByteArrayOutputStream> imageByteStreamMap;

    public HashMap<String, ByteArrayOutputStream> getImageByteStreamMap() {
        return imageByteStreamMap;
    }

    public void setImageByteStreamMap(HashMap<String, ByteArrayOutputStream> imageByteStreamMap) {
        this.imageByteStreamMap = imageByteStreamMap;
    }


    public HashMap<String, Bitmap> getImageByteHashMap() {
        return imageByteHashMap;
    }

    public void setImageByteHashMap(HashMap<String, Bitmap> imageByteHashMap) {
        this.imageByteHashMap = imageByteHashMap;
    }


    // pass context to Calligraphy
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_kycregistration);

        thisActivity = this;

        imageByteHashMap = new HashMap<>();

        imageByteStreamMap = new HashMap<>();

        objRegistrationRequest = getIntent().getExtras().getParcelable(IntentConstants.REG_REQUEST);

        fromActivity = getIntent().getExtras().getString(IntentConstants.FROM_ACTIVITY);


        addSlide(new KYCRegistrationStep1Fragment());
        addSlide(new KYCRegistrationStep2Fragment());
        addSlide(new KYCRegistrationStep2_LastPageFragment());
        // addSlide(new KYCRegistrationFragmentStepThree());
        addSlide(new KYCRegistrationFragmentStepFour());


        // To disable paging after swipen
        getPager().setPagingEnabled(false);

        getPager().setOffscreenPageLimit(0);

        showSeparator(false);
        showPagerIndicator(false);

        // Hide Skip/Done button.
        showSkipButton(false);
        showDoneButton(false);


    }

    public void showProgress() {
        progressDialog = new ProgressDialog(KYCRegistrationActivity.this);
        progressDialog.setCanceledOnTouchOutside(false);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        progressDialog.show();

    }

    public void dismissProgress() {

        if (progressDialog != null && progressDialog.isShowing()) {
            LogUtils.Verbose("inside if", "dismissProgress......");
            progressDialog.dismiss();
        }
    }


    public void setToolbar(Toolbar toolbar, String titleText, int leftImageResourceId, int rightImageResourceId, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener) {

        if (!TextUtils.isEmpty(titleText)) {
            TextView title = (TextView) toolbar.findViewById(R.id.title_pay);
            title.setText(titleText);
        } else if (titleText.equalsIgnoreCase(getString(R.string.menu_nearby))) {
            RelativeLayout relSettings = (RelativeLayout) findViewById(R.id.relSettings);
            relSettings.setVisibility(View.VISIBLE);
        }

        // setSupportActionBar(toolbar);

        ImageView imgvwLeft, imgvwRight;

        imgvwLeft = (ImageView) toolbar.findViewById(R.id.imgvwLeft);
        imgvwRight = (ImageView) toolbar.findViewById(R.id.imgvwRight);

        // For left image

        if (imgvwLeft != null) {

            if (leftImageResourceId > 0) {

                imgvwLeft.setImageResource(leftImageResourceId);

                if (imgvwLeft != null) {
                    imgvwLeft.setOnClickListener(leftClickListener);
                }
            } else {
                imgvwLeft.setVisibility(View.INVISIBLE);
            }

        }

        // For right  image

        if (imgvwRight != null) {

            if (rightImageResourceId > 0) {

                imgvwRight.setImageResource(rightImageResourceId);

                if (imgvwRight != null) {
                    imgvwRight.setOnClickListener(rightClickListener);
                }
            } else {
                imgvwRight.setVisibility(View.INVISIBLE);
            }
        }


    }

    public RegistrationRequest getRegistrationRequestObject() {

        if (objRegistrationRequest == null) {
            objRegistrationRequest = new RegistrationRequest();
        }

        return objRegistrationRequest;

        //  return objRegistrationRequest;

    }

    public void setFileHashMap(HashMap<String, File> fileHashMap) {
        this.fileHashMap = fileHashMap;
    }


    public ArrayList<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<Document> documents) {
        this.documents = documents;
    }

    public void setSelectedDocument(Document document) {
        selectedDocument = document;
    }

    public Document getSelectedDocument() {
        return selectedDocument;
    }

    public Map<Document, Map<String, Bitmap>> getDocumentMap() {
        if (documentMap == null)
            documentMap = new HashMap<>();
        return documentMap;
    }


    public void setRequestBodyMap(Map<String, RequestBody> requestBodyMap) {

        this.requestBodyMap = requestBodyMap;
    }

    public Map<String, RequestBody> getRequestBodyMap() {
        return requestBodyMap;
    }

    public void deleteCacheFiles() {

        try {
            if (fileHashMap != null) {
                for (String s : fileHashMap.keySet()) {
                    File file = fileHashMap.get(s);
                    boolean b = file.delete();
                    Log.v("TAG", "Deleted " + b);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        fileHashMap = null;
    }


    public HashMap<String, File> getFileHashMap() {

        if (fileHashMap == null) {
            fileHashMap = new HashMap<>();
        }
        return fileHashMap;
    }

    public byte[] readBytesFromURI(Uri uri) throws IOException {
        // this dynamically extends to take the bytes you read
        InputStream inputStream = getContentResolver().openInputStream(uri);
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        deleteCacheFiles();
        requestBodyMap = null;
        documentMap = null;
    }

    @Override
    public void onBackPressed() {


        if (getPager().getCurrentItem() == 3) {
            //  openQuitDialog();
            Intent intent = new Intent(KYCRegistrationActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(IntentConstants.FROM_ACTIVITY_REGISTRATION, Constants.RegistrationSuccessActivity);
            startActivity(intent);
            //BaseActivity.openNewActivity(RegistrationSuccessActivity.this, LoginActivity.class.getCanonicalName(), true);
            finish();
        } else {
            super.onBackPressed();
        }


    }

    private void openQuitDialog() {


        AlertDialog.Builder quitDialog
                = new AlertDialog.Builder(KYCRegistrationActivity.this);
        quitDialog.setTitle("Confirm to Quit?");

        quitDialog.setPositiveButton("OK, Quit!", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                //WelcomeActivity.super.onBackPressed();

            }
        });

        quitDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        });

        quitDialog.show();
    }
}
