package toml.uaeexchange.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.models.Offer;
import toml.movitwallet.utils.IntentConstants;
import toml.uaeexchange.R;

public class OffersDetailActivity extends AppCompatActivity {

    @BindView(R.id.tvPromcodeName)
    TextView tvPromcodeName;
    @BindView(R.id.tvOfferDescription)
    TextView tvOfferDescription;
    @BindView(R.id.tvTandCInfo)
    TextView tvTandCInfo;
    @BindView(R.id.tvValidity)
    TextView tvValidity;
    private Offer offer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers_detail);
        ButterKnife.bind(this);

        offer = getIntent().getParcelableExtra(IntentConstants.OFFER);
        tvPromcodeName.setText(offer.getPromoCodeName());
        tvOfferDescription.setText(offer.getDescription());
        tvValidity.setText("Valid till " + offer.getToDate());
    }
}
