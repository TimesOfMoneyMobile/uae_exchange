package toml.uaeexchange.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import toml.uaeexchange.R;


public class AboutWayve extends BaseActivity {

    WebView webView;
    private String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_wayve);
        if (getIntent().hasExtra("terms")) {
            setToolbarWithBalance(getString(R.string.terms_and_conditions), R.drawable.icon_back, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            }, 0, null, false);


            link = "https://wayveonline.com/terms.html";

        } else {
            setToolbarWithBalance(getString(R.string.about), R.drawable.icon_back, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            }, 0, null, false);
            link = "https://wayveonline.com/";

        }
        webView = (WebView) findViewById(R.id.web);
        webView.setWebViewClient(new MyWebViewClient());

        webView.loadUrl(link);
        showProgress();
    }

    class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);


            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            dismissProgress();

        }
    }
}
