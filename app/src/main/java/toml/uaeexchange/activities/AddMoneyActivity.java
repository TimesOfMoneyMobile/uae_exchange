package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.controllers.AddMoneyController;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;

public class AddMoneyActivity extends BaseActivity {

    @BindView(R.id.edtxtLayoutAmount)
    CustomEditTextLayout edtxtLayoutAmount;
    @BindView(R.id.errorLabelAmount)
    ErrorLabelLayout errorLabelAmount;
    @BindView(R.id.btnProceed)
    CustomButton btnProceed;
    public static AddMoneyActivity thisActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money);
        ButterKnife.bind(this);

        thisActivity = this;

        setToolbarWithBalance(getString(R.string.addMoney), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);

        edtxtLayoutAmount.setErrorLabelLayout(errorLabelAmount);
        edtxtLayoutAmount.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutAmount.getImgLeft().setVisibility(View.GONE);


        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(edtxtLayoutAmount.getText().toString())) {

                    Double enteredValue = Double.parseDouble(edtxtLayoutAmount.getText().toString());
                    Double walletBalance = Double.parseDouble(MovitConsumerApp.getInstance().getWalletBalance());

                    if (enteredValue + walletBalance <= Double.parseDouble(MovitConsumerApp.getInstance().getMaxAmount())) {
                        callAddMoneyAPI(String.format("%.2f", Double.valueOf(edtxtLayoutAmount.getText().toString())));
                    } else {
                        new OkDialog(AddMoneyActivity.this, "Wallet Balance cannot be greater than AED " + String.format("%.2f", Double.valueOf(MovitConsumerApp.getInstance().getMaxAmount())), new OkDialog.IOkDialogCallback() {
                            @Override
                            public void handleResponse() {

                            }
                        });
                    }


                } else {
                    errorLabelAmount.setError(getString(R.string.enter_amount_to_add_money));
                    edtxtLayoutAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);

                }


            }
        });
    }

    private void callAddMoneyAPI(String amount) {

        showProgress();

        AddMoneyController addMoneyController = new AddMoneyController(amount);
        addMoneyController.init(new IMovitWalletController<Bundle>() {
            @Override
            public void onSuccess(Bundle response) {
                dismissProgress();
                //showError("Add Money Success");

                String PGMerchantId = response.getString(IntentConstants.PG_MERCHANT_ID);
                String CollaboratorID = Constants.NEOCollaboratorId;
                String EncryptedRequest = response.getString(IntentConstants.ENCRYPTED_REQUEST);


                String link = PGMerchantId + "||" + CollaboratorID + "||" + EncryptedRequest;


                //link = "201708011000001||NI||A5MAOnx5z+HLpXjNHFO6W6LJyq2VF7w8A5sHsWKwjbXBiMYGe/nSWibBozXL8Bz6WMYvzGRhPqfBU0pL4ycBewoWr+GsJAoF25i2Q0LbHtnnd3X6aW6tJo6ARVfjtZ52ojsgau/a+99NQrU93kLGISaZHv3PyLPuxUCdGrpUdsDrhemMxzvOf2lvmVajUywfpernlLI5M3zCEEgCsTielifeEXu4n5rlSnD3l0jRqfvhCDRPo3g+Tm4OKwXrUDdQkb9P8xt7PruN7x8OBuGpbXJc7dr94MLmM9fSGNdZ2oHjn9V5jLp26t9OC2wnYDi9vsSyHqa18IuA8IVjom5WQ87Axc78JDu+cwjrEcv8FSDQrFu7fCAEJ3LUoh1ZghL8bto34VoUUrMoJ/It8YASQR32CuSPOjLuy55jSJz+9iNGYXUq/xkbol0ruRRtGWJ6i5ae6MHTFktGZX1Nes38J5olPZmiXa5Mmmj1ZOrHfpB/s1t1x2/XStR1ihKZaqelFwNGraLFKcsy/lNtFzpO4rX/4dqeB7axXlSFMn/4+O77Bz+J0i+dpQIvu3vk8BxFSW1cXuINcsVt5xzc9exaiDuX5YwlootQ9OjgDGeb5Pv5Qrr41gDinzRLBNODdCla";

                Intent intent = new Intent(AddMoneyActivity.this, AddMoneyWebView.class);
                intent.putExtra("link", link);
                startActivity(intent);

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();

                errorLabelAmount.setError(reason);
                edtxtLayoutAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                // showError("Add Money Failure");

            }
        });

    }
}
