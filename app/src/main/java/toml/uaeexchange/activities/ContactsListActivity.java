package toml.uaeexchange.activities;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ListView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import toml.movitwallet.models.Nationality;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;
import toml.uaeexchange.adapters.ContactsAdapter;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.movitwallet.models.ContactsModel;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 9/14/2017.
 */

public class ContactsListActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    @BindView(R.id.act_cl_et_search)
    CustomEditTextLayout etSearch;
    @BindView(R.id.act_cl_lv_contact_list)
    ListView lvContact;
    @BindView(R.id.act_ct_tv_no_contacts)
    TextView tvNoContacts;

    Unbinder unbinder;

    String cursorFilter;

    ContactsAdapter contactAdapter;
    ArrayList<ContactsModel> allContactsList;
    ArrayList<ContactsModel> serachContactsList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_list);

        setToolbarWithBalance(getString(R.string.contacts), R.drawable.icon_cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);


        unbinder = ButterKnife.bind(this);

        etSearch.getVerticalDivider().setVisibility(View.GONE);
        etSearch.getEditText().addTextChangedListener(textWatcher);
        etSearch.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);

        getSupportLoaderManager().initLoader(1, null, ContactsListActivity.this);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        Uri baseUri;
//        if (cursorFilter != null) {
//            baseUri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI,
//                    Uri.encode(cursorFilter));
//        } else {
            baseUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        //}


        CursorLoader cursorLoader = new CursorLoader(this,
                baseUri, // URI
                ContactsQuery.PROJECTION,// projection fields
                ContactsQuery.SELECTION,// the selection criteria
                null,// the selection args
                ContactsQuery.SORT_ORDER// the sort order
        );


        return cursorLoader;
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

//        if (cursor.getCount() == 0) {
//            tvNoContacts.setVisibility(View.VISIBLE);
//        } else {
//            tvNoContacts.setVisibility(View.GONE);
//        }


        allContactsList = new ArrayList<>();
        serachContactsList = new ArrayList<>();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            // The Cursor is now set to the right position
            ContactsModel objContactsModel = new ContactsModel();
            objContactsModel.setName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
            objContactsModel.setNumber(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            try {

                if (!TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI)))) {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI))));
                    objContactsModel.setImage(bitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            // To remove invalid numbers from contacts list.
            if (objContactsModel.getNumber().trim().length() >= 9) {
                allContactsList.add(objContactsModel);
                serachContactsList.add(objContactsModel);
            }


        }


        contactAdapter = new ContactsAdapter(this, cursor,serachContactsList);

        lvContact.setAdapter(contactAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }


    /**
     * This interface defines constants for the Cursor and CursorLoader, based on constants defined
     * in the {@link android.provider.ContactsContract.Contacts} class.
     */
    public interface ContactsQuery {


        // The selection clause for the CursorLoader query. The search criteria defined here
        // restrict results to contacts that have a display name and are linked to visible groups.
        // Notice that the search on the string provided by the user is implemented by appending
        // the search string to CONTENT_FILTER_URI.
        @SuppressLint("InlinedApi")
        final static String SELECTION =
                (Constants.hasHoneycomb() ? ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME) +
                        "<>''" + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1";

        // The desired sort order for the returned Cursor. In Android 3.0 and later, the primary
        // sort key allows for localization. In earlier versions. use the display name as the sort
        // key.
        @SuppressLint("InlinedApi")
        final static String SORT_ORDER =
                Constants.hasHoneycomb() ? ContactsContract.Contacts.SORT_KEY_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME;

        // The projection for the CursorLoader query. This is a list of columns that the Contacts
        // Provider should return in the Cursor.
        @SuppressLint("InlinedApi")
        final static String[] PROJECTION = {

                // The contact's row id
                // ContactsContract.Contacts._ID,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                // A pointer to the contact that is guaranteed to be more permanent than _ID. Given
                // a contact's current _ID value and LOOKUP_KEY, the Contacts Provider can generate
                // a "permanent" contact URI.
//                ContactsContract.Contacts.LOOKUP_KEY,
//                ContactsContract.Contacts.HAS_PHONE_NUMBER,
                // In platform version 3.0 and later, the Contacts table contains
                // DISPLAY_NAME_PRIMARY, which either contains the contact's displayable name or
                // some other useful identifier such as an email address. This column isn't
                // available in earlier versions of Android, so you must use Contacts.DISPLAY_NAME
                // instead.
                Constants.hasHoneycomb() ? ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,

                // In Android 3.0 and later, the thumbnail image is pointed to by
                // PHOTO_THUMBNAIL_URI. In earlier versions, there is no direct pointer; instead,
                // you generate the pointer from the contact's ID value and constants defined in
                // android.provider.ContactsContract.Contacts.
                Constants.hasHoneycomb() ? ContactsContract.Contacts.PHOTO_THUMBNAIL_URI : ContactsContract.Contacts._ID,

                // The sort order column for the returned Cursor, used by the AlphabetIndexer
                // SORT_ORDER,
        };


    }

    private void fetchContacts() {

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME + ") ASC");

        if ((cur != null ? cur.getCount() : 0) > 0) {

            while (cur != null && cur.moveToNext()) {

                long id = cur.getLong(cur.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));

                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                String phoneNo = "";
                Bitmap bm = null;


                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {

                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id + ""}, null);

                    if (pCur != null) {
                        while (pCur.moveToNext()) {
                            phoneNo = pCur.getString(pCur.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER));
                            LogUtils.Verbose("Name: ", name);
                            LogUtils.Verbose("Phone Number: ", phoneNo);

                            LogUtils.Verbose("Phone Number: ", id + "");
                        }


                        pCur.close();
                    }
                }


                Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
                Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
                Cursor cursor = cr.query(photoUri,
                        new String[]{ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
                if (cursor == null) {
                    LogUtils.Verbose("Image", "it is null");
                } else {
                    try {
                        if (cursor.moveToFirst()) {
                            byte[] data = cursor.getBlob(0);
                            if (data != null) {

                                bm = BitmapFactory.decodeStream(new ByteArrayInputStream(data));


                            }
                        }
                    } finally {
                        cursor.close();
                    }
                }

                ContactsModel contactsModel = new ContactsModel();
                contactsModel.setName(name);
                contactsModel.setNumber(phoneNo);
                contactsModel.setImage(bm);
                //list.add(contactsModel);
                //contactsAdapter = new ContactsAdapter(ContactsListActivity.this,list);
                lvContact.setAdapter(contactAdapter);


            }
        }
        if (cur != null) {
            cur.close();
        }


    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//            cursorFilter = !TextUtils.isEmpty(charSequence.toString()) ? charSequence.toString() : null;
//
//
//
//
//            getSupportLoaderManager().restartLoader(1, null, ContactsListActivity.this);
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (s.toString().length() == 0) {
                serachContactsList.clear();
                serachContactsList.addAll(allContactsList);
                contactAdapter.notifyDataSetChanged();
                tvNoContacts.setVisibility(View.GONE);
            } else {
                search(s.toString());
            }


        }
    };

    private void search(String text) {
        serachContactsList.clear();
        for (ContactsModel contactsModel : allContactsList) {
            if (contactsModel.getName().toLowerCase(Locale.ENGLISH).contains(text.toLowerCase())) {
                serachContactsList.add(contactsModel);

            }
        }


        if(serachContactsList.size() == 0){
            tvNoContacts.setVisibility(View.VISIBLE);
        }else{
            tvNoContacts.setVisibility(View.GONE);
        }

        contactAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
