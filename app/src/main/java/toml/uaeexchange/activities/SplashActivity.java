package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import toml.movitwallet.utils.AppSettings;
import toml.uaeexchange.R;

/**
 * Created by vishwanathp on 7/27/2017.
 */

public class SplashActivity extends AppCompatActivity {
    Handler handler = new Handler();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);



        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(AppSettings.getBooleanData(SplashActivity.this, AppSettings.IS_FIRSTTIMELOGIN)) {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    // close splash activity
                    finish();
                }
                else {
                    startActivity(new Intent(SplashActivity.this, WelcomeActivity.class));
                    // close splash activity
                    finish();
                }
            }
        }, 2000);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler = null;
    }
}

