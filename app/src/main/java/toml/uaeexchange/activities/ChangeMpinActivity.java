package toml.uaeexchange.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.ChangeMpinController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;

public class ChangeMpinActivity extends BaseActivity {

@BindView(R.id.act_cm_el_current_mpin)
    ErrorLabelLayout elCurrentMpin;
    @BindView(R.id.act_cm_et_current_mpin)
    CustomEditTextLayout etCurrentMpin;

    @BindView(R.id.act_cm_el_new_mpin)
    ErrorLabelLayout elNewMpin;
    @BindView(R.id.act_cm_et_new_mpin)
    CustomEditTextLayout etNewMpin;

    @BindView(R.id.act_cm_el_confirm_mpin)
    ErrorLabelLayout elConfirmMpin;
    @BindView(R.id.act_cm_et_confirm_mpin)
    CustomEditTextLayout etConfirmMpin;

    @BindView(R.id.act_cm_b_change_mpin)
    Button bChangeMpin;


    GenericValidations genericValidations;
    String strCurrentMpin, strNewMpin, strConfirmMpin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mpin);

        setToolbar(getString(R.string.change_mpin), R.drawable.icon_back, 0, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, null);
        ButterKnife.bind(this);

        genericValidations = new GenericValidations(ChangeMpinActivity.this);


        etCurrentMpin.setErrorLabelLayout(elCurrentMpin);
        etCurrentMpin.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etCurrentMpin.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                etNewMpin.requestFocus();
                return false;
            }
        });

        etNewMpin.setErrorLabelLayout(elNewMpin);
        etNewMpin.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etNewMpin.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                etConfirmMpin.requestFocus();
                return false;
            }
        });

        etConfirmMpin.setErrorLabelLayout(elConfirmMpin);
        etConfirmMpin.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);




        etCurrentMpin.getEditText().addTextChangedListener(textWatcher);
        etNewMpin.getEditText().addTextChangedListener(textWatcher);
        etConfirmMpin.getEditText().addTextChangedListener(textWatcher);

        etCurrentMpin.getImgRight().setTag("hide_pin");
        etCurrentMpin.getImgRight().setImageResource(R.drawable.hide_pin);
        etCurrentMpin.getImgRight().setVisibility(View.VISIBLE);

        etCurrentMpin.getImgLeft().setPadding(getInDp(18),getInDp(14),getInDp(18),getInDp(14));

        etCurrentMpin.getImgRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag().toString().equalsIgnoreCase("show_pin")) {
                    etCurrentMpin.getImgRight().setTag("hide_pin");
                    etCurrentMpin.getImgRight().setImageResource(R.drawable.hide_pin);
                    etCurrentMpin.getEditText().setTransformationMethod(new PasswordTransformationMethod());
                    //etValue.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    etCurrentMpin.getEditText().setSelection(etCurrentMpin.getEditText().getText().length());
                } else {
                    etCurrentMpin.getImgRight().setTag("show_pin");
                    etCurrentMpin.getImgRight().setImageResource(R.drawable.show);
                    etCurrentMpin.getEditText().setTransformationMethod(null);
                    //etValue.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    etCurrentMpin.getEditText().setSelection(etCurrentMpin.getEditText().getText().length());
                }

            }
        });


        etNewMpin.getImgRight().setTag("hide_pin");
        etNewMpin.getImgRight().setImageResource(R.drawable.hide_pin);
        etNewMpin.getImgRight().setVisibility(View.VISIBLE);

        etNewMpin.getImgRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag().toString().equalsIgnoreCase("show_pin")) {
                    etNewMpin.getImgRight().setTag("hide_pin");
                    etNewMpin.getImgRight().setImageResource(R.drawable.hide_pin);
                    etNewMpin.getEditText().setTransformationMethod(new PasswordTransformationMethod());
                    //etValue.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    etNewMpin.getEditText().setSelection(etNewMpin.getEditText().getText().length());
                } else {
                    etNewMpin.getImgRight().setTag("show_pin");
                    etNewMpin.getImgRight().setImageResource(R.drawable.show);
                    etNewMpin.getEditText().setTransformationMethod(null);
                    //etValue.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    etNewMpin.getEditText().setSelection(etNewMpin.getEditText().getText().length());
                }

            }
        });


    }


    @OnClick(R.id.act_cm_b_change_mpin)
    public void onClickOfChangeMpin() {
        strCurrentMpin = etCurrentMpin.getText();
        strNewMpin = etNewMpin.getText();
        strConfirmMpin = etConfirmMpin.getText();


        if (!genericValidations.validateChangeMpin(strCurrentMpin,etCurrentMpin, strNewMpin,etNewMpin, strConfirmMpin,etConfirmMpin)) {

            //showError(genericValidations.getErrorMessage());

        } else if (!Constants.isEasyMPIN(strNewMpin)) {
            new OkDialog(ChangeMpinActivity.this, getString(R.string.errorEasyMPIN), null, null);
            return ;
        }

        else {
            callApi(strCurrentMpin, strNewMpin, strConfirmMpin);

        }




    }

    private void callApi(String strCurrentMpin, String strNewMpin, String strConfirmMpin) {

        showProgress();
        MovitWalletController movitWalletController = new ChangeMpinController(strCurrentMpin, strNewMpin, strConfirmMpin);
        movitWalletController.init(new IMovitWalletController<Bundle>() {
            @Override
            public void onSuccess(Bundle response) {
                dismissProgress();
                new OkDialog(ChangeMpinActivity.this, response.getString("Message"), new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                        BaseActivity.openDashboardActivity(ChangeMpinActivity.this);
                        finish();

                    }
                });
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                showError(reason);
            }
        });


    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            strCurrentMpin = etCurrentMpin.getText();
            strNewMpin = etNewMpin.getText();
            strConfirmMpin = etConfirmMpin.getText();


            if (!genericValidations.validateChangeMpin(strCurrentMpin, strNewMpin, strConfirmMpin)) {

                etConfirmMpin.getImgRight().setVisibility(View.GONE);
            } else {
                etConfirmMpin.getImgRight().setVisibility(View.VISIBLE);
                etConfirmMpin.getImgRight().setImageResource(R.drawable.form_success);

            }

            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{etCurrentMpin, etNewMpin,etConfirmMpin})) {
                bChangeMpin.setEnabled(true);
            } else {
                bChangeMpin.setEnabled(false);

            }



        }
    };
    public int getInDp(int value) {
        int valueInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, getResources().getDisplayMetrics());
        return valueInDp;
    }

}
