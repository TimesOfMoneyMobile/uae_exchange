package toml.uaeexchange.activities;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.StringTokenizer;

import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;
import toml.uaeexchange.dialogs.OkDialog;


public class AddMoneyWebView extends BaseActivity {

    WebView webView;
    private String link = "";
    ProgressDialog progressDialog;
    String POST_PARAM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_wayve);
        if (getIntent().hasExtra("link")) {
            setToolbarWithBalance(getString(R.string.addMoney), R.drawable.icon_back, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            }, 0, null, false);


            link = getIntent().getStringExtra("link");

            POST_PARAM = "requestParameter=" + Uri.encode(link);

        }


        webView = (WebView) findViewById(R.id.web);
        //webView.setWebViewClient(new DPWebClient());
        //dhase

        //webView.loadUrl(link);


        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new DPWebClient());
        webView.setWebChromeClient(new DPWebChromeClient());
        webView.addJavascriptInterface(new DPResponseHandler(), "DPResponse");

      /*  if (Build.VERSION.SDK_INT >= 21) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }*/

        webView.postUrl("https://test.timesofmoney.com/direcpay/secure/PaymentTxnServlet",
                POST_PARAM.getBytes());
        // showProgress();

        progressDialog = new ProgressDialog(AddMoneyWebView.this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Please Wait");
        progressDialog.show();
    }

    class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            LogUtils.Verbose("Link", url);

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            dismissProgress();

        }
    }

    public class DPWebChromeClient extends WebChromeClient {


        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {

            Log.v("TAG", " JS ALert");
            return false;
        }
    }

    public class DPWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.v("TAG", "Loading URL " + url);
            if (!progressDialog.isShowing())
                progressDialog.show();

        }


        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            progressDialog.dismiss();
            progressDialog.setMessage("Processing Transaction");

        }


        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            // handler.proceed();

            Log.v("TAG", "SSL Error is ");

        }


        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            progressDialog.dismiss();
//
//            if (url.equals("https://acs.onlinesbi.com/sbi/jsp/PAReqEntry.jsp")) {
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        webView.loadUrl("javascript:document.frmvds.action = \"ACSAuthReqEntry.jsp\";document.frmvds.cancel.value = \"true\";document.frmvds.submit();");
//
//                    }
//                }, 10000);
//            }
//
//            if (url.contains("https://netsafe.hdfcbank.com/ACSWeb/jsp/payerAuthOptions.jsp") && !isExecuted) {
//
//
//                new Handler().postDelayed(
//                        new Runnable() {
//                            @Override
//                            public void run() {
//
//                                showAuthOptionsDialog();
//                            }
//                        }, 2000);
//                isExecuted = true;
//
//            }
//
//            if (url.contains("https://netsafe.hdfcbank.com/ACSWeb/jsp/dynamicAuth.jsp?transType=payerAuth")) {
//                new Handler().postDelayed(
//                        new Runnable() {
//                            @Override
//                            public void run() {
//
//                                showInputDialog("otp");
//                            }
//                        }, 2000);
//            }

            Log.v("TAG", "Finished URL " + url);
        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;

        }
    }


    class DPResponseHandler {

        String decrypted, paymentResponse, errorData;
        ProgressDialog responseDialog;


        @JavascriptInterface
        public void pgResponse(String response) {
            paymentResponse = response;
            Log.v("TAG", "PG Response is " + response);

            // Response Format - ResponseType | TxnID | Amount

            response = response.replaceAll("'", "");

            StringTokenizer st = new StringTokenizer(response, "|");

            final String ResponseType = st.nextToken();
            final String TxnID = st.nextToken();
            final String Amount = st.nextToken();

            //  showError("Response received = " + response);

            if (ResponseType.equalsIgnoreCase("Success")) {

                new OkDialog(AddMoneyWebView.this, "Amount has been added into your wallet.", new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {
                        AddMoneyActivity.thisActivity.finish();
                        finish();

                    }
                });
            } else {
                new OkDialog(AddMoneyWebView.this, "Transaction Failed", new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {
                        AddMoneyActivity.thisActivity.finish();
                        finish();

                    }
                });

            }

          /*  Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    responseDialog.dismiss();
                    showError("Response received = " + id);
                }
            }, 2000);*/


        }
    }

}
