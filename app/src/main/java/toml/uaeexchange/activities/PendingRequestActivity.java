package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.controllers.PendingRequestListController;
import toml.movitwallet.models.MoneyRequest;
import toml.movitwallet.utils.IMovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.adapter.PendingRequestAdapter;

public class PendingRequestActivity extends BaseActivity {

    @BindView(R.id.lvPendingReq)
    ListView lvPendingReq;
    @BindView(R.id.txtvwNoDataFound)
    TextView txtvwNoDataFound;
    List<MoneyRequest> pendingRequestList;

    private PendingRequestAdapter pendingRequestAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_request);
        ButterKnife.bind(this);

        setToolbarWithBalance(getString(R.string.pendingrequest), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);

        callPendingRequestAPI();

    }

    private void callPendingRequestAPI() {

        showProgress();
        PendingRequestListController pendingRequestListController = new PendingRequestListController();
        pendingRequestListController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                dismissProgress();

                lvPendingReq.setVisibility(View.VISIBLE);
                txtvwNoDataFound.setVisibility(View.GONE);

                handleSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                lvPendingReq.setVisibility(View.GONE);
                txtvwNoDataFound.setVisibility(View.VISIBLE);
                txtvwNoDataFound.setText(reason);

            }
        });

    }

    private void handleSuccess(Object response) {

        pendingRequestList = (ArrayList<MoneyRequest>) response;
        pendingRequestAdapter = new PendingRequestAdapter(PendingRequestActivity.this, pendingRequestList);
        lvPendingReq.setAdapter(pendingRequestAdapter);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                callPendingRequestAPI();
            }
        }

    }
}
