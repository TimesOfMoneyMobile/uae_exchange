package toml.uaeexchange.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;

import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.models.ResetQuestionsRequest;
import toml.movitwallet.utils.IntentConstants;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;

public class ForgotSecurityAnswerActivity extends BaseActivity {

    @BindView(R.id.edtxtLayoutDateOfBirth)
    CustomEditTextLayout edtxtLayoutDateOfBirth;
    @BindView(R.id.errorLabelDateOfBirth)
    ErrorLabelLayout errorLabelDateOfBirth;
    @BindView(R.id.edtxtLayoutEmitatesID)
    CustomEditTextLayout edtxtLayoutEmitatesID;
    @BindView(R.id.errorLabelEmiratesID)
    ErrorLabelLayout errorLabelEmiratesID;
    @BindView(R.id.edtxtLayoutEmailID)
    CustomEditTextLayout edtxtLayoutEmailID;
    @BindView(R.id.errorLabelEmailID)
    ErrorLabelLayout errorLabelEmailID;
    @BindView(R.id.btnContinue)
    CustomButton btnContinue;

    String selectedeDate = "";
    GenericValidations genericValidations;

    ResetQuestionsRequest objResetQuestionsRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_security_answer);
        ButterKnife.bind(this);

        genericValidations = new GenericValidations(ForgotSecurityAnswerActivity.this);

        setToolbarWithBalance(getString(R.string.forgot_security_answer), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);

        edtxtLayoutDateOfBirth.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutDateOfBirth.getImgLeft().setVisibility(View.GONE);

        edtxtLayoutEmitatesID.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutEmitatesID.getImgLeft().setVisibility(View.GONE);

        edtxtLayoutEmailID.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutEmailID.getImgLeft().setVisibility(View.GONE);

        edtxtLayoutDateOfBirth.getImgRight().setVisibility(View.VISIBLE);
        edtxtLayoutDateOfBirth.getImgRight().setImageResource(R.drawable.date_of_birth_icon);

        Constants.setEditTextMaxLength(edtxtLayoutEmailID.getEditText(), 50, false, false);

        edtxtLayoutEmitatesID.getEditText().addTextChangedListener(new PatternedTextWatcher("###-####-#######-#"));

        // Setting error labels here
        edtxtLayoutDateOfBirth.setErrorLabelLayout(errorLabelDateOfBirth);
        edtxtLayoutEmitatesID.setErrorLabelLayout(errorLabelEmiratesID);
        edtxtLayoutEmailID.setErrorLabelLayout(errorLabelEmailID);


        edtxtLayoutDateOfBirth.getEditText().setInputType(InputType.TYPE_NULL);
        edtxtLayoutDateOfBirth.getEditText().setFocusable(false);
        edtxtLayoutDateOfBirth.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtxtLayoutDateOfBirth.performClick();
            }
        });
        edtxtLayoutDateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar cal = Calendar.getInstance(Locale.ENGLISH);
              /* // This is alternative
                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 18);*/
                cal.add(Calendar.YEAR, -18);
                DatePickerDialog datePicker = new DatePickerDialog(ForgotSecurityAnswerActivity.this, R.style.CustomDatePickerDialogTheme,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                                month = month + 1;
                                String strMonth = month > 9 ? month + "" : "0" + month;
                                String strDay = dayOfMonth > 9 ? dayOfMonth + "" : "0" + dayOfMonth;

                                selectedeDate = strDay + "-" + strMonth + "-" + year;

                                String formattedDate = "";//Constants.getFormattedDate("dd-MM-yyyy", "dd MMMM, yyyy", date);

                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d");
                                String day = null;
                                try {
                                    day = simpleDateFormat.format(sdf.parse(selectedeDate));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                                if (day.endsWith("1"))
                                    simpleDateFormat = new SimpleDateFormat("d'st' MMMM, yyyy");
                                else if (day.endsWith("2"))
                                    simpleDateFormat = new SimpleDateFormat("d'nd' MMMM, yyyy");
                                else if (day.endsWith("3"))
                                    simpleDateFormat = new SimpleDateFormat("d'rd' MMMM, yyyy");
                                else
                                    simpleDateFormat = new SimpleDateFormat("d'th' MMMM, yyyy");

                                try {
                                    formattedDate = simpleDateFormat.format(sdf.parse(selectedeDate));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                edtxtLayoutDateOfBirth.setText(selectedeDate);
                                edtxtLayoutDateOfBirth.getImgRight().setImageResource(R.drawable.date_of_birth_selected);
                            }
                        },
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());

                datePicker.show();

            }
        });


        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emiratesID = edtxtLayoutEmitatesID.getText().toString().trim();
                emiratesID = emiratesID.replaceAll("-", "");
                if (TextUtils.isEmpty(selectedeDate)) {
                    edtxtLayoutDateOfBirth.getErrorLabelLayout().setError("Please select your Date of Birth.");
                    edtxtLayoutDateOfBirth.setBackgroundResource(R.drawable.custom_edittext_border_red);
                    edtxtLayoutDateOfBirth.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(ForgotSecurityAnswerActivity.this, R.color.grapefruit));

                } else if (emiratesID.length() < 15) {
                    edtxtLayoutEmitatesID.getErrorLabelLayout().setError("Please enter valid Emirates ID.");
                    edtxtLayoutEmitatesID.setBackgroundResource(R.drawable.custom_edittext_border_red);
                    edtxtLayoutEmitatesID.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(ForgotSecurityAnswerActivity.this, R.color.grapefruit));

                } else if (genericValidations.isValidEmail(edtxtLayoutEmailID.getText().trim(), edtxtLayoutEmailID)) {


                    // Make ResetQuestionsRequest object and set  values
                    objResetQuestionsRequest = new ResetQuestionsRequest();
                    objResetQuestionsRequest.setDateOfBirth(selectedeDate);
                    objResetQuestionsRequest.setEmiratesID(emiratesID);
                    objResetQuestionsRequest.setEmailId(edtxtLayoutEmailID.getText().trim());


                    Intent intent = new Intent(ForgotSecurityAnswerActivity.this, SecurityQuestionsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(IntentConstants.RESET_QUESTIONS_REQUEST, objResetQuestionsRequest);
                    intent.putExtra(IntentConstants.FROM_ACTIVITY, ForgotSecurityAnswerActivity.class.getCanonicalName());
                    intent.putExtras(bundle);
                    startActivity(intent);

                }

            }
        });


    }
}
