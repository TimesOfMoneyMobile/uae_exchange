package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.uaeexchange.R;
import toml.uaeexchange.utilities.GenericValidations;
import toml.movitwallet.controllers.AuthoriseRequestMoneyController;
import toml.movitwallet.models.MoneyRequest;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;

public class AuthoriseDeclineRequestActivity extends BaseActivity {

    @BindView(R.id.imgvwLeft)
    ImageView toolbarDashboardIvHamIcon;
    @BindView(R.id.relBack)
    RelativeLayout relBack;
    @BindView(R.id.title_pay)
    TextView titlePay;
    @BindView(R.id.toolbar_pay)
    Toolbar toolbarPay;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.etAmount)
    EditText etAmount;
    @BindView(R.id.etTpin)
    EditText etTpin;
    @BindView(R.id.ivFavourite)
    ImageView ivFavourite;
    @BindView(R.id.llFavourite)
    LinearLayout llFavourite;
    @BindView(R.id.tvForgotTpin)
    TextView tvForgotTpin;
    @BindView(R.id.btnSendMoney)
    Button btnSendMoney;
    private String status;
    private String  strName,strMobile,strAmount,strTpin;
    private MoneyRequest moneyRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorise_decline_request);
        ButterKnife.bind(this);

        Intent intent = this.getIntent();

        if (intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            moneyRequest = (MoneyRequest) bundle.getSerializable(IntentConstants.MONEYREQUEST);
            status = bundle.getString(IntentConstants.STATUS);
            strName = moneyRequest.getFirstName() + " " + moneyRequest.getLastName();
            strMobile = moneyRequest.getMobileNumber();
            strAmount = moneyRequest.getAmount();
            etName.setText(strName);
            etMobile.setText(strMobile);
            etAmount.setText(strAmount);

            if (status.equalsIgnoreCase("Reject"))
                btnSendMoney.setText("Reject Request");
        }

    }

    @OnClick(R.id.btnSendMoney)
    public void onViewClicked() {

        strTpin = etTpin.getText().toString();

        GenericValidations genericValidations = new GenericValidations(AuthoriseDeclineRequestActivity.this);
        if(!genericValidations.isValidTPIN(strTpin)){
            showError(genericValidations.getErrorMessage());
        }else {
            callAuthoriseReqMoney();
        }

    }

    private void callAuthoriseReqMoney() {

        showProgress();

        AuthoriseRequestMoneyController authoriseRequestMoneyController = new AuthoriseRequestMoneyController(moneyRequest, strTpin,strAmount,status);
        authoriseRequestMoneyController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                dismissProgress();
                handleSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                showError(reason);

            }
        });

    }

    private void handleSuccess(Object response) {

    }
}
