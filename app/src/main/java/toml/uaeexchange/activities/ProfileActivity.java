package toml.uaeexchange.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.UpdateProfileController;
import toml.movitwallet.controllers.ViewProfileController;
import toml.movitwallet.models.RegistrationRequest;
import toml.movitwallet.models.ViewProfileDetails;
import toml.movitwallet.models.ViewProfileResponse;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;


public class ProfileActivity extends BaseActivity implements IMovitWalletController<ViewProfileResponse> {

    /*    @BindView(R.id.imgvwLeft)
        ImageView toolbarDashboardIvHamIcon;
       @BindView(R.id.relBack)
        RelativeLayout relBack;
        @BindView(R.id.title_pay)
        TextView titlePay;
        @BindView(R.id.toolbar_pay)
        Toolbar toolbarPay;*/
    @BindView(R.id.ivProfilecomplete)
    ImageView ivProfilecomplete;
    @BindView(R.id.tvKYCInstructionLabel)
    TextView tvKYCInstructionLabel;
    @BindView(R.id.btnCompleteProfile)
    Button btnCompleteProfile;
    @BindView(R.id.etFirstName)
    CustomEditTextLayout etFirstName;
    @BindView(R.id.etLastName)
    CustomEditTextLayout etLastName;
    @BindView((R.id.ellFirstName))
    ErrorLabelLayout ellFirstName;
    @BindView((R.id.ellLastName))
    ErrorLabelLayout ellLastName;
    @BindView(R.id.etEmailID)
    CustomEditTextLayout etEmailID;
    @BindView(R.id.ellEmailID)
    ErrorLabelLayout ellEmailID;
    @BindView(R.id.etMobileno)
    CustomEditTextLayout etMobileno;
    @BindView(R.id.ellMobileno)
    ErrorLabelLayout ellMobileno;
    @BindView(R.id.etEmiratID)
    CustomEditTextLayout etEmiratID;
    @BindView(R.id.ellEmiratsID)
    ErrorLabelLayout ellEmiratsID;
    @BindView(R.id.etNationality)
    CustomEditTextLayout etNationality;
    @BindView(R.id.ellNationality)
    ErrorLabelLayout ellNationality;
    @BindView(R.id.etDOB)
    CustomEditTextLayout etDOB;
    @BindView(R.id.ellDOB)
    ErrorLabelLayout ellDOB;
    @BindView(R.id.etAlias)
    CustomEditTextLayout etAlias;
    @BindView(R.id.ellAlias)
    ErrorLabelLayout ellAlias;
    @BindView(R.id.btnSaveChanges)
    CustomButton btnSaveChanges;
    @BindView(R.id.llWithoutKyc)
    LinearLayout llWithoutKyc;
    @BindView(R.id.tvCompletedKYC)
    TextView tvCompletedKYC;
    @BindView(R.id.llCompleKYC)
    LinearLayout llCompleKYC;
    @BindView(R.id.ivCompleteHundred)
    ImageView ivCompleteHundred;
    @BindView(R.id.imgvwRight)
    ImageView imgvwRight;

    private boolean shouldExecuteOnResume;
    private String strMobile, strFirstName, strLastName, strAliseName, strEmail, strEmiratsID, strDOB, strNationality;
    RegistrationRequest objRegistrationRequest;
    private GenericValidations genericValidations;
    private ViewProfileDetails viewProfileDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
       /* setToolbarWithBalance(getString(R.string.myProfile), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);*/
        // setToolbar(getString(R.string.myProfile));

        setToolbarWithBalance(getString(R.string.myProfile), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, R.drawable.notification, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callNotification();
            }
        }, false);
        handleUI();


    }


    private void handleUI() {

        etFirstName.getEditText().addTextChangedListener(textWatcher);
        etLastName.getEditText().addTextChangedListener(textWatcher);
        etAlias.getEditText().addTextChangedListener(textWatcher);
        etEmailID.getEditText().addTextChangedListener(textWatcher);

        etFirstName.getVerticalDivider().setVisibility(View.GONE);
        etFirstName.getImgLeft().setVisibility(View.GONE);
        etLastName.getVerticalDivider().setVisibility(View.GONE);
        etLastName.getImgLeft().setVisibility(View.GONE);
        etEmailID.getVerticalDivider().setVisibility(View.GONE);
        etEmailID.getImgLeft().setVisibility(View.GONE);
        etEmiratID.getVerticalDivider().setVisibility(View.GONE);
        etEmailID.getImgLeft().setVisibility(View.GONE);
        etMobileno.getVerticalDivider().setVisibility(View.GONE);
        etMobileno.getImgLeft().setVisibility(View.GONE);
        etEmiratID.getVerticalDivider().setVisibility(View.GONE);
        etEmiratID.getImgLeft().setVisibility(View.GONE);
        etDOB.getVerticalDivider().setVisibility(View.GONE);
        etDOB.getImgLeft().setVisibility(View.GONE);
        etNationality.getVerticalDivider().setVisibility(View.GONE);
        etNationality.getImgLeft().setVisibility(View.GONE);
        etAlias.getVerticalDivider().setVisibility(View.GONE);
        etAlias.getImgLeft().setVisibility(View.GONE);

        Constants.setEditTextMaxLength(etAlias.getEditText(), 50, true, false);
        llWithoutKyc.setVisibility(View.GONE);
        llCompleKYC.setVisibility(View.GONE);


        if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("N")) {
            handleNonKYCElements();
        } else if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("Y")) {
            handleKYCElements();
        } else if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("P")) {
            handlePendingKYCEelements();
        }

    }

    private void handleNonKYCElements() {
        commonDisabledEditTexts();
        ellEmiratsID.setVisibility(View.GONE);
        ellNationality.setVisibility(View.GONE);
        ellDOB.setVisibility(View.GONE);
        llWithoutKyc.setVisibility(View.VISIBLE);
        llCompleKYC.setVisibility(View.GONE);
        ivProfilecomplete.setImageResource(R.drawable.thirty_three);
    }

    private void handlePendingKYCEelements() {
        commonDisabledEditTexts();
        ellEmiratsID.setVisibility(View.VISIBLE);
        ellNationality.setVisibility(View.VISIBLE);
        etFirstName.getEditText().setEnabled(false);
        etFirstName.getEditText().setTextColor(getResources().getColor(R.color.battleship_grey));
        etLastName.getEditText().setEnabled(false);
        etLastName.getEditText().setTextColor(getResources().getColor(R.color.battleship_grey));
        llWithoutKyc.setVisibility(View.VISIBLE);
        llCompleKYC.setVisibility(View.GONE);
        ivProfilecomplete.setImageResource(R.drawable.sixty_six);
    }

    private void handleKYCElements() {

        commonDisabledEditTexts();
        ellEmiratsID.setVisibility(View.VISIBLE);
        ellNationality.setVisibility(View.VISIBLE);
        etFirstName.getEditText().setEnabled(false);
        etFirstName.getEditText().setTextColor(getResources().getColor(R.color.battleship_grey));
        etLastName.getEditText().setEnabled(false);
        etLastName.getEditText().setTextColor(getResources().getColor(R.color.battleship_grey));
        llWithoutKyc.setVisibility(View.GONE);
        llCompleKYC.setVisibility(View.VISIBLE);
        ivCompleteHundred.setImageResource(R.drawable.hundred_perc);
    }

    private void commonDisabledEditTexts() {
        etMobileno.getEditText().setEnabled(false);
        etMobileno.getEditText().setTextColor(getResources().getColor(R.color.battleship_grey));
        etNationality.getEditText().setEnabled(false);
        etNationality.getEditText().setTextColor(getResources().getColor(R.color.battleship_grey));
        etEmiratID.getEditText().setEnabled(false);
        etEmiratID.getEditText().setTextColor(getResources().getColor(R.color.battleship_grey));
        etDOB.getEditText().setEnabled(false);
        etDOB.getEditText().setTextColor(getResources().getColor(R.color.battleship_grey));
    }

    @Override
    protected void onResume() {
        super.onResume();
        callViewProfileAPI();

    }


    private void callViewProfileAPI() {
        showProgress();
        ViewProfileController viewProfileController = new ViewProfileController(MovitConsumerApp.getInstance().getMobileNumber());
        viewProfileController.init(ProfileActivity.this);
    }


    @Override
    public void onSuccess(ViewProfileResponse response) {
        dismissProgress();
        if (response != null) {
            viewProfileDetails = response.getViewProfileDetails();
            etFirstName.setText(viewProfileDetails.getFirstName());
            etLastName.setText(viewProfileDetails.getLastName());
            etMobileno.setText(getString(R.string.country_code) + " " + viewProfileDetails.getMobileNumber());
            etEmailID.setText(viewProfileDetails.getEmailId());

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

            if (!TextUtils.isEmpty(viewProfileDetails.getDateOfBirth())) {
                Date dt = null;
                try {
                    dt = format.parse(viewProfileDetails.getDateOfBirth());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat your_format = new SimpleDateFormat("dd-MM-yyyy");
                etDOB.setText(your_format.format(dt));
            }


            etAlias.setText(viewProfileDetails.getAlias());
            etNationality.setText(viewProfileDetails.getNationality());

            AppSettings.putData(this, AppSettings.FIRST_NAME, viewProfileDetails.getFirstName());
            AppSettings.putData(this, AppSettings.LAST_NAME, viewProfileDetails.getLastName());
            AppSettings.putData(this, AppSettings.EMAIL_ID, viewProfileDetails.getEmailId());
            AppSettings.putData(this, AppSettings.ALIAS, viewProfileDetails.getAlias());

            if (viewProfileDetails.getKycDocuments() != null) {
                if (viewProfileDetails.getKycDocuments().getkycDocumentList().size() > 0) {
                    etEmiratID.getEditText().addTextChangedListener(new PatternedTextWatcher("###-####-#######-#"));
                    etEmiratID.setText(viewProfileDetails.getKycDocuments().getkycDocumentList().get(0).getValue()
                    );
                }
            }
            //etEmiratID.setText(viewProfileDetails.getEmiratesId());
/*            viewProfileDetails.getCity();
            viewProfileDetails.getGender();
            viewProfileDetails.getAddress1();
            viewProfileDetails.getAddress2();*/

        }

    }

    @Override
    public void onFailed(String errorCode, String reason) {

    }

    @OnClick({R.id.btnCompleteProfile, R.id.btnSaveChanges})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCompleteProfile:
                callKYCFlow();
                break;
            case R.id.btnSaveChanges:
                onSaveClick();
                break;

        }
    }

    private void callNotification() {
        BaseActivity.openNewActivity(this, NotificationActivity.class.getCanonicalName(), true);
    }

    private void getDataFromEditText() {
        //strMobile = etMobileno.getText().toString();
        strMobile = viewProfileDetails.getMobileNumber();
        strEmail = etEmailID.getText().toString();
        strAliseName = etAlias.getText().toString();
        strFirstName = etFirstName.getText().toString();
        strLastName = etLastName.getText().toString();
        strDOB = etDOB.getText().toString();
        strNationality = etNationality.getText().toString();
        strEmiratsID = etEmiratID.getText().toString();
    }

    private void callKYCFlow() {

        getDataFromEditText();

        objRegistrationRequest = new RegistrationRequest();
        objRegistrationRequest.setMobileNumber(strMobile);
        objRegistrationRequest.setEmailId(strEmail);
        objRegistrationRequest.setFirstName(strFirstName);
        objRegistrationRequest.setLastName(strLastName);
        objRegistrationRequest.setDateOfBirth(strDOB);

        Intent intent = new Intent(this, KYCRegistrationActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(IntentConstants.REG_REQUEST, objRegistrationRequest);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    private void
    onSaveClick() {

        genericValidations = new GenericValidations(this);
        getDataFromEditText();
        etAlias.setErrorLabelLayout(ellAlias);
        etFirstName.setErrorLabelLayout(ellFirstName);
        etLastName.setErrorLabelLayout(ellLastName);
        etEmailID.setErrorLabelLayout(ellEmailID);
        if (genericValidations.isValidProfileData(strFirstName, etFirstName, strLastName, etLastName, "", null, strEmail, etEmailID)) {
            callUpdateProfile();
        }

    }

    private void callUpdateProfile() {
        // showError(strAliseName);
        UpdateProfileController updateProfileController = new UpdateProfileController();
        updateProfileController.setFirstName(strFirstName);
        updateProfileController.setLastName(strLastName);
        updateProfileController.setAliasName(strAliseName);
        updateProfileController.setEmailId(strEmail);
        updateProfileController.setMobileNo(strMobile);
        updateProfileController.setEmiratsId(strEmiratsID);
        updateProfileController.setDob(strDOB);
        updateProfileController.setNationality(strNationality);

        updateProfileController.init(new IMovitWalletController<Bundle>() {
            @Override
            public void onSuccess(Bundle response) {
                AppSettings.putData(ProfileActivity.this, AppSettings.ALIAS, strAliseName);
                AppSettings.putData(ProfileActivity.this, AppSettings.EMAIL_ID, strEmail);
                showCustomDialog();
            }

            @Override
            public void onFailed(String errorCode, String reason) {

            }
        });

    }

    private void showCustomDialog() {


        final Dialog openDialog = new Dialog(this);
        openDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        openDialog.setContentView(R.layout.dialog_profile_update_success);

        TextView tvContinue = (TextView) openDialog.findViewById(R.id.tvContinue);

        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //callViewProfileAPI();
                openDialog.dismiss();
                BaseActivity.openDashboardActivity(ProfileActivity.this);
            }
        });
        openDialog.show();
    }


    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if (Constants.checkForFocusActive(new CustomEditTextLayout[]{etFirstName, etLastName, etAlias, etEmailID})) {
                btnSaveChanges.setEnabled(true);
            } else {
                btnSaveChanges.setEnabled(false);
            }

        }
    };


}
