package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.OffersController;
import toml.movitwallet.controllers.ValidateOfferController;
import toml.movitwallet.models.Offer;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.uaeexchange.R;
import toml.uaeexchange.adapters.OffersListAdapter;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;

public class AddPromoActivity extends BaseActivity {

    @BindView(R.id.etPromoCode)
    CustomEditTextLayout etPromoCode;
    @BindView(R.id.ellPromoCode)
    ErrorLabelLayout ellPromoCode;
    @BindView(R.id.btnApply)
    CustomButton btnApply;
    @BindView(R.id.lvOffer)
    ListView lvOffer;
    private String strPromoCode;
    private String strAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_promo);
        ButterKnife.bind(this);

        setToolbarWithBalance(getString(R.string.add_promocode), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);

        strAmount = getIntent().getStringExtra(IntentConstants.AMOUNT);
        etPromoCode.getVerticalDivider().setVisibility(View.GONE);
        etPromoCode.getImgLeft().setVisibility(View.GONE);
        etPromoCode.getEditText().addTextChangedListener(textWatcher);
        etPromoCode.setErrorLabelLayout(ellPromoCode);
        callOffersList();
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            if (!TextUtils.isEmpty(charSequence)) {


            } else {

            }
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (Constants.checkForFocusActive(new CustomEditTextLayout[]{etPromoCode})) {
                btnApply.setEnabled(true);
            } else {
                btnApply.setEnabled(false);
            }

        }
    };

    @OnClick(R.id.btnApply)
    public void onViewClicked() {

        strPromoCode = etPromoCode.getText().toString();
        callOfferValidAPI();

    }

    private void callOfferValidAPI() {

        showProgress();
        ValidateOfferController validateOfferController = new ValidateOfferController(strPromoCode, strAmount);
        validateOfferController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                dismissProgress();
                handleOfferSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                etPromoCode.getErrorLabelLayout().setError(reason);
                etPromoCode.setBackgroundResource(R.drawable.custom_edittext_border_red);

            }
        });
    }

    private void handleOfferSuccess(Object response) {

        Bundle bundle = (Bundle) response;

        bundle.getString(IntentConstants.ResponseType);
        bundle.getString(IntentConstants.Message);
        bundle.getString(IntentConstants.Reason);

        if (bundle.get(IntentConstants.ResponseType) != null && bundle.getString(IntentConstants.ResponseType).equalsIgnoreCase("Success")) {

            Intent intent = new Intent();
            intent.putExtra(IntentConstants.PROMOCODE, strPromoCode);
            setResult(RESULT_OK, intent);
            finish();


        } else {
            //customDialog.edtxtPromoCode.setText("");
            //  customDialog.edtxtPromoCode.setError(bundle.getString(IntentConstants.Reason));
            etPromoCode.getErrorLabelLayout().setError(bundle.getString(IntentConstants.Reason));
            etPromoCode.setBackgroundResource(R.drawable.custom_edittext_border_red);
        }

    }

    private void callOffersList() {
        OffersController offersController = new OffersController();

        showProgress();

        offersController.init(new IMovitWalletController<List<Offer>>() {
            @Override
            public void onSuccess(List<Offer> response) {
                dismissProgress();

                if (response.size() > 0) {
                    lvOffer.setAdapter(new OffersListAdapter(AddPromoActivity.this, response,false, new OffersListAdapter.IPromoCodeClickListener() {
                        @Override
                        public void onPromoCodeClick(String promoCode,int position,View view) {

                            etPromoCode.setText(promoCode);
                        }
                    }));
                } else {
                    // txtvwNoOffers.setVisibility(View.VISIBLE);
                }

            }


            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                //txtvwNoOffers.setVisibility(View.VISIBLE);
            }
        });
    }

}
