package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.controllers.GenerateOTPController;
import toml.movitwallet.models.RegistrationRequest;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.BuildConfig;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;

public class RegisterActivity extends BaseActivity implements BaseActivity.ClickableSpanListener {


    GenericValidations genericValidations;
    RegistrationRequest objRegistrationRequest;
    @BindView(R.id.edtxtLayoutFirstName)
    CustomEditTextLayout edtxtLayoutFirstName;
    @BindView(R.id.errorLabelFirstName)
    ErrorLabelLayout errorLabelFirstName;
    @BindView(R.id.edtxtLayoutLastName)
    CustomEditTextLayout edtxtLayoutLastName;
    @BindView(R.id.errorLabelLastName)
    ErrorLabelLayout errorLabelLastName;
    @BindView(R.id.edtxtLayoutEmailID)
    CustomEditTextLayout edtxtLayoutEmailID;
    @BindView(R.id.errorLabelEmailID)
    ErrorLabelLayout errorLabelEmailID;
    @BindView(R.id.edtxtLayoutMobileNumber)
    CustomEditTextLayout edtxtLayoutMobileNumber;
    @BindView(R.id.errorLabelMobileNumber)
    ErrorLabelLayout errorLabelMobileNumber;
    @BindView(R.id.btnSubmit)
    CustomButton btnSubmit;
    @BindView(R.id.txtvwAgreement)
    TextView txtvwAgreement;

    Spannable spannable;
    @BindView(R.id.root_layout)
    LinearLayout rootLayout;
    @BindView(R.id.scrollView)
    ScrollView scrollView;

    private String TXN_TYPE = "CUSTREG";
    private String strMobile, strEmail, strFirstName, strLastName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        if (Constants.isDeviceRooted() && !BuildConfig.DEBUG) {
            new OkDialog(this, getString(R.string.rooted_device_error),  new OkDialog.IOkDialogCallback() {
                @Override
                public void handleResponse() {
                    finish();
                }
            });

        }

        setToolbar("", R.drawable.icon_back, 0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }, null);

        setClickableSpanListener(this);

        genericValidations = new GenericValidations(RegisterActivity.this);

        // Setting text watcher here
        edtxtLayoutFirstName.getEditText().addTextChangedListener(textWatcher);
        edtxtLayoutLastName.getEditText().addTextChangedListener(textWatcher);
        edtxtLayoutEmailID.getEditText().addTextChangedListener(textWatcher);
        edtxtLayoutMobileNumber.getEditText().addTextChangedListener(mobileTextWatcher);

        Constants.setEditTextMaxLength(edtxtLayoutFirstName.getEditText(), 50, true, false);
        Constants.setEditTextMaxLength(edtxtLayoutLastName.getEditText(), 50, true, false);
        Constants.setEditTextMaxLength(edtxtLayoutEmailID.getEditText(), 50, false, false);

        // Setting error labels here
        edtxtLayoutFirstName.setErrorLabelLayout(errorLabelFirstName);
        edtxtLayoutLastName.setErrorLabelLayout(errorLabelLastName);
        edtxtLayoutEmailID.setErrorLabelLayout(errorLabelEmailID);
        edtxtLayoutMobileNumber.setErrorLabelLayout(errorLabelMobileNumber);

        edtxtLayoutMobileNumber.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);


        String fullText = getString(R.string.userAgreementInfoText);

        spannable = new SpannableString(fullText);

        setColorAndClick(txtvwAgreement, fullText, spannable, "User Agreement", ContextCompat.getColor(RegisterActivity.this, R.color.clear_blue));

        setColorAndClick(txtvwAgreement, fullText, spannable, "Privacy Policy", ContextCompat.getColor(RegisterActivity.this, R.color.clear_blue));

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                strFirstName = edtxtLayoutFirstName.getText().toString().trim();
                strLastName = edtxtLayoutLastName.getText().toString().trim();
                strEmail = edtxtLayoutEmailID.getText().toString().trim();
                strMobile = edtxtLayoutMobileNumber.getText().toString().trim();


                if (!genericValidations.validateSelfRegister(strFirstName, edtxtLayoutFirstName, strLastName, edtxtLayoutLastName, strEmail, edtxtLayoutEmailID, strMobile, edtxtLayoutMobileNumber)) {
                    // showError(genericValidations.getErrorMessage());
                } else {


                    callGenerateOTP();


                }


            }
        });

        rootLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Constants.hideKeyboard(RegisterActivity.this);
                return false;
            }
        });

        txtvwAgreement.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Constants.hideKeyboard(RegisterActivity.this);
                return false;
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Constants.hideKeyboard(RegisterActivity.this);
                return false;
            }
        });


    }


    private TextWatcher mobileTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {


            String s = editable.toString();

            if (s.toString().trim().length() == getResources().getInteger(R.integer.mobileNoLength)) {
                toml.movitwallet.utils.Constants.hideKeyboard(RegisterActivity.this);
            }

            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{edtxtLayoutFirstName, edtxtLayoutLastName, edtxtLayoutEmailID, edtxtLayoutMobileNumber})) {
                btnSubmit.setEnabled(true);
            } else {
                btnSubmit.setEnabled(false);
            }


        }
    };


    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{edtxtLayoutFirstName, edtxtLayoutLastName, edtxtLayoutEmailID, edtxtLayoutMobileNumber})) {
                btnSubmit.setEnabled(true);
            } else {
                btnSubmit.setEnabled(false);
            }


        }
    };


    @Override
    public void onSpanClick(String strText) {
        if (strText.contains("User")) {

        } else if (strText.contains("Policy")) {

        }
    }

    private void callGenerateOTP() {
        showProgress();

        MovitConsumerApp.getInstance().setMobileNumber(strMobile);

        MovitWalletController generateOTPController = new GenerateOTPController(TXN_TYPE);

        generateOTPController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                dismissProgress();

                objRegistrationRequest = new RegistrationRequest();
                objRegistrationRequest.setMobileNumber(strMobile);
                objRegistrationRequest.setEmailId(strEmail);
                objRegistrationRequest.setFirstName(strFirstName);
                objRegistrationRequest.setLastName(strLastName);
                // objRegistrationRequest.setDateOfBirth(strDOB);
                // objRegistrationRequest.setReferralCode(strReferralCode);

                Bundle bundle = new Bundle();
                bundle.putParcelable(IntentConstants.REG_REQUEST, objRegistrationRequest);

                MovitConsumerApp.getInstance().setMobileNumber(strMobile);

                Intent intent = new Intent(RegisterActivity.this, VerifyMobile.class);
                intent.putExtras(bundle);
                startActivity(intent);


            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                edtxtLayoutMobileNumber.getErrorLabelLayout().setError(reason);
                edtxtLayoutMobileNumber.setBackgroundResource(R.drawable.custom_edittext_border_red);
                edtxtLayoutMobileNumber.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(RegisterActivity.this, R.color.grapefruit));
                //showError(reason);

            }
        });
    }


}
