package toml.uaeexchange.activities;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.controllers.GetMappedDevicesListController;
import toml.movitwallet.controllers.UnmapDeviceController;
import toml.movitwallet.models.MappedDevice;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.uaeexchange.dialogs.OkDialog;


public class ManageDevicesActivity extends BaseActivity {

    @BindView(R.id.txtvwNameCurrentDevice)
    TextView txtvwNameCurrentDevice;
    @BindView(R.id.linlayOtherDeviceContainer1)
    LinearLayout linlayOtherDeviceContainer1;
    @BindView(R.id.linlayOtherDeviceContainer2)
    LinearLayout linlayOtherDeviceContainer2;
    @BindView(R.id.txtvwNameDevice1)
    TextView txtvwNameDevice1;
    @BindView(R.id.txtvwLocationDevice1)
    TextView txtvwLocationDevice1;
    @BindView(R.id.rightViewDevice1)
    FrameLayout rightViewDevice1;
    @BindView(R.id.txtvwNameDevice2)
    TextView txtvwNameDevice2;
    @BindView(R.id.txtvwLocationDevice2)
    TextView txtvwLocationDevice2;
    @BindView(R.id.rightViewDevice2)
    FrameLayout rightViewDevice2;
    @BindView(R.id.linlayCurrentDeviceContainer)
    LinearLayout linlayCurrentDeviceContainer;


    MappedDevice currentMappedDevice;
    List<MappedDevice> otherDevicesList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_devices);
        ButterKnife.bind(this);

        setToolbarWithBalance(getString(R.string.manage_devices), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);


        showProgress();

        GetMappedDevicesListController getMappedDevicesListController = new GetMappedDevicesListController();
        getMappedDevicesListController.init(new IMovitWalletController<List<MappedDevice>>() {
            @Override
            public void onSuccess(List<MappedDevice> list) {

                dismissProgress();


                otherDevicesList = new ArrayList<>();

                for (MappedDevice mappedDevice : list) {
                    if (mappedDevice.getId().equalsIgnoreCase(MovitConsumerApp.getInstance().getIMEINo())) {
                        linlayCurrentDeviceContainer.setVisibility(View.VISIBLE);
                        if (!TextUtils.isEmpty(mappedDevice.getName().trim())) {
                            txtvwNameCurrentDevice.setText(mappedDevice.getName());
                        } else {
                            txtvwNameCurrentDevice.setText(Build.MANUFACTURER + " phone");
                        }
                        currentMappedDevice = mappedDevice;
                    } else {
                        otherDevicesList.add(mappedDevice);
                    }
                }

                if (otherDevicesList != null && otherDevicesList.size() > 0) {
                    int firstIndex = 0;
                    if (firstIndex >= 0 && firstIndex < otherDevicesList.size()) {
                        MappedDevice mappedDevice = otherDevicesList.get(firstIndex);
                        linlayOtherDeviceContainer1.setVisibility(View.VISIBLE);
                        txtvwNameDevice1.setText(mappedDevice.getName());
                    }

                    int secondIndex = 1;

                    if (secondIndex >= 0 && secondIndex < otherDevicesList.size()) {
                        MappedDevice mappedDevice = otherDevicesList.get(secondIndex);
                        linlayOtherDeviceContainer2.setVisibility(View.VISIBLE);
                        txtvwNameDevice2.setText(mappedDevice.getName());
                    }
                }


                // showError("" + list.size());

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();

                showError(reason);

            }
        });


        rightViewDevice1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                unmapDevice(0);

            }
        });

        rightViewDevice2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                unmapDevice(1);

            }
        });


    }

    private void unmapDevice(final int index) {
        showProgress();

        UnmapDeviceController unmapDeviceController = new UnmapDeviceController(otherDevicesList.get(index).getId());
        unmapDeviceController.init(new IMovitWalletController<Bundle>() {
            @Override
            public void onSuccess(Bundle response) {

                dismissProgress();

                new OkDialog(ManageDevicesActivity.this, response.getString(IntentConstants.Message), new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                        otherDevicesList.remove(index);
                        if (index == 0) {
                            linlayOtherDeviceContainer1.setVisibility(View.GONE);
                        } else if (index == 1) {
                            linlayOtherDeviceContainer2.setVisibility(View.GONE);
                        }

                    }
                });

            }

            @Override
            public void onFailed(String errorCode, String reason) {

                dismissProgress();
                new OkDialog(ManageDevicesActivity.this, reason, new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                    }
                });

            }
        });

    }
}
