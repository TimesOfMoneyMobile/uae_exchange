package toml.uaeexchange.activities;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.GenerateOTPController;
import toml.movitwallet.controllers.ValidateOTPController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.NumericInputField;
import toml.uaeexchange.utilities.Constants;

public class ForgotMpinOTPActivity extends BaseActivity implements BaseActivity.ClickableSpanListener {

    @BindView(R.id.numericFieldOTP)
    NumericInputField numericFieldOTP;
    @BindView(R.id.linlayAutoDetectingLoaderContainer)
    LinearLayout linlayAutoDetectingLoaderContainer;
    @BindView(R.id.txtvwDidntRecieveOTP)
    TextView txtvwDidntRecieveOTP;
    @BindView(R.id.btnVerify)
    CustomButton btnVerify;
    @BindView(R.id.txtvwMobileNumber)
    TextView txtvwMobileNumber;
    @BindView(R.id.txtvwReceiveSMSText)
    TextView txtvwReceiveSMSText;
    @BindView(R.id.txtvwTimerValue)
    TextView txtvwTimerValue;
    @BindView(R.id.linlayTimerContainer)
    LinearLayout linlayTimerContainer;
    @BindView(R.id.imgvwEditMobileNumber)
    ImageView imgvwEditMobileNumber;

    private String TXN_TYPE = "FORGOT";

    private String mobileNo;
    Spannable spannable;
    private boolean isSpanClicked;

    private SmsVerifyCatcher smsVerifyCatcher;
    MyCountDownTimer objMyCountDownTimer;
    private String otpValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_mpin_otp);
        ButterKnife.bind(this);
        setClickableSpanListener(this);

        setToolbarWithBalance(getString(R.string.forgot_mpin), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);


        mobileNo = getIntent().getStringExtra(IntentConstants.MOBILE_NUMBER);
        txtvwMobileNumber.setText(getString(R.string.country_code) + " " + mobileNo);

        // String fullText = getString(R.string.resendOtp);
        String fullText = "Didn’t receive OTP? Resend OTP";
        spannable = new SpannableString(fullText);

        // To remove highlight text color
        txtvwDidntRecieveOTP.setHighlightColor(Color.TRANSPARENT);

        setColorAndClick(txtvwDidntRecieveOTP, fullText, spannable, "Resend OTP", ContextCompat.getColor(this, R.color.clear_blue));

        // callGenerateOTP();

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = Constants.parseSMSForOTP(message);//Parse verification code
                try {
                    numericFieldOTP.setText(code);//set code in edit text
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //then you can send verification code to server
            }
        });


        linlayAutoDetectingLoaderContainer.setVisibility(View.VISIBLE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                linlayAutoDetectingLoaderContainer.setVisibility(View.INVISIBLE);

                btnVerify.setEnabled(true);
            }
        }, Constants.AUTO_DETECTING_LAYOUT_DISPALY_TIME_IN_MILIS);

    }


    private void callGenerateOTP() {
        showProgress();

        numericFieldOTP.clearText();

        MovitWalletController generateOTPController = new GenerateOTPController(TXN_TYPE);

        generateOTPController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                dismissProgress();

                linlayAutoDetectingLoaderContainer.setVisibility(View.VISIBLE);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String code = "";
                        try {
                            numericFieldOTP.setText(code);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        linlayAutoDetectingLoaderContainer.setVisibility(View.INVISIBLE);

                        btnVerify.setEnabled(true);
                    }
                }, Constants.AUTO_DETECTING_LAYOUT_DISPALY_TIME_IN_MILIS);

                if (isSpanClicked) {
                    objMyCountDownTimer = new MyCountDownTimer(Constants.RESEND_OTP_TIME_IN_MILIS, 1000);
                    objMyCountDownTimer.start();
                    isSpanClicked = false;

                }

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                finish();
                showError(reason);

            }
        });
    }

    @OnClick(R.id.btnVerify)
    public void onViewClicked() {
        isSpanClicked = false;
        verifyOtp();
    }

    @Override
    public void onSpanClick(String strText) {
        isSpanClicked = true;
        // linlayTimerContainer.setVisibility(View.VISIBLE);

        callGenerateOTP();
    }


    private class MyCountDownTimer extends CountDownTimer {
        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            linlayTimerContainer.setVisibility(View.VISIBLE);
            txtvwReceiveSMSText.setVisibility(View.VISIBLE);
            txtvwTimerValue.setVisibility(View.VISIBLE);

            txtvwReceiveSMSText.setText(getString(R.string.waitforOTP) + " " + getString(R.string.country_code) + " " + MovitConsumerApp.getInstance().getMobileNumber() + " in ");
            txtvwTimerValue.setText("" + (Constants.RESEND_OTP_TIME_IN_MILIS / 1000) + " Secs");

            txtvwDidntRecieveOTP.setEnabled(false);
            txtvwDidntRecieveOTP.setAlpha(0.5f);

        }

        @Override
        public void onTick(long millisUntilFinished) {

            txtvwTimerValue.setText((millisUntilFinished / 1000) > 1 ? (millisUntilFinished / 1000) + " Secs" : (millisUntilFinished / 1000) + " Sec");

        }

        @Override
        public void onFinish() {
            txtvwDidntRecieveOTP.setEnabled(true);
            txtvwDidntRecieveOTP.setAlpha(1.0f);
            linlayTimerContainer.setVisibility(View.GONE);
            //txtvwTimerValue.setVisibility(View.GONE);
            // txtvwReceiveSMSText.setVisibility(View.GONE);
            objMyCountDownTimer.cancel();
        }
    }




    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    public void verifyOtp() {
        otpValue = numericFieldOTP.getText().toString();
        if (otpValue.length() == getResources().getInteger(R.integer.OTPLength)) {

            showProgress();

            MovitWalletController validateOTPController = new ValidateOTPController(TXN_TYPE, otpValue);

            validateOTPController.init(new IMovitWalletController() {
                @Override
                public void onSuccess(Object response) {

                    dismissProgress();

                    Intent intent = new Intent(ForgotMpinOTPActivity.this, ForgotMpinSecurityQuestionActivity.class);
                    intent.putExtra(IntentConstants.OTP, otpValue);
                    intent.putExtra(IntentConstants.MOBILE_NUMBER, mobileNo);
                    startActivity(intent);
                    finish();

                }

                @Override
                public void onFailed(String errorCode, String reason) {
                    dismissProgress();
                    showError(reason);
                }
            });

        } else {
         //   showError("Please enter " + getResources().getInteger(R.integer.OTPLength) + " digit OTP.");
            showError(getString(R.string.invalidOtpError));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


}
