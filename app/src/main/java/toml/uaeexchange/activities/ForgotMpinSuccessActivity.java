package toml.uaeexchange.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;

public class ForgotMpinSuccessActivity extends BaseActivity {

    @BindView(R.id.btnGoToLogin)
    CustomButton btnGoToLogin;
    @BindView(R.id.tvCustomerQuery)
    TextView tvCustomerQuery;

    @BindView(R.id.act_fms_ll_call)
    LinearLayout llCall;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_mpin_success);
        ButterKnife.bind(this);

        setToolbarWithBalance(getString(R.string.forgot_mpin), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        },0,null, false);
    }

    @OnClick({R.id.btnGoToLogin,R.id.act_fms_ll_call})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnGoToLogin:
                callLogin();
                break;

            case R.id.act_fms_ll_call:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+getString(R.string.dummyMobile)));
                startActivity(intent);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        callLogin();
    }

    private void callLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
