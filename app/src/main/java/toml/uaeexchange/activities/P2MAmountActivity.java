package toml.uaeexchange.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.GetP2MTransactionFee;
import toml.movitwallet.controllers.ValidateOfferController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;
import toml.uaeexchange.utilities.TwoDecimalInputFilter;

public class P2MAmountActivity extends BaseActivity {

    /*   @BindView(R.id.ivBack)
       ImageView ivBack;
       @BindView(R.id.toolBarP2M)
       Toolbar toolBarP2M;*/
    @BindView(R.id.etMerchantId)
    CustomEditTextLayout etMerchantId;
    @BindView(R.id.ellMerchantID)
    ErrorLabelLayout ellMerchantID;

    @BindView(R.id.etMerchantName)
    CustomEditTextLayout etMerchantName;
    @BindView(R.id.ellMerchantName)
    ErrorLabelLayout ellMerchantName;

    @BindView(R.id.etTpin)
    CustomEditTextLayout etTpin;
    @BindView(R.id.ellMpin)
    ErrorLabelLayout ellMpin;

    @BindView(R.id.ellAmount)
    ErrorLabelLayout ellAmount;

    @BindView(R.id.act_p2m_amount_etAmount)
    CustomEditTextLayout etAmount;

    @BindView(R.id.btnProceed)
    CustomButton btnProceed;
    @BindView(R.id.tvHavePromoCodeP2M)
    TextView tvHavePromoCodeP2M;
    @BindView(R.id.ivCouponCancel)
    ImageView ivCouponCancel;
    @BindView(R.id.tvPromocodeName)
    TextView tvPromocodeName;
    @BindView(R.id.rlPromocode)
    RelativeLayout rlPromocode;
    @BindView(R.id.rlAmountPromCode)
    RelativeLayout rlAmountPromCode;
    private CustomDialog customDialog;

    String merchantID, externalMerchantId, tPin, amount, merchantName, strPromoCode = "";

    GenericValidations genericValidations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p2_mamount);
        setToolbarWithBalance(getString(R.string.payamerchant), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, true);
        ButterKnife.bind(this);

        handleUI();


    }

    private void handleUI() {
        genericValidations = new GenericValidations(P2MAmountActivity.this);


        etMerchantName.getVerticalDivider().setVisibility(View.GONE);
        etAmount.getVerticalDivider().setVisibility(View.GONE);
        etMerchantName.getImgLeft().setVisibility(View.GONE);
        etAmount.getImgLeft().setVisibility(View.GONE);
        etAmount.setErrorLabelLayout(ellAmount);

        if (!TextUtils.isEmpty(MovitConsumerApp.getInstance().getMaxAmount())) {
            etAmount.getEditText().setFilters(new InputFilter[]{new TwoDecimalInputFilter(MovitConsumerApp.getInstance().getMaxAmount().length())
            });
        }


        LogUtils.Verbose("P2MAmountActivity", "" + getIntent().getStringExtra(IntentConstants.MERCHANTID));
        if (!TextUtils.isEmpty(getIntent().getStringExtra(IntentConstants.MERCHANTNAME))) {
            merchantName = getIntent().getStringExtra(IntentConstants.MERCHANTNAME);
            etMerchantName.setText(merchantName);
            etMerchantName.getEditText().setEnabled(false);
            etMerchantName.getEditText().setFocusable(false);
        }

        if (!TextUtils.isEmpty(getIntent().getStringExtra(IntentConstants.EXTERNAL_MERCHANTID))) {
            externalMerchantId = getIntent().getStringExtra(IntentConstants.EXTERNAL_MERCHANTID);
        }

        if (!TextUtils.isEmpty(getIntent().getStringExtra(IntentConstants.MERCHANTID))) {
            merchantID = getIntent().getStringExtra(IntentConstants.MERCHANTID);
        }

        if (TextUtils.isEmpty(getIntent().getStringExtra(IntentConstants.QR_CODE_AMOUNT))) {
            etMerchantName.getEditText().addTextChangedListener(textWatcher);
            etAmount.getEditText().addTextChangedListener(textWatcher);
            etAmount.getEtCountryCode().setVisibility(View.GONE);

        } else {
            amount = getIntent().getStringExtra(IntentConstants.QR_CODE_AMOUNT);
            etAmount.setText(amount);
            etAmount.getEtCountryCode().setText(getString(R.string.AED));
            etAmount.getEtCountryCode().setVisibility(View.VISIBLE);
            etAmount.getEditText().setEnabled(false);
            etAmount.getEditText().setFocusable(false);
            btnProceed.setEnabled(true);
        }

        getIntent().getStringExtra(IntentConstants.MOBILE_NUMBER);



       /* linlayAppliedPromocodeContainer.setVisibility(View.GONE);
        txtvwPromoCode.setPaintFlags(txtvwPromoCode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);*/
    }


/*

    @OnClick(R.id.ivBack)
    public void ivBack() {
        finish();
    }
*/


    public void onProceed() {
        // merchantID = etMerchantId.getText().toString();
        //  tPin = etTpin.getText().toString();
        amount = etAmount.getText();
        merchantName = etMerchantName.getText();

//        if (validate()) {
//            showProgress();
//            callTransactionFeeApi();
//            //finish();
//
//        }


        if (genericValidations.isValidAmount(amount, etAmount)) {
            if (Double.valueOf(amount) > Double.valueOf(MovitConsumerApp.getInstance().getWalletBalance())) {
                new OkDialog(P2MAmountActivity.this, "", getString(R.string.amount_should_be_less_than_balance), null);
            } else {
                showProgress();
                callTransactionFeeApi();
            }
        }


//        if ((Float.valueOf(amount) > Float.valueOf(MovitConsumerApp.getInstance().getMaxAmount()))) {
//            etAmount.getErrorLabelLayout().setError("Please enter amount less than " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount()));
//            // new OkDialog(P2MAmountActivity.this, "", "Please enter amount less than " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount()), null);
//        } else if ((Float.valueOf(amount) < Float.valueOf(MovitConsumerApp.getInstance().getMinAmount()))) {
//            etAmount.getErrorLabelLayout().setError("Please enter amount greater than " + MovitConsumerApp.getInstance().getMinAmount());
//            //new OkDialog(P2MAmountActivity.this, "", "Please enter amount greater than " + MovitConsumerApp.getInstance().getMinAmount(), null);
//        } else if (!(Float.valueOf(amount) < Float.valueOf(MovitConsumerApp.getInstance().getWalletBalance()))) {
//            new OkDialog(P2MAmountActivity.this, "", getString(R.string.amount_should_be_less_than_balance), null);
//        } else {
//            showProgress();
//            callTransactionFeeApi();
//        }


    }

    private void callTransactionFeeApi() {
        MovitWalletController movitWalletController = new GetP2MTransactionFee(merchantID, amount, strPromoCode);
        movitWalletController.init(new IMovitWalletController<Bundle>() {

            @Override
            public void onSuccess(Bundle response) {

                Intent intent = new Intent(P2MAmountActivity.this, P2MConfirmationActivity.class);
                intent.putExtra(IntentConstants.EXTERNAL_MERCHANTID, externalMerchantId);
                intent.putExtra(IntentConstants.MERCHANTID, merchantID);
                intent.putExtra(IntentConstants.AMOUNT, amount);
                intent.putExtra(IntentConstants.MERCHANTNAME, merchantName);
                intent.putExtra(IntentConstants.PROMOCODE, strPromoCode);
                intent.putExtra(IntentConstants.TRANSACTIONFEES, response.get("TxnFees") + "");
                intent.putExtra(IntentConstants.DISCOUNT,response.get(IntentConstants.DISCOUNT) + "");
                intent.putExtra(IntentConstants.DISCOUNT_ON,response.get(IntentConstants.DISCOUNT_ON)+ "");
                startActivity(intent);

                dismissProgress();

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                new OkDialog(P2MAmountActivity.this, "", reason, null);
            }
        });

    }


    private boolean validate() {
        GenericValidations genericValidations = new GenericValidations(this);
       /* if (!genericValidations.isValidMerchantID(merchantID)) {
            showError(genericValidations.getErrorMessage());
            return false;
        }*/

        if (!genericValidations.isValidAmount(amount)) {
            showError(genericValidations.getErrorMessage());
            return false;
        }
        return true;
    }

    private void checkPromoValidation(String promocode) {

        strPromoCode = promocode;
        amount = etAmount.getText().toString().trim();

        if (TextUtils.isEmpty(amount)) {

            customDialog.dismiss();
            etAmount.requestFocus();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.errorEmptyAmount))
                    .setTitle("mWallet");
            AlertDialog dialog = builder.create();
            dialog.setCanceledOnTouchOutside(true);
            dialog.show();
            return;
        } else {
            callOfferValidAPI();
        }
    }

    private void callOfferValidAPI() {

        ValidateOfferController validateOfferController = new ValidateOfferController(strPromoCode, amount);
        validateOfferController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                handleOfferSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String reason) {

            }
        });
    }

    private void handleOfferSuccess(Object response) {

        Bundle bundle = (Bundle) response;

        bundle.getString(IntentConstants.ResponseType);
        bundle.getString(IntentConstants.Message);
        bundle.getString(IntentConstants.Reason);

        if (bundle.get(IntentConstants.ResponseType) != null && bundle.getString(IntentConstants.ResponseType).equalsIgnoreCase("Success")) {

            customDialog.dismiss();


        } else {
            //customDialog.edtxtPromoCode.setText("");
            customDialog.edtxtPromoCode.setError(bundle.getString(IntentConstants.Reason));
        }

    }

    @OnClick({R.id.btnProceed, R.id.tvHavePromoCodeP2M,R.id.ivCouponCancel})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.btnProceed:
                onProceed();
                break;
            case R.id.tvHavePromoCodeP2M:
                onPromocodeClick();
                break;
            case R.id.ivCouponCancel:
                onCancelPromocode();
                break;
        }
    }

    private void onCancelPromocode() {
        strPromoCode = "";
        tvHavePromoCodeP2M.setVisibility(View.VISIBLE);
        rlPromocode.setVisibility(View.GONE);
    }

    private void onPromocodeClick() {
       /* tvHavePromoCodeP2M.setVisibility(View.GONE);
        rlPromocode.setVisibility(View.VISIBLE);*/

        amount = etAmount.getText();
        if (!TextUtils.isEmpty(amount)) {
            Intent intent = new Intent(this, AddPromoActivity.class);
            intent.putExtra(IntentConstants.AMOUNT, amount);
            startActivityForResult(intent, 1);
        } else {
            showError(getString(R.string.please_enter_valid_amount));
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                strPromoCode = data.getStringExtra(IntentConstants.PROMOCODE);
                tvHavePromoCodeP2M.setVisibility(View.GONE);
                rlPromocode.setVisibility(View.VISIBLE);
                tvPromocodeName.setText(strPromoCode + " applied" );

            }
        }
    }

    public class CustomDialog extends Dialog implements
            View.OnClickListener {

        public Activity mActivity;
        public Dialog d;
        public Button btnApplyPromoCode;
        public EditText edtxtPromoCode;

        private ImageView imgvwClose;

        public CustomDialog(Activity mActivity) {
            super(mActivity);
            // TODO Auto-generated constructor stub
            this.mActivity = mActivity;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_apply_promocode);

            edtxtPromoCode = (EditText) findViewById(R.id.edtxtPromoCode);
            btnApplyPromoCode = (Button) findViewById(R.id.btnAppyPromoCode);
            imgvwClose = (ImageView) findViewById(R.id.imgvwClose);

            edtxtPromoCode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            btnApplyPromoCode.setOnClickListener(this);
            imgvwClose.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnAppyPromoCode:
                    checkPromoValidation(edtxtPromoCode.getText().toString());
                    break;
                case R.id.imgvwClose:
                    dismiss();
                    break;

                default:
                    break;
            }

        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            if (!TextUtils.isEmpty(charSequence)) {
                etAmount.getEtCountryCode().setText(getString(R.string.AED));
                etAmount.getEtCountryCode().setVisibility(View.VISIBLE);

            } else {
                etAmount.getEtCountryCode().setVisibility(View.GONE);
                etAmount.setBackgroundResource(R.drawable.pinkish_grey_border);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (Constants.checkForFocusActive(new CustomEditTextLayout[]{etMerchantName, etAmount})) {
                btnProceed.setEnabled(true);
            } else {
                btnProceed.setEnabled(false);
            }

        }
    };

}
