package toml.uaeexchange.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.TypefaceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.controllers.SplitBillRequestController;
import toml.movitwallet.models.ContactsModel;
import toml.movitwallet.models.SplitBillRequestReceiver;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.CustomTypefaceSpan;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.TwoDecimalInputFilter;

public class BillSetupActivity extends BaseActivity {

    @BindView(R.id.txtvwAddMembers)
    TextView txtvwAddMembers;
    @BindView(R.id.imgvwAddMembers)
    ImageView imgvwAddMembers;
    @BindView(R.id.edtxtLayoutWhatIsExpenseFor)
    CustomEditTextLayout edtxtLayoutWhatIsExpenseFor;
    @BindView(R.id.errorLabelWhatIsExpenseFor)
    ErrorLabelLayout errorLabelWhatIsExpenseFor;
    @BindView(R.id.edtxtLayoutAmount)
    CustomEditTextLayout edtxtLayoutAmount;
    @BindView(R.id.errorLabelAmount)
    ErrorLabelLayout errorLabelAmount;
    @BindView(R.id.radiobuttonSplitEqually)
    RadioButton radiobuttonSplitEqually;
    @BindView(R.id.radiobuttonSplitManually)
    RadioButton radiobuttonSplitManually;
    @BindView(R.id.radiogroupSharingOption)
    RadioGroup radiogroupSharingOption;
    @BindView(R.id.txtvwAmountLeft)
    TextView txtvwAmountLeft;
    @BindView(R.id.linlayNameAmountTextContainer)
    LinearLayout linlayNameAmountTextContainer;
    @BindView(R.id.linlayNameAmountContainer)
    LinearLayout linlayNameAmountContainer;
    @BindView(R.id.linlaySplitManuallyCalculatorContainer)
    LinearLayout linlaySplitManuallyCalculatorContainer;

    @BindView(R.id.btnDone)
    CustomButton btnDone;

    double totalAmount, leftAmount;
    String purpose = "";

    ArrayList<EditText> allEditTexts = new ArrayList<>();
    ArrayList<EditText> youEditText = new ArrayList<>();
    ArrayList<EditText> otherEditTexts = new ArrayList<>();


    private boolean bSplitEqually = true, bSplitManually = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_setup);
        ButterKnife.bind(this);

        setToolbarWithBalance(getString(R.string.bill_setup), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }, 0, null, false);


        updateText();

        imgvwAddMembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // finish();
                Intent intent = new Intent(BillSetupActivity.this, MultipleContactsListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivityForResult(intent, 1);
                // BaseActivity.openNewActivity(BillSetupActivity.this, MultipleContactsListActivity.class.getCanonicalName(), true);
            }
        });

        edtxtLayoutWhatIsExpenseFor.getImgLeft().setVisibility(View.GONE);
        edtxtLayoutWhatIsExpenseFor.getVerticalDivider().setVisibility(View.GONE);

        edtxtLayoutAmount.getImgLeft().setVisibility(View.GONE);
        edtxtLayoutAmount.getVerticalDivider().setVisibility(View.GONE);


        edtxtLayoutWhatIsExpenseFor.setErrorLabelLayout(errorLabelWhatIsExpenseFor);
        edtxtLayoutAmount.setErrorLabelLayout(errorLabelAmount);

        edtxtLayoutWhatIsExpenseFor.getEditText().addTextChangedListener(textWatcherExpense);
        edtxtLayoutAmount.getEditText().addTextChangedListener(textWatcherAmount);

        edtxtLayoutAmount.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String s = editable.toString();
                if (!TextUtils.isEmpty(s)) {
                    totalAmount = leftAmount = Double.parseDouble(s);
                    txtvwAmountLeft.setText(String.format(getString(R.string.aed_left), leftAmount));

                    if (totalAmount > 0) {
                        enableDynamicEditTexts(true);
                    } else {
                        enableDynamicEditTexts(false);
                    }

                    //txtvwAmountLeft.setText("" + toml.movitwallet.utils.Constants.formatAmount("" + totalAmount));
                } else {
                    totalAmount = leftAmount = 0;
                    txtvwAmountLeft.setText(String.format(getString(R.string.aed_left), totalAmount));


                }


            }
        });


        radiogroupSharingOption.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {

                if (radiobuttonSplitEqually.isChecked()) {
                    radiobuttonSplitEqually.setTextColor(ContextCompat.getColor(BillSetupActivity.this, R.color.black_two));
                    radiobuttonSplitManually.setTextColor(ContextCompat.getColor(BillSetupActivity.this, R.color.battleship_grey));
                    bSplitEqually = true;
                    bSplitManually = false;

                    linlaySplitManuallyCalculatorContainer.setVisibility(View.GONE);

                } else if (radiobuttonSplitManually.isChecked()) {
                    radiobuttonSplitManually.setTextColor(ContextCompat.getColor(BillSetupActivity.this, R.color.black_two));
                    radiobuttonSplitEqually.setTextColor(ContextCompat.getColor(BillSetupActivity.this, R.color.battleship_grey));
                    bSplitManually = true;
                    bSplitEqually = false;

                    showManualSplitCalculatorView();
                }


            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(edtxtLayoutWhatIsExpenseFor.getText())) {
                    errorLabelWhatIsExpenseFor.setError(getString(R.string.please_enter_expense_name));
                    edtxtLayoutWhatIsExpenseFor.setBackgroundResource(R.drawable.custom_edittext_border_red);

                } else if (totalAmount > 0) {

                    purpose = edtxtLayoutWhatIsExpenseFor.getText();

                    List<SplitBillRequestReceiver> splitBillRequestReceiverList = new ArrayList<>();

                    // Split Equally
                    if (bSplitEqually) {

                        if (MovitConsumerApp.getInstance().getListSelectedContacts().size() > 0) {

                            showProgress();

                            int receiverCount = 1 + MovitConsumerApp.getInstance().getListSelectedContacts().size();

                            double spittedAmount = totalAmount / receiverCount;

                            SplitBillRequestReceiver objSelfSplitBillRequestReceiver = new SplitBillRequestReceiver();
                            objSelfSplitBillRequestReceiver.setMobileNumber(MovitConsumerApp.getInstance().getMobileNumber());
                            objSelfSplitBillRequestReceiver.setSplittedAmount(String.format("%.2f", spittedAmount));

                            //  splitBillRequestReceiverList.add(objSelfSplitBillRequestReceiver);


                            for (ContactsModel objContactsModel : MovitConsumerApp.getInstance().getListSelectedContacts()) {
                                SplitBillRequestReceiver objSplitBillRequestReceiver = new SplitBillRequestReceiver();
                                objSplitBillRequestReceiver.setMobileNumber(objContactsModel.getNumber());
                                objSplitBillRequestReceiver.setSplittedAmount(String.format("%.2f", spittedAmount));
                                splitBillRequestReceiverList.add(objSplitBillRequestReceiver);

                            }

                            callSplitBillRequest(splitBillRequestReceiverList);
                        } else {
                            new OkDialog(BillSetupActivity.this, getString(R.string.please_select_contacts_to_split_bill), new OkDialog.IOkDialogCallback() {
                                @Override
                                public void handleResponse() {

                                }
                            });

                        }

                    }
                    // Split Manually
                    else if (bSplitManually) {

                        if (leftAmount == 0) {

                            SplitBillRequestReceiver objSelfSplitBillRequestReceiver = new SplitBillRequestReceiver();
                            objSelfSplitBillRequestReceiver.setMobileNumber(MovitConsumerApp.getInstance().getMobileNumber());
                            if (!TextUtils.isEmpty(allEditTexts.get(0).getText())) {

                                objSelfSplitBillRequestReceiver.setSplittedAmount(String.format("%.2f", Double.valueOf(allEditTexts.get(0).getText().toString())));
                                // splitBillRequestReceiverList.add(objSelfSplitBillRequestReceiver);
                            } else {
                                new OkDialog(BillSetupActivity.this, "Please enter amount for self to split manually.", new OkDialog.IOkDialogCallback() {
                                    @Override
                                    public void handleResponse() {

                                    }
                                });

                                return;
                            }

                            for (int i = 0; i < MovitConsumerApp.getInstance().getListSelectedContacts().size(); i++) {
                                ContactsModel objContactsModel = MovitConsumerApp.getInstance().getListSelectedContacts().get(i);
                                if (!TextUtils.isEmpty(otherEditTexts.get(i).getText())) {

                                    SplitBillRequestReceiver objSplitBillRequestReceiver = new SplitBillRequestReceiver();
                                    objSplitBillRequestReceiver.setMobileNumber(objContactsModel.getNumber());

                                    objSplitBillRequestReceiver.setSplittedAmount(String.format("%.2f", Double.valueOf(otherEditTexts.get(i).getText().toString())));
                                    splitBillRequestReceiverList.add(objSplitBillRequestReceiver);
                                } else {
                                    new OkDialog(BillSetupActivity.this, "Please enter amount for " + objContactsModel.getName().trim() + " to split manually.", new OkDialog.IOkDialogCallback() {
                                        @Override
                                        public void handleResponse() {

                                        }
                                    });

                                    return;
                                }

                            }

                            showProgress();
                            callSplitBillRequest(splitBillRequestReceiverList);

                        } else {
                            new OkDialog(BillSetupActivity.this, String.format(getString(R.string.aed_pending_to_split), leftAmount), new OkDialog.IOkDialogCallback() {
                                @Override
                                public void handleResponse() {

                                }
                            });

                            return;
                        }


                    }

                }else if(TextUtils.isEmpty(edtxtLayoutAmount.getEditText().getText().toString())){

                    errorLabelAmount.setError(getString(R.string.please_enter_amount_to_split));
                    edtxtLayoutAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                    //showError("Please enter total amount");
                } else if(totalAmount == 0){
                    errorLabelAmount.setError(getString(R.string.please_enter_valid_amount));
                    edtxtLayoutAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                }

            }


        });


    }


    private void callSplitBillRequest(List<SplitBillRequestReceiver> splitBillRequestReceiverList) {

        if (splitBillRequestReceiverList.size() > 0) {

            MovitWalletController splitBillRequestController = new SplitBillRequestController(String.format("%.2f", totalAmount), purpose, splitBillRequestReceiverList);

            splitBillRequestController.init(new IMovitWalletController<Bundle>() {
                @Override
                public void onSuccess(Bundle response) {

                    dismissProgress();
                    new OkDialog(BillSetupActivity.this, response.getString("Message"), new OkDialog.IOkDialogCallback() {
                        @Override
                        public void handleResponse() {

                               /* Intent intent = new Intent(BillSetupActivity.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);*/

                            SplitAndShareActivity.isNeedToRefresh = true;
                            MultipleContactsListActivity.thisActivity.finish();
                            finish();


                        }
                    });

                }

                @Override
                public void onFailed(String errorCode, String reason) {
                    dismissProgress();
                    showError(reason);
                }
            });
        } else {
            dismissProgress();
            new OkDialog(BillSetupActivity.this, getString(R.string.please_select_contacts_to_split_bill), new OkDialog.IOkDialogCallback() {
                @Override
                public void handleResponse() {

                }
            });
        }

    }

    @SuppressLint("NewApi")
    private void showManualSplitCalculatorView() {

        linlaySplitManuallyCalculatorContainer.setVisibility(View.VISIBLE);
        // To remove previously added views if any
        linlayNameAmountContainer.removeAllViews();

        txtvwAmountLeft.setText(String.format(getString(R.string.aed_left), totalAmount));

        addYouView();

       /* LayoutTransition layoutTransition = ((LinearLayout) linlaySplitManuallyCalculatorContainer).getLayoutTransition();
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING);*/

        for (int i = 0; i < MovitConsumerApp.getInstance().getListSelectedContacts().size(); i++) {
            addNameAmountView(MovitConsumerApp.getInstance().getListSelectedContacts().get(i), i);
        }

        if (totalAmount > 0) {
            enableDynamicEditTexts(true);
        } else {
            enableDynamicEditTexts(false);
        }


    }

    public double calculateAmountLeft(double totalValue) {

        double sum = 0.0;
        for (int i = 0; i < allEditTexts.size(); i++) {
            if (!TextUtils.isEmpty(allEditTexts.get(i).getText().toString()))
                sum = sum + Double.parseDouble(allEditTexts.get(i).getText().toString());
        }

        leftAmount = totalValue - sum;

        return leftAmount;
    }

    private void addYouView() {
        View parent = LayoutInflater.from(this).inflate(R.layout.name_amount_item, linlayNameAmountContainer, false);

        TextView txtvwName = ButterKnife.findById(parent, R.id.txtvwName);
        TextView txtvwDelete = ButterKnife.findById(parent, R.id.txtvwDelete);
        final EditText edtxtAmount = ButterKnife.findById(parent, R.id.edtxtAmount);
        TextView txtvwPrefixAED = ButterKnife.findById(parent, R.id.txtvwPrefixAED);

        txtvwName.setText(AppSettings.getData(this, AppSettings.FIRST_NAME) + " " + AppSettings.getData(this, AppSettings.LAST_NAME) + " (You) ");
        txtvwDelete.setVisibility(View.GONE);
        edtxtAmount.setFilters(new InputFilter[]{new TwoDecimalInputFilter()});

        youEditText.add(edtxtAmount);
        allEditTexts.add(edtxtAmount);

        txtvwPrefixAED.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtxtAmount.requestFocus();
            }
        });

        edtxtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


                String s = editable.toString();

                if (TextUtils.isEmpty(s)) {
                    s = "0.00";
                }

                if (!TextUtils.isEmpty(s) && totalAmount > 0) {

                    try {
                        if (Double.parseDouble(s) <= totalAmount) {

                            txtvwAmountLeft.setText(String.format(getString(R.string.aed_left), calculateAmountLeft(totalAmount)));
                        }
                    }
                    catch (NumberFormatException e){

                    }
                }


            }
        });


        linlayNameAmountContainer.addView(parent);


    }


    private void addNameAmountView(final ContactsModel objContactsModel, int index) {
        final View parent = LayoutInflater.from(this).inflate(R.layout.name_amount_item, linlayNameAmountContainer, false);


        TextView txtvwName = ButterKnife.findById(parent, R.id.txtvwName);
        final TextView txtvwDelete = ButterKnife.findById(parent, R.id.txtvwDelete);
        final EditText edtxtAmount = ButterKnife.findById(parent, R.id.edtxtAmount);
        TextView txtvwPrefixAED = ButterKnife.findById(parent, R.id.txtvwPrefixAED);


        txtvwDelete.setTag("" + index);

        parent.setId(index);


        txtvwName.setText(objContactsModel.getName());
        edtxtAmount.setFilters(new InputFilter[]{new TwoDecimalInputFilter()});

        otherEditTexts.add(edtxtAmount);
        allEditTexts.add(edtxtAmount);

        txtvwPrefixAED.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtxtAmount.requestFocus();
            }
        });


        txtvwDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int index = Integer.parseInt("" + txtvwDelete.getTag());

                // Here + 1 because first view (You) is already added
                linlayNameAmountContainer.removeView(parent);

                allEditTexts.remove(allEditTexts.indexOf(edtxtAmount));
                otherEditTexts.remove(otherEditTexts.indexOf(edtxtAmount));

                Iterator<ContactsModel> iterator = MovitConsumerApp.getInstance().getListSelectedContacts().iterator();
                while (iterator.hasNext()) {
                    ContactsModel obj = iterator.next();
                    if (obj.equals(objContactsModel)) {
                        objContactsModel.setChecked(false);
                        iterator.remove();
                    }
                }

                updateText();

                if (totalAmount > 0) {

                    txtvwAmountLeft.setText(String.format(getString(R.string.aed_left), calculateAmountLeft(totalAmount)));
                }


            }
        });

        edtxtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                try {
                    String s = editable.toString();

                    if (TextUtils.isEmpty(s)) {
                        s = "0.00";
                    }

                    if (!TextUtils.isEmpty(s) && totalAmount > 0) {

                        if (Double.parseDouble(s) <= totalAmount) {

                            txtvwAmountLeft.setText(String.format(getString(R.string.aed_left), calculateAmountLeft(totalAmount)));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


        linlayNameAmountContainer.addView(parent);


    }


    private void enableDynamicEditTexts(boolean enable) {
        for (int i = 0; i < allEditTexts.size(); i++) {
            allEditTexts.get(i).setEnabled(enable);
        }

    }


    private void updateText() {
        String input = "Between you and: ";//text + txtvwAddMembers.getText().toString();
        //input = input + ", ";

        for (ContactsModel objContactsModel : MovitConsumerApp.getInstance().getListSelectedContacts()) {
            input = input + objContactsModel.getName() + ", ";
        }

        if (!input.endsWith(": ")) {

            String resultText = input;
            int ind = input.lastIndexOf(", ");
            if (ind >= 0) {
                resultText = new StringBuilder(resultText).replace(ind, ind + 1, "").toString();
            }

            // SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(resultText);
            SpannableString spannableStringBuilder = new SpannableString(resultText);
            //  Typeface fontMedium = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Medium.ttf");
            Typeface fontBold = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");

            TypefaceSpan fontBoldSpan = new CustomTypefaceSpan("", fontBold);
            spannableStringBuilder.setSpan(fontBoldSpan, 0, resultText.lastIndexOf(":") + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            // spannableStringBuilder.setSpan(fontMedium, resultText.lastIndexOf(":") + 1, resultText.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtvwAddMembers.setText(spannableStringBuilder);
        } else {
            String defaultText = "Between you and: Add Members";
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(defaultText);
            // Typeface fontMedium = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Medium.ttf");
            Typeface fontBold = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");

            TypefaceSpan fontBoldSpan = new CustomTypefaceSpan("", fontBold);
            spannableStringBuilder.setSpan(fontBoldSpan, 0, defaultText.lastIndexOf(":") + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            // spannableStringBuilder.setSpan(fontBold, 0, defaultText.lastIndexOf(":") + 1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            //spannableStringBuilder.setSpan(fontMedium, defaultText.lastIndexOf(":") + 1, defaultText.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtvwAddMembers.setText(spannableStringBuilder);
        }


    }


    private TextWatcher textWatcherExpense = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            if (TextUtils.isEmpty(charSequence)) {
                edtxtLayoutWhatIsExpenseFor.setBackgroundResource(R.drawable.pinkish_grey_border);

            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{edtxtLayoutWhatIsExpenseFor, edtxtLayoutAmount})) {
                btnDone.setEnabled(true);
            } else {
                btnDone.setEnabled(false);

            }

        }
    };


    private TextWatcher textWatcherAmount = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            if (!TextUtils.isEmpty(charSequence)) {
                edtxtLayoutAmount.getEtCountryCode().setText(getString(R.string.AED));
                edtxtLayoutAmount.getEtCountryCode().setVisibility(View.VISIBLE);

            } else {
                edtxtLayoutAmount.getEtCountryCode().setVisibility(View.GONE);
                edtxtLayoutAmount.setBackgroundResource(R.drawable.pinkish_grey_border);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{edtxtLayoutWhatIsExpenseFor, edtxtLayoutAmount})) {
                btnDone.setEnabled(true);
            } else {
                btnDone.setEnabled(false);

            }

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {


            if (requestCode == 1) {
                updateText();
                if (bSplitManually) {
                    showManualSplitCalculatorView();
                }

            }
        }

    }
}
