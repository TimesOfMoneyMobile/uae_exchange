package toml.uaeexchange.activities;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomTabLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.fragments.SendMoneyFragment;
import toml.uaeexchange.fragments.SocialPaymentFragment;
import toml.uaeexchange.utilities.GenericValidations;
import toml.movitwallet.controllers.PayPersonController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.whatsappp2p.MyAccessibilityService;


/**
 * Created by vishwanathp on 7/25/2017.
 */

public class PayPersonActivity extends BaseActivity {


    @BindView(R.id.act_pp_tab_layout)
    LinearLayout tabLayout;
    @BindView(R.id.act_pp_viewpager)
    ViewPager viewPager;

    CustomTabLayout customTabLayout;


    Intent intent;

    SendMoneyFragment sendMoneyFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_person);
        ButterKnife.bind(this);

        setToolbarWithBalance(getString(R.string.payaperson), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, R.drawable.icon_settings, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleWhatsAppP2P();
            }
        }, true);


//        setToolbarWithBalance(getString(R.string.payaperson), R.drawable.icon_back, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        }, 0,null,true);


        customTabLayout = new CustomTabLayout(tabLayout);

        customTabLayout.setTabListener(new CustomTabLayout.IOnTabClicked() {
            @Override
            public void onTabClicked(int index) {

                viewPager.setCurrentItem(index);
            }

        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                customTabLayout.setCurrentActiveTab(position, ContextCompat.getColor(PayPersonActivity.this, R.color.white), 0);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        customTabLayout.setTabTexts(getString(R.string.sendmoney), getString(R.string.social_payments));
        customTabLayout.setTabTextColor(ContextCompat.getColor(PayPersonActivity.this, R.color.white), ContextCompat.getColor(PayPersonActivity.this, R.color.white));
        customTabLayout.setCurrentActiveTab(0, ContextCompat.getColor(PayPersonActivity.this, R.color.white), 0);

        viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager()));

    }


    private void handleWhatsAppP2P() {

        if (ContextCompat.checkSelfPermission(PayPersonActivity.this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(PayPersonActivity.this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    1);

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(PayPersonActivity.this)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, 2);

                } else {
                    requestAccessebilityPermission();
                }
            } else {
                requestAccessebilityPermission();
            }
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.canDrawOverlays(PayPersonActivity.this)) {

                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            startActivityForResult(intent, 2);

                        } else {
                            requestAccessebilityPermission();
                        }
                    } else {
                        requestAccessebilityPermission();
                    }
                }
            }
            break;

            case 2:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendMoneyFragment.showContactList();
                } else {
                    showError("Please accept permission to see Contacts list.");
                }

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(PayPersonActivity.this)) {
                    requestAccessebilityPermission();
                } else {
                    //showError("Permission Denied");
                }
            }
        }
    }

    private void requestAccessebilityPermission() {
        new OkDialog(this, "To use this feature,you need to turn on Accessibility service for this application", new OkDialog.IOkDialogCallback() {
            @Override
            public void handleResponse() {

                Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                startActivity(intent);
            }
        });
    }


    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;

        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public int getCount() {
            return PAGE_COUNT;
        }


        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return sendMoneyFragment = new SendMoneyFragment();
            return new SocialPaymentFragment();

        }
    }
}
