package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.BookRechargeRequestCotroller;
import toml.movitwallet.controllers.GenerateOTPController;
import toml.movitwallet.controllers.GetP2MTransactionFee;
import toml.movitwallet.jsonmodels.RechargeSuccessResponseFromWallet;
import toml.movitwallet.jsonmodels.RechargeSuccessResponsePG;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.dialogs.ValidateOTPDialog;

public class RechargeConfirmationActivity extends BaseActivity implements BaseActivity.ClickableSpanListener {


    @BindView(R.id.tvAmountValue)
    TextView tvAmountValue;
    @BindView(R.id.tvTotalAmount)
    TextView tvTotalAmount;
    @BindView(R.id.tvSendAmount)
    TextView tvSendAmount;
    @BindView(R.id.tvRecieverNo)
    TextView tvRecieverNo;
    @BindView(R.id.tvFees)
    TextView tvFees;
    @BindView(R.id.btnConfirm)
    CustomButton btnConfirm;
    @BindView((R.id.rlEdit))
    RelativeLayout rlEdit;
    @BindView(R.id.ivProviderIcon)
    ImageView ivProviderIcon;

    @BindView(R.id.tvDiscountLable)
    TextView tvDiscountLable;
    @BindView(R.id.tvDiscountValue)
    TextView tvDiscountValue;
    @BindView(R.id.rlDiscount)
    RelativeLayout rlDiscount;
    @BindView(R.id.rbWallet)
    RadioButton rbWallet;
    @BindView(R.id.rbPG)
    RadioButton rbPG;
    @BindView(R.id.rgPaymentOption)
    RadioGroup rgPaymentOption;

    private boolean isSpanClicked;

    private String skuCode, sendCurrencyISO, receiverNo, sendValue, distributorRef, promoCode;
    boolean validateOnly;
    String TXN_TYPE = "RECHARGE";
    ValidateOTPDialog otpDialog;
    private String paymentMode;

    private SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_confirmation);
        ButterKnife.bind(this);

        setToolbarWithBalance(getString(R.string.review_and_recharge), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);

        setClickableSpanListener(this);
        receiverNo = getIntent().getStringExtra(IntentConstants.ACCOUNT_NO);
        sendCurrencyISO = getIntent().getStringExtra(IntentConstants.SEND_CURRENCY_ISO);
        skuCode = getIntent().getStringExtra(IntentConstants.SKU_CODE);
        sendValue = getIntent().getStringExtra(IntentConstants.SEND_VALUE);
        distributorRef = getIntent().getStringExtra(IntentConstants.DISTRIBUTER_REF);
        validateOnly = getIntent().getBooleanExtra(IntentConstants.VALIDATE_ONLY, true);
        promoCode = getIntent().getStringExtra(IntentConstants.PROMOCODE);
        //callGetFee();


        if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("Y")) {
            rgPaymentOption.check(R.id.rbWallet);

        } else {
            rgPaymentOption.check(R.id.rbPG);
            rbWallet.setEnabled(false);

        }
        if (!TextUtils.isEmpty(skuCode)) {
            if (skuCode.equalsIgnoreCase(Constants.ETISALAT_SKU_CODE)) {
                ivProviderIcon.setImageResource(R.drawable.etisalat_icon);
            } else if (skuCode.equalsIgnoreCase(Constants.DU_SKU_CODE)) {
                ivProviderIcon.setImageResource(R.drawable.du_icon);
            }
        }
        tvSendAmount.setText(getString(R.string.AED) + " " + toml.uaeexchange.utilities.Constants.formatAmount(sendValue));
        tvRecieverNo.setText(getString(R.string.country_code) + " " + receiverNo);
        tvAmountValue.setText(getString(R.string.AED) + " " + toml.uaeexchange.utilities.Constants.formatAmount(sendValue));


        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = toml.uaeexchange.utilities.Constants.parseSMSForOTP(message);//Parse verification code
                try {
                    if (otpDialog != null && otpDialog.isShowing()) {
                        otpDialog.setOTPText(code);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //then you can send verification code to server
            }
        });



        callGetFee();
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (smsVerifyCatcher != null)
            smsVerifyCatcher.onStop();
    }


    private void callGetFee() {
        showProgress();

        MovitWalletController movitWalletController = new GetP2MTransactionFee(Constants.BILLER_MERCHANT, sendValue, promoCode);
        movitWalletController.init(new IMovitWalletController<Bundle>() {

            @Override
            public void onSuccess(Bundle response) {
                dismissProgress();
                String fees = "" + response.get("TxnFees");
                String discount = "" + response.get(IntentConstants.DISCOUNT);
                String discount_on = "" + response.get(IntentConstants.DISCOUNT_ON);


                if (!discount.equalsIgnoreCase("null") && Double.valueOf(discount) != 0) {
                    tvDiscountValue.setText(getString(R.string.AED) + " " + discount);
                    if (discount_on.equalsIgnoreCase("T")) {
                        tvDiscountLable.setText(R.string.discount_on_transaction_amount);
                        String discountedAmount = toml.uaeexchange.utilities.Constants.formatAmount(String.valueOf(Double.valueOf(sendValue) - Double.valueOf(discount)));
                        tvTotalAmount.setText(getString(R.string.AED) + " " + toml.uaeexchange.utilities.Constants.formatAmount(String.valueOf(Double.valueOf(discountedAmount) + Double.valueOf(fees))));
                    } else if (discount_on.equalsIgnoreCase("F")) {
                        tvDiscountLable.setText(R.string.discount_on_fees_amount);
                        String discountedFees = toml.uaeexchange.utilities.Constants.formatAmount(String.valueOf(Double.valueOf(fees) - Double.valueOf(discount)));
                        tvTotalAmount.setText(getString(R.string.AED) + " " + toml.uaeexchange.utilities.Constants.formatAmount(String.valueOf(Double.valueOf(sendValue) + Double.valueOf(discountedFees))));
                    }
                } else {
                    rlDiscount.setVisibility(View.GONE);
                    tvTotalAmount.setText(getString(R.string.AED) + " " + toml.uaeexchange.utilities.Constants.formatAmount(String.valueOf(Double.valueOf(sendValue) + Double.valueOf(fees))));
                }

                tvFees.setText(getString(R.string.AED) + " " + toml.uaeexchange.utilities.Constants.formatAmount(fees));
                //  tvTotalAmount.setText(getString(R.string.AED) + " " + toml.uaeexchange.utilities.Constants.formatAmount(String.valueOf(Float.valueOf(sendValue) + Float.valueOf(fees))));

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                new OkDialog(RechargeConfirmationActivity.this, "", reason, null);
            }
        });


    }


    @OnClick({R.id.btnConfirm, R.id.rlEdit})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.btnConfirm:
                handleConfirm();

                break;

            case R.id.rlEdit:
                onBackPressed();

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void handleConfirm() {
        isSpanClicked = false;

        switch (rgPaymentOption.getCheckedRadioButtonId()) {
            case R.id.rbWallet:
                Constants.PAYMODE_TYPE = "W2W";
                break;
            case R.id.rbPG:
                Constants.PAYMODE_TYPE = "PG";
                break;
        }


        if(Constants.PAYMODE_TYPE.equalsIgnoreCase("W2W")) {
            if (toml.uaeexchange.utilities.Constants.checkToShowOtpDialog(RechargeConfirmationActivity.this, sendValue)) {
                callGenerateOTP(isSpanClicked);
            } else {
                callBookRequest("");
            }
        }
        else
        {
            callBookRequest("");
        }


    }

    private void callGenerateOTP(final boolean isSpanClicked) {
        showProgress();

        MovitWalletController generateOTPController = new GenerateOTPController(TXN_TYPE);

        generateOTPController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                dismissProgress();


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String code = "";
                        try {
                            if (isSpanClicked) {
                                if (otpDialog.isShowing()) {
                                    otpDialog.refreshOnResend();
                                }
                            } else {
                                showOTPDialog();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }, 200);


            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                showError(reason);

            }
        });
        if (isSpanClicked) {
            this.isSpanClicked = false;
        }
    }

    private void showOTPDialog() {

        otpDialog = new ValidateOTPDialog(this, new ValidateOTPDialog.IValidateOTPDialogCallBack() {


            @Override
            public void resendOTPCallBack(String otpValue) {

            }

            @Override
            public void verifyOTPCallBack(String otpValue) {
                // verifyOTP(otpValue);
                callBookRequest(otpValue);
            }
        });
        otpDialog.show();

    }

    private void callBookRequest(final String otpValue) {

        showProgress();
        MovitWalletController movitWalletController = new BookRechargeRequestCotroller(Constants.BOOK_REQUEST_TYPE,
                skuCode, sendValue, sendCurrencyISO, receiverNo, distributorRef, validateOnly, otpValue, promoCode);

        movitWalletController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                if (otpDialog != null && otpDialog.isShowing()) {
                    otpDialog.dismiss();
                }
                dismissProgress();

                if (response instanceof RechargeSuccessResponseFromWallet) {

                    handleSuccessForWallet((RechargeSuccessResponseFromWallet) response);
                }
                else if(response instanceof RechargeSuccessResponsePG)
                {
                    handleSuccessForPG((RechargeSuccessResponsePG) response);
                }


            }

            @Override
            public void onFailed(String errorCode, String reason) {

                dismissProgress();

                if (errorCode.equalsIgnoreCase("138")) {
                    new OkDialog(RechargeConfirmationActivity.this, reason, new OkDialog.IOkDialogCallback() {
                        @Override
                        public void handleResponse() {
                            // BaseActivity.openDashboardActivity(RechargeConfirmationActivity.this);
                        }
                    });

                } else {
                    new OkDialog(RechargeConfirmationActivity.this, getString(R.string.transaction_unsuccessfull), new OkDialog.IOkDialogCallback() {
                        @Override
                        public void handleResponse() {
                            BaseActivity.openDashboardActivity(RechargeConfirmationActivity.this);
                        }
                    });
                }


                // showError(reason);

            }
        });
    }

    private void handleSuccessForPG(RechargeSuccessResponsePG response) {

        String PGMerchantId = response.getPGMerchantId();
        String CollaboratorID = response.getCollaboratorID();
        String EncryptedRequest = response.getEncryptedRequest();

        String link = PGMerchantId + "||" + CollaboratorID + "||" + EncryptedRequest;
        Intent intent = new Intent(this, RechargeMoneyWebView.class);
        intent.putExtra("link", link);
        startActivity(intent);
    }

    private void handleSuccessForWallet(RechargeSuccessResponseFromWallet response) {

        String txnId = response.getTransferRecord().getTransferId().getDistributorRef();
        Intent intent = new Intent(this, TransactionSuccessActivity.class);
        intent.putExtra(IntentConstants.TRANSACTION_TYPE, IntentConstants.RECHARGE_TYPE);
        intent.putExtra(IntentConstants.TXNID, txnId);
        intent.putExtra(IntentConstants.TRANSFER_AMOUNT,sendValue);
        startActivity(intent);


    }

    @Override
    public void onSpanClick(String strText) {
        isSpanClicked = true;
        callGenerateOTP(isSpanClicked);
    }
}
