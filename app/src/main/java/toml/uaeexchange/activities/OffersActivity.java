package toml.uaeexchange.activities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import toml.movitwallet.utils.IntentConstants;
import toml.uaeexchange.R;
import toml.uaeexchange.adapters.OffersListAdapter;
import toml.movitwallet.controllers.OffersController;
import toml.movitwallet.models.Offer;
import toml.movitwallet.utils.IMovitWalletController;

public class OffersActivity extends BaseActivity {

    ListView lstvwOffers;
    TextView txtvwNoOffers;
    private List<Offer> offersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);


        setToolbarWithBalance(getString(R.string.offers), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);

        lstvwOffers = (ListView) findViewById(R.id.listViewOffers);
        txtvwNoOffers = (TextView) findViewById(R.id.txtvwNoOffers);

        OffersController offersController = new OffersController();

        showProgress();

        offersController.init(new IMovitWalletController<List<Offer>>() {
            @Override
            public void onSuccess(List<Offer> response) {
                dismissProgress();

                if (response.size() > 0) {
                    offersList = response;
                    lstvwOffers.setAdapter(new OffersListAdapter(OffersActivity.this, response, true, new OffersListAdapter.IPromoCodeClickListener() {
                        @Override
                        public void onPromoCodeClick(String promoCode,int position, View view) {
                            if (view.getId() == R.id.btnAppyPromoCode) {
                                ClipboardManager clipboard = (ClipboardManager) OffersActivity.this.getSystemService(OffersActivity.this.CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText(getString(R.string.offers), promoCode);
                                clipboard.setPrimaryClip(clip);
                                Toast.makeText(OffersActivity.this, getString(R.string.text_copied_to_clipbaord), Toast.LENGTH_SHORT).show();
                            } else {
                                Intent intent = new Intent(OffersActivity.this, OffersDetailActivity.class);
                                intent.putExtra(IntentConstants.OFFER, offersList.get(position));
                                startActivity(intent);

                            }
                        }
                    }));
                } else {
                    txtvwNoOffers.setVisibility(View.VISIBLE);
                }

            }


            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                txtvwNoOffers.setVisibility(View.VISIBLE);
            }


        });

        lstvwOffers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(OffersActivity.this, OffersDetailActivity.class);
                intent.putExtra(IntentConstants.OFFER, offersList.get(position));
                startActivity(intent);
            }
        });

    }
}
