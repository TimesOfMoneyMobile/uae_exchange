package toml.uaeexchange.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.controllers.GetIdentificationDocsController;
import toml.movitwallet.controllers.ListSecurityQuestionsController;
import toml.movitwallet.controllers.RegistrationController;
import toml.movitwallet.controllers.ResetSecurityQuestionController;
import toml.movitwallet.models.Document;
import toml.movitwallet.models.RegistrationRequest;
import toml.movitwallet.models.ResetQuestionsRequest;
import toml.movitwallet.models.SecurityQuestion;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.adapter.SecurityQuestionsListAdapter;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;


public class SecurityQuestionsActivity extends BaseActivity {


    String mobileNumber = "", listMode = "";
    @BindView(R.id.linlayImageTextContainer)
    LinearLayout linlayImageTextContainer;
    @BindView(R.id.imgvwSecurityQuestions)
    ImageView imgvwSecurityQuestions;


    private String LIST_MODE_ALL = "ALL";
    private String LIST_MODE_ANS = "ANS";
    private String LIST_MODE_REN = "REN";
    String previousQuestion1,previousQuestion2;

    LinearLayout linlayQuestionAnswerRoot;
    Button btnSubmit;

    boolean userSelect1 = false, userSelect2;
    EditText editTextAnswer1, editTextAnswer2;

    String questionID1, answer1, questionID2, answer2;

    List<SecurityQuestion> tempSecurityQuestions = new ArrayList<>();
    List<SecurityQuestion> allSecurityQuestions = new ArrayList<>();

    RegistrationRequest objRegistrationRequest;
    ResetQuestionsRequest objResetQuestionsRequest;


    View viewTitleWithLogo, viewTitleWithoutLogo;
    String strFromActivity = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_questions);
        ButterKnife.bind(this);

        viewTitleWithLogo = findViewById(R.id.viewTitleWithLogo);
        viewTitleWithoutLogo = findViewById(R.id.viewTitleWithoutLogo);

        if (getIntent().hasExtra(IntentConstants.FROM_ACTIVITY)) {

            strFromActivity = getIntent().getStringExtra(IntentConstants.FROM_ACTIVITY);

            if (strFromActivity.equalsIgnoreCase(ForgotSecurityAnswerActivity.class.getCanonicalName())) {

                viewTitleWithLogo.setVisibility(View.GONE);
                imgvwSecurityQuestions.setVisibility(View.GONE);
                viewTitleWithoutLogo.setVisibility(View.VISIBLE);

                setToolbarWithBalance(getString(R.string.forgot_security_answer), R.drawable.icon_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                }, 0, null, false);

                if (getIntent().getExtras() != null) {
                    objResetQuestionsRequest = getIntent().getExtras().getParcelable(IntentConstants.RESET_QUESTIONS_REQUEST);
                }

                //listMode = LIST_MODE_ANS;
            }

        } else {
            viewTitleWithLogo.setVisibility(View.VISIBLE);
            viewTitleWithoutLogo.setVisibility(View.GONE);

            setToolbar("", R.drawable.icon_back, 0, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            }, null);

            if (getIntent().getExtras() != null) {
                objRegistrationRequest = getIntent().getExtras().getParcelable(IntentConstants.REG_REQUEST);
            }


        }


        linlayQuestionAnswerRoot = (LinearLayout) findViewById(R.id.linalyQuestionAnswerRoot);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        listMode = LIST_MODE_ALL;

        showProgress();
        MovitWalletController listSecurityQuestionsCOntroller = new ListSecurityQuestionsController(mobileNumber, listMode);

        listSecurityQuestionsCOntroller.init(new IMovitWalletController<List<SecurityQuestion>>() {

            @Override
            public void onSuccess(List<SecurityQuestion> response) {
                dismissProgress();

                drawQuestionAnswerLayout(response);


            }

            @Override
            public void onFailed(String errorCode, String reason) {

                dismissProgress();

                showError(reason);

            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (strFromActivity.equalsIgnoreCase(ForgotSecurityAnswerActivity.class.getCanonicalName())) {
                    callResetQuestionsAPI();

                } else {
                    callRegistrationAPI();
                }


                //BaseActivity.openNewActivity(SecurityQuestionsActivity.this, VerifyMobile.class);
            }
        });


    }


    private void callRegistrationAPI() {

        showProgress();

        answer1 = editTextAnswer1.getText().toString().trim();
        answer2 = editTextAnswer2.getText().toString().trim();

        String result = "<SecurityQuestion id = \"" + questionID1 + "\" answer = \"" + answer1 + "\" />\n"
                + "<SecurityQuestion id = \"" + questionID2 + "\" answer = \"" + answer2 + "\" />\n";

        LogUtils.Verbose("Result", result);

        objRegistrationRequest.setSecurityQuestionResult(result);

        showProgress();

        MovitWalletController registrationController = new RegistrationController(objRegistrationRequest);

        registrationController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                dismissProgress();

                Intent intent = new Intent(SecurityQuestionsActivity.this, RegistrationSuccessActivity.class);
                // To clear all previous activities.
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                Bundle bundle = new Bundle();
                bundle.putParcelable(IntentConstants.REG_REQUEST, objRegistrationRequest);
                intent.putExtras(bundle);
                startActivity(intent);
                // finish();

            }

            @Override
            public void onFailed(String errorCode, String reason) {

                dismissProgress();
                showError(reason);

            }
        });
    }

    private void callResetQuestionsAPI() {

        // First call get docs api here on on success of it we have called ResetQuestion api
        callGetDocsAPI();


    }

    private void callGetDocsAPI() {
        showProgress();

        MovitWalletController getIdentificationDocsController = new GetIdentificationDocsController();

        getIdentificationDocsController.init(new IMovitWalletController<List<Document>>() {
            @Override
            public void onSuccess(List<Document> response) {

                if (response.size() > 0) {

                    final ArrayList<Document> documents = (ArrayList<Document>) response;
                    // KYCRegistrationActivity.thisActivity.setDocuments(documents);

                    objResetQuestionsRequest.setMobileNumber(MovitConsumerApp.getInstance().getMobileNumber());

                    answer1 = editTextAnswer1.getText().toString().trim();
                    answer2 = editTextAnswer2.getText().toString().trim();

                    String KYCDocResult = "";

                    for (Document document : documents) {
                        if (document.getName().contains("EMIRATES"))
                            KYCDocResult = "<Document id = \"" + document.getId() + "\" name = \"" + document.getName() + "\" value =\"" + objResetQuestionsRequest.getEmiratesID() + "\" />\n";
                    }


                    String SecurityQuestionResult = "<SecurityQuestion id = \"" + questionID1 + "\" answer = \"" + answer1 + "\" />\n"
                            + "<SecurityQuestion id = \"" + questionID2 + "\" answer = \"" + answer2 + "\" />\n";

                    LogUtils.Verbose("KYCDocResult", KYCDocResult);
                    LogUtils.Verbose("SecurityQuestionResult", SecurityQuestionResult);

                    objResetQuestionsRequest.setKYCDocumentsResult(KYCDocResult);
                    objResetQuestionsRequest.setSecurityQuestionResult(SecurityQuestionResult);

                    MovitWalletController registrationController = new ResetSecurityQuestionController(objResetQuestionsRequest);

                    registrationController.init(new IMovitWalletController() {
                        @Override
                        public void onSuccess(Object response) {

                            dismissProgress();

                            Intent intent = new Intent(SecurityQuestionsActivity.this, ResetSecurityQuestionSuccessActivity.class);
                            // To clear all previous activities.
                            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            // finish();

                        }

                        @Override
                        public void onFailed(String errorCode, String reason) {

                            dismissProgress();
                            //   showError(reason);

                            new OkDialog(SecurityQuestionsActivity.this, reason, new OkDialog.IOkDialogCallback() {
                                @Override
                                public void handleResponse() {

                                }
                            });

                        }
                    });


                } else {
                    dismissProgress();
                    // txtvwNoOffers.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailed(String errorCode, String reason) {

                dismissProgress();
                showError(reason);
                return;

            }
        });
    }

    private void drawQuestionAnswerLayout(final List<SecurityQuestion> securityQuestionList) {


        // Create the Typeface you want to apply to certain text
        Typeface quickSandMedium = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Medium.ttf");

        linlayQuestionAnswerRoot.removeAllViews();

        allSecurityQuestions.addAll(securityQuestionList);
        tempSecurityQuestions.addAll(securityQuestionList);

        final String[] questionArray = new String[tempSecurityQuestions.size()];

        for (int i = 0; i < tempSecurityQuestions.size(); i++) {
            questionArray[i] = tempSecurityQuestions.get(i).question;
        }


        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getInDp(50));

        //linlayQuestionAnswerRoot.addView(textView);
        TextView txtvwQuestion1Header = new TextView(this);
        txtvwQuestion1Header.setText("QUESTION 1");
        txtvwQuestion1Header.setTypeface(quickSandMedium);
        txtvwQuestion1Header.setTextColor(ContextCompat.getColor(this, R.color.black_two));
        txtvwQuestion1Header.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        //txtvwQuestion1Header.setTypeface(quickSandMedium);
        txtvwQuestion1Header.setPadding(getInDp(20), getInDp(5), 0, getInDp(5));
        txtvwQuestion1Header.setGravity(Gravity.CENTER_VERTICAL);
        txtvwQuestion1Header.setBackgroundColor(ContextCompat.getColor(this, R.color.pale_grey_two));
        txtvwQuestion1Header.setLayoutParams(params);


        TextView txtvwQuestion2Header = new TextView(this);
        txtvwQuestion2Header.setText("QUESTION 2");
        txtvwQuestion2Header.setTypeface(quickSandMedium);
        txtvwQuestion2Header.setTextColor(ContextCompat.getColor(this, R.color.black_two));
        txtvwQuestion2Header.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        //txtvwQuestion1Header.setTypeface(quickSandMedium);
        txtvwQuestion2Header.setPadding(getInDp(20), getInDp(5), 0, getInDp(5));
        txtvwQuestion2Header.setGravity(Gravity.CENTER_VERTICAL);
        txtvwQuestion2Header.setBackgroundColor(ContextCompat.getColor(this, R.color.pale_grey_two));
        txtvwQuestion2Header.setLayoutParams(params);


        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getInDp(70));

        //params1.setMargins(10, 10, 10, 10);

        final TextView txtvwQuestion1 = new TextView(this);
        txtvwQuestion1.setText("Question");
        txtvwQuestion1.setId(1);
        txtvwQuestion1.setTypeface(quickSandMedium);
        txtvwQuestion1.setTextColor(ContextCompat.getColor(this, R.color.battleship_grey));
        txtvwQuestion1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

        txtvwQuestion1.setPadding(getInDp(20), getInDp(0), 0, getInDp(0));
        txtvwQuestion1.setGravity(Gravity.LEFT | Gravity.CENTER);
        // txtvwQuestion1Header.setTypeface(quickSandMedium);
        txtvwQuestion1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.question, 0, R.drawable.ic_keyboard_arrow_down_black, 0);
        txtvwQuestion1.setCompoundDrawablePadding(getInDp(20));
        txtvwQuestion1.setLayoutParams(params1);


        final TextView txtvwQuestion2 = new TextView(this);
        txtvwQuestion2.setText("Question");
        txtvwQuestion2.setId(2);
        txtvwQuestion2.setTypeface(quickSandMedium);
        txtvwQuestion2.setTextColor(ContextCompat.getColor(this, R.color.battleship_grey));
        txtvwQuestion2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

        txtvwQuestion2.setPadding(getInDp(20), getInDp(0), 0, getInDp(0));
        txtvwQuestion2.setGravity(Gravity.LEFT | Gravity.CENTER);
        // txtvwQuestion1Header.setTypeface(quickSandMedium);
        txtvwQuestion2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.question, 0, R.drawable.ic_keyboard_arrow_down_black, 0);
        txtvwQuestion2.setCompoundDrawablePadding(getInDp(20));
        txtvwQuestion2.setLayoutParams(params1);

        editTextAnswer1 = new EditText(this);
        editTextAnswer1.setHint("Answer");
        editTextAnswer1.setTypeface(quickSandMedium);
        editTextAnswer1.setTextColor(ContextCompat.getColor(this, R.color.battleship_grey));
        editTextAnswer1.setSingleLine(true);
        editTextAnswer1.setPadding(getInDp(20), getInDp(0), getInDp(10), getInDp(0));
        editTextAnswer1.setBackgroundResource(0);
        editTextAnswer1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        editTextAnswer1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.answer, 0, 0, 0);
        editTextAnswer1.setCompoundDrawablePadding(getInDp(20));
        editTextAnswer1.setLayoutParams(params1);

        editTextAnswer2 = new EditText(this);
        editTextAnswer2.setHint("Answer");
        editTextAnswer2.setTypeface(quickSandMedium);
        editTextAnswer2.setTextColor(ContextCompat.getColor(this, R.color.battleship_grey));
        editTextAnswer2.setSingleLine(true);
        editTextAnswer2.setPadding(getInDp(20), getInDp(0), getInDp(10), getInDp(0));
        editTextAnswer2.setBackgroundResource(0);
        editTextAnswer2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        editTextAnswer2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.answer, 0, 0, 0);
        editTextAnswer2.setCompoundDrawablePadding(getInDp(20));
        editTextAnswer2.setLayoutParams(params1);

        editTextAnswer1.addTextChangedListener(textWatcher);
        editTextAnswer2.addTextChangedListener(textWatcher);

        editTextAnswer1.setEnabled(false);
        editTextAnswer2.setEnabled(false);

//        editTextAnswer1.setHint("Answer");
//        editTextAnswer2.setHint("Answer");


        txtvwQuestion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tempSecurityQuestions != null && tempSecurityQuestions.size() > 0) {
                    tempSecurityQuestions.clear();
                    tempSecurityQuestions.addAll(allSecurityQuestions);
                }

                Iterator<SecurityQuestion> iter = tempSecurityQuestions.iterator();
                while (iter.hasNext()) {
                    SecurityQuestion obj = iter.next();
                    if (obj.id == questionID2) iter.remove();
                    // if (obj.id == questionID2) iter.remove();
                }

                openBottomSheet(tempSecurityQuestions, txtvwQuestion1);

             /*   if(!TextUtils.isEmpty(editTextAnswer1.getText().toString())){
                    editTextAnswer1.setText("");
                }*/


            }
        });

        txtvwQuestion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tempSecurityQuestions != null && tempSecurityQuestions.size() > 0) {
                    tempSecurityQuestions.clear();
                    tempSecurityQuestions.addAll(allSecurityQuestions);
                }

                Iterator<SecurityQuestion> iter = tempSecurityQuestions.iterator();
                while (iter.hasNext()) {
                    SecurityQuestion obj = iter.next();
                    if (obj.id == questionID1) iter.remove();
                    // if (obj.id == questionID2) iter.remove();
                }

                openBottomSheet(tempSecurityQuestions, txtvwQuestion2);

             /*   if(!TextUtils.isEmpty(editTextAnswer2.getText().toString())){
                    editTextAnswer2.setText("");
                }*/


            }
        });

        Constants.setEditTextMaxLength(editTextAnswer1, 250, false, true);
        Constants.setEditTextMaxLength(editTextAnswer2, 250, false, true);

        // Divider line 1
        View ruler1 = new View(this);
        ruler1.setBackgroundColor(ContextCompat.getColor(this, R.color.silver));

        // Divider line 2
        View ruler2 = new View(this);
        ruler2.setBackgroundColor(ContextCompat.getColor(this, R.color.silver));


        // Adding views to layout
        linlayQuestionAnswerRoot.addView(txtvwQuestion1Header);
        linlayQuestionAnswerRoot.addView(txtvwQuestion1);
        linlayQuestionAnswerRoot.addView(ruler1, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
        linlayQuestionAnswerRoot.addView(editTextAnswer1);

        linlayQuestionAnswerRoot.addView(txtvwQuestion2Header);
        linlayQuestionAnswerRoot.addView(txtvwQuestion2);
        linlayQuestionAnswerRoot.addView(ruler2, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
        linlayQuestionAnswerRoot.addView(editTextAnswer2);


        // toml.uaeexchange.utilities.Constants.overrideFonts(this, linlayQuestionAnswerRoot, quickSandMedium);


    }


    public void openBottomSheet(final List<SecurityQuestion> listData, final TextView txtvwSource) {


        View view = getLayoutInflater().inflate(R.layout.dialog_security_question_list, null);

        final SecurityQuestionsListAdapter listAdapter = new SecurityQuestionsListAdapter(SecurityQuestionsActivity.this, listData);

        final Dialog mBottomSheetDialog = new Dialog(SecurityQuestionsActivity.this, R.style.MaterialDialogSheet);

        TextView txtvwCancel;
        ListView lstvwSecurityQuestions;

        lstvwSecurityQuestions = (ListView) view.findViewById(R.id.lstvwSecurityQuestions);
        txtvwCancel = (TextView) view.findViewById(R.id.txtvwCancel);

        txtvwCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });


        lstvwSecurityQuestions.setAdapter(listAdapter);

        lstvwSecurityQuestions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {



                if (!listData.get(pos).question.contains("?")) {
                    txtvwSource.setText(listData.get(pos).question + "?");
                    previousQuestion1 = txtvwSource.getText().toString();
                } else {
                    txtvwSource.setText(listData.get(pos).question);
                    previousQuestion1 = txtvwSource.getText().toString();
                }

                LogUtils.Verbose("previousQuestion1",previousQuestion1);

                LogUtils.Verbose("if",previousQuestion1+"==="+txtvwSource.getText().toString());
                if(!txtvwSource.getText().toString().equalsIgnoreCase(previousQuestion1)){

                    if(txtvwSource.getId() == 1){
                        editTextAnswer1.setText("");
                    }else{
                        editTextAnswer2.setText("");
                    }

                }




                // Question 1
                if (txtvwSource.getId() == 1) {
                    questionID1 = tempSecurityQuestions.get(pos).id;
                    editTextAnswer1.setText("");
                    editTextAnswer1.setEnabled(true);




                }
                // Question 2
                else if (txtvwSource.getId() == 2) {
                    questionID2 = tempSecurityQuestions.get(pos).id;
                    editTextAnswer2.setText("");
                    editTextAnswer2.setEnabled(true);



                }


                mBottomSheetDialog.dismiss();


            }
        });

        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before setcontentview
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

    }


    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            String strAnswer1 = editTextAnswer1.getText().toString().trim();
            String strAnswer2 = editTextAnswer2.getText().toString().trim();


            if (TextUtils.isEmpty(questionID1) || TextUtils.isEmpty(questionID2) || TextUtils.isEmpty(strAnswer1) || TextUtils.isEmpty(strAnswer2)) {
                btnSubmit.setEnabled(false);
            } else {
                btnSubmit.setEnabled(true);
            }


        }
    };


    public int getInDp(int value) {
        int valueInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, getResources().getDisplayMetrics());
        return valueInDp;
    }

}
