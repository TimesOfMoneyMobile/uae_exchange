package toml.uaeexchange.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.controllers.AuthorizeSplitBillController;
import toml.movitwallet.controllers.GenerateOTPController;
import toml.movitwallet.controllers.ListSplitBillController;
import toml.movitwallet.controllers.ManagedSplitBillController;
import toml.movitwallet.models.ContactsModel;
import toml.movitwallet.models.SplitBillRequest;
import toml.movitwallet.models.SplitBillRequestReceiver;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.dialogs.ValidateOTPDialog;
import toml.uaeexchange.utilities.Constants;

public class SplitAndShareActivity extends BaseActivity implements BaseActivity.ClickableSpanListener {

    @BindView(R.id.txtvwSeeNotifications)
    TextView txtvwSeeNotifications;
    @BindView(R.id.linlayDebtorListContainer)
    LinearLayout linlayDebtorListContainer;
    @BindView(R.id.linlayDebtorContainer)
    LinearLayout linlayDebtorContainer;
    @BindView(R.id.linlayCreditorContainer)
    LinearLayout linlayCreditorContainer;
    @BindView(R.id.linlayDebtorCreditorContainer)
    LinearLayout linlayDebtorCreditorContainer;
    @BindView(R.id.txtvwDebtorTotal)
    TextView txtvwDebtorTotal;
    @BindView(R.id.txtvwDebtorExpandCollapse)
    TextView txtvwDebtorExpandCollapse;
    @BindView(R.id.linlayCreditorListContainer)
    LinearLayout linlayCreditorListContainer;
    @BindView(R.id.txtvwCreditorTotal)
    TextView txtvwCreditorTotal;
    @BindView(R.id.txtvwCreditorExpandCollapse)
    TextView txtvwCreditorExpandCollapse;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.btnAddSplit)
    CustomButton btnAddSplit;
    @BindView(R.id.txtvwSplitTotalAmount)
    TextView txtvwSplitTotalAmount;
    @BindView(R.id.linlayDebtorHeader)
    LinearLayout linlayDebtorHeader;
    @BindView(R.id.linlayCreditorHeader)
    LinearLayout linlayCreditorHeader;

    double debtorTotal, creditorTotal, splitResult;

    String strOTP = "";

    List<SplitBillRequest> debtorList = new ArrayList<>();
    List<SplitBillRequest> creditorList = new ArrayList<>();
    List<SplitBillRequestReceiver> listCreditorReceivers = new ArrayList<>();

    ValidateOTPDialog otpDialog;
    @BindView(R.id.txtvwAmountMsg)
    TextView txtvwAmountMsg;
    @BindView(R.id.txtvwAED)
    TextView txtvwAED;
    @BindView(R.id.linlaySplitAmountContainer)
    LinearLayout linlaySplitAmountContainer;
    private boolean isSpanClicked;
    private static final String TXN_TYPE = "P2P";
    private static final String AUTHORIZE = "AUTHORIZE";
    private static final String REJECT = "REJECT";


    public static boolean isNeedToRefresh = false;

    // private boolean bIsDialogShowing = false;

    private SmsVerifyCatcher smsVerifyCatcher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split_and_share);
        ButterKnife.bind(this);


        setToolbarWithBalance(getString(R.string.split_and_share), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }, 0, null, false);

        // This code to show and hide button on scroll change

        final ViewTreeObserver.OnScrollChangedListener onScrollChangedListener = new
                ViewTreeObserver.OnScrollChangedListener() {

                    @Override
                    public void onScrollChanged() {
                        if (scrollView.getScrollY() == 0) {
                            btnAddSplit.setVisibility(View.VISIBLE);
                            //btnAddSplit.setEnabled(true);
                        } else {
                            btnAddSplit.setVisibility(View.GONE);
                            //btnAddSplit.setEnabled(false);
                        }
                    }
                };

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            private ViewTreeObserver observer;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (observer == null) {
                    observer = scrollView.getViewTreeObserver();
                    observer.addOnScrollChangedListener(onScrollChangedListener);
                } else if (!observer.isAlive()) {
                    observer.removeOnScrollChangedListener(onScrollChangedListener);
                    observer = scrollView.getViewTreeObserver();
                    observer.addOnScrollChangedListener(onScrollChangedListener);
                }
                return false;
            }
        });


        getAndDisplayDebtorCreditorView();


        linlayDebtorHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txtvwDebtorExpandCollapse.getTag().toString().equalsIgnoreCase("collapse")) {
                    // expandOrCollapse(linlayDebtorListContainer, "expand");
                    linlayDebtorListContainer.setVisibility(View.VISIBLE);
                    txtvwDebtorExpandCollapse.setTag("expand");
                    txtvwDebtorExpandCollapse.setText("-");
                } else if (txtvwDebtorExpandCollapse.getTag().toString().equalsIgnoreCase("expand")) {
                    //expandOrCollapse(linlayDebtorListContainer, "collapse");
                    linlayDebtorListContainer.setVisibility(View.GONE);
                    txtvwDebtorExpandCollapse.setTag("collapse");
                    txtvwDebtorExpandCollapse.setText("+");
                }

            }
        });

        linlayCreditorHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtvwCreditorExpandCollapse.getTag().toString().equalsIgnoreCase("collapse")) {
                    // expandOrCollapse(linlayDebtorListContainer, "expand");
                    linlayCreditorListContainer.setVisibility(View.VISIBLE);
                    txtvwCreditorExpandCollapse.setTag("expand");
                    txtvwCreditorExpandCollapse.setText("-");
                } else if (txtvwCreditorExpandCollapse.getTag().toString().equalsIgnoreCase("expand")) {
                    //expandOrCollapse(linlayDebtorListContainer, "collapse");
                    linlayCreditorListContainer.setVisibility(View.GONE);
                    txtvwCreditorExpandCollapse.setTag("collapse");
                    txtvwCreditorExpandCollapse.setText("+");
                }
            }
        });


        // Debtor Expand Collapse click

        txtvwDebtorExpandCollapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                linlayDebtorHeader.performClick();

            }
        });


        // Creditor Expand Collapse click

        txtvwCreditorExpandCollapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                linlayCreditorHeader.performClick();

            }
        });


        txtvwSeeNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(SplitAndShareActivity.this, NotificationActivity.class);
                intent.putExtra("From", "Split");
                startActivity(intent);
                //BaseActivity.openNewActivity(SplitAndShareActivity.this, NotificationActivity.class.getCanonicalName(), true);
            }
        });

        btnAddSplit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // showError("Recent contacts " + MovitConsumerApp.getInstance().getListRecentContacts().size() + " contacts.");
                if (ContextCompat.checkSelfPermission(SplitAndShareActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SplitAndShareActivity.this, new String[]{Manifest.permission.READ_CONTACTS},
                            2);
                } else {

                    if (MovitConsumerApp.getInstance().getListSelectedContacts() != null) {
                        MovitConsumerApp.getInstance().getListSelectedContacts().clear();
                    }

                    for (ContactsModel objContactsModel : MovitConsumerApp.getInstance().getListRecentContacts()) {
                        if (objContactsModel.isChecked()) {
                            objContactsModel.setChecked(false);
                        }
                    }


                  /*  if (MovitConsumerApp.getInstance().getListRecentContacts() != null) {
                        MovitConsumerApp.getInstance().getListRecentContacts().clear();
                    }*/

                    Intent intent = new Intent(SplitAndShareActivity.this, MultipleContactsListActivity.class);
                    startActivityForResult(intent, 1);

                    //BaseActivity.openNewActivity(SplitAndShareActivity.this, MultipleContactsListActivity.class.getCanonicalName(), true);
                }
            }
        });


        linlayDebtorHeader.performClick();
        linlayCreditorHeader.performClick();


        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = Constants.parseSMSForOTP(message);//Parse verification code
                try {
                    if (otpDialog != null && otpDialog.isShowing()) {
                        otpDialog.setOTPText(code);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //then you can send verification code to server
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (smsVerifyCatcher != null)
            smsVerifyCatcher.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isNeedToRefresh) {
            isNeedToRefresh = false;
            getAndDisplayDebtorCreditorView();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //switch (requestCode) {

        if (requestCode == 2) {

            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                BaseActivity.openNewActivity(SplitAndShareActivity.this, MultipleContactsListActivity.class.getCanonicalName(), true);
            } else {
                showError("Please accept permission to see Contacts list.");
            }
        } else {
            smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        //       break;
        //}
    }


    private void getAndDisplayDebtorCreditorView() {

        linlayDebtorListContainer.removeAllViews();
        linlayCreditorListContainer.removeAllViews();

        // Clear previous values if any
        creditorTotal = 0;
        debtorTotal = 0;
        splitResult = 0;

        linlaySplitAmountContainer.setVisibility(View.INVISIBLE);
        txtvwAmountMsg.setVisibility(View.INVISIBLE);
        txtvwSplitTotalAmount.setVisibility(View.INVISIBLE);

        getDebtorList();

        //  getCreditorList();

    }

    private void getDebtorList() {
        showProgress();

        MovitWalletController listSplitBillController = new ListSplitBillController("ListSplitBill");

        listSplitBillController.init(new IMovitWalletController<List<SplitBillRequest>>() {
            @Override
            public void onSuccess(List<SplitBillRequest> list) {

                // dismissProgress();

                LogUtils.Verbose("ListSplitBill", "" + list.size());

                if (debtorList != null && debtorList.size() > 0) {

                    debtorList.clear();
                }

                debtorList.addAll(list);

                //list.clear();

                if (list != null && list.size() > 0) {
                    for (SplitBillRequest objSplitBillRequest :
                            list) {

                        if (objSplitBillRequest.getStatus().equalsIgnoreCase("PENDING")) {
                            addDebtorView(objSplitBillRequest);
                            debtorTotal = debtorTotal + Double.parseDouble(objSplitBillRequest.getAmount());
                        }
                    }

                }

                if (debtorTotal > 0) {

                    linlayDebtorHeader.setVisibility(View.VISIBLE);

                    txtvwDebtorTotal.setVisibility(View.VISIBLE);
                    txtvwDebtorTotal.setText("- " + getString(R.string.AED) + " " + Constants.formatAmount(String.valueOf(debtorTotal)));
                } else {
                    linlayDebtorHeader.setVisibility(View.GONE);
                }

                splitResult = creditorTotal - debtorTotal;

                getCreditorList();

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                // dismissProgress();

               /* bIsDialogShowing = true;

                new OkDialog(SplitAndShareActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {
                        // getAndDisplayDebtorCreditorView();
                    }
                });*/

                if (debtorTotal > 0) {

                    linlayDebtorHeader.setVisibility(View.VISIBLE);

                    txtvwDebtorTotal.setVisibility(View.VISIBLE);
                    txtvwDebtorTotal.setText("- " + getString(R.string.AED) + " " + Constants.formatAmount(String.valueOf(debtorTotal)));
                } else {
                    linlayDebtorHeader.setVisibility(View.GONE);
                }

                splitResult = creditorTotal - debtorTotal;

                getCreditorList();


            }
        });
    }

    private void getCreditorList() {

        MovitWalletController listSplitBillDetailsController = new ListSplitBillController("SplitBillDetails");

        listSplitBillDetailsController.init(new IMovitWalletController<List<SplitBillRequest>>() {
            @Override
            public void onSuccess(List<SplitBillRequest> list) {

                dismissProgress();


                LogUtils.Verbose("SplitBillDetails", "" + list.size());

                if (creditorList != null && creditorList.size() > 0) {
                    creditorList.clear();

                }

                creditorList.addAll(list);

                //list.addAll(list);
                //list.clear();

                if (list != null && list.size() > 0) {

                    if (listCreditorReceivers != null && listCreditorReceivers.size() > 0) {
                        listCreditorReceivers.clear();
                    }
                    for (SplitBillRequest objSplitBillRequest :
                            list) {


                        addCreditorView(objSplitBillRequest);


                        //   creditorTotal += Double.parseDouble(objSplitBillRequest.getSplitBillRequestReceiver().getSplittedAmount());
                    }

                }

                if (creditorTotal > 0) {

                    linlayCreditorHeader.setVisibility(View.VISIBLE);

                    txtvwCreditorTotal.setVisibility(View.VISIBLE);
                    txtvwCreditorTotal.setText(" + " + getString(R.string.AED) + " " + Constants.formatAmount(String.valueOf(creditorTotal)));

                } else {
                    linlayCreditorHeader.setVisibility(View.GONE);
                }

                splitResult = creditorTotal - debtorTotal;

                // Setting Split Amount here
                linlaySplitAmountContainer.setVisibility(View.VISIBLE);
                txtvwAmountMsg.setVisibility(View.VISIBLE);
                txtvwSplitTotalAmount.setVisibility(View.VISIBLE);

                if (splitResult >= 0) {

                    txtvwAmountMsg.setText("You have to receive");
                    txtvwAED.setTextColor(ContextCompat.getColor(SplitAndShareActivity.this, R.color.aqua_green));
                    txtvwSplitTotalAmount.setTextColor(ContextCompat.getColor(SplitAndShareActivity.this, R.color.aqua_green));
                    txtvwSplitTotalAmount.setText("" + Constants.formatAmount("" + splitResult));
                } else {
                    txtvwAmountMsg.setText("You have to pay");
                    txtvwAED.setTextColor(ContextCompat.getColor(SplitAndShareActivity.this, R.color.grapefruit));
                    txtvwSplitTotalAmount.setTextColor(ContextCompat.getColor(SplitAndShareActivity.this, R.color.grapefruit));
                    txtvwSplitTotalAmount.setText("" + Constants.formatAmount("" + (-splitResult)));
                }

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();


                if (creditorTotal > 0) {

                    linlayCreditorHeader.setVisibility(View.VISIBLE);

                    txtvwCreditorTotal.setVisibility(View.VISIBLE);
                    txtvwCreditorTotal.setText(" + " + getString(R.string.AED) + " " + Constants.formatAmount(String.valueOf(creditorTotal)));

                } else {
                    linlayCreditorHeader.setVisibility(View.GONE);
                }

                splitResult = creditorTotal - debtorTotal;


                // Setting Split Amount here
                linlaySplitAmountContainer.setVisibility(View.VISIBLE);
                txtvwAmountMsg.setVisibility(View.VISIBLE);
                txtvwSplitTotalAmount.setVisibility(View.VISIBLE);

                splitResult = creditorTotal - debtorTotal;

                if (splitResult >= 0) {

                    txtvwAmountMsg.setText("You owed");
                    txtvwAED.setTextColor(ContextCompat.getColor(SplitAndShareActivity.this, R.color.aqua_green));
                    txtvwSplitTotalAmount.setTextColor(ContextCompat.getColor(SplitAndShareActivity.this, R.color.aqua_green));
                    txtvwSplitTotalAmount.setText("" + Constants.formatAmount("" + splitResult));
                } else {
                    txtvwAmountMsg.setText("You are owed");
                    txtvwAED.setTextColor(ContextCompat.getColor(SplitAndShareActivity.this, R.color.grapefruit));
                    txtvwSplitTotalAmount.setTextColor(ContextCompat.getColor(SplitAndShareActivity.this, R.color.grapefruit));
                    txtvwSplitTotalAmount.setText("" + Constants.formatAmount("" + (-splitResult)));
                }


                // To avoid two dialogs

                if (debtorList.size() == 0 && creditorList.size() == 0) {
                    new OkDialog(SplitAndShareActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
                        @Override
                        public void handleResponse() {
                            //getAndDisplayDebtorCreditorView();
                        }
                    });


                }

               /* if (bIsDialogShowing) {
                    bIsDialogShowing = false;
                } else {


                }*/


                // showError(reason);

            }
        });
    }

    public void addDebtorView(final SplitBillRequest objSplitBillRequest) {

        final View parent = LayoutInflater.from(this).inflate(R.layout.debtor_list_item, linlayDebtorListContainer, false);

        final SwipeLayout swipeLayout = ButterKnife.findById(parent, R.id.swipe);
        ImageView imgvwUser = ButterKnife.findById(parent, R.id.imgvwUser);
        TextView txtvwName = ButterKnife.findById(parent, R.id.txtvwName);
        TextView txtvwMobileNumber = ButterKnife.findById(parent, R.id.txtvwMobileNumber);
        TextView txtvwAmount = ButterKnife.findById(parent, R.id.txtvwAmount);
        TextView txtvwAccept = ButterKnife.findById(parent, R.id.txtvwAccept);
        TextView txtvwDecline = ButterKnife.findById(parent, R.id.txtvwDecline);

        ContactsModel objContactsModel = new ContactsModel();
        objContactsModel.setName(objSplitBillRequest.getFirstName() + " " + objSplitBillRequest.getLastName());
        objContactsModel.setNumber(objSplitBillRequest.getMobileNumber());

        if (!MovitConsumerApp.getInstance().getListRecentContacts().contains(objContactsModel)) {
            MovitConsumerApp.getInstance().getListRecentContacts().add(objContactsModel);
        }

        txtvwName.setText(objSplitBillRequest.getFirstName() + " " + objSplitBillRequest.getLastName());
        txtvwMobileNumber.setText(getString(R.string.country_code) + " " + objSplitBillRequest.getMobileNumber());
        txtvwAmount.setText(" - " + getString(R.string.AED) + " " + Constants.formatAmount(objSplitBillRequest.getAmount()));

        final boolean isClose = false;

      /*  parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isClose) {
                    swipeLayout.open();

                }


            }
        });*/

        txtvwAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (Constants.checkToShowOtpDialog(SplitAndShareActivity.this, objSplitBillRequest.getAmount())) {
                    callGenerateOTP(isSpanClicked, objSplitBillRequest);
                } else {
                    callAuthorizeAPI(objSplitBillRequest, strOTP, AUTHORIZE);
                }


            }
        });


        txtvwDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callAuthorizeAPI(objSplitBillRequest, strOTP, REJECT);
            }
        });

        linlayDebtorListContainer.addView(parent);


    }

    OnSmsCatchListener onSmsCatchListener = new OnSmsCatchListener<String>() {
        @Override
        public void onSmsCatch(String message) {
            String code = Constants.parseSMSForOTP(message);//Parse verification code
            try {
                if (otpDialog != null && otpDialog.isShowing()) {
                    otpDialog.setOTPText(code);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    public void addCreditorView(final SplitBillRequest objSplitBillRequest) {


        if (objSplitBillRequest.getListReceivers() != null) {

            for (SplitBillRequestReceiver objReceiver : objSplitBillRequest.getListReceivers()) {

                if (objReceiver.getStatus().equalsIgnoreCase("PENDING")) {

                    final View parent = LayoutInflater.from(this).inflate(R.layout.creditor_list_item, linlayCreditorListContainer, false);

                    ImageView imgvwUser = ButterKnife.findById(parent, R.id.imgvwUser);
                    TextView txtvwName = ButterKnife.findById(parent, R.id.txtvwName);
                    TextView txtvwMobileNumber = ButterKnife.findById(parent, R.id.txtvwMobileNumber);
                    TextView txtvwAmount = ButterKnife.findById(parent, R.id.txtvwAmount);
                    TextView txtvwCancel = ButterKnife.findById(parent, R.id.txtvwCancel);
                    SwipeLayout swipeLayout = ButterKnife.findById(parent, R.id.swipe);


                    listCreditorReceivers.add(objReceiver);

                    objSplitBillRequest.setSplitBillRequestReceiver(objReceiver);
                    ContactsModel objContactsModel = new ContactsModel();

                    objContactsModel.setName(objSplitBillRequest.getSplitBillRequestReceiver().getFirstName() + " " + objSplitBillRequest.getSplitBillRequestReceiver().getLastName());
                    objContactsModel.setNumber(objSplitBillRequest.getSplitBillRequestReceiver().getMobileNumber());


      /*  ContactsModel objContactsModel = new ContactsModel();
        objContactsModel.setName(objSplitBillRequest.getSplitBillRequestReceiver().getFirstName() + " " + objSplitBillRequest.getSplitBillRequestReceiver().getLastName());
        objContactsModel.setNumber(objSplitBillRequest.getSplitBillRequestReceiver().getMobileNumber());*/

                    if (!MovitConsumerApp.getInstance().getListRecentContacts().contains(objContactsModel)) {
                        MovitConsumerApp.getInstance().getListRecentContacts().add(objContactsModel);
                    }

                    txtvwName.setText(objSplitBillRequest.getSplitBillRequestReceiver().getFirstName() + " " + objSplitBillRequest.getSplitBillRequestReceiver().getLastName());
                    txtvwMobileNumber.setText(getString(R.string.country_code) + " " + objSplitBillRequest.getSplitBillRequestReceiver().getMobileNumber());
                    txtvwAmount.setText(" + " + getString(R.string.AED) + " " + Constants.formatAmount(objSplitBillRequest.getSplitBillRequestReceiver().getSplittedAmount()));


                    creditorTotal += Double.parseDouble(objSplitBillRequest.getSplitBillRequestReceiver().getSplittedAmount());


                    txtvwAmount.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //showError("Amount Clicked");
                        }
                    });
                    txtvwCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            List<SplitBillRequestReceiver> listReceivers = new ArrayList<>();

                            int index = ((ViewGroup) parent.getParent()).indexOfChild(parent);

                            listReceivers.add(listCreditorReceivers.get(index));

                            callManagedSplitBillRequest(objSplitBillRequest, listReceivers);


                        }
                    });

                    linlayCreditorListContainer.addView(parent);
                }
            }
        }

    }

    private void callAuthorizeAPI(SplitBillRequest objSplitBillRequest, String strOTP, String ReqType) {
        showProgress();
        MovitWalletController authorizeSplitBillController = new AuthorizeSplitBillController(objSplitBillRequest.getId(), strOTP, ReqType);
        authorizeSplitBillController.init(new IMovitWalletController<Bundle>() {
            @Override
            public void onSuccess(final Bundle response) {

                dismissProgress();

                if (otpDialog != null) {
                    otpDialog.dismiss();
                }
                //showError(response.getString(IntentConstants.Message));

                new OkDialog(SplitAndShareActivity.this, null, response.getString(IntentConstants.Message), new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                        getAndDisplayDebtorCreditorView();

                    }
                });

                //  linlayDebtorContainer.removeView(parent);


            }

            @Override
            public void onFailed(String errorCode, String reason) {

                dismissProgress();
                new OkDialog(SplitAndShareActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                    }
                });
                //showError(reason);

            }
        });
    }

    private void callManagedSplitBillRequest(SplitBillRequest objSplitBillRequest, List<SplitBillRequestReceiver> listReceivers) {
        showProgress();
        MovitWalletController managedSplitBillController = new ManagedSplitBillController(objSplitBillRequest.getId(), listReceivers);
        managedSplitBillController.init(new IMovitWalletController<Bundle>() {
            @Override
            public void onSuccess(Bundle response) {

                dismissProgress();
                new OkDialog(SplitAndShareActivity.this, null, response.getString(IntentConstants.Message), new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {
                        getAndDisplayDebtorCreditorView();
                    }
                });
                //showError(response.getString(IntentConstants.Message));

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                new OkDialog(SplitAndShareActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                    }
                });
                //showError(reason);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {


            if (requestCode == 1) {
                Intent intent = new Intent(SplitAndShareActivity.this, BillSetupActivity.class);
                startActivity(intent);

            }
        }

    }

    private void callGenerateOTP(final boolean isSpanClicked, final SplitBillRequest objSplitBillRequest) {
        showProgress();

        MovitWalletController generateOTPController = new GenerateOTPController(TXN_TYPE);

        generateOTPController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                dismissProgress();


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String code = "";
                        try {
                            if (isSpanClicked) {
                                if (otpDialog.isShowing()) {
                                    otpDialog.refreshOnResend();
                                }
                            } else {
                                showOTPDialog(objSplitBillRequest);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }, 200);


            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();

                new OkDialog(SplitAndShareActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                    }
                });
                // showError(reason);

            }
        });
        if (isSpanClicked) {
            this.isSpanClicked = false;
        }
    }

    private void showOTPDialog(final SplitBillRequest objSplitBillRequest) {

        otpDialog = new ValidateOTPDialog(this, new ValidateOTPDialog.IValidateOTPDialogCallBack() {


            @Override
            public void resendOTPCallBack(String otpValue) {

            }

            @Override
            public void verifyOTPCallBack(String otpValue) {
                // verifyOTP(otpValue);

                // showError("Authorize success");
                // otpDialog.dismiss();
                strOTP = otpValue;
                callAuthorizeAPI(objSplitBillRequest, strOTP, AUTHORIZE);
            }
        });
        otpDialog.show();

    }


    public void expandOrCollapse(final View v, String exp_or_colpse) {
        TranslateAnimation anim = null;
        if (exp_or_colpse.equals("expand")) {
            anim = new TranslateAnimation(0.0f, 0.0f, -v.getHeight(), 0.0f);
            v.setVisibility(View.VISIBLE);
        } else {
            anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, -v.getHeight());
            Animation.AnimationListener collapselistener = new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    v.setVisibility(View.GONE);
                }
            };

            anim.setAnimationListener(collapselistener);
        }

        // To Collapse
        //

        anim.setDuration(300);
        anim.setInterpolator(new AccelerateInterpolator(0.5f));
        v.startAnimation(anim);
    }

    @Override
    public void onSpanClick(String strText) {
        isSpanClicked = true;
        callGenerateOTP(isSpanClicked, null);

    }
}
