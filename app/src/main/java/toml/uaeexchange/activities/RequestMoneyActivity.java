package toml.uaeexchange.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.FavouritesController;
import toml.movitwallet.models.Beneficiary;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.adapters.FavouritesAdapter;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;
import toml.movitwallet.controllers.AskMoneyController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.uaeexchange.utilities.TwoDecimalInputFilter;

public class RequestMoneyActivity extends BaseActivity {

    private static final int REQUEST_CODE = 1;

    @BindView(R.id.relSettings)
    RelativeLayout relSettings;
    @BindView(R.id.tvSwitchName)
    TextView tvSwitch;


    @BindView(R.id.act_rm_sv_for_content)
    ScrollView svForContent;

    @BindView(R.id.act_rm_el_MobileNumber)
    ErrorLabelLayout elMobileNumber;
    @BindView(R.id.act_rm_et_MobileNumber)
    CustomEditTextLayout etMobileNumber;

    @BindView(R.id.act_rm_el_Amount)
    ErrorLabelLayout elAmount;
    @BindView(R.id.act_rm_et_Amount)
    CustomEditTextLayout etAmount;

    @BindView(R.id.act_rm_el_NickName)
    ErrorLabelLayout elNickName;
    @BindView(R.id.act_rm_et_NickName)
    CustomEditTextLayout etNickName;

    @BindView(R.id.act_rm_rl_favourite)
    RelativeLayout rlFavourite;
    @BindView(R.id.act_rm_iv_favourite)
    ImageView ivFavourite;
    boolean isFavChecked;

    @BindView(R.id.act_rm_cb_proceed)
    Button cbProceed;

    @BindView(R.id.act_rm_lv_favourites)
    ListView lvFavourites;
    @BindView(R.id.act_rm_tv_no_favourites)
    TextView tvNoFavourites;
    @BindView(R.id.act_rm_pb_progressBar)
    ProgressBar pbProgress;


    String strMobileNumber, strAmount, strNickname, strSource;
    String favNumber, favAmount, favNickName;
    private boolean perfromOnlyOnce;


    GenericValidations genericValidations;
    FavouritesAdapter favouritesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_money);
        setToolBarWithRightText(getString(R.string.requestmoney), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.openNewActivity(RequestMoneyActivity.this, PendingRequestActivity.class.getCanonicalName(), true);
            }
        });
        ButterKnife.bind(this);


        handleLayout();

    }

    private void handleLayout() {
        tvSwitch.setText(getString(R.string.pendingrequest));


        genericValidations = new GenericValidations(RequestMoneyActivity.this);


        etMobileNumber.getImgLeft().setVisibility(View.GONE);
        etMobileNumber.getVerticalDivider().setVisibility(View.GONE);

        etMobileNumber.getImgRight().setImageResource(R.drawable.contacts);
        etMobileNumber.getImgRight().setVisibility(View.VISIBLE);

        etMobileNumber.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etMobileNumber.setErrorLabelLayout(elMobileNumber);
        etMobileNumber.getEditText().addTextChangedListener(textWatcher);

        etMobileNumber.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etAmount.getEditText().requestFocus();
                    return true;
                }
                return false;
            }
        });

        etMobileNumber.getImgRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show contacts list
                // SendMoneyFragmentPermissionsDispatcher.showContactListWithCheck(SendMoneyFragment.this);

                if (ContextCompat.checkSelfPermission(RequestMoneyActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(RequestMoneyActivity.this, new String[]{Manifest.permission.READ_CONTACTS},
                            1);
                } else {
                    showContactList();
                }

            }
        });


        etAmount.getImgLeft().setVisibility(View.GONE);
        etAmount.getVerticalDivider().setVisibility(View.GONE);

        etAmount.setErrorLabelLayout(elAmount);
        if (elNickName.getVisibility() == View.GONE) {
            etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        }
        etAmount.getEditText().addTextChangedListener(textWatcherAmount);
        etAmount.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etNickName.requestFocus();
                    return true;
                }
                return false;
            }
        });
        if (!TextUtils.isEmpty(MovitConsumerApp.getInstance().getMaxAmount())) {
            etAmount.getEditText().setFilters(new InputFilter[]{new TwoDecimalInputFilter(MovitConsumerApp.getInstance().getMaxAmount().length())
            });
        }


        etNickName.getImgLeft().setVisibility(View.GONE);
        etNickName.setErrorLabelLayout(elNickName);
        etNickName.getVerticalDivider().setVisibility(View.GONE);
        etNickName.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        etNickName.getEditText().addTextChangedListener(textWatcherNickName);


        pbProgress.setVisibility(View.VISIBLE);
        tvNoFavourites.setVisibility(View.GONE);

        callFavouritesApi();

    }

    private void callFavouritesApi() {
        MovitWalletController movitWalletController = new FavouritesController("REQ");
        movitWalletController.init(new IMovitWalletController<List<Beneficiary>>() {


            @Override
            public void onSuccess(List<Beneficiary> response) {
                setFavAdapter(response, "", "");
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                setFavAdapter(null, errorCode, reason);

            }
        });
    }

    public void setFavAdapter(List<Beneficiary> response, String errorCode, String reason) {

        if (response != null) {


            pbProgress.setVisibility(View.GONE);
            tvNoFavourites.setVisibility(View.GONE);

            favouritesAdapter = new FavouritesAdapter(this, response);
            lvFavourites.setAdapter(favouritesAdapter);

        } else {

            pbProgress.setVisibility(View.GONE);
            tvNoFavourites.setVisibility(View.VISIBLE);
            tvNoFavourites.setText(reason);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case 1:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showContactList();
                } else {
                    showError("Please accept permission to see Contacts list.");
                }

                break;
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (TextUtils.isEmpty(charSequence)) {
                etMobileNumber.setBackgroundResource(R.drawable.pinkish_grey_border);
            }

            if (charSequence.toString().length() == getResources().getInteger(R.integer.mobileNoLength)) {
                etAmount.requestFocus();
            } else {


                if (perfromOnlyOnce) {
                    perfromOnlyOnce = false;


                    Constants.hideKeyboard(RequestMoneyActivity.this);


                    etMobileNumber.clearFocus();
                    etAmount.clearFocus();


                    etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);

                    ivFavourite.setImageResource(R.drawable.favourites_unselected);
                    rlFavourite.setEnabled(true);
                    isFavChecked = false;
                    favNickName = "";
                    elNickName.setVisibility(View.GONE);
                    etNickName.setText(favNickName);

                }

            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{etMobileNumber, etAmount, etNickName})) {
                cbProceed.setEnabled(true);
            } else {
                cbProceed.setEnabled(false);

            }
        }
    };

    private TextWatcher textWatcherAmount = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            if (!TextUtils.isEmpty(charSequence)) {
                etAmount.getEtCountryCode().setText(getString(R.string.AED));
                etAmount.getEtCountryCode().setVisibility(View.VISIBLE);

            } else {
                etAmount.getEtCountryCode().setVisibility(View.GONE);
                etAmount.setBackgroundResource(R.drawable.pinkish_grey_border);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{etMobileNumber, etAmount, etNickName})) {
                cbProceed.setEnabled(true);
            } else {
                cbProceed.setEnabled(false);

            }

        }
    };

    private TextWatcher textWatcherNickName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            if (TextUtils.isEmpty(charSequence)) {
                etNickName.setBackgroundResource(R.drawable.pinkish_grey_border);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{etMobileNumber, etAmount, etNickName})) {
                cbProceed.setEnabled(true);
            } else {
                cbProceed.setEnabled(false);

            }

        }
    };


    @OnClick({R.id.relSettings, R.id.act_rm_rl_favourite, R.id.act_rm_cb_proceed})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relSettings:
                BaseActivity.openNewActivity(this, PendingRequestActivity.class.getCanonicalName(), true);
                break;

            case R.id.act_rm_rl_favourite:

                etMobileNumber.clearFocus();
                etAmount.clearFocus();
                etNickName.clearFocus();

                if (isFavChecked) {
                    elNickName.setVisibility(View.GONE);
                    // if (TextUtils.isEmpty(strSource)) {
                    etNickName.setText(null);
                    //}

                    etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
                    isFavChecked = false;
                    ivFavourite.setImageResource(R.drawable.favourites_unselected);
                    strNickname = "";

                } else {


                    etNickName.requestFocus();
                    etNickName.setText(favNickName);
                    elNickName.setVisibility(View.VISIBLE);
                    etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
                    ivFavourite.setImageResource(R.drawable.favourites_selected);
                    isFavChecked = true;
                    strNickname = etNickName.getText();


                    svForContent.post(new Runnable() {
                        @Override
                        public void run() {
                            svForContent.setSmoothScrollingEnabled(true);
                            svForContent.smoothScrollTo(0, (int) cbProceed.getY());
                        }
                    });
                }

                break;
            case R.id.act_rm_cb_proceed:

                strMobileNumber = etMobileNumber.getEditText().getText().toString();
                strAmount = etAmount.getEditText().getText().toString();
                strNickname = etNickName.getEditText().getText().toString();


                if (isFavChecked) {
                    if (genericValidations.valdiatePayPerson(strNickname, etNickName, strMobileNumber, etMobileNumber, strAmount, etAmount, "")) {
                        //  BaseActivity.openNewActivity(mContext,ReviewAndConfirmActivity.class.getCanonicalName(),false);

//                        if ((Float.valueOf(strAmount) > Float.valueOf(MovitConsumerApp.getInstance().getMaxAmount()))) {
//                            etAmount.getErrorLabelLayout().setError("Please enter amount less than " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount()));
//                           // new OkDialog(mContext, "", "Please enter amount less than " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount()), null);
//                        } else if ((Float.valueOf(strAmount) < Float.valueOf(MovitConsumerApp.getInstance().getMinAmount()))) {
//                            etAmount.getErrorLabelLayout().setError("Please enter amount greater than " + MovitConsumerApp.getInstance().getMinAmount());
//                           // new OkDialog(mContext, "", "Please enter amount greater than " + MovitConsumerApp.getInstance().getMinAmount(), null);
//                        } else


                        if (Double.valueOf(strAmount) > Double.valueOf(MovitConsumerApp.getInstance().getWalletBalance())) {
                            new OkDialog(RequestMoneyActivity.this, "", getString(R.string.amount_should_be_less_than_balance), null);
                        } else {
                            Bundle bundle = new Bundle();
                            Intent intent = new Intent(this, ReviewAndConfirmActivity.class);
                            bundle.putString("txnType", "REQ");
                            bundle.putString("amount", strAmount);
                            bundle.putString("mobile_number", strMobileNumber);
                            bundle.putString("nick_name", strNickname);
                            bundle.putBoolean("isFav", isFavChecked);
                            intent.putExtras(bundle);
                            this.startActivity(intent);
                        }
                    }

                } else {


                    if (genericValidations.valdiatePayPerson("", strMobileNumber, etMobileNumber, strAmount, etAmount, "")) {

//                        if ((Float.valueOf(strAmount) > Float.valueOf(MovitConsumerApp.getInstance().getMaxAmount()))) {
//                            new OkDialog(mContext, "", "Please enter amount less than " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount()), null);
//                        } else if ((Float.valueOf(strAmount) < Float.valueOf(MovitConsumerApp.getInstance().getMinAmount()))) {
//                            new OkDialog(mContext, "", "Please enter amount greater than " + MovitConsumerApp.getInstance().getMinAmount(), null);
//                        } else

                        if (Double.valueOf(strAmount) > Double.valueOf(MovitConsumerApp.getInstance().getWalletBalance())) {
                            new OkDialog(this, "", getString(R.string.amount_should_be_less_than_balance), null);
                        } else {
                            Bundle bundle = new Bundle();
                            Intent intent = new Intent(this, ReviewAndConfirmActivity.class);
                            bundle.putString("txnType", "REQ");
                            bundle.putString("amount", strAmount);
                            bundle.putString("mobile_number", strMobileNumber);
                            bundle.putString("nick_name", strNickname);
                            bundle.putBoolean("isFav", isFavChecked);
                            intent.putExtras(bundle);
                            this.startActivity(intent);
                        }
                    }
                }

                break;
        }

    }

    public void showContactList() {
        Intent intent = new Intent(this, ContactsListActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                setValuesToEditText(data.getStringExtra("number"), "", data.getStringExtra("name"), data.getBooleanExtra("isFav", false), data.getStringExtra("source"));

            }
        }
    }

    public void setValuesToEditText(String number, String amount, String nickname, boolean isFav, String source) {
        perfromOnlyOnce = true;

        if (!TextUtils.isEmpty(number)) {
            favNumber = number;
            etMobileNumber.setText(favNumber);
        }


        if (!TextUtils.isEmpty(amount.trim()) && source.equalsIgnoreCase("recentTransactions"))
            etAmount.setText(amount);
        else {
            etAmount.setText("");
            etAmount.requestFocus();
        }

        if (!TextUtils.isEmpty(nickname) && source.equalsIgnoreCase("favourites"))
            favNickName = nickname;
        else
            favNickName = "";


        if (!TextUtils.isEmpty(source))
            strSource = source;


//        ivFavourite.setImageResource(R.drawable.favourites_unselected);
//        isFavChecked = false;
//        strNickname = "";
        etMobileNumber.clearFocus();
        etAmount.clearFocus();
        etNickName.clearFocus();
        if (isFav) {


            etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
            elNickName.setVisibility(View.VISIBLE);
            etNickName.setText(favNickName);
            ivFavourite.setImageResource(R.drawable.favourites_selected);
            rlFavourite.setEnabled(false);
            isFavChecked = true;


            svForContent.post(new Runnable() {
                @Override
                public void run() {
                    svForContent.setSmoothScrollingEnabled(true);
                    svForContent.smoothScrollTo(0, (int) cbProceed.getY());
                }
            });

        } else {
            etAmount.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);

            etNickName.setText(favNickName);
            elNickName.setVisibility(View.GONE);

            ivFavourite.setImageResource(R.drawable.favourites_unselected);
            rlFavourite.setEnabled(true);
            isFavChecked = false;
        }

    }


    private void callAskMoneyApi() {

        showProgress();

//        AskMoneyController askMoneyController = new AskMoneyController("", strMobile, strAmount, strTpin);
//        askMoneyController.init(new IMovitWalletController() {
//            @Override
//            public void onSuccess(Object response) {
//                handleSuccess(response);
//            }
//
//            @Override
//            public void onFailed(String errorCode, String reason) {
//                dismissProgress();
//                showError(reason);
//
//            }
//        });

    }

    private void handleSuccess(Object response) {


        dismissProgress();

    }


}
