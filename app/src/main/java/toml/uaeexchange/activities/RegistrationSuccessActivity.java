package toml.uaeexchange.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;

import toml.movitwallet.models.RegistrationRequest;
import toml.movitwallet.utils.IntentConstants;
import toml.uaeexchange.R;
import toml.uaeexchange.utilities.Constants;


public class RegistrationSuccessActivity extends BaseActivity {

    Button btnProceedToKYC, btnSkip;

    RegistrationRequest objRegistrationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_success);


        btnProceedToKYC = (Button) findViewById(R.id.btnProceedToKYC);
        btnSkip = (Button) findViewById(R.id.btnSkip);

        if (getIntent().hasExtra(IntentConstants.REG_REQUEST)) {

            objRegistrationRequest = getIntent().getExtras().getParcelable(IntentConstants.REG_REQUEST);
        }

        btnProceedToKYC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegistrationSuccessActivity.this, KYCRegistrationActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(IntentConstants.REG_REQUEST, objRegistrationRequest);
                intent.putExtras(bundle);
                startActivity(intent);
                // finish();

            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegistrationSuccessActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(IntentConstants.FROM_ACTIVITY_REGISTRATION, Constants.RegistrationSuccessActivity);
                startActivity(intent);
                //BaseActivity.openNewActivity(RegistrationSuccessActivity.this, LoginActivity.class.getCanonicalName(), true);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        openQuitDialog();
    }


    private void openQuitDialog() {


        AlertDialog.Builder quitDialog
                = new AlertDialog.Builder(RegistrationSuccessActivity.this);
        quitDialog.setTitle("Confirm to Quit?");

        quitDialog.setPositiveButton("OK, Quit!", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                //WelcomeActivity.super.onBackPressed();
                finish();

            }
        });

        quitDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        });

        quitDialog.show();
    }
}
