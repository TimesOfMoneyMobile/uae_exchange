package toml.uaeexchange.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.uaeexchange.R;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.utilities.GenericValidations;
import toml.movitwallet.controllers.ForgotTpinController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.MovitWalletController;

/**
 * Created by vishwanathp on 7/31/2017.
 */

public class ForgotTpinActivity extends BaseActivity {

    @BindView(R.id.act_ft_et_mobile)
    EditText etMobile;
    @BindView(R.id.act_ft_et_email)
    EditText etEmail;
    @BindView(R.id.act_ft_et_mpin)
    EditText etMpin;
    @BindView(R.id.act_ft_b_submit)
    Button bSubmit;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_tpin);

        setToolbar("Forgot TPIN", 0, 0, null, null);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.act_ft_b_submit)
    public void onClickOfSubmit() {
        String strMobile = etMobile.getText().toString();
        String strEmail = etEmail.getText().toString();
        String strMpin = etMpin.getText().toString();

        GenericValidations genericValidations = new GenericValidations(ForgotTpinActivity.this);

        if (!genericValidations.validateForgotTpin(strMobile, strEmail, strMpin)) {

            showError(genericValidations.getErrorMessage());

        } else {
            showProgress();
            MovitWalletController movitWalletController = new ForgotTpinController(strMobile, strEmail, strMpin);
            movitWalletController.init(new IMovitWalletController<Bundle>() {
                @Override
                public void onSuccess(Bundle response) {

                    new OkDialog(ForgotTpinActivity.this, response.getString("Message"), null);
                    dismissProgress();
                }

                @Override
                public void onFailed(String errorCode, String reason) {
                    dismissProgress();
                    showError(reason);
                }
            });
        }

    }

}
