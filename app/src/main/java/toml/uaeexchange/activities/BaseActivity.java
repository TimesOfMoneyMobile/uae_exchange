package toml.uaeexchange.activities;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.ShakeDetector;
import toml.uaeexchange.R;
import toml.uaeexchange.utilities.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by kunalk on 7/6/2017.
 */

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    ClickableSpanListener objClickableSpanListener;


    public interface ClickableSpanListener {
        void onSpanClick(String strText);
    }

    public void setClickableSpanListener(ClickableSpanListener listener) {
        objClickableSpanListener = listener;
    }


    // pass context to Calligraphy
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        // registerReceiver(broadcastReceiver, new IntentFilter("Invalid_Session"));
//        ShakeDetector.create(this, new ShakeDetector.OnShakeListener() {
//            @Override
//            public void OnShake() {
//                Toast.makeText(getApplicationContext(), "Device shaken!", Toast.LENGTH_SHORT).show();
//
//                AppLogger.launchSendLogWithAttachment(BaseActivity.this);
//
//                /*if (!AppLogger.toggleLogging(DashboardActivity.this)) {
//                    AppLogger.launchSendLogWithAttachment(DashboardActivity.this);
//
//                    //item.setTitle("Start logging");
//                } else {
//                    //item.setTitle("Stop logging");
//                }*/
//
//            }
//        });

    }

    public void setupUI(View view) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Constants.hideKeyboard(BaseActivity.this);
                    return false;
                }
            });
        }
        //If a layout container, iterate over children and seed recursion.
      /*  if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }*/
    }


    public void setToolbar(String titleText) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_pay);
        setTitle(null);
        if (!TextUtils.isEmpty(titleText)) {
            TextView title = (TextView) toolbar.findViewById(R.id.title_pay);
            title.setText(titleText);
        } else if (titleText.equalsIgnoreCase(getString(R.string.menu_nearby))) {
            RelativeLayout relSettings = (RelativeLayout) findViewById(R.id.relSettings);
            relSettings.setVisibility(View.VISIBLE);
        }

        setSupportActionBar(toolbar);


    }


    public void setToolbarWithBalance(String titleText, int leftImageResourceId, View.OnClickListener leftClickListener, int rightImageResourceId, View.OnClickListener rightClickListener, boolean isBalanceRequired) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_pay_person);
        setTitle(null);
        if (!TextUtils.isEmpty(titleText)) {
            TextView title = (TextView) toolbar.findViewById(R.id.toolbar_pay_person_title);
            title.setText(titleText);
        } else if (titleText.equalsIgnoreCase(getString(R.string.menu_nearby))) {
            RelativeLayout relSettings = (RelativeLayout) findViewById(R.id.relSettings);
            relSettings.setVisibility(View.VISIBLE);
        }

        setSupportActionBar(toolbar);


        RelativeLayout relLeftImage = (RelativeLayout) toolbar.findViewById(R.id.relLeftIamge);
        ImageView imgvwLeft = (ImageView) findViewById(R.id.imgvwLeft);

        RelativeLayout relRightImage = (RelativeLayout) toolbar.findViewById(R.id.relRightIamge);
        ImageView imgvwRight = (ImageView) findViewById(R.id.imgvwRight);

        RelativeLayout relBalance = (RelativeLayout) findViewById(R.id.relBalance);
        TextView txtBalance = (TextView) findViewById(R.id.txt_pay_Balance);

        if (isBalanceRequired) {

            if (!TextUtils.isEmpty(MovitConsumerApp.getInstance().getWalletBalance())) {
                relBalance.setVisibility(View.VISIBLE);
                txtBalance.setText(getString(R.string.AED) + " " + Constants.formatAmount(MovitConsumerApp.getInstance().getWalletBalance()));
            } else {
                relBalance.setVisibility(View.GONE);
            }
        } else {
            relBalance.setVisibility(View.GONE);
        }

        // For left image

        if (leftImageResourceId > 0) {
            imgvwLeft.setVisibility(View.VISIBLE);
            imgvwLeft.setImageResource(leftImageResourceId);

            if (imgvwLeft != null) {
                relLeftImage.setOnClickListener(leftClickListener);
            }
        } else {
            relLeftImage.setVisibility(View.INVISIBLE);
        }

        // For right  image

        if (rightImageResourceId > 0) {
            relRightImage.setVisibility(View.VISIBLE);
            imgvwRight.setVisibility(View.VISIBLE);
            imgvwRight.setImageResource(rightImageResourceId);

            if (imgvwRight != null) {
                relRightImage.setOnClickListener(rightClickListener);
            }
        } else {
            relRightImage.setVisibility(View.GONE);
        }


    }


    public void setToolbarWithDoneButton(String titleText, int leftImageResourceId, View.OnClickListener leftClickListener, View.OnClickListener doneButtonClickListener) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_pay_person);
        setTitle(null);
        if (!TextUtils.isEmpty(titleText)) {
            TextView title = (TextView) toolbar.findViewById(R.id.toolbar_pay_person_title);
            title.setText(titleText);
        } else if (titleText.equalsIgnoreCase(getString(R.string.menu_nearby))) {
            RelativeLayout relSettings = (RelativeLayout) findViewById(R.id.relSettings);
            relSettings.setVisibility(View.VISIBLE);
        }

        setSupportActionBar(toolbar);


        RelativeLayout relLeftImage = (RelativeLayout) toolbar.findViewById(R.id.relLeftIamge);
        ImageView imgvwLeft = (ImageView) findViewById(R.id.imgvwLeft);

        RelativeLayout relRightImage = (RelativeLayout) toolbar.findViewById(R.id.relRightIamge);
        ImageView imgvwRight = (ImageView) findViewById(R.id.imgvwRight);

        Button btnDone = (Button) toolbar.findViewById(R.id.btnDoneToolbar);
        btnDone.setVisibility(View.VISIBLE);

        btnDone.setOnClickListener(doneButtonClickListener);


        // For left image

        if (leftImageResourceId > 0) {

            imgvwLeft.setImageResource(leftImageResourceId);

            if (imgvwLeft != null) {
                relLeftImage.setOnClickListener(leftClickListener);
            }
        } else {
            relLeftImage.setVisibility(View.INVISIBLE);
        }


    }


    public void setToolbar(String titleText, int leftImageResourceId, int rightImageResourceId, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_pay);
        setTitle(null);
        if (!TextUtils.isEmpty(titleText)) {
            TextView title = (TextView) toolbar.findViewById(R.id.title_pay);
            title.setText(titleText);
        } else if (titleText.equalsIgnoreCase(getString(R.string.menu_nearby))) {
            RelativeLayout relSettings = (RelativeLayout) findViewById(R.id.relSettings);
            relSettings.setVisibility(View.VISIBLE);
        }

        setSupportActionBar(toolbar);

        ImageView imgvwLeft, imgvwRight;

        imgvwLeft = (ImageView) findViewById(R.id.imgvwLeft);
        imgvwRight = (ImageView) findViewById(R.id.imgvwRight);

        // For left image

        if (imgvwLeft != null) {

            if (leftImageResourceId > 0) {

                imgvwLeft.setImageResource(leftImageResourceId);

                if (imgvwLeft != null) {
                    imgvwLeft.setOnClickListener(leftClickListener);
                }
            } else {
                imgvwLeft.setVisibility(View.INVISIBLE);
            }
        }

        // For right  image
        if (imgvwRight != null) {
            if (rightImageResourceId > 0) {

                imgvwRight.setImageResource(rightImageResourceId);

                if (imgvwRight != null) {
                    imgvwRight.setOnClickListener(rightClickListener);
                }
            } else {
                imgvwRight.setVisibility(View.INVISIBLE);
            }
        }


    }

    public void setToolBarWithRightText(String titleText, int leftImageResourceId, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_right_text);
        setTitle(null);
        if (!TextUtils.isEmpty(titleText)) {
            TextView title = (TextView) toolbar.findViewById(R.id.title);
            title.setText(titleText);
        }

        setSupportActionBar(toolbar);


        RelativeLayout relLeftImage = (RelativeLayout) toolbar.findViewById(R.id.relBack);
        ImageView imgvwLeft = (ImageView) findViewById(R.id.imgvwLeft);

        // For left image

        if (leftImageResourceId > 0) {
            imgvwLeft.setVisibility(View.VISIBLE);
            imgvwLeft.setImageResource(leftImageResourceId);

            if (imgvwLeft != null) {
                relLeftImage.setOnClickListener(leftClickListener);
            }
        } else {
            relLeftImage.setVisibility(View.INVISIBLE);
        }


        RelativeLayout relSettings = (RelativeLayout) toolbar.findViewById(R.id.relSettings);
        relSettings.setOnClickListener(rightClickListener);
    }


    /**
     * @param textView
     * @param fulltext
     * @param spannable
     * @param subtext
     * @param color
     */

    public void setColorAndClick(TextView textView, String fulltext, Spannable spannable, final String subtext, int color) {

        int i = fulltext.indexOf(subtext);
        // Note - First clickable span and then foreground colop span. Sequence is IMP.
        if (!subtext.equalsIgnoreCase("Click here") || (subtext.equalsIgnoreCase("‘Wayve’") || subtext.equalsIgnoreCase("Whatsapp!") || subtext.equalsIgnoreCase("Create Account"))) {
            spannable.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View view) {

                    if (subtext.contains("User")) {
                        //  showError("User clicked!!!");
                        objClickableSpanListener.onSpanClick("User");
                    } else if (subtext.contains("Policy")) {
                        //showError("Policy clicked!!!");
                        objClickableSpanListener.onSpanClick("Policy");
                    } else if (subtext.contains("Resend OTP")) {
                        // showError("Resend OTP clicked!!!");
                        objClickableSpanListener.onSpanClick("Resend OTP");
                    }

                }
            }, i, i + subtext.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        spannable.setSpan(new ForegroundColorSpan(color), i, i + subtext.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        textView.setText(spannable);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
//        if(broadcastReceiver != null){
//        unregisterReceiver(broadcastReceiver);}
        //  overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        // overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }


    public static void openNewActivity(Context context, String newActivity, boolean isLoginCheckRequired) {


        if (isLoginCheckRequired) {
            if (!Constants.isUserLoggedIn(context)) {
                Intent intent = new Intent(context, LoginActivity.class);
                intent.putExtra(IntentConstants.ACTIVITY_TO_OPEN, newActivity);


                context.startActivity(intent);
            } else {
                Intent intent = new Intent(context, getClassName(newActivity));
                context.startActivity(intent);
            }
        } else {
            Intent intent = new Intent(context, getClassName(newActivity));
            if (context.getClass().getSimpleName().equalsIgnoreCase("WelcomeActivity"))
                intent.putExtra(IntentConstants.FROM_ACTIVITY, DashboardActivity.class.getCanonicalName());

            context.startActivity(intent);
        }


    }

    public static Class<?> getClassName(String newActivity) {

        try {

            Class<?> clazz = Class.forName(newActivity);
            LogUtils.Verbose("clazz", clazz + "");

            return clazz;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }


    public void showError(String error) {

        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        progressDialog.show();

    }

    public void dismissProgress() {

        if (progressDialog != null && progressDialog.isShowing()) {
            LogUtils.Verbose("inside if", "dismissProgress......");
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ShakeDetector.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        ShakeDetector.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgress();
        ShakeDetector.destroy();
        // unregisterReceiver(broadcastReceiver);
    }


    public static void openDashboardActivity(Context context) {
        Intent intent = new Intent(context, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

//    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            // internet lost alert dialog method call from here...
//
//            MovitConsumerApp.getInstance().setSessionID(null);
//            BaseActivity.openDashboardActivity(BaseActivity.this);
//
//            finish();
//
//        }
//    };


}




