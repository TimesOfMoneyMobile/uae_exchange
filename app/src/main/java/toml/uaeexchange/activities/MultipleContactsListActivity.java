package toml.uaeexchange.activities;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.TypefaceSpan;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import toml.movitwallet.models.ContactListItem;
import toml.movitwallet.models.ContactsHeaderItem;
import toml.movitwallet.models.ContactsItem;
import toml.movitwallet.models.ContactsModel;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.uaeexchange.adapters.NewMultipleContactsSelectionAdapter;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.CustomTypefaceSpan;
import toml.uaeexchange.utilities.Constants;

/**
 * Created by vishwanathp on 9/14/2017.
 */

public class MultipleContactsListActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor>, NewMultipleContactsSelectionAdapter.UpdateTextListener {

    @BindView(R.id.recyclerViewContactsList)
    RecyclerView recyclerViewContactsList;
    @BindView(R.id.act_ct_tv_no_contacts)
    TextView tvNoContacts;

    Unbinder unbinder;

    String cursorFilter;

    NewMultipleContactsSelectionAdapter contactAdapter;
    @BindView(R.id.txtvwAddMembers)
    TextView txtvwAddMembers;
    @BindView(R.id.imgvwAddMembers)
    ImageView imgvwAddMembers;
    @BindView(R.id.edtxtLayoutSearch)
    CustomEditTextLayout edtxtLayoutSearch;

    List<ContactListItem> consolidatedList = new ArrayList<>();
    public List<ContactsModel> listSelectedContacts = new ArrayList<>();

    public static MultipleContactsListActivity thisActivity;
    ArrayList<ContactListItem> serachContactsList;
    ArrayList<ContactsModel> allContactsList;

    HashMap<String, List<ContactsModel>> groupedHashMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_contacts_list);

        thisActivity = MultipleContactsListActivity.this;

        unbinder = ButterKnife.bind(this);

        setToolbarWithDoneButton(getString(R.string.select_friends), R.drawable.icon_cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MovitConsumerApp.getInstance().setListSelectedContacts(listSelectedContacts);

                setResult(RESULT_OK);

                finish();

                // showError("You have selected " + MovitConsumerApp.getInstance().getListSelectedContacts().size() + " contacts.");

                //BaseActivity.openNewActivity(MultipleContactsListActivity.this, BillSetupActivity.class.getCanonicalName(), true);

            }
        });


        // etSearch.setVisibility(View.GONE);
       /* etSearch.getVerticalDivider().setVisibility(View.GONE);
        etSearch.getEditText().addTextChangedListener(textWatcher);
        etSearch.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);*/

        edtxtLayoutSearch.getVerticalDivider().setVisibility(View.GONE);
        edtxtLayoutSearch.getEditText().addTextChangedListener(textWatcher);
        edtxtLayoutSearch.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);


        getSupportLoaderManager().initLoader(1, null, MultipleContactsListActivity.this);


    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // cursorFilter = !TextUtils.isEmpty(charSequence.toString()) ? charSequence.toString() : null;
            //  getSupportLoaderManager().restartLoader(1, null, MultipleContactsListActivity.this);
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (s.toString().length() == 0) {
                serachContactsList.clear();
                serachContactsList.addAll(consolidatedList);
                contactAdapter.notifyDataSetChanged();
                tvNoContacts.setVisibility(View.GONE);
            } else {
                search(s.toString());
            }
        }
    };

    private void search(String text) {
        serachContactsList.clear();

        //Search Functionality from All Contacts List only.
        for (String key : groupedHashMap.keySet()) {
            if (key.equalsIgnoreCase("2All")) {
                for (ContactsModel contactsModel : groupedHashMap.get(key)) {
                    if (contactsModel.getName().toLowerCase(Locale.ENGLISH).contains(text.toLowerCase())) {
                        ContactsItem objContactsItem = new ContactsItem();
                        objContactsItem.setContactsModel(contactsModel);//setBookingDataTabs(bookingDataTabs);
                        serachContactsList.add(objContactsItem);

                    }
                }
           }

        }


        if (serachContactsList.size() == 0) {
            tvNoContacts.setVisibility(View.VISIBLE);
        } else {
            tvNoContacts.setVisibility(View.GONE);
        }

        contactAdapter.notifyDataSetChanged();
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (MovitConsumerApp.getInstance().getListSelectedContacts() != null) {
            if (listSelectedContacts != null) {
                listSelectedContacts.clear();
                listSelectedContacts.addAll(MovitConsumerApp.getInstance().getListSelectedContacts());
            }
        }
        updateText();

       /* if (contactAdapter != null) {

            // for (int i = 0; i < l)
            contactAdapter.notifyDataSetChanged();

            updateText();
        }*/
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        Uri baseUri;
//        if (cursorFilter != null) {
//            baseUri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI,
//                    Uri.encode(cursorFilter));
//        } else {
        baseUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        //}


        CursorLoader cursorLoader = new CursorLoader(this,
                baseUri, // URI
                ContactsQuery.PROJECTION,// projection fields
                ContactsQuery.SELECTION,// the selection criteria
                null,// the selection args
                ContactsQuery.SORT_ORDER// the sort order
        );


        return cursorLoader;
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        if (MovitConsumerApp.getInstance().getListRecentContacts() != null && MovitConsumerApp.getInstance().getListRecentContacts().size() == 0) {
            if (cursor.getCount() == 0) {
                tvNoContacts.setVisibility(View.VISIBLE);
            } else {
                tvNoContacts.setVisibility(View.GONE);
            }
        } else {

        }

        allContactsList = new ArrayList<>();
        serachContactsList = new ArrayList<>();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            // The Cursor is now set to the right position
            ContactsModel objContactsModel = new ContactsModel();
            objContactsModel.setName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
            objContactsModel.setNumber(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            try {

                if (!TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI)))) {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI))));
                    objContactsModel.setImage(bitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            // To remove invalid numbers from contacts list.
            if (objContactsModel.getNumber().trim().length() >= 9) {
                allContactsList.add(objContactsModel);

            }


        }


        groupedHashMap = new HashMap<>();
        //Here 1, 2 added before keys to maintain order.
        if (MovitConsumerApp.getInstance().getListRecentContacts() != null && MovitConsumerApp.getInstance().getListRecentContacts().size() > 0) {
            /*List<ContactsModel> tempList = new ArrayList<>();
            tempList.addAll(MovitConsumerApp.getInstance().getListRecentContacts());
            for (ContactsModel objContactsModel : tempList) {
                if (objContactsModel.isChecked())
                    objContactsModel.setChecked(false);



            }*/

            for (ContactsModel objContactsModel : MovitConsumerApp.getInstance().getListRecentContacts()) {
                for (ContactsModel objSelectedModel : MovitConsumerApp.getInstance().getListSelectedContacts()) {
                    if (objContactsModel.equals(objSelectedModel)) {
                        objContactsModel.setChecked(true);
                    }
                }
            }
            groupedHashMap.put("1Recent Contacts", MovitConsumerApp.getInstance().getListRecentContacts());
        }

        for (ContactsModel objContactsModel : allContactsList) {
            for (ContactsModel objSelectedModel : MovitConsumerApp.getInstance().getListSelectedContacts()) {
                if (objContactsModel.equals(objSelectedModel)) {
                    objContactsModel.setChecked(true);
                }
            }
        }

        for (ContactsModel objSelectedModel : listSelectedContacts) {
            if (allContactsList.contains(objSelectedModel)) {
                allContactsList.get(allContactsList.indexOf(objSelectedModel)).setChecked(true);
            }
        }


        groupedHashMap.put("2All", allContactsList);


        if (consolidatedList != null && consolidatedList.size() > 0) {
            consolidatedList.clear();
        }

        if (serachContactsList != null && serachContactsList.size() > 0) {
            serachContactsList.clear();
        }

        for (String key : groupedHashMap.keySet()) {
            ContactsHeaderItem contactsHeaderItem = new ContactsHeaderItem();
            String header = key;
            header = header.replace(header.substring(0, 1), "");
            if (header.equalsIgnoreCase("Recent Contacts")) {
                header = "Recent";
            } else if (header.equalsIgnoreCase("All")) {
                header = "All Friends";
            }
            contactsHeaderItem.setHeader(header);
            consolidatedList.add(contactsHeaderItem);
            serachContactsList.add(contactsHeaderItem);

            for (ContactsModel contactsModel : groupedHashMap.get(key)) {
                ContactsItem objContactsItem = new ContactsItem();
                objContactsItem.setContactsModel(contactsModel);//setBookingDataTabs(bookingDataTabs);
                consolidatedList.add(objContactsItem);
                serachContactsList.add(objContactsItem);
            }
        }


        contactAdapter = new NewMultipleContactsSelectionAdapter(this, serachContactsList, allContactsList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewContactsList.setLayoutManager(layoutManager);
        recyclerViewContactsList.setAdapter(contactAdapter);
        contactAdapter.setUpdateTextListener(this);

/*
        if (contactAdapter != null) {
            MovitConsumerApp.getInstance().setListSelectedContacts(contactAdapter.getCheckedItems());
        }*/

        updateText();

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void updateText() {
        String input = "Between you and: ";//text + txtvwAddMembers.getText().toString();
        //input = input + ", ";

        for (ContactsModel objContactsModel : listSelectedContacts) {
            input = input + objContactsModel.getName() + ", ";
        }

        if (!input.endsWith(": ")) {

            String resultText = input;
            int ind = input.lastIndexOf(", ");
            if (ind >= 0) {
                resultText = new StringBuilder(resultText).replace(ind, ind + 1, "").toString();
            }

            // SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(resultText);
            SpannableString spannableStringBuilder = new SpannableString(resultText);
            //  Typeface fontMedium = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Medium.ttf");
            Typeface fontBold = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");

            TypefaceSpan fontBoldSpan = new CustomTypefaceSpan("", fontBold);
            spannableStringBuilder.setSpan(fontBoldSpan, 0, resultText.lastIndexOf(":") + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            // spannableStringBuilder.setSpan(fontMedium, resultText.lastIndexOf(":") + 1, resultText.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtvwAddMembers.setText(spannableStringBuilder);
        } else {
            String defaultText = "Between you and: Add Members";
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(defaultText);
            // Typeface fontMedium = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Medium.ttf");
            Typeface fontBold = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");

            TypefaceSpan fontBoldSpan = new CustomTypefaceSpan("", fontBold);
            spannableStringBuilder.setSpan(fontBoldSpan, 0, defaultText.lastIndexOf(":") + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            // spannableStringBuilder.setSpan(fontBold, 0, defaultText.lastIndexOf(":") + 1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            //spannableStringBuilder.setSpan(fontMedium, defaultText.lastIndexOf(":") + 1, defaultText.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtvwAddMembers.setText(spannableStringBuilder);
        }


    }

    @Override
    public void onTextChanged() {
        try {
          /*  if (contactAdapter != null) {
                MovitConsumerApp.getInstance().setListSelectedContacts(contactAdapter.getCheckedItems());
            }*/
            updateText();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * This interface defines constants for the Cursor and CursorLoader, based on constants defined
     * in the {@link ContactsContract.Contacts} class.
     */
    public interface ContactsQuery {


        // The selection clause for the CursorLoader query. The search criteria defined here
        // restrict results to contacts that have a display name and are linked to visible groups.
        // Notice that the search on the string provided by the user is implemented by appending
        // the search string to CONTENT_FILTER_URI.
        @SuppressLint("InlinedApi")
        final static String SELECTION =
                (Constants.hasHoneycomb() ? ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME) +
                        "<>''" + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1";

        // The desired sort order for the returned Cursor. In Android 3.0 and later, the primary
        // sort key allows for localization. In earlier versions. use the display name as the sort
        // key.
        @SuppressLint("InlinedApi")
        final static String SORT_ORDER =
                Constants.hasHoneycomb() ? ContactsContract.Contacts.SORT_KEY_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME;

        // The projection for the CursorLoader query. This is a list of columns that the Contacts
        // Provider should return in the Cursor.
        @SuppressLint("InlinedApi")
        final static String[] PROJECTION = {

                // The contact's row id
                // ContactsContract.Contacts._ID,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                // A pointer to the contact that is guaranteed to be more permanent than _ID. Given
                // a contact's current _ID value and LOOKUP_KEY, the Contacts Provider can generate
                // a "permanent" contact URI.
//                ContactsContract.Contacts.LOOKUP_KEY,
//                ContactsContract.Contacts.HAS_PHONE_NUMBER,
                // In platform version 3.0 and later, the Contacts table contains
                // DISPLAY_NAME_PRIMARY, which either contains the contact's displayable name or
                // some other useful identifier such as an email address. This column isn't
                // available in earlier versions of Android, so you must use Contacts.DISPLAY_NAME
                // instead.
                Constants.hasHoneycomb() ? ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,

                // In Android 3.0 and later, the thumbnail image is pointed to by
                // PHOTO_THUMBNAIL_URI. In earlier versions, there is no direct pointer; instead,
                // you generate the pointer from the contact's ID value and constants defined in
                // android.provider.ContactsContract.Contacts.
                Constants.hasHoneycomb() ? ContactsContract.Contacts.PHOTO_THUMBNAIL_URI : ContactsContract.Contacts._ID,

                // The sort order column for the returned Cursor, used by the AlphabetIndexer
                // SORT_ORDER,
        };


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
