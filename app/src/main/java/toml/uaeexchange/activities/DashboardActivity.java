package toml.uaeexchange.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.controllers.LogoutController;
import toml.movitwallet.models.RegistrationRequest;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.dialogs.YesNoDialog;
import toml.uaeexchange.fragments.DashboardFragment;
import toml.uaeexchange.fragments.LeftMenuFragment;
import toml.uaeexchange.utilities.Constants;

public class DashboardActivity extends BaseActivity implements IMovitWalletController<Bundle> {


    //ToolBar
    @BindView(R.id.navigation_toolbar)
    Toolbar navToolbar;
    @BindView(R.id.toolbar_dashboard_rl_for_notification)
    RelativeLayout rlNotification;
    @BindView(R.id.toolbar_dashboard_tv_notification_count)
    TextView tvNotificationCount;
    @BindView(R.id.imgvwWhatsAppP2P)
    ImageView imgvwWhatsAppP2P;


    private int dialogTimeInMillis = 1000;

    DashboardFragment dashboardFragment;

    //For Handling Notification
//    IntentFilter intentFilter;
    MessageBroadcast messageBroadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        dashboardFragment = DashboardFragment.getInstance(getIntent().getBundleExtra(IntentConstants.Bundle));
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, dashboardFragment).commit();
        IntentFilter intentFilter = new IntentFilter("message");
        messageBroadcast = new MessageBroadcast();
        registerReceiver(messageBroadcast, intentFilter);

    }


    @Override
    protected void onResume() {
        super.onResume();
        checkWhetherUserIsAuthorized();

    }

    private void checkWhetherUserIsAuthorized() {
        if (!TextUtils.isEmpty(MovitConsumerApp.getInstance().getKycStatus())) {
            if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("N")) {
//                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


           /* WindowManager.LayoutParams wp = getWindow().getAttributes();
            wp.dimAmount = 0.75f;
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
*/


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        new YesNoDialog(DashboardActivity.this, "KYC incomplete", getString(R.string.kyc_not_completed_alert), getString(R.string.proceed_to_kyc), "Cancel", new YesNoDialog.IYesNoDialogCallback() {
                            @Override
                            public void handleResponse(int responsecode) {
                                if (responsecode == 1) {
                                    RegistrationRequest objRegistrationRequest = new RegistrationRequest();
                                    objRegistrationRequest.setMobileNumber(MovitConsumerApp.getInstance().getMobileNumber());
                                    Intent intent = new Intent(DashboardActivity.this, KYCRegistrationActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putParcelable(IntentConstants.REG_REQUEST, objRegistrationRequest);
                                    bundle.putString(IntentConstants.FROM_ACTIVITY, "Dashboard");
                                    intent.putExtras(bundle);
                                    startActivity(intent);

                                    //finish();
                                }

                            }
                        });


                    }
                }, dialogTimeInMillis);


            } else if (MovitConsumerApp.getInstance().getKycStatus().equalsIgnoreCase("P")) {
//                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

           /* WindowManager.LayoutParams wp = getWindow().getAttributes();
            wp.dimAmount = 0.75f;
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);*/


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        new OkDialog(DashboardActivity.this, "Authorization pending", getString(R.string.authorisation_pending_info_message), new OkDialog.IOkDialogCallback() {
                            @Override
                            public void handleResponse() {


                            }
                        });

                    }
                }, dialogTimeInMillis);


            } else {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }

    }

    public static void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;

            for (int idx = 0; idx < group.getChildCount(); idx++) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }


    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {

            //additional code
            if (Constants.isUserLoggedIn(DashboardActivity.this)) {
                new YesNoDialog(DashboardActivity.this, getString(R.string.are_you_sure_you_want_to_logout), null, new YesNoDialog.IYesNoDialogCallback() {
                    @Override
                    public void handleResponse(int responsecode) {
                        if (responsecode == 1) {
                            handleLogout();
                            MovitConsumerApp.getInstance().setSessionID(null);
                        }
                    }
                });
            } else {
                super.onBackPressed();
            }

        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.frame);
            if(f instanceof LeftMenuFragment){
               checkWhetherUserIsAuthorized();
            }

            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(messageBroadcast);
    }

    public void handleLogout() {
        showProgress();
        MovitWalletController movitWalletController = new LogoutController();
        movitWalletController.init(DashboardActivity.this);

    }


    @Override
    public void onSuccess(Bundle response) {
        dismissProgress();
        MovitConsumerApp.getInstance().setSessionID(null);

        new OkDialog(DashboardActivity.this, response.getString(IntentConstants.Message), new OkDialog.IOkDialogCallback() {
            @Override
            public void handleResponse() {
                // onBackPressed();
                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                // intent.putExtra(IntentConstants.FROM_ACTIVITY_REGISTRATION, Constants.RegistrationSuccessActivity);
                AppSettings.putData(DashboardActivity.this, AppSettings.NOTIFICATION_COUNT, "0");
                startActivity(intent);
                //BaseActivity.openNewActivity(RegistrationSuccessActivity.this, LoginActivity.class.getCanonicalName(), true);
                finish();
            }
        });
        //handleLayout();
    }

    @Override
    public void onFailed(String errorCode, String reason) {
        dismissProgress();
        new OkDialog(DashboardActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
            @Override
            public void handleResponse() {
                onBackPressed();
            }
        });
    }


    class MessageBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {

                if (intent.hasExtra("type")) {
                    int type = intent.getIntExtra("type", 0);

                    //For Invalid Session
                    if (type == 1) {
                        MovitConsumerApp.getInstance().setSessionID(null);
                        BaseActivity.openDashboardActivity(DashboardActivity.this);

                        finish();

                    }  //For Handling NotificationCount
                    else if (type == 2) {
                        dashboardFragment.handleNotificationCount();
                    }
                }
            } catch (Exception e) {
                LogUtils.Verbose("Brodcast Reciever", e.getMessage());
            }
        }
    }

}

