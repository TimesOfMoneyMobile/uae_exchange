package toml.uaeexchange.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.utils.AppSettings;
import toml.uaeexchange.R;
import toml.uaeexchange.adapters.NotificationAdapter;
import toml.uaeexchange.models.Notification;


public class NotificationActivity extends BaseActivity {

    @BindView(R.id.lstvwNotifications)
    ListView lvNotifications;
    @BindView(R.id.txtvwNoNotifications)
    TextView tvNoNotifications;
    NotificationAdapter notificationAdapter;
    List<Notification> notificationList = null;
    List<Notification> splitNotificationList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_notification);

        setToolbarWithBalance(getString(R.string.title_notifications), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, R.drawable.icon_more, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUp(v);
            }
        }, false);

        ButterKnife.bind(this);


        AppSettings.putData(NotificationActivity.this, AppSettings.NOTIFICATION_COUNT, "0");


        Gson gson = new Gson();
        String json = AppSettings.getData(NotificationActivity.this, AppSettings.NOTIFICATIONS);

        Type type = new TypeToken<ArrayList<Notification>>() {
        }.getType();
        notificationList = gson.fromJson(json, type);

        if (getIntent().hasExtra("From")) {
            if (getIntent().getStringExtra("From").equalsIgnoreCase("SPLIT")) {


                if (notificationList != null) {
                    splitNotificationList = new ArrayList<>();

                    for (Notification objNotification : notificationList) {
                        if (objNotification.getTxnType().equalsIgnoreCase("SPLITBILL")) {
                            splitNotificationList.add(objNotification);
                        }
                    }

                    if (splitNotificationList.size() > 0) {

                        notificationAdapter = new NotificationAdapter(NotificationActivity.this, splitNotificationList);

                        lvNotifications.setAdapter(notificationAdapter);
                    } else {
                        tvNoNotifications.setVisibility(View.VISIBLE);
                    }

                } else {
                    tvNoNotifications.setVisibility(View.VISIBLE);

                }
            }
        } else {
            if (notificationList != null) {
                notificationAdapter = new NotificationAdapter(NotificationActivity.this, notificationList);
                lvNotifications.setAdapter(notificationAdapter);

            } else {
                tvNoNotifications.setVisibility(View.VISIBLE);

            }
        }


//        notificationFragment = new NotificationFragment();
//
//
//        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
//        android.support.v4.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
//        fragmentTransaction.replace(R.id.frame_layout_for_notification, notificationFragment);
//        fragmentTransaction.commit();
//
//        relRightImage = (RelativeLayout) findViewById(R.id.relRightIamge);


    }

    private void showPopUp(View v) {


        PopupMenu popupMenu = new PopupMenu(NotificationActivity.this, v);
        popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_settings) {
                    BaseActivity.openNewActivity(NotificationActivity.this, SettingsActivity.class.getCanonicalName(), false);
                } else if (item.getItemId() == R.id.menu_clear_all) {
                    tvNoNotifications.setVisibility(View.VISIBLE);
                    AppSettings.putData(NotificationActivity.this, AppSettings.NOTIFICATIONS, "");
                    if (notificationList != null && notificationAdapter != null) {
                        notificationList.clear();
                        notificationAdapter.notifyDataSetChanged();
                    }

                }
                return true;
            }
        });

        popupMenu.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
