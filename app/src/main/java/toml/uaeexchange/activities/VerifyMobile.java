package toml.uaeexchange.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.controllers.GenerateOTPController;
import toml.movitwallet.controllers.ValidateOTPController;
import toml.movitwallet.models.RegistrationRequest;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.customviews.ErrorLabelLayout;
import toml.uaeexchange.customviews.NumericInputField;
import toml.uaeexchange.utilities.Constants;
import toml.uaeexchange.utilities.GenericValidations;

public class VerifyMobile extends BaseActivity implements BaseActivity.ClickableSpanListener {

    @BindView(R.id.numericFieldOTP)
    NumericInputField numericFieldOTP;
    @BindView(R.id.linlayAutoDetectingLoaderContainer)
    LinearLayout linlayAutoDetectingLoaderContainer;
    @BindView(R.id.txtvwDidntRecieveOTP)
    TextView txtvwDidntRecieveOTP;
    @BindView(R.id.btnRegister)
    CustomButton btnRegister;
    @BindView(R.id.txtvwMobileNumber)
    TextView txtvwMobileNumber;
    @BindView(R.id.txtvwReceiveSMSText)
    TextView txtvwReceiveSMSText;
    @BindView(R.id.txtvwTimerValue)
    TextView txtvwTimerValue;
    @BindView(R.id.linlayTimerContainer)
    LinearLayout linlayTimerContainer;
    @BindView(R.id.imgvwEditMobileNumber)
    ImageView imgvwEditMobileNumber;
    private EditText edtMobile;

    private SmsVerifyCatcher smsVerifyCatcher;
    MyCountDownTimer objMyCountDownTimer;

    private String TXN_TYPE = "CUSTREG";

    RegistrationRequest objRegistrationRequest;
    Spannable spannable;
    private boolean isSpanClicked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_mobile);

        setClickableSpanListener(this);
        ButterKnife.bind(this);

        setToolbar("", R.drawable.icon_back, 0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }, null);

        objRegistrationRequest = getIntent().getExtras().getParcelable(IntentConstants.REG_REQUEST);

        txtvwMobileNumber.setText(getString(R.string.country_code) + " " + objRegistrationRequest.getMobileNumber());

        String fullText = "Didn’t receive OTP? Resend OTP";
        spannable = new SpannableString(fullText);

        // To remove highlight text color
        txtvwDidntRecieveOTP.setHighlightColor(Color.TRANSPARENT);

        setColorAndClick(txtvwDidntRecieveOTP, fullText, spannable, "Resend OTP", ContextCompat.getColor(VerifyMobile.this, R.color.clear_blue));

        //callGenerateOTP();

        toml.movitwallet.utils.Constants.hideKeyboard(VerifyMobile.this);


        linlayAutoDetectingLoaderContainer.setVisibility(View.VISIBLE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                linlayAutoDetectingLoaderContainer.setVisibility(View.INVISIBLE);

                btnRegister.setEnabled(true);
            }
        }, Constants.AUTO_DETECTING_LAYOUT_DISPALY_TIME_IN_MILIS);


        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = Constants.parseSMSForOTP(message);//Parse verification code
                try {
                    numericFieldOTP.setText(code);//set code in edit text
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //then you can send verification code to server
            }
        });

        imgvwEditMobileNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditMobileNumberDialog();
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (numericFieldOTP.getText().toString().length() == getResources().getInteger(R.integer.OTPLength)) {

                    showProgress();

                    objRegistrationRequest.setOTP(numericFieldOTP.getText().toString().trim());

                    MovitWalletController validateOTPController = new ValidateOTPController(TXN_TYPE, objRegistrationRequest.getOTP());

                    validateOTPController.init(new IMovitWalletController() {
                        @Override
                        public void onSuccess(Object response) {

                            dismissProgress();

                            Intent intent = new Intent(VerifyMobile.this, SecurityQuestionsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putParcelable(IntentConstants.REG_REQUEST, objRegistrationRequest);
                            intent.putExtras(bundle);
                            startActivity(intent);

                            finish();

                        }

                        @Override
                        public void onFailed(String errorCode, String reason) {
                            dismissProgress();
                            showError(reason);
                        }
                    });


                } else {
                    //showError("Please enter " + getResources().getInteger(R.integer.OTPLength) + " digit OTP.");
                    showError(getString(R.string.invalidOtpError));
                }

            }
        });


    }


    private void showEditMobileNumberDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_edit_mobile_number, null);

        final Dialog dialog = new Dialog(VerifyMobile.this);

        final CustomEditTextLayout edtxtLayoutMobileNumber = (CustomEditTextLayout) view.findViewById(R.id.edtxtLayoutMobileNumber);
        ErrorLabelLayout errorLabelLayout = (ErrorLabelLayout) view.findViewById(R.id.errorLabelMobileNumber);
        final Button btnDone = (Button) view.findViewById(R.id.btnDone);
        //RelativeLayout rlForCancel = (RelativeLayout)view.findViewById(R.id.rl_for_cancel);

        edtxtLayoutMobileNumber.setErrorLabelLayout(errorLabelLayout);
        edtxtLayoutMobileNumber.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (Constants.checkForNonEmpty(new CustomEditTextLayout[]{edtxtLayoutMobileNumber})) {
                    btnDone.setEnabled(true);
                } else {
                    btnDone.setEnabled(false);
                }


            }
        });

//        rlForCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });


        //Button click
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strMobile = edtxtLayoutMobileNumber.getText().toString().trim();

                GenericValidations genericValidations = new GenericValidations(VerifyMobile.this);

                if (!genericValidations.isValidMobile(strMobile, edtxtLayoutMobileNumber)) {

                } else {
                    dialog.dismiss();
                    //txtvwMobileNumber.setText(strMobile);
                    txtvwMobileNumber.setText(getString(R.string.country_code) + " " + strMobile);
                    MovitConsumerApp.getInstance().setMobileNumber(strMobile);
                    objRegistrationRequest.setMobileNumber(strMobile);
                    callGenerateOTP();
                }

            }
        });
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before setcontentview
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }


    private void callGenerateOTP() {
        showProgress();

        numericFieldOTP.clearText();

        MovitWalletController generateOTPController = new GenerateOTPController(TXN_TYPE);

        generateOTPController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                dismissProgress();

                linlayAutoDetectingLoaderContainer.setVisibility(View.VISIBLE);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String code = "";
                        try {
                            numericFieldOTP.setText(code);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        linlayAutoDetectingLoaderContainer.setVisibility(View.INVISIBLE);

                        btnRegister.setEnabled(true);
                    }
                }, Constants.AUTO_DETECTING_LAYOUT_DISPALY_TIME_IN_MILIS);

                if (isSpanClicked) {
                    objMyCountDownTimer = new MyCountDownTimer(Constants.RESEND_OTP_TIME_IN_MILIS, 1000);
                    objMyCountDownTimer.start();
                    isSpanClicked = false;

                }

            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                showError(reason);

            }
        });
    }

    @Override
    public void onSpanClick(String strText) {
        isSpanClicked = true;
        // linlayTimerContainer.setVisibility(View.VISIBLE);
        callGenerateOTP();
    }

    private class MyCountDownTimer extends CountDownTimer {
        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            linlayTimerContainer.setVisibility(View.VISIBLE);
            txtvwReceiveSMSText.setVisibility(View.VISIBLE);
            txtvwTimerValue.setVisibility(View.VISIBLE);

            txtvwReceiveSMSText.setText("You will receive OTP via SMS to your mobile number" + " " + getString(R.string.country_code) + " " + MovitConsumerApp.getInstance().getMobileNumber() + " in ");
            txtvwTimerValue.setText("" + (Constants.RESEND_OTP_TIME_IN_MILIS / 1000) + " Secs");

            txtvwDidntRecieveOTP.setEnabled(false);
            txtvwDidntRecieveOTP.setAlpha(0.5f);

        }

        @Override
        public void onTick(long millisUntilFinished) {

            txtvwTimerValue.setText((millisUntilFinished / 1000) > 1 ? (millisUntilFinished / 1000) + " Secs" : (millisUntilFinished / 1000) + " Sec");

        }

        @Override
        public void onFinish() {
            txtvwDidntRecieveOTP.setEnabled(true);
            txtvwDidntRecieveOTP.setAlpha(1.0f);
            linlayTimerContainer.setVisibility(View.GONE);
            //txtvwTimerValue.setVisibility(View.GONE);
            // txtvwReceiveSMSText.setVisibility(View.GONE);
            objMyCountDownTimer.cancel();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
