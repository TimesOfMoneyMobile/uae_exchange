package toml.uaeexchange.activities;

import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.GenerateMpinOnForgotController;
import toml.movitwallet.controllers.ListSecurityQuestionsController;
import toml.movitwallet.models.SecurityQuestion;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.utilities.Constants;

public class ForgotMpinSecurityQuestionActivity extends BaseActivity {

    @BindView(R.id.tvQuestionValue)
    TextView tvQuestionValue;
    @BindView(R.id.etAnswerValue)
    EditText etAnswerValue;
    @BindView(R.id.btnSubmit)
    CustomButton btnSubmit;
    @BindView(R.id.txtvwForgotSecurityQuestion)
    TextView txtvwForgotSecurityQuestion;
    private String mobileNo;
    private String otpValue;
    //Constatnt REN for only one random Question
    private String listMode = "REN";
    private List<SecurityQuestion> listSecurityQue;
    private String questionID1;
    private String answer1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_mpin_security_question);
        ButterKnife.bind(this);

        setToolbarWithBalance(getString(R.string.forgot_mpin), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);


        mobileNo = getIntent().getStringExtra(IntentConstants.MOBILE_NUMBER);
        otpValue = getIntent().getStringExtra(IntentConstants.OTP);
        Constants.setEditTextMaxLength(etAnswerValue, 250, false, true);
        callSecurityQuetion();

        // Underline the textview.
       // txtvwForgotSecurityQuestion.setPaintFlags(txtvwForgotSecurityQuestion.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        txtvwForgotSecurityQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseActivity.openNewActivity(ForgotMpinSecurityQuestionActivity.this, ForgotSecurityAnswerActivity.class.getCanonicalName(), false);

            }
        });

    }

    private void callSecurityQuetion() {
        showProgress();
        MovitWalletController listSecurityQuestionsCOntroller = new ListSecurityQuestionsController(mobileNo, listMode);

        listSecurityQuestionsCOntroller.init(new IMovitWalletController<List<SecurityQuestion>>() {

            @Override
            public void onSuccess(List<SecurityQuestion> response) {
                dismissProgress();

                listSecurityQue = response;
                questionID1 = listSecurityQue.get(0).id;
                tvQuestionValue.setText(listSecurityQue.get(0).question);


            }

            @Override
            public void onFailed(String errorCode, String reason) {

                dismissProgress();

                showError(reason);

            }
        });

    }

    @OnClick(R.id.btnSubmit)
    public void onViewClicked() {

        if (!TextUtils.isEmpty(etAnswerValue.getText().toString().trim())) {
            //Final Forgot mpin
            callGenrateMpin();
        } else {
            showError("Please answer the Security Question.");
        }
    }

    private void callGenrateMpin() {

        answer1 = etAnswerValue.getText().toString();
        String securityQuestionResult = "<SecurityQuestion id = \"" + questionID1 + "\" answer = \"" + answer1 + "\" />";

        showProgress();
        MovitWalletController listSecurityQuestionsCOntroller = new GenerateMpinOnForgotController(mobileNo, securityQuestionResult, otpValue);

        listSecurityQuestionsCOntroller.init(new IMovitWalletController() {

            @Override
            public void onSuccess(Object response) {
                dismissProgress();

                BaseActivity.openNewActivity(ForgotMpinSecurityQuestionActivity.this, ForgotMpinSuccessActivity.class.getCanonicalName(), false);

            }

            @Override
            public void onFailed(String errorCode, String reason) {

                dismissProgress();

                showError(reason);

            }
        });


    }
}
