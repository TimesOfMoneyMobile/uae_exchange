package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.controllers.GenerateOTPController;
import toml.movitwallet.controllers.P2MController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.IntentConstants;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.dialogs.ValidateOTPDialog;
import toml.uaeexchange.utilities.Constants;

public class P2MConfirmationActivity extends BaseActivity implements BaseActivity.ClickableSpanListener {

    private static final String TXN_TYPE = "P2M";
    @BindView(R.id.tvMerchantIDValue)
    TextView tvMerchantIDValue;
    @BindView(R.id.tvMerchantName)
    TextView tvMerchantName;
    @BindView(R.id.tvAmountValue)
    TextView tvAmountValue;
    @BindView(R.id.act_rvs_tv_transaction_fee)
    TextView tvTransactionFee;

    @BindView(R.id.btnConfirm)
    CustomButton btnConfirm;
    private String merchantId, externalMerchantId, merchantName, amount, transactionFees, promoCode, discount, discount_on;
    @BindView(R.id.rlOfferInfo)
    RelativeLayout rlOfferInfo;
    @BindView(R.id.tvTotalAmount)
    TextView tvTotalAmount;
    @BindView(R.id.act_rac_rl_edit)
    RelativeLayout rlEdit;
    @BindView(R.id.act_rac_tv_amount)
    TextView tvAmountLable;
    @BindView(R.id.tvDiscountLable)
    TextView tvDiscountLable;
    @BindView(R.id.tvDiscountValue)
    TextView tvDiscountValue;
    @BindView(R.id.rlDiscount)
    RelativeLayout rlDiscount;


    ValidateOTPDialog otpDialog;
    private boolean isSpanClicked;

    private SmsVerifyCatcher smsVerifyCatcher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p2_mconfirmation);
        ButterKnife.bind(this);
        setToolbarWithBalance(getString(R.string.review_and_send), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);
        setClickableSpanListener(this);

        merchantId = getIntent().getStringExtra(IntentConstants.MERCHANTID);
        externalMerchantId = getIntent().getStringExtra(IntentConstants.EXTERNAL_MERCHANTID);
        merchantName = getIntent().getStringExtra(IntentConstants.MERCHANTNAME);
        amount = getIntent().getStringExtra(IntentConstants.AMOUNT);
        promoCode = getIntent().getStringExtra(IntentConstants.PROMOCODE);
        transactionFees = getIntent().getStringExtra(IntentConstants.TRANSACTIONFEES);
        discount = getIntent().getStringExtra(IntentConstants.DISCOUNT);
        discount_on = getIntent().getStringExtra(IntentConstants.DISCOUNT_ON);
        handleUI();


        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = Constants.parseSMSForOTP(message);//Parse verification code
                try {
                    if (otpDialog != null && otpDialog.isShowing()) {
                        otpDialog.setOTPText(code);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //then you can send verification code to server
            }
        });
    }

    private void handleUI() {
        rlOfferInfo.setVisibility(View.GONE);
        tvMerchantIDValue.setText(externalMerchantId);
        tvMerchantName.setText(merchantName);

        /*if (discount_on.equalsIgnoreCase("T")) {
            tvDiscountLable.setText(R.string.discount_on_transaction_amount);

        } else if (discount_on.equalsIgnoreCase("F")) {
            tvDiscountLable.setText(R.string.discount_on_fees_amount);
        } else {
            rlDiscount.setVisibility(View.GONE);
        }*/

        if (!discount.equalsIgnoreCase("null") && Double.valueOf(discount) != 0) {
            tvDiscountValue.setText(getString(R.string.AED) + " " + discount);
            if (discount_on.equalsIgnoreCase("T")) {
                tvDiscountLable.setText(R.string.discount_on_transaction_amount);
                String discountedAmount = Constants.formatAmount(String.valueOf(Double.valueOf(amount) - Double.valueOf(discount)));
                tvTotalAmount.setText(getString(R.string.AED) + " " + Constants.formatAmount(String.valueOf(Double.valueOf(discountedAmount) + Double.valueOf(transactionFees))));
            } else if (discount_on.equalsIgnoreCase("F")) {
                tvDiscountLable.setText(R.string.discount_on_fees_amount);
                String discountedFees = Constants.formatAmount(String.valueOf(Double.valueOf(transactionFees) - Double.valueOf(discount)));
                tvTotalAmount.setText(getString(R.string.AED) + " " + Constants.formatAmount(String.valueOf(Double.valueOf(amount) + Double.valueOf(discountedFees))));
            }
        } else {
            rlDiscount.setVisibility(View.GONE);
            tvTotalAmount.setText(getString(R.string.AED) + " " + Constants.formatAmount(String.valueOf(Double.valueOf(amount) + Double.valueOf(transactionFees))));
        }

        tvAmountValue.setText(getString(R.string.AED) + " " + Constants.formatAmount(amount));
        tvTransactionFee.setText((getString(R.string.AED) + " " + Constants.formatAmount(transactionFees)));


        tvAmountLable.setText(getString(R.string.AED) + " " + Constants.formatAmount(amount));
    }

    @OnClick({R.id.btnConfirm, R.id.act_rac_rl_edit})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btnConfirm:
                handleConfirm();
                break;
            case R.id.act_rac_rl_edit:
                onBackPressed();
                break;

        }
        // showOTPDialog();
    }

    private void handleConfirm() {
        isSpanClicked = false;

        if (Constants.checkToShowOtpDialog(P2MConfirmationActivity.this, amount)) {
            callGenerateOTP(isSpanClicked);
        } else {
            callP2MApi("");
        }
    }

    private void showOTPDialog() {

        otpDialog = new ValidateOTPDialog(this, new ValidateOTPDialog.IValidateOTPDialogCallBack() {


            @Override
            public void resendOTPCallBack(String otpValue) {

            }

            @Override
            public void verifyOTPCallBack(String otpValue) {
                // verifyOTP(otpValue);
                callP2MApi(otpValue);
            }
        });
        otpDialog.show();

    }

/*    private void verifyOTP(final String otpValue) {

        if (otpValue.length() == getResources().getInteger(R.integer.OTPLength)) {

            showProgress();

            MovitWalletController validateOTPController = new ValidateOTPController(TXN_TYPE, otpValue);

            validateOTPController.init(new IMovitWalletController() {
                @Override
                public void onSuccess(Object response) {

                    dismissProgress();
                    Toast.makeText(P2MConfirmationActivity.this, "Verified Success" , Toast.LENGTH_SHORT).show();
                    callP2MApi(otpValue);
                    otpDialog.cancel();

                }

                @Override
                public void onFailed(String errorCode, String reason) {
                    dismissProgress();
                    showError(reason);
                }
            });

        } else {
            showError("Please enter " + getResources().getInteger(R.integer.OTPLength) + " digit OTP.");
        }

    }*/

    private void callP2MApi(String otpValue) {
        showProgress();

        P2MController p2MController = new P2MController(merchantId, amount, otpValue, promoCode);
        p2MController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {
                if (otpDialog != null) {
                    otpDialog.dismiss();
                }
                handleP2MSuccess(response);
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                //showError(reason);

                new OkDialog(P2MConfirmationActivity.this, null, reason, new OkDialog.IOkDialogCallback() {
                    @Override
                    public void handleResponse() {

                    }
                });

            }
        });

    }

    private void handleP2MSuccess(Object response) {
        Bundle bundle = (Bundle) response;
        bundle.putString(IntentConstants.MERCHANTNAME, merchantName);
        Intent intent = new Intent(this, TransactionSuccessActivity.class);
        intent.putExtras(bundle);
        //  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(IntentConstants.TRANSACTION_TYPE, IntentConstants.P2M);
        startActivity(intent);
        //finish();


    }


    @Override
    public void onSpanClick(String strText) {
        isSpanClicked = true;
        callGenerateOTP(isSpanClicked);
    }

    private void callGenerateOTP(final boolean isSpanClicked) {
        showProgress();

        MovitWalletController generateOTPController = new GenerateOTPController(TXN_TYPE);

        generateOTPController.init(new IMovitWalletController() {
            @Override
            public void onSuccess(Object response) {

                dismissProgress();


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String code = "";
                        try {
                            if (isSpanClicked) {
                                if (otpDialog.isShowing()) {
                                    otpDialog.refreshOnResend();
                                }
                            } else {
                                showOTPDialog();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }, 200);


            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                showError(reason);

            }
        });
        if (isSpanClicked) {
            this.isSpanClicked = false;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
