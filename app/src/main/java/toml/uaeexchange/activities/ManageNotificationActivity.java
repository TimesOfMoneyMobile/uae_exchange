package toml.uaeexchange.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;

public class ManageNotificationActivity extends BaseActivity {

    @BindView(R.id.imgvwLeft)
    ImageView imgvwLeft;
    /*@BindView(R.id.relBack)
    RelativeLayout relBack;
    @BindView(R.id.title_pay)
    TextView titlePay;*/
    @BindView(R.id.imgWallet)
    ImageView imgWallet;
    @BindView(R.id.txt_pay_Balance)
    TextView txtPayBalance;
    @BindView(R.id.relBalance)
    RelativeLayout relBalance;
    @BindView(R.id.imgvwRight)
    ImageView imgvwRight;
    /* @BindView(R.id.toolbar_pay)
     Toolbar toolbarPay;*/
    @BindView(R.id.tvManageInfo)
    TextView tvManageInfo;
    @BindView(R.id.rlManagePush)
    RelativeLayout rlManagePush;
    @BindView(R.id.vManagePush)
    View vManagePush;
    @BindView(R.id.ivDownArrowSetting)
    ImageView ivDownArrowSetting;
    @BindView(R.id.rlEmailManage)
    RelativeLayout rlEmailManage;
    @BindView(R.id.tvDouWantEmail)
    TextView tvDouWantEmail;
    @BindView(R.id.rlEmailValue)
    RelativeLayout rlEmailValue;
    @BindView(R.id.ivToggleEnable)
    ImageView ivToggleEnable;
    @BindView(R.id.ivEmailCb)
    ImageView ivEmailCb;

    @BindView(R.id.act_mn_tv_email)
    TextView tvEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_notification);
        ButterKnife.bind(this);

        setToolbarWithBalance(getString(R.string.manage_notifications), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, 0, null, false);

        handleUI();


    }

    private void handleUI() {

        if (AppSettings.getData(ManageNotificationActivity.this, AppSettings.EMAIL_ID).isEmpty()) {
            ivEmailCb.setVisibility(View.GONE);
            tvEmail.setVisibility(View.GONE);
        } else {
            tvEmail.setVisibility(View.VISIBLE);
            tvEmail.setText(AppSettings.getData(ManageNotificationActivity.this, AppSettings.EMAIL_ID));
            ivEmailCb.setVisibility(View.VISIBLE);
        }


        if (AppSettings.getBooleanData(this, AppSettings.NOTIFICATION_ACTIVE)) {
            // AppSettings.putBooleanData(this, AppSettings.NOTIFICATION_TOGGLE, false);
            ivToggleEnable.setImageResource(R.drawable.togle_enable);

        } else {
            ivToggleEnable.setImageResource(R.drawable.toggle_disable);
            //  AppSettings.putBooleanData(this, AppSettings.NOTIFICATION_TOGGLE, true);
        }

        rlEmailValue.setVisibility(View.GONE);
        ivDownArrowSetting.setImageResource(R.drawable.down_arrow_setting);

    }

    @OnClick({R.id.ivToggleEnable, R.id.ivEmailCb, R.id.rlEmailManage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivToggleEnable:
                togglePushNotification();
                break;
            case R.id.rlEmailManage:

                if (rlEmailValue.getVisibility() == View.GONE) {
                    rlEmailValue.setVisibility(View.VISIBLE);
                    ivDownArrowSetting.setImageResource(R.drawable.up_arrow_setting);

                } else {
                    rlEmailValue.setVisibility(View.GONE);
                    ivDownArrowSetting.setImageResource(R.drawable.down_arrow_setting);
                }


                break;
            case R.id.ivEmailCb:
                break;
        }
    }

    private void togglePushNotification() {


        if (AppSettings.getBooleanData(this, AppSettings.NOTIFICATION_TOGGLE)) {
            ivToggleEnable.setImageResource(R.drawable.togle_enable);
            AppSettings.putBooleanData(this, AppSettings.NOTIFICATION_TOGGLE, false);
            AppSettings.putBooleanData(this, AppSettings.NOTIFICATION_ACTIVE, true);
            //showError("toggle_disable");


        } else {
            AppSettings.putBooleanData(this, AppSettings.NOTIFICATION_TOGGLE, true);
            ivToggleEnable.setImageResource(R.drawable.toggle_disable);
            AppSettings.putBooleanData(this, AppSettings.NOTIFICATION_ACTIVE, false);
            //showError("togle_enable");

        }
    }
}