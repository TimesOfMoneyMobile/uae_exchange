package toml.uaeexchange.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomTabLayout;
import toml.uaeexchange.fragments.RecentTransactionsFragment;
import toml.uaeexchange.fragments.RecentTransactionsTxnHistoryFragment;
import toml.uaeexchange.fragments.TransactionStatement;

public class TransactionHistoryActivity extends BaseActivity {


    CustomTabLayout customTabLayout;

    @BindView(R.id.history_tab_layout)
    LinearLayout tabLayout;
    @BindView(R.id.history_viewpager)
    ViewPager viewPager;

    //Refresh Icon on Toolbar
    @BindView(R.id.relRightIamge)
    RelativeLayout relRightIamge;
    @BindView(R.id.imgvwRight)
    ImageView imgvwRight;

    RecentTransactionsTxnHistoryFragment recentTransactionsTxnHistoryFragment;
    TransactionStatement transactionStatement;
    RecentTransactionsFragment transactionsFragment;

    TransactionHistoryFragmentPagerAdapter objTransactionHistoryFragmentPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        ButterKnife.bind(this);

        setToolbarWithBalance("Transaction History", R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }, 0, null, false);


        customTabLayout = new CustomTabLayout(tabLayout);

        customTabLayout.setTabListener(new CustomTabLayout.IOnTabClicked() {
            @Override
            public void onTabClicked(int index) {

                viewPager.setCurrentItem(index);
            }

        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                customTabLayout.setCurrentActiveTab(position, ContextCompat.getColor(TransactionHistoryActivity.this, R.color.white), 0);
                callRefreshApi(position);

               /* if (objTransactionHistoryFragmentPagerAdapter != null && position == 0) {
                    objTransactionHistoryFragmentPagerAdapter.notifyDataSetChanged();
                }
*/

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        customTabLayout.setTabTexts(getString(R.string.recent_transactions), getString(R.string.get_statement));
        customTabLayout.setTabTextColor(ContextCompat.getColor(TransactionHistoryActivity.this, R.color.white), ContextCompat.getColor(TransactionHistoryActivity.this, R.color.white));
        customTabLayout.setCurrentActiveTab(0, ContextCompat.getColor(TransactionHistoryActivity.this, R.color.white), 0);
        objTransactionHistoryFragmentPagerAdapter = new TransactionHistoryFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(objTransactionHistoryFragmentPagerAdapter);
        viewPager.setOffscreenPageLimit(0);


        //callApi();
    }

    public void callRefreshApi(int position) {

        final int tabClicked = position;

        if (tabClicked == 1) {
            relRightIamge.setVisibility(View.GONE);
        } else {
            relRightIamge.setVisibility(View.VISIBLE);
            imgvwRight.setImageResource(R.drawable.loading);
        }

        relRightIamge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tabClicked == 2) {
                    transactionStatement.checkAndCallApi();
                }


                if (tabClicked == 0) {

                    recentTransactionsTxnHistoryFragment.reLoadFragment();

                }

            }
        });
    }


    public class TransactionHistoryFragmentPagerAdapter extends FragmentStatePagerAdapter {
        final int PAGE_COUNT = 2;

        public TransactionHistoryFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public int getItemPosition(Object object) {
            // Causes adapter to reload all Fragments when
            // notifyDataSetChanged is called
            return POSITION_NONE;
        }


        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return recentTransactionsTxnHistoryFragment = new RecentTransactionsTxnHistoryFragment();
            return transactionStatement = new TransactionStatement();

        }
    }


}
