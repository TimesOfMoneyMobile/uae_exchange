package toml.uaeexchange.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toml.movitwallet.utils.Constants;
import toml.movitwallet.utils.IntentConstants;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomButton;

public class TransactionSuccessActivity extends BaseActivity {


    @BindView(R.id.tvTransactionIdVal)
    TextView tvTransactionIdVal;
    @BindView(R.id.tvSuccessMsg)
    TextView tvSuccessMsg;
    @BindView(R.id.tvContinue)
    CustomButton tvContinue;
    @BindView(R.id.tvTransactionSuccess)
    TextView tvTransactionSuccess;
    private String merchantName;
    Spannable spannable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_success);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (getIntent().getStringExtra(IntentConstants.TRANSACTION_TYPE).equalsIgnoreCase(IntentConstants.P2M)) {

                merchantName = bundle.getString(IntentConstants.MERCHANTNAME);

                String sourceString = getString(R.string.thanks_part_1) + " " + merchantName + getString(R.string.thanks_part_2);

                int i = sourceString.indexOf(merchantName);

                SpannableStringBuilder str = new SpannableStringBuilder(sourceString);
                str.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TransactionSuccessActivity.this, R.color.black)), i, i + merchantName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvSuccessMsg.setText(str);


                tvTransactionIdVal.setText(getString(R.string.transactionId) + ": " + bundle.getString(IntentConstants.TXNID));

                //  tvDateVal.setText();

            } else if (getIntent().getStringExtra(IntentConstants.TRANSACTION_TYPE).equalsIgnoreCase(IntentConstants.RECHARGE_TYPE)) {
                tvTransactionIdVal.setText(getString(R.string.transactionId) + ": " + bundle.getString(IntentConstants.TXNID));

                String transferAmount = getString(R.string.AED) + " " + toml.uaeexchange.utilities.Constants.formatAmount(bundle.getString(IntentConstants.TRANSFER_AMOUNT));
                String sourceString = getString(R.string.thanks_part_recharge_1) + " " + transferAmount + " " + getString(R.string.thanks_part_recharge_2);

                int i = sourceString.indexOf(transferAmount);

                SpannableStringBuilder str = new SpannableStringBuilder(sourceString);
                str.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TransactionSuccessActivity.this, R.color.black)), i, i + transferAmount.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvSuccessMsg.setText(str);


                //tvSuccessMsg.setText(getString(R.string.thank_you_message_without_name));
                tvTransactionSuccess.setText(getString(R.string.recharge_success));

                if (Constants.PAYMODE_TYPE.equalsIgnoreCase("W2W")) {
                    //  tvSuccessMsg.setText(getString(R.string.thank_you_message_without_name));
                    tvTransactionSuccess.setText(getString(R.string.recharge_success));
                } else {
                    // tvSuccessMsg.setText(getString(R.string.thank_you_message_without_name));
                    tvTransactionSuccess.setText(getString(R.string.recharge_underprocess));
                }
            }
        }

    }


    @OnClick(R.id.tvContinue)
    public void onViewClicked() {
        BaseActivity.openDashboardActivity(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        BaseActivity.openDashboardActivity(this);
    }
}
