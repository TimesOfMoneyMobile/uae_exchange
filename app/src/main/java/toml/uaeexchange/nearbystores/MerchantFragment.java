package toml.uaeexchange.nearbystores;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomTabLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.models.LocationResponse;
import toml.uaeexchange.models.Store;

/**
 * Created by vishwanathp on 10/30/2017.
 */

public class MerchantFragment extends Fragment {


    Context mContext;
    Unbinder unbinder;
    private View mView;


    @BindView(R.id.frag_ml_rv_store_list)
    RecyclerView rvStoreList;
    @BindView(R.id.frag_ml_tv_no_merchants)
    TextView tvNoMerchants;
    @BindView(R.id.frag_ml_pb_progressBar)
    ProgressBar pbProgress;

    StoreAdapter storeAdapter;
    LocationResponse locationResponse;

    MerchantFragment merchantFragment;
    List<Store> storeList;



    public MerchantFragment(List<Store> storeList) {
        this.storeList = storeList;
    }


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_merchant_list, null, false);
        merchantFragment = this;
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handleLayout();

    }

    private void handleLayout() {

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvStoreList.setLayoutManager(horizontalLayoutManagaer);

        pbProgress.setVisibility(View.VISIBLE);
        tvNoMerchants.setVisibility(View.GONE);

        setAdapter();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void setAdapter() {

        //if (locationResponse.getResponseDetails().getNearbyStores().getStoreList() != null) {
          //  if (locationResponse.getResponseDetails().getNearbyStores().getStoreList().size() > 0) {
        if(storeList != null){
            if(storeList.size() > 0){

                pbProgress.setVisibility(View.GONE);
                tvNoMerchants.setVisibility(View.GONE);


                storeAdapter = new StoreAdapter(mContext,storeList, "merchant");
                rvStoreList.setAdapter(storeAdapter);
            } else {
                showError();
            }
        } else {
            showError();
        }

    }

    public void showError() {
        if (isAdded()) {
            tvNoMerchants.setVisibility(View.VISIBLE);
            tvNoMerchants.setText(getString(R.string.nonearbymerchant));
            pbProgress.setVisibility(View.GONE);
        }
    }
}
