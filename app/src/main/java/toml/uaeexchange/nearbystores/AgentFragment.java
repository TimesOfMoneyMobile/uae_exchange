package toml.uaeexchange.nearbystores;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;
import toml.uaeexchange.models.LocationResponse;
import toml.uaeexchange.models.Store;

/**
 * Created by vishwanathp on 10/30/2017.
 */

public class AgentFragment extends Fragment{

    Context mContext;
    Unbinder unbinder;
    private View mView;


    @BindView(R.id.frag_al_rv_agent_list)
    RecyclerView rvAgentList;
    @BindView(R.id.frag_al_tv_no_agents)
    TextView tvNoAgents;
    @BindView(R.id.frag_al_pb_progressBar)
    ProgressBar pbProgress;

    StoreAdapter storeAdapter;

    LocationResponse locationResponse;

    AgentFragment agentFragment;
    List<Store>storeList;

    public AgentFragment(LocationResponse response) {
        locationResponse = response;
        LogUtils.Verbose("locationResponse",locationResponse.getResponseDetails().getNearbyStores().getStoreList().size()+"");
    }



    public AgentFragment(List<Store> storeList) {
        this.storeList =storeList;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_agent_list, null, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handleLayout();

    }

    private void handleLayout() {

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvAgentList.setLayoutManager(horizontalLayoutManagaer);


        pbProgress.setVisibility(View.VISIBLE);
        tvNoAgents.setVisibility(View.GONE);


        setAdapter();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void setAdapter() {
      //  if (locationResponse.getResponseDetails().getNearbyStores().getStoreList() != null) {
        //    if (locationResponse.getResponseDetails().getNearbyStores().getStoreList().size() > 0) {
        if(storeList != null){

            if(storeList.size() > 0){
                pbProgress.setVisibility(View.GONE);
                tvNoAgents.setVisibility(View.GONE);


                storeAdapter = new StoreAdapter(mContext, storeList, "agent");
                rvAgentList.setAdapter(storeAdapter);
            } else {
                showError();
            }
        } else {
            showError();
        }
    }

    public void showError() {
        if(isAdded()) {
            tvNoAgents.setVisibility(View.VISIBLE);
            tvNoAgents.setText(getString(R.string.nonearbyagent));
            pbProgress.setVisibility(View.GONE);
        }
    }
}
