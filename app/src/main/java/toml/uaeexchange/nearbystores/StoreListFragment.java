package toml.uaeexchange.nearbystores;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import toml.movitwallet.models.Agent;
import toml.movitwallet.models.Merchant;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.customviews.CustomTabLayout;
import toml.uaeexchange.fragments.DashboardFragment;
import toml.uaeexchange.fragments.FavouritesFragment;
import toml.uaeexchange.fragments.RecentTransactionsFragment;
import toml.uaeexchange.fragments.SendMoneyFragment;
import toml.uaeexchange.models.LocationResponse;
import toml.uaeexchange.models.Store;

/**
 * Created by vishwanathp on 10/27/2017.
 */

public class StoreListFragment extends Fragment {

    Context mContext;
    Unbinder unbinder;
    private View mView;


    @BindView(R.id.frag_sl_tab_layout)
    LinearLayout tabLayout;
    @BindView(R.id.frag_sl_viewpager)
    ViewPager viewPager;

    CustomTabLayout customTabLayout;

    AgentFragment agentFragment;
    MerchantFragment merchantFragment;
    SampleFragmentPagerAdapter sampleFragmentPagerAdapter;
    LocationResponse response;

    List<Store>storeList;
    List <Store>agentList ;
    List<Store>merchantList;

    public StoreListFragment(LocationResponse locationResponse) {
        response = locationResponse;
        LogUtils.Verbose("locationResponse", response.getResponseDetails().getNearbyStores().getStoreList().size() + "");
    }



    public StoreListFragment(List<Store> categoryList) {
        storeList = categoryList;
        agentList  = new ArrayList<>();
        merchantList  = new ArrayList<>();
        reloadList();

        LogUtils.Verbose("StoreListFragment","StoreListFragment constructor");

    }

    private void reloadList() {

        if(agentList.size() > 0){
            agentList.clear();
        }

        if(merchantList.size() > 0){
            merchantList.clear();
        }

        for(int i=0;i<storeList.size();i++){
            if(storeList.get(i).getType().equalsIgnoreCase("ag")){

                agentList.add(storeList.get(i));

            }else if(storeList.get(i).getType().equalsIgnoreCase("me")){

                merchantList.add(storeList.get(i));
            }
        }
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_store_list, null, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handleLayout();

    }

    private void handleLayout() {

        // agentFragment = new AgentFragment();

        // merchantFragment = new MerchantFragment();


        sampleFragmentPagerAdapter = new SampleFragmentPagerAdapter(getChildFragmentManager());
        customTabLayout = new CustomTabLayout(tabLayout);

        customTabLayout.setTabListener(new CustomTabLayout.IOnTabClicked() {
            @Override
            public void onTabClicked(int index) {

                viewPager.setCurrentItem(index);
            }

        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                customTabLayout.setCurrentActiveTab(position, ContextCompat.getColor(mContext, R.color.clear_blue), 0);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        customTabLayout.setTabTexts(getString(R.string.agents), getString(R.string.merchants));
        customTabLayout.setTabTextColor(ContextCompat.getColor(mContext, R.color.clear_blue), ContextCompat.getColor(mContext, R.color.clear_blue));
        customTabLayout.setTabColor(ContextCompat.getColor(mContext, R.color.white));
        customTabLayout.setCurrentActiveTab(0, ContextCompat.getColor(mContext, R.color.clear_blue), 0);


        viewPager.setAdapter(sampleFragmentPagerAdapter);

        // setAdapter(response);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

//    public void setAdapter(LocationResponse locationResponse) {
//        agentFragment.setAdapter(locationResponse);
//        merchantFragment.setAdapter(locationResponse);
//    }

    public void reLoadFragment() {
        reloadList();
        sampleFragmentPagerAdapter.notifyDataSetChanged();
    }


    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;

        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public int getCount() {
            return PAGE_COUNT;
        }


        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                if (agentFragment == null) {
                    agentFragment = new AgentFragment(agentList);
                    // agentFragment.setAdapter(response);
                }

                return agentFragment;
            } else {
                if (merchantFragment == null) {
                    merchantFragment = new MerchantFragment(merchantList);
                    // merchantFragment.setAdapter(response);
                }
                return merchantFragment;
            }


        }

        @Override
        public int getItemPosition(Object object) {
            // Causes adapter to reload all Fragments when
            // notifyDataSetChanged is called
            return POSITION_NONE;
        }
    }
}
