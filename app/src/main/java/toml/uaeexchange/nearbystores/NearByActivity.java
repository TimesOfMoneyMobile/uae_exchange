package toml.uaeexchange.nearbystores;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.MarkerManager;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.controllers.NearByStoresController;
import toml.movitwallet.utils.IMovitWalletController;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitWalletController;
import toml.uaeexchange.R;
import toml.uaeexchange.activities.BaseActivity;
import toml.uaeexchange.activities.PendingRequestActivity;
import toml.uaeexchange.activities.RequestMoneyActivity;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.dialogs.OkDialog;
import toml.uaeexchange.fragments.LeftMenuFragment;
import toml.uaeexchange.fragments.NotificationFragment;
import toml.uaeexchange.models.LocationResponse;
import toml.uaeexchange.models.Store;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

/**
 * Created by vishwanathp on 10/26/2017.
 */

public class NearByActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener {

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;


    @BindView(R.id.relSettings)
    RelativeLayout relSettings;
    @BindView(R.id.tvSwitchName)
    TextView tvSwitch;
    @BindView(R.id.act_explore_near_by_rv_list)
    RecyclerView rvList;

    @BindView(R.id.act_nearby_cet_search)
    CustomEditTextLayout cetSearch;

    SupportMapFragment mapFragment;
    GoogleMap gMap;
    GoogleApiClient googleApiClient;
    LocationRequest mLocationRequest;
    LocationListener locationListener;
    Location mCurrentLocation;


    private ClusterManager<Store> mClusterManager;
    public static LatLngBounds.Builder boundsBuilder;


    List<Store> categoryList = new ArrayList<>();


    StoreAdapter adapter;

    double currentLatitude, currentLongitude;
    private boolean executeOnlyonce = true;

    StoreListFragment storeListFragment;

    LocationResponse locationResponse;
    PlaceAutocompleteFragment autocompleteFragment;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_by);
        ButterKnife.bind(this);

        setToolBarWithRightText(getString(R.string.menu_nearby_agents), R.drawable.icon_back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        }, null);

//        autocompleteFragment = (PlaceAutocompleteFragment)
//                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);


        cetSearch.getVerticalDivider().setVisibility(View.GONE);
        cetSearch.getImgRight().setVisibility(View.VISIBLE);
        cetSearch.getImgRight().setImageResource(R.drawable.ic_detect_location);

        cetSearch.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(NearByActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }


            }
        });

        cetSearch.getImgRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryList.clear();
                getCurrentLocation();
                showProgress();


            }
        });


        relSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tvSwitch.getText().equals(getString(R.string.list_view))) {
                    tvSwitch.setText(getString(R.string.map_view));

                    if (executeOnlyonce) {
                        executeOnlyonce = false;

                        storeListFragment = new StoreListFragment(categoryList);

                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.act_near_by_frame_layout, storeListFragment).commit();

                    } else {
                        storeListFragment.reLoadFragment();
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.show(storeListFragment).commit();
                    }

                } else if (tvSwitch.getText().equals(getString(R.string.map_view))) {

                    tvSwitch.setText(getString(R.string.list_view));
                    storeListFragment.reLoadFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.hide(storeListFragment).commit();
                }
            }
        });

        checkLocationPermissions();
        //getClustersFromApi();
        showProgress();


        mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.act_explore_near_by_map);
        mapFragment.getMapAsync(this);



        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(NearByActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rvList.setLayoutManager(horizontalLayoutManagaer);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                cetSearch.getEditText().setText(place.getAddress());
                cetSearch.getEditText().setSelection(place.getAddress().toString().length());


                LatLng latLng = place.getLatLng();
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        place.getLatLng(), 15);
                gMap.animateCamera(location);
                currentLatitude = latLng.latitude;
                currentLongitude = latLng.longitude;

                LogUtils.Verbose("currentLatitude", currentLatitude + "=====currentLongitude" + currentLongitude);

                showProgress();


                getClustersFromApi();

                // Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                // Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.getUiSettings().setMapToolbarEnabled(false);
        mClusterManager = new ClusterManager<>(NearByActivity.this, gMap);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        googleApiClient.connect();
    }


    private void checkLocationPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        } else {
            checkLocationSettings();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            checkLocationSettings();
        } else {
            //Show error message
            showMissingPermissionError();
        }
    }

    private void showMissingPermissionError() {
        Toast.makeText(this, " Location Permission is denied", Toast.LENGTH_SHORT).show();
    }


    private void checkLocationSettings() {

        googleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();


        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5 * 1000);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest
                .Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient,
                        builder.build());


        //showProgress();


        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                final LocationSettingsStates state = locationSettingsResult.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        if (ActivityCompat.checkSelfPermission(NearByActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                                &&
                                ActivityCompat.checkSelfPermission(NearByActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        } else {
                            getCurrentLocation();

                        }
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(NearByActivity.this, 2);
                        } catch (IntentSender.SendIntentException ignored) {
                        }
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        //  Toast.makeText(NearByActivity.this, " Location service is not activated", Toast.LENGTH_SHORT).show();

                        break;
                }

            }
        });

    }


    private void getCurrentLocation() {
        // Acquire a reference to the system Location Manager
        final LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // showProgress();
        // Define a listener that responds to location updates
        locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {

                //dismissProgress();
                // Called when a new location is found by the network location provider.
                if (ActivityCompat.checkSelfPermission(NearByActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                } else {
                    mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(
                            googleApiClient);
                    if (mCurrentLocation != null) {

                        currentLatitude = mCurrentLocation.getLatitude();
                        currentLongitude = mCurrentLocation.getLongitude();

                        LogUtils.Verbose("currentLatitude", currentLatitude + "=====currentLongitude" + currentLongitude);
                        gMap.setMyLocationEnabled(true);

                        getCompleteAddressString(currentLatitude, currentLongitude);
                        getClustersFromApi();

                        locationManager.removeUpdates(locationListener);

                    } else {
                        showError(getString(R.string.sorry_could_not_detect_your_location));
                    }

                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
                dismissProgress();
            }
        };


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                cetSearch.getEditText().setText(strReturnedAddress.toString());
                cetSearch.getEditText().setSelection(strReturnedAddress.toString().length());
                LogUtils.Verbose("My Current loction address", strReturnedAddress.toString());
            } else {
                LogUtils.Verbose("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.Verbose("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }

    private void getClustersFromApi() {
        MovitWalletController movitWalletController = new NearByStoresController(currentLatitude + "", currentLongitude + "");
        movitWalletController.init(new IMovitWalletController<String>() {

            @Override
            public void onSuccess(String response) {
                dismissProgress();
                handleAPIResult(response);
            }

            @Override
            public void onFailed(String errorCode, String reason) {
                dismissProgress();
                //  handleAPIResult();
                new OkDialog(NearByActivity.this, reason, "", null);
            }
        });
    }

    private void handleAPIResult(String response) {

        boundsBuilder = new LatLngBounds.Builder();

        //LogUtils.Verbose("TAG", "Response " + response);
        Serializer serializer = new Persister();
        try {

//            InputStream object = getResources()
//                    .openRawResource(R.raw.response);


            locationResponse = serializer.read(LocationResponse.class, response, false);


            if (locationResponse.getResponseDetails().getNearbyStores().getStoreList() != null) {
                if (locationResponse.getResponseDetails().getNearbyStores().getStoreList().size() > 0) {

                    if (categoryList.size() != 0) {
                        categoryList.clear();
                    }
                    categoryList.addAll(locationResponse.getResponseDetails().getNearbyStores().getStoreList());

                    setData();
                    setUpClusterer();

                } else {
                    new OkDialog(NearByActivity.this, "", getString(R.string.nonearbyagent), null);
                }
            } else {
                new OkDialog(NearByActivity.this, "", getString(R.string.nonearbyagent), null);
            }


        } catch (Exception e) {
            LogUtils.Verbose("Exception", e.toString());
            new OkDialog(NearByActivity.this, "", e.toString(), null);
        }


    }


    private void setData() {
        adapter = new StoreAdapter(NearByActivity.this, categoryList, "map");
        rvList.setAdapter(adapter);

        if(storeListFragment != null){
            storeListFragment.reLoadFragment();
        }

        rvList.setOnFlingListener(null);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rvList);
    }


    private void setUpClusterer() {

        if (mClusterManager != null) {
            mClusterManager.clearItems();
        }

        mClusterManager.setRenderer(new StoreMarkerRenderer());
        gMap.setOnCameraIdleListener(mClusterManager);
        gMap.setOnMarkerClickListener(mClusterManager);

        mClusterManager.addItems(categoryList);

        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<Store>() {
            @Override
            public boolean onClusterItemClick(Store store) {
                // showInfoWindow(store);

                rvList.getLayoutManager().smoothScrollToPosition(rvList, null, categoryList.indexOf(store));
                return false;
            }
        });
        gMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 200));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gMap.animateCamera(CameraUpdateFactory.zoomTo(gMap.getCameraPosition().zoom - 0.1f), 800, null);
            }
        }, 3000);


    }



    class StoreMarkerRenderer extends DefaultClusterRenderer<Store> {

        private final IconGenerator mIconGenerator = new IconGenerator(NearByActivity.this);
        private ImageView mImageView;
        private int mDimension;

        public StoreMarkerRenderer() {
            super(NearByActivity.this, gMap, mClusterManager);
            mImageView = new ImageView(NearByActivity.this);
            mDimension = 80;

            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = 10;
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);

            //mIconGenerator.
            //mIconGenerator.setStyle(IconGenerator.STYLE_PURPLE);
            //mIconGenerator.setColor(set);


        }

        @Override
        protected void onBeforeClusterItemRendered(Store item, MarkerOptions markerOptions) {
            item.setPositionValue(i);
            i++;
            LogUtils.Verbose("Tag", "onBeforeClusterItemRendered");
            switch (item.getType()) {
                case "Main Location":
                    mImageView.setImageResource(R.drawable.im_nearby_agent_blue);
                    break;


                case "ME":
                    mImageView.setImageResource(R.drawable.qnb_nearby_food_pink);
                    break;

                case "AG":
                    mImageView.setImageResource(R.drawable.qnb_nearby_hospital_pink);
                    break;

                case "Liquor":
                    mImageView.setImageResource(R.drawable.qnb_nearby_liquor_pink);
                    break;

                case "Shop":
                    mImageView.setImageResource(R.drawable.qnb_nearby_store_pink);
                    break;

                case "Supermarket":
                    mImageView.setImageResource(R.drawable.qnb_nearby_shoping_pink);
                    break;
                case "Petrochemicals":
                    mImageView.setImageResource(R.drawable.im_nearby_petrol_blue);
                    break;
            }

            if (item.getMCC_Code().isEmpty()) {
                mImageView.setImageResource(R.drawable.im_nearby_agent_blue);
            }
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
            markerOptions.alpha(1.0f);

        }


    }


}
