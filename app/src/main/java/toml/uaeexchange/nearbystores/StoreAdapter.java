package toml.uaeexchange.nearbystores;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toml.movitwallet.models.ListItem;
import toml.movitwallet.models.Transaction;
import toml.movitwallet.models.TransactionItem;
import toml.movitwallet.utils.LogUtils;
import toml.uaeexchange.R;
import toml.uaeexchange.adapters.NewTransactionsHistoryAdapter;
import toml.uaeexchange.models.Store;

/**
 * Created by vishwanathp on 10/26/2017.
 */

public class StoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Store> storeList = new ArrayList<>();
    private String typeOfview;


    public StoreAdapter(Context context, List<Store> storeList, String viewType) {
        mContext = context;
        this.storeList = storeList;
        typeOfview = viewType;


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(mContext);

        switch (typeOfview) {

            case "map":
                View v1 = inflater.inflate(R.layout.list_horizontal_nearby_store_data, parent,
                        false);
                viewHolder = new HorizontalStoreViewHolder(v1);
                break;

            case "merchant":

                View v2 = inflater.inflate(R.layout.list_vertical_nearby_store_data, parent, false);
                viewHolder = new VerticalStoreViewHolder(v2);
                break;

            case "agent":

                View v3 = inflater.inflate(R.layout.list_vertical_nearby_store_data, parent, false);
                viewHolder = new VerticalStoreViewHolder(v3);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Store store = storeList.get(position);
        switch (typeOfview) {

            case "map":
                HorizontalStoreViewHolder horizontalStoreViewHolder = (HorizontalStoreViewHolder) holder;
                horizontalStoreViewHolder.tvStoreName.setText(store.getName());
                horizontalStoreViewHolder.tvAddress.setText(store.getAddress());
                horizontalStoreViewHolder.tvMobile.setText(mContext.getString(R.string.country_code));
                horizontalStoreViewHolder.tvDistance.setText(String.format("%.2f", Double.valueOf(store.getDistanceInKm())) + " " + mContext.getString(R.string.kms_away));

                if (store.getType().equalsIgnoreCase("ag"))
                    horizontalStoreViewHolder.tvOffer.setText("");
                else
                    horizontalStoreViewHolder.tvOffer.setText("Offer : 10% off on Dine In");

                horizontalStoreViewHolder.rlCheckIn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?daddr=" + store.getLatitude() + "," + store.getLongitude()));
                        mContext.startActivity(intent);

                    }
                });

                break;

            case "agent":
                VerticalStoreViewHolder verticalStoreViewHolder = (VerticalStoreViewHolder) holder;

                if (store.getType().equalsIgnoreCase("ag")) {

                    verticalStoreViewHolder.cvCardView.setVisibility(View.VISIBLE);

                    verticalStoreViewHolder.tvStoreName.setText(store.getName());
                    verticalStoreViewHolder.tvDistance.setText(String.format("%.2f", Double.valueOf(store.getDistanceInKm())) + " " + mContext.getString(R.string.kms_away));
                    verticalStoreViewHolder.tvAddress.setText(store.getAddress());
                    verticalStoreViewHolder.tvMobile.setText(mContext.getString(R.string.country_code));

                    verticalStoreViewHolder.tvOffer.setText("");

                    verticalStoreViewHolder.rlCheckIn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://maps.google.com/maps?daddr=" + store.getLatitude() + "," + store.getLongitude()));
                            mContext.startActivity(intent);

                        }
                    });


                } else {
                    verticalStoreViewHolder.cvCardView.setVisibility(View.GONE);

                }


                break;

            case "merchant":


                VerticalStoreViewHolder verticalViewHolder = (VerticalStoreViewHolder) holder;

                if (store.getType().equalsIgnoreCase("me")) {


                    verticalViewHolder.cvCardView.setVisibility(View.VISIBLE);

                    verticalViewHolder.tvStoreName.setText(store.getName());
                    verticalViewHolder.tvDistance.setText(String.format("%.2f", Double.valueOf(store.getDistanceInKm())) + " " + mContext.getString(R.string.kms_away));
                    verticalViewHolder.tvAddress.setText(store.getAddress());
                    verticalViewHolder.tvMobile.setText(mContext.getString(R.string.country_code));

                    verticalViewHolder.tvOffer.setText("Offer : 10% off on Dine In");

                    verticalViewHolder.rlCheckIn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://maps.google.com/maps?daddr=" + store.getLatitude() + "," + store.getLongitude()));
                            mContext.startActivity(intent);

                        }
                    });


                } else {
                    verticalViewHolder.cvCardView.setVisibility(View.GONE);


                }

                break;
        }

    }

    @Override
    public int getItemCount() {
        return storeList != null ? storeList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    // View holder for general row item
    class HorizontalStoreViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.list_near_by_rl_store_name)
        RelativeLayout rlStoreName;
        @BindView(R.id.list_near_by_tv_store_name)
        TextView tvStoreName;


        @BindView(R.id.list_near_by_rl_address)
        RelativeLayout rlAddress;
        @BindView(R.id.list_near_by_tv_address)
        TextView tvAddress;


        @BindView(R.id.list_near_by_rl_mobile)
        RelativeLayout rlMobile;
        @BindView(R.id.list_near_by_tv_mobile)
        TextView tvMobile;
        @BindView(R.id.list_near_by_tv_distance)
        TextView tvDistance;

        @BindView(R.id.list_near_by_tv_offer)
        TextView tvOffer;

        @BindView(R.id.list_near_by_rl_check_in)
        RelativeLayout rlCheckIn;

        HorizontalStoreViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


    // View holder for general row item
    class VerticalStoreViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.list_near_by_cv_card_view)
        CardView cvCardView;

        @BindView(R.id.list_near_by_rl_store_name)
        RelativeLayout rlStoreName;
        @BindView(R.id.list_near_by_tv_store_name)
        TextView tvStoreName;

        @BindView(R.id.list_near_by_rl_distance)
        RelativeLayout rlDistance;
        @BindView(R.id.list_near_by_tv_distance)
        TextView tvDistance;


        @BindView(R.id.list_near_by_rl_address)
        RelativeLayout rlAddress;
        @BindView(R.id.list_near_by_tv_address)
        TextView tvAddress;


        @BindView(R.id.list_near_by_rl_mobile)
        RelativeLayout rlMobile;
        @BindView(R.id.list_near_by_tv_mobile)
        TextView tvMobile;


        @BindView(R.id.list_near_by_rl_phone)
        RelativeLayout rlPhone;
        @BindView(R.id.list_near_by_tv_phone)
        TextView tvPhone;

        @BindView(R.id.list_near_by_rl_email)
        RelativeLayout rlEmail;
        @BindView(R.id.list_near_by_tv_email)
        TextView tvEmail;


        @BindView(R.id.list_near_by_rl_time)
        RelativeLayout rlTime;
        @BindView(R.id.list_near_by_tv_time)
        TextView tvTime;

        @BindView(R.id.list_near_by_ll_for_call)
        LinearLayout llCall;

        @BindView(R.id.list_near_by_tv_offer)
        TextView tvOffer;

        @BindView(R.id.list_near_by_rl_check_in)
        RelativeLayout rlCheckIn;

        VerticalStoreViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


}
