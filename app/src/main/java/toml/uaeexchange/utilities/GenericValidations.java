package toml.uaeexchange.utilities;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomEditTextLayout;
import toml.uaeexchange.dialogs.OkDialog;

/**
 * Created by kunalk on 7/6/2017.
 */

public class GenericValidations {

    private String errorMessage = null;
    Context context;

    public GenericValidations(Context context) {
        this.context = context;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isValidMobile(String mobileNumber) {

        if (TextUtils.isEmpty(mobileNumber)) {
            errorMessage = context.getString(R.string.errorEmptyMobile);
            return false;
        } else if (mobileNumber.length() != context.getResources().getInteger(R.integer.mobileNoLength)) {
            errorMessage = context.getString(R.string.errorInvalidMobile);
            return false;
        }

        return true;
    }

    public boolean isValidMPIN(String mpin) {
        if (TextUtils.isEmpty(mpin)) {
            errorMessage = context.getString(R.string.errorEmptyMPIN);
            return false;

        } else if (mpin.length() != context.getResources().getInteger(R.integer.mpinLength)) {
            errorMessage = context.getString(R.string.errorInvalidMPIN);
            return false;
        }

        return true;
    }


    public boolean isValidEmail(String emailStr) {

        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);

        if (TextUtils.isEmpty(emailStr)) {
            errorMessage = context.getString(R.string.errorEmptyEmail);
            return false;
        } else if (!matcher.find()) {
            errorMessage = context.getString(R.string.errorInvalidEmail);
            return false;
        }

        return true;

    }

    public boolean isValidMerchantID(String merchantID) {
        if (TextUtils.isEmpty(merchantID)) {
            errorMessage = context.getString(R.string.errorEmptyMerchantID);
            return false;
        }
        return true;
    }

    public boolean isValidAmount(String amount) {
        if (TextUtils.isEmpty(amount)) {
            errorMessage = context.getString(R.string.errorEmptyAmount);
            return false;
        } else if (Float.parseFloat(amount) <= 0) {
            errorMessage = context.getString(R.string.errorValidAmount);
            return false;
        }
        return true;
    }

    public boolean isValidAmount(String strAmount, CustomEditTextLayout etlAmount) {
//        if (TextUtils.isEmpty(amount)) {
//            errorMessage = context.getString(R.string.errorEmptyAmount);
//            return false;
//        } else if (Float.parseFloat(amount) <= 0) {
//            errorMessage = context.getString(R.string.errorValidAmount);
//            return false;
//        }
        try {
            if (TextUtils.isEmpty(strAmount)) {
                errorMessage = context.getString(R.string.errorEmptyAmount);
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            } else if ((Double.valueOf(strAmount) > Double.valueOf(MovitConsumerApp.getInstance().getMaxAmount()))) {
                errorMessage = context.getString(R.string.please_enter_amount_less_than) + " " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount());
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            } else if ((Double.valueOf(strAmount) < Double.valueOf(MovitConsumerApp.getInstance().getMinAmount()))) {
                errorMessage = context.getString(R.string.please_enter_amount_greater_than) + " " + Constants.formatAmount(MovitConsumerApp.getInstance().getMinAmount());
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            }
        } catch (NumberFormatException ex) {
            errorMessage = context.getString(R.string.please_enter_valid_amount);
            etlAmount.getErrorLabelLayout().setError(errorMessage);
            etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
            return false;
        }


        return true;
    }

    public boolean isValidTPIN(String tpin) {


        if (TextUtils.isEmpty(tpin)) {
            errorMessage = context.getString(R.string.errorEmptyTPIN);
            return false;

        } else if (tpin.length() != context.getResources().getInteger(R.integer.tpinLength)) {
            errorMessage = context.getString(R.string.errorInvalidTPIN);
            return false;
        }

        return true;
    }


    public boolean validateLogin(String mobile, String mpin) {

        if (!isValidMobile(mobile))
            return false;
        else if (!isValidMPIN(mpin))
            return false;

        return true;

    }


    public boolean validateForgotMpin(String date, String mobile, String email) {

        if (TextUtils.isEmpty(date)) {
            errorMessage = context.getString(R.string.errorEmptyDate);
            return false;
        } else if (!isValidMobile(mobile))
            return false;
        else if (!isValidEmail(email))
            return false;


        return true;

    }


    public boolean validateForgotTpin(String mobile, String email, String mpin) {

        if (!isValidMobile(mobile))
            return false;
        else if (!isValidEmail(email))
            return false;
        else if (!isValidMPIN(mpin))
            return false;

        return true;

    }

    public boolean validateSelfRegister(String firstName, String lastName, String email, String mobileNumber) {
        if (TextUtils.isEmpty(firstName)) {
            errorMessage = "Please enter your First Name.";
            return false;
        }

        if (TextUtils.isEmpty(lastName)) {
            errorMessage = "Please enter your Last Name.";
            return false;
        }
        if (!isValidEmail(email))
            return false;

        if (!isValidMobile(mobileNumber))
            return false;

        return true;
    }


    public boolean validateSelfRegister(String firstName, CustomEditTextLayout edtxtLayoutFirstName, String lastName, CustomEditTextLayout edtxtLayoutLastName, String email, CustomEditTextLayout edtxtLayoutEmail, String mobileNumber, CustomEditTextLayout edtxtLayoutMobile) {
        if (TextUtils.isEmpty(firstName)) {
            errorMessage = "Please enter your First Name.";
            edtxtLayoutFirstName.getErrorLabelLayout().setError(errorMessage);
            edtxtLayoutFirstName.setBackgroundResource(R.drawable.custom_edittext_border_red);
            edtxtLayoutFirstName.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else if (firstName.trim().length() < 2) {
            errorMessage = "You must provide at least 2 characters for First Name.";
            edtxtLayoutFirstName.getErrorLabelLayout().setError(errorMessage);
            edtxtLayoutFirstName.setBackgroundResource(R.drawable.custom_edittext_border_red);
            edtxtLayoutFirstName.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else {
            edtxtLayoutFirstName.getErrorLabelLayout().clearError();
        }


        if (TextUtils.isEmpty(lastName)) {
            errorMessage = "Please enter your Last Name.";
            edtxtLayoutLastName.getErrorLabelLayout().setError(errorMessage);
            edtxtLayoutLastName.setBackgroundResource(R.drawable.custom_edittext_border_red);
            edtxtLayoutLastName.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else if (lastName.trim().length() < 2) {
            errorMessage = "You must provide at least 2 characters for Last Name.";
            edtxtLayoutLastName.getErrorLabelLayout().setError(errorMessage);
            edtxtLayoutLastName.setBackgroundResource(R.drawable.custom_edittext_border_red);
            edtxtLayoutLastName.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else {
            edtxtLayoutLastName.getErrorLabelLayout().clearError();
        }

        if (!isValidEmail(email, edtxtLayoutEmail))

            return false;

        if (!isValidMobile(mobileNumber, edtxtLayoutMobile))
            return false;

        return true;
    }

    public boolean valdiatePayPerson(String strName, CustomEditTextLayout etlNickName, String strMobile, CustomEditTextLayout etlMobile, String strAmount, CustomEditTextLayout etlAmount, String strTpin) {

//        if (TextUtils.isEmpty(strName)) {
//            errorMessage = context.getString(R.string.errorEmptyName);
//            return false;
//        } else

        LogUtils.Verbose("MovitConsumerApp.getInstance().getMobileNumber()", MovitConsumerApp.getInstance().getMobileNumber() + "," + strMobile);
        try {
            if (!isValidMobile(strMobile, etlMobile)) {

                return false;
            } else if (MovitConsumerApp.getInstance().getMobileNumber().equalsIgnoreCase(strMobile)) {
                errorMessage = context.getString(R.string.errorSameMobileNumber);
                etlMobile.getErrorLabelLayout().setError(errorMessage);
                etlMobile.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            } else if (TextUtils.isEmpty(strAmount)) {
                errorMessage = context.getString(R.string.errorEmptyAmount);
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            } else if ((Double.valueOf(strAmount).equals(Double.valueOf("0")))) {
                errorMessage = context.getString(R.string.please_enter_valid_amount);
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            }else  if (Double.valueOf(strAmount) > Double.valueOf(MovitConsumerApp.getInstance().getWalletBalance())) {

                new OkDialog(context, "", context.getString(R.string.amount_should_be_less_than_balance), null);
                return false;

            } else if ((Double.valueOf(strAmount) > Double.valueOf(MovitConsumerApp.getInstance().getMaxAmount()))) {
                errorMessage = context.getString(R.string.please_enter_amount_less_than) + " " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount());
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            } else if ((Double.valueOf(strAmount) < Double.valueOf(MovitConsumerApp.getInstance().getMinAmount()))) {
                errorMessage = context.getString(R.string.please_enter_amount_greater_than) + " " + Constants.formatAmount(MovitConsumerApp.getInstance().getMinAmount());
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            } else if (TextUtils.isEmpty(strName)) {
                errorMessage = context.getString(R.string.errorEmptyNickName);
                etlNickName.getErrorLabelLayout().setError(errorMessage);
                etlNickName.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            }
        } catch (NumberFormatException ex) {
            errorMessage = context.getString(R.string.please_enter_valid_amount);
            etlAmount.getErrorLabelLayout().setError(errorMessage);
            etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
            return false;
        }


//        else if (!isValidTPIN(strTpin)) {
//            return false;
//        }


        return true;
    }


    public boolean valdiatePayPerson(String name, String strMobile, CustomEditTextLayout etlMobile, String strAmount, CustomEditTextLayout etlAmount, String strTpin) {

//        if (TextUtils.isEmpty(strName)) {
//            errorMessage = context.getString(R.string.errorEmptyName);
//            return false;
//        } else

        try {
            if (!isValidMobile(strMobile, etlMobile)) {

                return false;
            } else if (MovitConsumerApp.getInstance().getMobileNumber().equalsIgnoreCase(strMobile)) {
                errorMessage = context.getString(R.string.errorSameMobileNumber);
                etlMobile.getErrorLabelLayout().setError(errorMessage);
                etlMobile.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            } else if (TextUtils.isEmpty(strAmount)) {
                errorMessage = context.getString(R.string.errorEmptyAmount);
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            }else if ((Double.valueOf(strAmount).equals(Double.valueOf("0")))) {
                errorMessage = context.getString(R.string.please_enter_valid_amount);
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            }else  if (Double.valueOf(strAmount) > Double.valueOf(MovitConsumerApp.getInstance().getWalletBalance())) {

                new OkDialog(context, "", context.getString(R.string.amount_should_be_less_than_balance), null);
                return false;

            } else if ((Double.valueOf(strAmount) > Double.valueOf(MovitConsumerApp.getInstance().getMaxAmount()))) {
                errorMessage = "Please enter amount less than " + Constants.formatAmount(MovitConsumerApp.getInstance().getMaxAmount());
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            } else if ((Double.valueOf(strAmount) < Double.valueOf(MovitConsumerApp.getInstance().getMinAmount()))) {
                errorMessage = "Please enter amount greater than " + MovitConsumerApp.getInstance().getMinAmount();
                etlAmount.getErrorLabelLayout().setError(errorMessage);
                etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
                return false;
            }
        } catch (NumberFormatException ex) {
            errorMessage = context.getString(R.string.please_enter_valid_amount);
            etlAmount.getErrorLabelLayout().setError(errorMessage);
            etlAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
            return false;
        }


//        else if (!isValidTPIN(strTpin)) {
//            return false;
//        }


        return true;
    }


    public boolean validateChangeMpin(String strCurrentMpin, String strNewMpin, String strConfirmMpin) {
        if (TextUtils.isEmpty(strCurrentMpin)) {
            errorMessage = context.getString(R.string.errorEmptyCurrentMPIN);
            return false;

        } else if (strCurrentMpin.length() != context.getResources().getInteger(R.integer.mpinLength)) {
            errorMessage = context.getString(R.string.errorInvalidCurrentMPIN);
            return false;
        } else if (TextUtils.isEmpty(strNewMpin)) {
            errorMessage = context.getString(R.string.errorEmptyNewMPIN);
            return false;

        } else if (strNewMpin.length() != context.getResources().getInteger(R.integer.mpinLength)) {
            errorMessage = context.getString(R.string.errorInvalidNewMPIN);
            return false;
        } else if (TextUtils.isEmpty(strConfirmMpin)) {
            errorMessage = context.getString(R.string.errorEmptyConfirmMPIN);
            return false;

        } else if (strConfirmMpin.length() != context.getResources().getInteger(R.integer.mpinLength)) {
            errorMessage = context.getString(R.string.errorInvalidConfirmMPIN);
            return false;
        } else if (!strConfirmMpin.equals(strNewMpin)) {
            errorMessage = context.getString(R.string.errorMPINNotMatching);
            return false;
        }

        return true;


    }


    public boolean validateChangeMpin(String strCurrentMpin, CustomEditTextLayout etCurrentmpin, String strNewMpin, CustomEditTextLayout etNewmpin, String strConfirmMpin, CustomEditTextLayout etConfirmmpin) {
        if (TextUtils.isEmpty(strCurrentMpin)) {
            errorMessage = context.getString(R.string.errorEmptyCurrentMPIN);
            etCurrentmpin.getErrorLabelLayout().setError(context.getString(R.string.errorEmptyCurrentMPIN));
            etCurrentmpin.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etCurrentmpin.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;

        } else if (strCurrentMpin.length() != context.getResources().getInteger(R.integer.mpinLength)) {
            errorMessage = context.getString(R.string.errorInvalidCurrentMPIN);
            etCurrentmpin.getErrorLabelLayout().setError(context.getString(R.string.errorInvalidCurrentMPIN));

            etCurrentmpin.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etCurrentmpin.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else if (TextUtils.isEmpty(strNewMpin)) {
            errorMessage = context.getString(R.string.errorEmptyNewMPIN);
            etNewmpin.getErrorLabelLayout().setError(context.getString(R.string.errorEmptyNewMPIN));

            etNewmpin.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etNewmpin.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;

        } else if (strNewMpin.length() != context.getResources().getInteger(R.integer.mpinLength)) {
            errorMessage = context.getString(R.string.errorInvalidNewMPIN);
            etNewmpin.getErrorLabelLayout().setError(context.getString(R.string.errorInvalidNewMPIN));

            etNewmpin.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etNewmpin.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else if (TextUtils.isEmpty(strConfirmMpin)) {
            errorMessage = context.getString(R.string.errorEmptyConfirmMPIN);
            etConfirmmpin.getErrorLabelLayout().setError(context.getString(R.string.errorEmptyConfirmMPIN));

            etConfirmmpin.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etConfirmmpin.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;

        } else if (strConfirmMpin.length() != context.getResources().getInteger(R.integer.mpinLength)) {
            errorMessage = context.getString(R.string.errorInvalidConfirmMPIN);
            etConfirmmpin.getErrorLabelLayout().setError(context.getString(R.string.errorInvalidConfirmMPIN));

            etConfirmmpin.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etConfirmmpin.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));

            return false;
        } else if (!strConfirmMpin.equals(strNewMpin)) {
            errorMessage = context.getString(R.string.errorMPINNotMatching);
            etConfirmmpin.getErrorLabelLayout().setError(context.getString(R.string.errorMPINNotMatching));

            etConfirmmpin.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etConfirmmpin.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else if (strNewMpin.equals(strCurrentMpin)) {
            errorMessage = context.getString(R.string.errorNewmPinPreviousmPinSame);
            etConfirmmpin.getErrorLabelLayout().setError(context.getString(R.string.errorNewmPinPreviousmPinSame));

            etConfirmmpin.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etConfirmmpin.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        }

        return true;


    }

    // ***************** Validations with Error Label view **************** //

    public boolean validateLogin(String mobileNumber, CustomEditTextLayout errorLabelMobileNumber, String mpin, CustomEditTextLayout errorLabelMpin) {

        if (!isValidMobile(mobileNumber, errorLabelMobileNumber))

            return false;
        else if (!isValidMPIN(mpin, errorLabelMpin))
            return false;

        return true;

    }

    public boolean isValidEmail(String emailStr, CustomEditTextLayout customEditTextLayout) {

        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);

        if (TextUtils.isEmpty(emailStr)) {
            errorMessage = context.getString(R.string.errorEmptyEmail);
            customEditTextLayout.getErrorLabelLayout().setError(errorMessage);
            customEditTextLayout.setBackgroundResource(R.drawable.custom_edittext_border_red);
            customEditTextLayout.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else if (!matcher.find()) {
            errorMessage = context.getString(R.string.errorInvalidEmail);
            customEditTextLayout.getErrorLabelLayout().setError(errorMessage);
            customEditTextLayout.setBackgroundResource(R.drawable.custom_edittext_border_red);
            customEditTextLayout.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else {
            customEditTextLayout.getErrorLabelLayout().clearError();
        }

        return true;

    }


    public boolean isValidMobile(String mobileNumber, CustomEditTextLayout customEditTextLayout) {
        if (TextUtils.isEmpty(mobileNumber)) {
            errorMessage = context.getString(R.string.errorEmptyMobile);
            customEditTextLayout.getErrorLabelLayout().setError(errorMessage);

            customEditTextLayout.setBackgroundResource(R.drawable.custom_edittext_border_red);
            customEditTextLayout.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));

            return false;
        } else if (mobileNumber.length() != context.getResources().getInteger(R.integer.mobileNoLength)) {
            errorMessage = context.getString(R.string.errorInvalidMobile);
            customEditTextLayout.getErrorLabelLayout().setError(errorMessage);

            customEditTextLayout.setBackgroundResource(R.drawable.custom_edittext_border_red);
            customEditTextLayout.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));

            return false;
        } else {
            customEditTextLayout.getErrorLabelLayout().clearError();
        }


        return true;
    }

    private boolean isValidMPIN(String mpin, CustomEditTextLayout errorLabelMpin) {
        if (TextUtils.isEmpty(mpin)) {
            errorMessage = context.getString(R.string.errorEmptyMPIN);
            errorLabelMpin.getErrorLabelLayout().setError(errorMessage);

            errorLabelMpin.setBackgroundResource(R.drawable.custom_edittext_border_red);
            errorLabelMpin.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));

            return false;

        } else if (mpin.length() != context.getResources().getInteger(R.integer.mpinLength)) {
            errorMessage = context.getString(R.string.errorInvalidMPIN);
            errorLabelMpin.getErrorLabelLayout().setError(errorMessage);

            errorLabelMpin.setBackgroundResource(R.drawable.custom_edittext_border_red);
            errorLabelMpin.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));

            return false;
        }

        return true;
    }

    public boolean isValidProfileData(String firstName, CustomEditTextLayout etFirstName,
                                      String lastName, CustomEditTextLayout etLastName,
                                      String aliase, CustomEditTextLayout etAliase,
                                      String emailId, CustomEditTextLayout etEmailID) {
        if (TextUtils.isEmpty(firstName)) {
            errorMessage = context.getString(R.string.errorEmptyFirstName);
            etFirstName.getErrorLabelLayout().setError(errorMessage);

            etFirstName.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etFirstName.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else if (TextUtils.isEmpty(lastName)) {
            errorMessage = context.getString(R.string.errorEmptyLastName);
            etLastName.getErrorLabelLayout().setError(errorMessage);

            etLastName.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etLastName.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else if (TextUtils.isEmpty(aliase) && (etAliase != null)) {//<-And condition is added so as to skip alias validation
            errorMessage = context.getString(R.string.errorEmptyAliasName);
            etAliase.getErrorLabelLayout().setError(errorMessage);

            etAliase.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etAliase.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else if (!isValidEmail(emailId)) {

            etEmailID.getErrorLabelLayout().setError(errorMessage);

            etEmailID.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etEmailID.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        }
        return true;
    }

    public boolean isValidP2MData(String merchantName, CustomEditTextLayout etMerchantName, String amount, CustomEditTextLayout etAmount) {
        if (TextUtils.isEmpty(merchantName)) {
            errorMessage = context.getString(R.string.errorEmptyMerchantName);
            etMerchantName.getErrorLabelLayout().setError(errorMessage);

            etMerchantName.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etMerchantName.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
            return false;
        } else if (TextUtils.isEmpty(amount)) {
            errorMessage = context.getString(R.string.errorEmptyAmount);
            etAmount.getErrorLabelLayout().setError(errorMessage);

            etAmount.setBackgroundResource(R.drawable.custom_edittext_border_red);
            etAmount.getVerticalDivider().setBackgroundColor(ContextCompat.getColor(context, R.color.grapefruit));
        }
        return true;
    }


}
