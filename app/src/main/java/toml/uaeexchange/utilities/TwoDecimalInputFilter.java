package toml.uaeexchange.utilities;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;

/**
 * Created by pankajp on 9/27/2017.
 */

public class TwoDecimalInputFilter implements InputFilter {

    final int maxDigitsBeforeDecimalPoint = 12;
    final int maxDigitsAfterDecimalPoint = 2;
    int dynamicDigitLength;

    public TwoDecimalInputFilter() {
    }

    public TwoDecimalInputFilter(int length) {
        dynamicDigitLength = length;
    }

    @Override
    public CharSequence filter(CharSequence source, int sourceStart, int sourceEnd, Spanned dest, int destStart, int destEnd) {

        StringBuilder builder = new StringBuilder(dest);
        builder.replace(destStart, destEnd, source
                .subSequence(sourceStart, sourceEnd).toString());

        if(dynamicDigitLength!=0){
            if (!builder.toString().matches(
                    "(([1-9]{1})([0-9]{0," + (dynamicDigitLength-1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?"

            )) {
                if (source.length() == 0)
                    return dest.subSequence(destStart, destEnd);
                return "";
            }
        }else {
            if (!builder.toString().matches(
                    "(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?"

            )) {
                if (source.length() == 0)
                    return dest.subSequence(destStart, destEnd);
                return "";
            }
        }

        return null;

    }
}
