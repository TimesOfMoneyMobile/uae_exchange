package toml.uaeexchange.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import toml.movitwallet.utils.AppSettings;
import toml.movitwallet.utils.LogUtils;
import toml.movitwallet.utils.MovitConsumerApp;
import toml.uaeexchange.R;
import toml.uaeexchange.customviews.CustomEditTextLayout;

/**
 * Created by vishwanathp on 7/25/2017.
 */

public class Constants {

    public static final Float TRANSACTION_AMOUNT = 100.0f;
    public static final String DU = "DU";
    public static final String ETISALAT = "Etisalat";
    public static final String PG_URL = "https://test.timesofmoney.com/direcpay/secure/PaymentTxnServlet";
    public static String IS_LOGGED_IN = "IsLoggedIn";

    public static final int RESEND_OTP_TIME_IN_MILIS = 10000;

    public static final int AUTO_DETECTING_LAYOUT_DISPALY_TIME_IN_MILIS = 5000;

    public static final String SERVER_DATE_FROMAT = "dd-MM-yyyy HH:mm:ss";
    public static String RegistrationSuccessActivity = "RegistrationSuccessActivity";


    public static void hideKeyboard(Context context) {

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = ((Activity) context).getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static int getResourceId(Context mContext, String pVariableName, String pResourcename, String pPackageName) {
        try {
            return mContext.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }


    public static boolean isDeviceRooted() {
        return checkRootMethod1() || checkRootMethod2() || checkRootMethod3() || checkRootMethod4("su");
    }

    private static boolean checkRootMethod1() {
        String buildTags = android.os.Build.TAGS;
        return buildTags != null && buildTags.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        String[] paths = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    private static boolean checkRootMethod3() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            if (in.readLine() != null) return true;
            return false;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

    public static boolean checkRootMethod4(String binaryName) {
        boolean found = false;
        if (!found) {
            String[] places = {"/sbin/", "/system/bin/", "/system/xbin/",
                    "/data/local/xbin/", "/data/local/bin/",
                    "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
            for (String where : places) {
                if (new File(where + binaryName).exists()) {
                    found = true;

                    break;
                }
            }
        }
        return found;
    }


    public static String formatToYesterdayOrToday(String date) throws ParseException {
        Date dateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);

        DateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
        DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today " + timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday " + timeFormatter.format(dateTime);
        } else {
            return dateFormatter.format(dateTime) + " " + timeFormatter.format(dateTime);
        }
    }

    public static String removeLastFourChar(String str) {
        return str.substring(0, str.length() - 4);
    }

    public static Date stringToDate(String aDate, String aFormat) {

        if (aDate == null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        Date stringDate = simpledateformat.parse(aDate, pos);
        return stringDate;

    }

    public static boolean isSameYear(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(date1.getTime());

        Calendar cal2 = Calendar.getInstance();
        cal2.setTimeInMillis(date2.getTime());

        int year1 = cal1.get(Calendar.YEAR);
        int year2 = cal2.get(Calendar.YEAR);

        return year1 == year2;

    }


    public static String getFormattedDate(String currentFormat, String requiredFormat, String dateString) {
        String result = "";
        if (TextUtils.isEmpty(dateString)) {
            return result;
        }
        SimpleDateFormat formatterOld = new SimpleDateFormat(currentFormat, Locale.ENGLISH);
        SimpleDateFormat formatterNew = new SimpleDateFormat(requiredFormat, Locale.ENGLISH);
        Date date = null;
        try {
            date = formatterOld.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            result = formatterNew.format(date);
        }
        return result;
    }


    public static void putPref(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPref(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    public static boolean isUserLoggedIn(Context context) {
        /*SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (preferences.getString(IS_LOGGED_IN, "N").equalsIgnoreCase("Y")) {
            return true;
        } else {
            return false;
        }*/
        if (TextUtils.isEmpty(MovitConsumerApp.getInstance().getSessionID()))
            return false;
        else
            return true;

    }

    public static boolean checkForFocusActive(CustomEditTextLayout[] fields) {
        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i].getEditText();
            if (currentField.hasFocus()) {
                return true;
            }
        }
        return false;
    }


    public static void overrideFonts(final Context context, final View v, Typeface typeface) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child, typeface);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(typeface);
            } else if (v instanceof EditText) {
                ((EditText) v).setTypeface(typeface);
            } else if (v instanceof Button) {
                ((Button) v).setTypeface(typeface);
            }
        } catch (Exception e) {
        }
    }


    /**
     * This method returns true if any of the given edit texts has text. (length > 0)
     *
     * @param fields
     * @return
     */

    public static boolean checkForNonEmpty(CustomEditTextLayout[] fields) {
        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i].getEditText();
            if (currentField.getText().toString().length() > 0) {
                return true;
            }
        }
        return false;
    }


    public static String getUserNameForWelcome(Context context) {
        String userName = "";
        if (Constants.isUserLoggedIn(context)) {
            if (TextUtils.isEmpty(AppSettings.getData(context, AppSettings.ALIAS))) {
                return ", " + AppSettings.getData(context, AppSettings.FIRST_NAME) + "!";
            } else {
                return ", " + AppSettings.getData(context, AppSettings.ALIAS) + "!";
            }
        } else {
            return "";
        }
    }

    public static String formatAmount(String amount) {
        try {
//            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
//            nf.setMaximumFractionDigits(2);
//            nf.setMinimumFractionDigits(2);

            DecimalFormat formatter = new DecimalFormat("#,##,##0.00");

            Double amt = Double.parseDouble(amount);
            amount = formatter.format(amt);
        } catch (Exception e) {
            LogUtils.Exception(e);
        }
        return amount;
    }


    public static int getRandomImage() {
        int[] cards = {R.drawable.user_thumbnail_1_small, R.drawable.user_thumbnail_2_small, R.drawable.user_thumbnail_3_small, R.drawable.user_thumbnail_4_small, R.drawable.user_thumbnail_5_small, R.drawable.user_thumbnail_6_small};
        Random r = new Random();
        int n = r.nextInt(cards.length);

        return cards[n];
    }

    public static int getRandomBigImage() {
        int[] cards = {R.drawable.user_thumbnail_1, R.drawable.user_thumbnail_2, R.drawable.user_thumbnail_3, R.drawable.user_thumbnail_4, R.drawable.user_thumbnail_5, R.drawable.user_thumbnail_6};
        Random r = new Random();
        int n = r.nextInt(cards.length);

        return cards[n];
    }

    /*
    *   Used in Contacts Activity
    */
    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    /**
     * Uses static final constants to detect if the device's platform version is Honeycomb MR1 or
     * later.
     */
    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    /**
     * Uses static final constants to detect if the device's platform version is ICS or
     * later.
     */
    public static boolean hasICS() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    public static boolean checkToShowOtpDialog(Context context, String amount) {
        if (Float.valueOf(amount) > Float.valueOf(MovitConsumerApp.getInstance().getOtpAmount())) {
            return true;
        }
        return false;
    }

    public static boolean isEasyMPIN(String str) {


        ArrayList<String> SameStr = new ArrayList<>();
        SameStr.add("0000");
        SameStr.add("1111");
        SameStr.add("2222");
        SameStr.add("3333");
        SameStr.add("4444");
        SameStr.add("5555");
        SameStr.add("6666");
        SameStr.add("7777");
        SameStr.add("8888");
        SameStr.add("9999");

        String SequenceAsc = "0123456789";
        String SequenceDes = "9876543210";

        if (SameStr.contains(str) || SequenceAsc.contains(str) || SequenceDes.contains(str))
            return false;

        return true;


    }


    public static String parseSMSForOTP(String message) {

        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }


    public static void setEditTextMaxLength(final EditText editText, int length, boolean isAlphabetsOnly, boolean isAlphaNumeric) {

        if (isAlphabetsOnly) {
            InputFilter[] FilterArray = new InputFilter[2];
            FilterArray[0] = new InputFilter.LengthFilter(length);
            FilterArray[1] = getAlphabetsOnlyTextFilter();
            editText.setFilters(FilterArray);
        } else if (isAlphaNumeric) {

            InputFilter[] FilterArray = new InputFilter[2];
            FilterArray[0] = new InputFilter.LengthFilter(length);
            FilterArray[1] = getAlphaNumericTextFilter();
            editText.setFilters(FilterArray);
        } else {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(length);
            editText.setFilters(FilterArray);
        }
    }


    public static InputFilter getAlphabetsOnlyTextFilter() {
        return new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                Pattern ps = Pattern.compile("^[a-zA-Z ]+$");
                Matcher ms = ps.matcher(String.valueOf(c));
                return ms.matches();
            }
        };
    }


    public static InputFilter getAlphaNumericTextFilter() {
        return new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                Pattern ps = Pattern.compile("^[a-zA-Z0-9 ]+$");
                Matcher ms = ps.matcher(String.valueOf(c));
                return ms.matches();
            }
        };
    }


}
